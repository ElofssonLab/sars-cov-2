TITLE     SWISS-MODEL SERVER (https://swissmodel.expasy.org)
TITLE    2 Untitled Project
EXPDTA    THEORETICAL MODEL (SWISS-MODEL SERVER)
AUTHOR    SWISS-MODEL SERVER (SEE REFERENCE IN JRNL Records)
REVDAT   1   29-APR-20 1MOD    1       13:50
JRNL        AUTH   A.WATERHOUSE,M.BERTONI,S.BIENERT,G.STUDER,G.TAURIELLO,
JRNL        AUTH 2 R.GUMIENNY,F.T.HEER,T.A.P.DE BEER,C.REMPFER,L.BORDOLI,
JRNL        AUTH 3 R.LEPORE,T.SCHWEDE
JRNL        TITL   SWISS-MODEL: HOMOLOGY MODELLING OF PROTEIN STRUCTURES AND
JRNL        TITL 2 COMPLEXES
JRNL        REF    NUCLEIC.ACIDS.RES..           V.  46 W296  2018
JRNL        PMID   29788355
JRNL        DOI    10.1093/nar/gky427
REMARK   1
REMARK   1 REFERENCE 1
REMARK   1  AUTH   S.BIENERT,A.WATERHOUSE,T.A.P.DE BEER,G.TAURIELLO,G.STUDER,
REMARK   1  AUTH 2 L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   THE SWISS-MODEL REPOSITORY - NEW FEATURES AND FUNCTIONALITY
REMARK   1  REF    NUCLEIC.ACIDS.RES..           V.  22       2017
REMARK   1  REFN                   ISSN 0305-1048
REMARK   1  PMID   27899672
REMARK   1  DOI    10.1093/nar/gkw1132
REMARK   1
REMARK   1 REFERENCE 2
REMARK   1  AUTH   N.GUEX,M.C.PEITSCH,T.SCHWEDE
REMARK   1  TITL   AUTOMATED COMPARATIVE PROTEIN STRUCTURE MODELING WITH
REMARK   1  TITL 2 SWISS-MODEL AND SWISS-PDBVIEWER: A HISTORICAL PERSPECTIVE
REMARK   1  REF    ELECTROPHORESIS               V.  30       2009
REMARK   1  REFN                   ISSN 0173-0835
REMARK   1  PMID   19517507
REMARK   1  DOI    10.1002/elps.200900140
REMARK   1
REMARK   1 REFERENCE 3
REMARK   1  AUTH   P.BENKERT,M.BIASINI,T.SCHWEDE
REMARK   1  TITL   TOWARD THE ESTIMATION OF THE ABSOLUTE QUALITY OF INDIVIDUAL
REMARK   1  TITL 2 PROTEIN STRUCTURE MODELS
REMARK   1  REF    BIOINFORMATICS                V.  27       2011
REMARK   1  REFN                   ISSN 1367-4803
REMARK   1  PMID   21134891
REMARK   1  DOI    10.1093/bioinformatics/btq662
REMARK   1
REMARK   1 REFERENCE 4
REMARK   1  AUTH   M.BERTONI,F.KIEFER,M.BIASINI,L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   MODELING PROTEIN QUATERNARY STRUCTURE OF HOMO- AND
REMARK   1  TITL 2 HETERO-OLIGOMERS BEYOND BINARY INTERACTIONS BY HOMOLOGY
REMARK   1  REF    SCI.REP.                      V.   7       2017
REMARK   1  REFN                   ISSN
REMARK   1  PMID   28874689
REMARK   1  DOI    10.1038/s41598-017-09654-8
REMARK   1
REMARK   1 DISCLAIMER
REMARK   1 The SWISS-MODEL SERVER produces theoretical models for proteins.
REMARK   1 The results of any theoretical modelling procedure is
REMARK   1 NON-EXPERIMENTAL and MUST be considered with care. These models may
REMARK   1 contain significant errors. This is especially true for automated
REMARK   1 modeling since there is no human intervention during model
REMARK   1 building. Please read the header section and the logfile carefully
REMARK   1 to know what templates and alignments were used during the model
REMARK   1 building process. All information by the SWISS-MODEL SERVER is
REMARK   1 provided "AS-IS", without any warranty, expressed or implied.
REMARK   2
REMARK   2 COPYRIGHT NOTICE
REMARK   2 This SWISS-MODEL protein model is copyright. It is produced by the
REMARK   2 SWISS-MODEL server, developed by the Computational Structural
REMARK   2 Biology Group at the SIB Swiss Institute of Bioinformatics at the
REMARK   2 Biozentrum, University of Basel (https://swissmodel.expasy.org). This
REMARK   2 model is licensed under the CC BY-SA 4.0 Creative Commons
REMARK   2 Attribution-ShareAlike 4.0 International License
REMARK   2 (https://creativecommons.org/licenses/by-sa/4.0/legalcode), i.e. you
REMARK   2 can copy and redistribute the model in any medium or format,
REMARK   2 transform and build upon the model for any purpose, even
REMARK   2 commercially, under the following terms:
REMARK   2 Attribution - You must give appropriate credit, provide a link to
REMARK   2 the license, and indicate if changes were made. You may do so in any
REMARK   2 reasonable manner, but not in any way that suggests the licensor
REMARK   2 endorses you or your use. When you publish, patent or distribute
REMARK   2 results that were fully or partially based on the model, please cite
REMARK   2 the corresponding papers mentioned under JRNL.
REMARK   2 ShareAlike - If you remix, transform, or build upon the material,
REMARK   2 you must distribute your contributions under the same license as the
REMARK   2 original.
REMARK   2 No additional restrictions - you may not apply legal terms or
REMARK   2 technological measures that legally restrict others from doing
REMARK   2 anything the license permits.
REMARK   2 Find a human-readable summary of (and not a substitute for) the
REMARK   2 CC BY-SA 4.0 license at this link:
REMARK   2 https://creativecommons.org/licenses/by-sa/4.0/
REMARK   3 
REMARK   3 MODEL INFORMATION
REMARK   3  ENGIN   PROMOD3
REMARK   3  VERSN   3.0.0
REMARK   3  OSTAT   monomer
REMARK   3  OSRSN   PREDICTION
REMARK   3  QSPRD   0.000
REMARK   3  QMN4    -2.13
REMARK   3  MODT    FALSE
REMARK   3 
REMARK   3 TEMPLATE 1
REMARK   3  PDBID   6nu2
REMARK   3  CHAIN   AO
REMARK   3  MMCIF   RB
REMARK   3  PDBV    2020-04-17
REMARK   3  SMTLE   6nu2.66.A
REMARK   3  SMTLV   2020-04-22
REMARK   3  MTHD    ELECTRON MICROSCOPY 0.00 A
REMARK   3  FOUND   HHblits
REMARK   3  SIM     0.39
REMARK   3  SID     40.74
REMARK   3  OSTAT   monomer
REMARK   3  ALN A TRG MDTEFGASSFFHSPASCHESHSSLSPESSAPQHSSPSRSALKLLTSVEVDNIEPSAFH
REMARK   3  ALN A TRG RQGLPKAPGWTEKNSHHSWEPLDAPEGKLQGSRCDNSSCSKLPPQEGRGIAQEQLFQE
REMARK   3  ALN A TRG KKDPANPSPVMPGIATSERGDEHSLGCSPSNSSAQPSLPLYRTCHPIMPVASSFVLHC
REMARK   3  ALN A TRG PDPVQKTNQCLQGQSLKTSLTLKVDRGSEETYRPEFPSTKGLVRSLAEQFQRMQGVSM
REMARK   3  ALN A TRG RDSTGFKDRSLSGSLRKNSSPSDSKPPFSQGQEKGHWPWAKQQSSLEGGDRPLSWEES
REMARK   3  ALN A TRG TEHSSLALNSGLPNGETSSGGQPRLAEPDIYQEKLSQVRDVRSKDLGSSTDLGTSLPL
REMARK   3  ALN A TRG DSWVNITRFCDSQLKHGAPRPGMKSSPHDSHTCVTYPERNHILLHPHWNQDTEQETSE
REMARK   3  ALN A TRG LESLYQASLQASQAGCSGWGQQDTAWHPLSQTGSADGMGRRLHSAHDPGLSKTSTAEM
REMARK   3  ALN A TRG EHGLHEARTVRTSQATPCRGLSRECGEDEQYSAENLRRISRSLSGTVVSEREEAPVSS
REMARK   3  ALN A TRG HSFDSSNVRKPLETGHRCSSSSSLPVIHDPSVFLLGPQLYLPQPQFLSPDVLMPTMAG
REMARK   3  ALN A TRG EPNRLPGTSRSVQQFLAMCDRGETSQGAKYTGRTLNYQSLPHRSRTDNSWAPWSETNQ
REMARK   3  ALN A TRG HIGTRFLTTPGCNPQLTYTATLPERSKGLQVPHTQSWSDLFHSPSHPPIVHPVYPPSS
REMARK   3  ALN A TRG SLHVPLRSAWNSDPVPGSRTPGPRRVDMPPDDDWRQSSYASHSGHRRTVGEGFLFVLS
REMARK   3  ALN A TRG DAPRREQIRARVLQHSQW
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ---------------------------------------------YNWKQPPERELSR
REMARK   3  ALN A TPL LRRLYQGHLQEESG--------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ------------------
REMARK   3  ALN A OFF 151
ATOM      1  N   PRO A 394     213.126 168.734  83.408  1.00  0.20           N  
ATOM      2  CA  PRO A 394     211.963 169.585  83.839  1.00  0.20           C  
ATOM      3  C   PRO A 394     210.611 169.282  83.232  1.00  0.20           C  
ATOM      4  O   PRO A 394     209.662 169.285  84.001  1.00  0.20           O  
ATOM      5  CB  PRO A 394     212.383 171.028  83.534  1.00  0.20           C  
ATOM      6  CG  PRO A 394     213.890 171.019  83.275  1.00  0.20           C  
ATOM      7  CD  PRO A 394     214.222 169.601  82.800  1.00  0.20           C  
ATOM      8  N   HIS A 395     210.455 169.074  81.906  1.00  0.19           N  
ATOM      9  CA  HIS A 395     209.166 168.766  81.291  1.00  0.19           C  
ATOM     10  C   HIS A 395     208.695 167.351  81.611  1.00  0.19           C  
ATOM     11  O   HIS A 395     209.335 166.625  82.373  1.00  0.19           O  
ATOM     12  CB  HIS A 395     209.196 169.045  79.765  1.00  0.19           C  
ATOM     13  CG  HIS A 395     210.334 168.388  79.056  1.00  0.19           C  
ATOM     14  ND1 HIS A 395     210.252 167.042  78.815  1.00  0.19           N  
ATOM     15  CD2 HIS A 395     211.500 168.880  78.567  1.00  0.19           C  
ATOM     16  CE1 HIS A 395     211.352 166.726  78.177  1.00  0.19           C  
ATOM     17  NE2 HIS A 395     212.154 167.806  77.999  1.00  0.19           N  
ATOM     18  N   TRP A 396     207.518 166.956  81.086  1.00  0.20           N  
ATOM     19  CA  TRP A 396     206.986 165.625  81.263  1.00  0.20           C  
ATOM     20  C   TRP A 396     207.862 164.542  80.643  1.00  0.20           C  
ATOM     21  O   TRP A 396     208.326 164.641  79.511  1.00  0.20           O  
ATOM     22  CB  TRP A 396     205.551 165.539  80.696  1.00  0.20           C  
ATOM     23  CG  TRP A 396     204.868 164.218  80.994  1.00  0.20           C  
ATOM     24  CD1 TRP A 396     204.659 163.165  80.151  1.00  0.20           C  
ATOM     25  CD2 TRP A 396     204.377 163.817  82.282  1.00  0.20           C  
ATOM     26  NE1 TRP A 396     204.065 162.123  80.827  1.00  0.20           N  
ATOM     27  CE2 TRP A 396     203.859 162.514  82.132  1.00  0.20           C  
ATOM     28  CE3 TRP A 396     204.328 164.474  83.510  1.00  0.20           C  
ATOM     29  CZ2 TRP A 396     203.248 161.869  83.197  1.00  0.20           C  
ATOM     30  CZ3 TRP A 396     203.729 163.809  84.589  1.00  0.20           C  
ATOM     31  CH2 TRP A 396     203.184 162.529  84.431  1.00  0.20           C  
ATOM     32  N   ASN A 397     208.082 163.450  81.382  1.00  0.29           N  
ATOM     33  CA  ASN A 397     208.886 162.362  80.909  1.00  0.29           C  
ATOM     34  C   ASN A 397     208.313 161.170  81.628  1.00  0.29           C  
ATOM     35  O   ASN A 397     208.633 160.928  82.789  1.00  0.29           O  
ATOM     36  CB  ASN A 397     210.385 162.607  81.243  1.00  0.29           C  
ATOM     37  CG  ASN A 397     211.285 161.542  80.634  1.00  0.29           C  
ATOM     38  OD1 ASN A 397     210.863 160.660  79.890  1.00  0.29           O  
ATOM     39  ND2 ASN A 397     212.600 161.635  80.943  1.00  0.29           N  
ATOM     40  N   GLN A 398     207.400 160.439  80.954  1.00  0.31           N  
ATOM     41  CA  GLN A 398     206.811 159.217  81.459  1.00  0.31           C  
ATOM     42  C   GLN A 398     207.876 158.175  81.714  1.00  0.31           C  
ATOM     43  O   GLN A 398     208.506 157.674  80.782  1.00  0.31           O  
ATOM     44  CB  GLN A 398     205.770 158.637  80.462  1.00  0.31           C  
ATOM     45  CG  GLN A 398     204.806 157.600  81.088  1.00  0.31           C  
ATOM     46  CD  GLN A 398     203.766 158.284  81.972  1.00  0.31           C  
ATOM     47  OE1 GLN A 398     203.807 158.269  83.197  1.00  0.31           O  
ATOM     48  NE2 GLN A 398     202.780 158.947  81.319  1.00  0.31           N  
ATOM     49  N   ASP A 399     208.124 157.881  83.000  1.00  0.36           N  
ATOM     50  CA  ASP A 399     209.055 156.878  83.434  1.00  0.36           C  
ATOM     51  C   ASP A 399     208.732 155.468  82.915  1.00  0.36           C  
ATOM     52  O   ASP A 399     207.728 155.183  82.260  1.00  0.36           O  
ATOM     53  CB  ASP A 399     209.354 157.023  84.961  1.00  0.36           C  
ATOM     54  CG  ASP A 399     208.161 156.784  85.875  1.00  0.36           C  
ATOM     55  OD1 ASP A 399     208.251 157.238  87.045  1.00  0.36           O  
ATOM     56  OD2 ASP A 399     207.164 156.188  85.412  1.00  0.36           O  
ATOM     57  N   THR A 400     209.661 154.528  83.119  1.00  0.52           N  
ATOM     58  CA  THR A 400     209.395 153.119  82.871  1.00  0.52           C  
ATOM     59  C   THR A 400     208.398 152.543  83.869  1.00  0.52           C  
ATOM     60  O   THR A 400     208.436 152.841  85.058  1.00  0.52           O  
ATOM     61  CB  THR A 400     210.653 152.256  82.891  1.00  0.52           C  
ATOM     62  OG1 THR A 400     211.325 152.343  84.141  1.00  0.52           O  
ATOM     63  CG2 THR A 400     211.623 152.771  81.821  1.00  0.52           C  
ATOM     64  N   GLU A 401     207.517 151.618  83.430  1.00  0.53           N  
ATOM     65  CA  GLU A 401     206.487 151.023  84.277  1.00  0.53           C  
ATOM     66  C   GLU A 401     207.011 150.242  85.481  1.00  0.53           C  
ATOM     67  O   GLU A 401     206.313 150.023  86.465  1.00  0.53           O  
ATOM     68  CB  GLU A 401     205.607 150.071  83.431  1.00  0.53           C  
ATOM     69  CG  GLU A 401     204.523 150.794  82.594  1.00  0.53           C  
ATOM     70  CD  GLU A 401     203.405 151.359  83.471  1.00  0.53           C  
ATOM     71  OE1 GLU A 401     203.094 150.722  84.506  1.00  0.53           O  
ATOM     72  OE2 GLU A 401     202.845 152.413  83.080  1.00  0.53           O  
ATOM     73  N   GLN A 402     208.290 149.810  85.440  1.00  0.51           N  
ATOM     74  CA  GLN A 402     208.947 149.179  86.570  1.00  0.51           C  
ATOM     75  C   GLN A 402     209.049 150.096  87.784  1.00  0.51           C  
ATOM     76  O   GLN A 402     208.674 149.715  88.881  1.00  0.51           O  
ATOM     77  CB  GLN A 402     210.361 148.685  86.161  1.00  0.51           C  
ATOM     78  CG  GLN A 402     211.215 148.107  87.319  1.00  0.51           C  
ATOM     79  CD  GLN A 402     210.588 146.854  87.933  1.00  0.51           C  
ATOM     80  OE1 GLN A 402     209.805 146.132  87.308  1.00  0.51           O  
ATOM     81  NE2 GLN A 402     210.954 146.566  89.198  1.00  0.51           N  
ATOM     82  N   GLU A 403     209.486 151.361  87.614  1.00  0.52           N  
ATOM     83  CA  GLU A 403     209.568 152.307  88.712  1.00  0.52           C  
ATOM     84  C   GLU A 403     208.188 152.655  89.256  1.00  0.52           C  
ATOM     85  O   GLU A 403     207.967 152.738  90.463  1.00  0.52           O  
ATOM     86  CB  GLU A 403     210.297 153.587  88.259  1.00  0.52           C  
ATOM     87  CG  GLU A 403     211.739 153.346  87.735  1.00  0.52           C  
ATOM     88  CD  GLU A 403     212.765 152.957  88.801  1.00  0.52           C  
ATOM     89  OE1 GLU A 403     212.737 153.557  89.904  1.00  0.52           O  
ATOM     90  OE2 GLU A 403     213.661 152.151  88.432  1.00  0.52           O  
ATOM     91  N   THR A 404     207.186 152.794  88.353  1.00  0.64           N  
ATOM     92  CA  THR A 404     205.777 152.982  88.714  1.00  0.64           C  
ATOM     93  C   THR A 404     205.259 151.861  89.581  1.00  0.64           C  
ATOM     94  O   THR A 404     204.740 152.105  90.666  1.00  0.64           O  
ATOM     95  CB  THR A 404     204.868 153.143  87.500  1.00  0.64           C  
ATOM     96  OG1 THR A 404     205.240 154.349  86.870  1.00  0.64           O  
ATOM     97  CG2 THR A 404     203.388 153.323  87.864  1.00  0.64           C  
ATOM     98  N   SER A 405     205.498 150.590  89.183  1.00  0.49           N  
ATOM     99  CA  SER A 405     205.120 149.411  89.957  1.00  0.49           C  
ATOM    100  C   SER A 405     205.805 149.344  91.319  1.00  0.49           C  
ATOM    101  O   SER A 405     205.182 149.042  92.334  1.00  0.49           O  
ATOM    102  CB  SER A 405     205.310 148.059  89.189  1.00  0.49           C  
ATOM    103  OG  SER A 405     206.668 147.616  89.119  1.00  0.49           O  
ATOM    104  N   GLU A 406     207.113 149.678  91.391  1.00  0.49           N  
ATOM    105  CA  GLU A 406     207.868 149.781  92.631  1.00  0.49           C  
ATOM    106  C   GLU A 406     207.329 150.834  93.583  1.00  0.49           C  
ATOM    107  O   GLU A 406     207.165 150.600  94.781  1.00  0.49           O  
ATOM    108  CB  GLU A 406     209.349 150.095  92.346  1.00  0.49           C  
ATOM    109  CG  GLU A 406     210.082 148.900  91.698  1.00  0.49           C  
ATOM    110  CD  GLU A 406     211.551 149.192  91.409  1.00  0.49           C  
ATOM    111  OE1 GLU A 406     212.056 150.239  91.869  1.00  0.49           O  
ATOM    112  OE2 GLU A 406     212.185 148.300  90.782  1.00  0.49           O  
ATOM    113  N   LEU A 407     206.987 152.025  93.057  1.00  0.53           N  
ATOM    114  CA  LEU A 407     206.300 153.058  93.807  1.00  0.53           C  
ATOM    115  C   LEU A 407     204.899 152.683  94.257  1.00  0.53           C  
ATOM    116  O   LEU A 407     204.523 152.975  95.384  1.00  0.53           O  
ATOM    117  CB  LEU A 407     206.245 154.401  93.056  1.00  0.53           C  
ATOM    118  CG  LEU A 407     207.619 155.065  92.856  1.00  0.53           C  
ATOM    119  CD1 LEU A 407     207.447 156.301  91.964  1.00  0.53           C  
ATOM    120  CD2 LEU A 407     208.307 155.436  94.184  1.00  0.53           C  
ATOM    121  N   GLU A 408     204.091 152.001  93.429  1.00  0.48           N  
ATOM    122  CA  GLU A 408     202.814 151.441  93.846  1.00  0.48           C  
ATOM    123  C   GLU A 408     202.941 150.393  94.943  1.00  0.48           C  
ATOM    124  O   GLU A 408     202.150 150.338  95.875  1.00  0.48           O  
ATOM    125  CB  GLU A 408     202.087 150.813  92.653  1.00  0.48           C  
ATOM    126  CG  GLU A 408     201.604 151.852  91.619  1.00  0.48           C  
ATOM    127  CD  GLU A 408     200.957 151.177  90.416  1.00  0.48           C  
ATOM    128  OE1 GLU A 408     201.045 149.927  90.306  1.00  0.48           O  
ATOM    129  OE2 GLU A 408     200.353 151.924  89.605  1.00  0.48           O  
ATOM    130  N   SER A 409     203.982 149.541  94.894  1.00  0.45           N  
ATOM    131  CA  SER A 409     204.324 148.650  96.000  1.00  0.45           C  
ATOM    132  C   SER A 409     204.698 149.364  97.294  1.00  0.45           C  
ATOM    133  O   SER A 409     204.381 148.885  98.379  1.00  0.45           O  
ATOM    134  CB  SER A 409     205.477 147.673  95.669  1.00  0.45           C  
ATOM    135  OG  SER A 409     205.056 146.703  94.712  1.00  0.45           O  
ATOM    136  N   LEU A 410     205.403 150.514  97.212  1.00  0.45           N  
ATOM    137  CA  LEU A 410     205.639 151.424  98.328  1.00  0.45           C  
ATOM    138  C   LEU A 410     204.380 152.118  98.846  1.00  0.45           C  
ATOM    139  O   LEU A 410     204.104 152.139 100.043  1.00  0.45           O  
ATOM    140  CB  LEU A 410     206.678 152.498  97.900  1.00  0.45           C  
ATOM    141  CG  LEU A 410     208.131 152.103  98.215  1.00  0.45           C  
ATOM    142  CD1 LEU A 410     209.119 152.824  97.284  1.00  0.45           C  
ATOM    143  CD2 LEU A 410     208.449 152.400  99.690  1.00  0.45           C  
ATOM    144  N   TYR A 411     203.560 152.675  97.938  1.00  0.33           N  
ATOM    145  CA  TYR A 411     202.335 153.384  98.246  1.00  0.33           C  
ATOM    146  C   TYR A 411     201.150 152.520  97.862  1.00  0.33           C  
ATOM    147  O   TYR A 411     200.425 152.823  96.923  1.00  0.33           O  
ATOM    148  CB  TYR A 411     202.231 154.733  97.483  1.00  0.33           C  
ATOM    149  CG  TYR A 411     203.327 155.670  97.885  1.00  0.33           C  
ATOM    150  CD1 TYR A 411     203.216 156.430  99.058  1.00  0.33           C  
ATOM    151  CD2 TYR A 411     204.473 155.814  97.088  1.00  0.33           C  
ATOM    152  CE1 TYR A 411     204.240 157.311  99.433  1.00  0.33           C  
ATOM    153  CE2 TYR A 411     205.506 156.678  97.469  1.00  0.33           C  
ATOM    154  CZ  TYR A 411     205.385 157.431  98.641  1.00  0.33           C  
ATOM    155  OH  TYR A 411     206.405 158.326  99.015  1.00  0.33           O  
ATOM    156  N   GLN A 412     200.923 151.401  98.581  1.00  0.26           N  
ATOM    157  CA  GLN A 412     199.912 150.414  98.219  1.00  0.26           C  
ATOM    158  C   GLN A 412     198.474 150.923  98.162  1.00  0.26           C  
ATOM    159  O   GLN A 412     197.892 151.154  97.109  1.00  0.26           O  
ATOM    160  CB  GLN A 412     199.964 149.230  99.220  1.00  0.26           C  
ATOM    161  CG  GLN A 412     201.255 148.395  99.111  1.00  0.26           C  
ATOM    162  CD  GLN A 412     201.244 147.233 100.102  1.00  0.26           C  
ATOM    163  OE1 GLN A 412     200.335 147.065 100.914  1.00  0.26           O  
ATOM    164  NE2 GLN A 412     202.303 146.394 100.041  1.00  0.26           N  
ATOM    165  N   ALA A 413     197.857 151.137  99.336  1.00  0.23           N  
ATOM    166  CA  ALA A 413     196.501 151.622  99.445  1.00  0.23           C  
ATOM    167  C   ALA A 413     196.452 153.144  99.546  1.00  0.23           C  
ATOM    168  O   ALA A 413     195.673 153.707 100.310  1.00  0.23           O  
ATOM    169  CB  ALA A 413     195.826 150.951 100.660  1.00  0.23           C  
ATOM    170  N   SER A 414     197.294 153.861  98.775  1.00  0.23           N  
ATOM    171  CA  SER A 414     197.342 155.314  98.849  1.00  0.23           C  
ATOM    172  C   SER A 414     197.861 155.856  97.539  1.00  0.23           C  
ATOM    173  O   SER A 414     199.039 156.171  97.394  1.00  0.23           O  
ATOM    174  CB  SER A 414     198.209 155.854 100.020  1.00  0.23           C  
ATOM    175  OG  SER A 414     198.030 157.262 100.198  1.00  0.23           O  
ATOM    176  N   LEU A 415     196.977 155.924  96.526  1.00  0.22           N  
ATOM    177  CA  LEU A 415     197.328 156.341  95.187  1.00  0.22           C  
ATOM    178  C   LEU A 415     196.240 157.247  94.654  1.00  0.22           C  
ATOM    179  O   LEU A 415     195.135 157.323  95.192  1.00  0.22           O  
ATOM    180  CB  LEU A 415     197.475 155.141  94.209  1.00  0.22           C  
ATOM    181  CG  LEU A 415     198.614 154.163  94.555  1.00  0.22           C  
ATOM    182  CD1 LEU A 415     198.510 152.873  93.728  1.00  0.22           C  
ATOM    183  CD2 LEU A 415     199.999 154.807  94.377  1.00  0.22           C  
ATOM    184  N   GLN A 416     196.543 157.954  93.554  1.00  0.17           N  
ATOM    185  CA  GLN A 416     195.645 158.879  92.903  1.00  0.17           C  
ATOM    186  C   GLN A 416     195.633 158.598  91.419  1.00  0.17           C  
ATOM    187  O   GLN A 416     196.426 157.819  90.898  1.00  0.17           O  
ATOM    188  CB  GLN A 416     196.060 160.357  93.132  1.00  0.17           C  
ATOM    189  CG  GLN A 416     195.525 160.931  94.465  1.00  0.17           C  
ATOM    190  CD  GLN A 416     196.621 161.125  95.514  1.00  0.17           C  
ATOM    191  OE1 GLN A 416     197.611 160.402  95.587  1.00  0.17           O  
ATOM    192  NE2 GLN A 416     196.441 162.163  96.366  1.00  0.17           N  
ATOM    193  N   ALA A 417     194.691 159.228  90.692  1.00  0.18           N  
ATOM    194  CA  ALA A 417     194.570 159.062  89.265  1.00  0.18           C  
ATOM    195  C   ALA A 417     195.505 160.020  88.542  1.00  0.18           C  
ATOM    196  O   ALA A 417     195.334 161.238  88.581  1.00  0.18           O  
ATOM    197  CB  ALA A 417     193.108 159.314  88.838  1.00  0.18           C  
ATOM    198  N   SER A 418     196.536 159.484  87.868  1.00  0.21           N  
ATOM    199  CA  SER A 418     197.486 160.266  87.103  1.00  0.21           C  
ATOM    200  C   SER A 418     197.032 160.354  85.659  1.00  0.21           C  
ATOM    201  O   SER A 418     196.262 159.529  85.172  1.00  0.21           O  
ATOM    202  CB  SER A 418     198.933 159.701  87.215  1.00  0.21           C  
ATOM    203  OG  SER A 418     199.009 158.346  86.769  1.00  0.21           O  
ATOM    204  N   GLN A 419     197.466 161.409  84.951  1.00  0.26           N  
ATOM    205  CA  GLN A 419     197.133 161.640  83.564  1.00  0.26           C  
ATOM    206  C   GLN A 419     198.440 161.723  82.796  1.00  0.26           C  
ATOM    207  O   GLN A 419     199.512 161.482  83.353  1.00  0.26           O  
ATOM    208  CB  GLN A 419     196.280 162.930  83.388  1.00  0.26           C  
ATOM    209  CG  GLN A 419     194.928 162.901  84.146  1.00  0.26           C  
ATOM    210  CD  GLN A 419     193.986 161.836  83.581  1.00  0.26           C  
ATOM    211  OE1 GLN A 419     193.886 161.629  82.372  1.00  0.26           O  
ATOM    212  NE2 GLN A 419     193.237 161.151  84.477  1.00  0.26           N  
ATOM    213  N   ALA A 420     198.342 161.996  81.484  1.00  0.26           N  
ATOM    214  CA  ALA A 420     199.461 162.223  80.600  1.00  0.26           C  
ATOM    215  C   ALA A 420     200.179 163.586  80.805  1.00  0.26           C  
ATOM    216  O   ALA A 420     199.716 164.415  81.634  1.00  0.26           O  
ATOM    217  CB  ALA A 420     198.944 162.175  79.145  1.00  0.26           C  
ATOM    218  OXT ALA A 420     201.200 163.799  80.094  1.00  0.26           O  
TER     219      ALA A 420                                                      
END   
