TITLE     SWISS-MODEL SERVER (https://swissmodel.expasy.org)
TITLE    2 Untitled Project
EXPDTA    THEORETICAL MODEL (SWISS-MODEL SERVER)
AUTHOR    SWISS-MODEL SERVER (SEE REFERENCE IN JRNL Records)
REVDAT   1   28-APR-20 1MOD    1       15:06
JRNL        AUTH   A.WATERHOUSE,M.BERTONI,S.BIENERT,G.STUDER,G.TAURIELLO,
JRNL        AUTH 2 R.GUMIENNY,F.T.HEER,T.A.P.DE BEER,C.REMPFER,L.BORDOLI,
JRNL        AUTH 3 R.LEPORE,T.SCHWEDE
JRNL        TITL   SWISS-MODEL: HOMOLOGY MODELLING OF PROTEIN STRUCTURES AND
JRNL        TITL 2 COMPLEXES
JRNL        REF    NUCLEIC.ACIDS.RES..           V.  46 W296  2018
JRNL        PMID   29788355
JRNL        DOI    10.1093/nar/gky427
REMARK   1
REMARK   1 REFERENCE 1
REMARK   1  AUTH   S.BIENERT,A.WATERHOUSE,T.A.P.DE BEER,G.TAURIELLO,G.STUDER,
REMARK   1  AUTH 2 L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   THE SWISS-MODEL REPOSITORY - NEW FEATURES AND FUNCTIONALITY
REMARK   1  REF    NUCLEIC.ACIDS.RES..           V.  22       2017
REMARK   1  REFN                   ISSN 0305-1048
REMARK   1  PMID   27899672
REMARK   1  DOI    10.1093/nar/gkw1132
REMARK   1
REMARK   1 REFERENCE 2
REMARK   1  AUTH   N.GUEX,M.C.PEITSCH,T.SCHWEDE
REMARK   1  TITL   AUTOMATED COMPARATIVE PROTEIN STRUCTURE MODELING WITH
REMARK   1  TITL 2 SWISS-MODEL AND SWISS-PDBVIEWER: A HISTORICAL PERSPECTIVE
REMARK   1  REF    ELECTROPHORESIS               V.  30       2009
REMARK   1  REFN                   ISSN 0173-0835
REMARK   1  PMID   19517507
REMARK   1  DOI    10.1002/elps.200900140
REMARK   1
REMARK   1 REFERENCE 3
REMARK   1  AUTH   P.BENKERT,M.BIASINI,T.SCHWEDE
REMARK   1  TITL   TOWARD THE ESTIMATION OF THE ABSOLUTE QUALITY OF INDIVIDUAL
REMARK   1  TITL 2 PROTEIN STRUCTURE MODELS
REMARK   1  REF    BIOINFORMATICS                V.  27       2011
REMARK   1  REFN                   ISSN 1367-4803
REMARK   1  PMID   21134891
REMARK   1  DOI    10.1093/bioinformatics/btq662
REMARK   1
REMARK   1 REFERENCE 4
REMARK   1  AUTH   M.BERTONI,F.KIEFER,M.BIASINI,L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   MODELING PROTEIN QUATERNARY STRUCTURE OF HOMO- AND
REMARK   1  TITL 2 HETERO-OLIGOMERS BEYOND BINARY INTERACTIONS BY HOMOLOGY
REMARK   1  REF    SCI.REP.                      V.   7       2017
REMARK   1  REFN                   ISSN
REMARK   1  PMID   28874689
REMARK   1  DOI    10.1038/s41598-017-09654-8
REMARK   1
REMARK   1 DISCLAIMER
REMARK   1 The SWISS-MODEL SERVER produces theoretical models for proteins.
REMARK   1 The results of any theoretical modelling procedure is
REMARK   1 NON-EXPERIMENTAL and MUST be considered with care. These models may
REMARK   1 contain significant errors. This is especially true for automated
REMARK   1 modeling since there is no human intervention during model
REMARK   1 building. Please read the header section and the logfile carefully
REMARK   1 to know what templates and alignments were used during the model
REMARK   1 building process. All information by the SWISS-MODEL SERVER is
REMARK   1 provided "AS-IS", without any warranty, expressed or implied.
REMARK   2
REMARK   2 COPYRIGHT NOTICE
REMARK   2 This SWISS-MODEL protein model is copyright. It is produced by the
REMARK   2 SWISS-MODEL server, developed by the Computational Structural
REMARK   2 Biology Group at the SIB Swiss Institute of Bioinformatics at the
REMARK   2 Biozentrum, University of Basel (https://swissmodel.expasy.org). This
REMARK   2 model is licensed under the CC BY-SA 4.0 Creative Commons
REMARK   2 Attribution-ShareAlike 4.0 International License
REMARK   2 (https://creativecommons.org/licenses/by-sa/4.0/legalcode), i.e. you
REMARK   2 can copy and redistribute the model in any medium or format,
REMARK   2 transform and build upon the model for any purpose, even
REMARK   2 commercially, under the following terms:
REMARK   2 Attribution - You must give appropriate credit, provide a link to
REMARK   2 the license, and indicate if changes were made. You may do so in any
REMARK   2 reasonable manner, but not in any way that suggests the licensor
REMARK   2 endorses you or your use. When you publish, patent or distribute
REMARK   2 results that were fully or partially based on the model, please cite
REMARK   2 the corresponding papers mentioned under JRNL.
REMARK   2 ShareAlike - If you remix, transform, or build upon the material,
REMARK   2 you must distribute your contributions under the same license as the
REMARK   2 original.
REMARK   2 No additional restrictions - you may not apply legal terms or
REMARK   2 technological measures that legally restrict others from doing
REMARK   2 anything the license permits.
REMARK   2 Find a human-readable summary of (and not a substitute for) the
REMARK   2 CC BY-SA 4.0 license at this link:
REMARK   2 https://creativecommons.org/licenses/by-sa/4.0/
REMARK   3 
REMARK   3 MODEL INFORMATION
REMARK   3  ENGIN   PROMOD3
REMARK   3  VERSN   3.0.0
REMARK   3  OSTAT   monomer
REMARK   3  OSRSN   PREDICTION
REMARK   3  QSPRD   0.000
REMARK   3  QMN4    -0.95
REMARK   3  MODT    FALSE
REMARK   3 
REMARK   3 TEMPLATE 1
REMARK   3  PDBID   6rxu
REMARK   3  CHAIN   CZ
REMARK   3  MMCIF   LB
REMARK   3  PDBV    2020-04-17
REMARK   3  SMTLE   6rxu.62.A
REMARK   3  SMTLV   2020-04-22
REMARK   3  MTHD    ELECTRON MICROSCOPY 0.00 A
REMARK   3  FOUND   BLAST
REMARK   3  SIM     0.37
REMARK   3  SID     33.33
REMARK   3  OSTAT   monomer
REMARK   3  ALN A TRG MAGPQPLALQLEQLLNPRPSEADPEADPEEATAARVIDRFDEGEDGEGDFLVVGSIRK
REMARK   3  ALN A TRG LASASLLDTDKRYCGKTTSRKAWNEDHWEQTLPGSSDEEISDEEGSGDEDSEGLGLEE
REMARK   3  ALN A TRG YDEDDLGAAEEQECGDHRESKKSRSHSAKTPGFSVQSISDFEKFTKGMDDLGSSEEEE
REMARK   3  ALN A TRG DEESGMEEGDDAEDSQGESEEDRAGDRNSEDDGVVMTFSSVKVSEEVEKGRAVKNQIA
REMARK   3  ALN A TRG LWDQLLEGRIKLQKALLTTNQLPQPDVFPLFKDKGGPEFSSALKNSHKALKALLRSLV
REMARK   3  ALN A TRG GLQEELLFQYPDTRYLVDGTKPNAGSEEISSEDDELVEEKKQQRRRVPAKRKLEM---
REMARK   3  ALN A TRG -EDYPSFMAKRFADFTVYRNRTLQKWHDKTKLASGKLG--KGFGAFERSILTQIDHIL
REMARK   3  ALN A TRG MDKERLLRRTQTKRSVYRVLGKPEPAAQPVPESLPGEPEILPQAPANAHLKDLDEEIF
REMARK   3  ALN A TRG DDDDFYHQLLRELIERKTSSLDPNDQVAMGRQWLAIQKLRSKIHKKVDRKASKGRKLR
REMARK   3  ALN A TRG FHVLSKLLSFMAPIDHTTMNDDARTELYRSLFGQLHPPDEGHGD
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ---------------------------------------------DAEKGLAVRKQRR
REMARK   3  ALN A TPL AFDSILNLRIRLQKALIAANS------FHVVEKAEGQDSTEAYQAAEEAAIKLWNTIA
REMARK   3  ALN A TPL SVRHHFMPESCQTK----------------------VGEKR--------KREIELDTA
REMARK   3  ALN A TPL NQEIWETMEEVEEVVMSHRRKVLDKWSNKVKKSNAGLNTRKLVNSEGQTLVAALDEQI
REMARK   3  ALN A TPL MSSERLIKKARTPRSCA-----PVQAAKKVEE----DPAIYDDAD-------------
REMARK   3  ALN A TPL ----FYQLLLKELVEQRSADTGLAGEGVATVRWAALKE--TKTRKNVDRRASKGRKLR
REMARK   3  ALN A TPL YTVHEKLQNFMAPEDRRSWEEHAIDRLFGTLFGQ----------
REMARK   3  ALN A OFF 306
ATOM      1  N   ARG A 507     245.631 280.950 375.180  1.00  0.29           N  
ATOM      2  CA  ARG A 507     244.827 281.323 376.414  1.00  0.29           C  
ATOM      3  C   ARG A 507     244.485 280.069 377.214  1.00  0.29           C  
ATOM      4  O   ARG A 507     244.906 278.993 376.821  1.00  0.29           O  
ATOM      5  CB  ARG A 507     243.533 282.089 375.984  1.00  0.29           C  
ATOM      6  CG  ARG A 507     243.791 283.506 375.408  1.00  0.29           C  
ATOM      7  CD  ARG A 507     242.839 283.913 374.266  1.00  0.29           C  
ATOM      8  NE  ARG A 507     243.509 285.027 373.501  1.00  0.29           N  
ATOM      9  CZ  ARG A 507     243.158 285.427 372.267  1.00  0.29           C  
ATOM     10  NH1 ARG A 507     242.091 284.933 371.649  1.00  0.29           N  
ATOM     11  NH2 ARG A 507     243.882 286.344 371.626  1.00  0.29           N  
ATOM     12  N   LYS A 508     243.726 280.140 378.338  1.00  0.55           N  
ATOM     13  CA  LYS A 508     243.430 278.950 379.130  1.00  0.55           C  
ATOM     14  C   LYS A 508     242.182 278.208 378.638  1.00  0.55           C  
ATOM     15  O   LYS A 508     241.878 277.088 379.036  1.00  0.55           O  
ATOM     16  CB  LYS A 508     243.256 279.371 380.613  1.00  0.55           C  
ATOM     17  CG  LYS A 508     244.563 279.892 381.240  1.00  0.55           C  
ATOM     18  CD  LYS A 508     244.381 280.250 382.727  1.00  0.55           C  
ATOM     19  CE  LYS A 508     245.665 280.746 383.411  1.00  0.55           C  
ATOM     20  NZ  LYS A 508     245.417 281.102 384.831  1.00  0.55           N  
ATOM     21  N   ALA A 509     241.463 278.827 377.683  1.00  0.50           N  
ATOM     22  CA  ALA A 509     240.219 278.346 377.152  1.00  0.50           C  
ATOM     23  C   ALA A 509     240.416 277.939 375.704  1.00  0.50           C  
ATOM     24  O   ALA A 509     240.794 278.767 374.887  1.00  0.50           O  
ATOM     25  CB  ALA A 509     239.182 279.477 377.166  1.00  0.50           C  
ATOM     26  N   SER A 510     240.143 276.649 375.393  1.00  0.49           N  
ATOM     27  CA  SER A 510     240.397 276.062 374.077  1.00  0.49           C  
ATOM     28  C   SER A 510     239.504 274.845 373.842  1.00  0.49           C  
ATOM     29  O   SER A 510     238.584 274.898 373.038  1.00  0.49           O  
ATOM     30  CB  SER A 510     241.876 275.630 373.833  1.00  0.49           C  
ATOM     31  OG  SER A 510     242.692 276.743 373.473  1.00  0.49           O  
ATOM     32  N   LYS A 511     239.723 273.712 374.572  1.00  0.49           N  
ATOM     33  CA  LYS A 511     238.948 272.473 374.426  1.00  0.49           C  
ATOM     34  C   LYS A 511     237.543 272.542 375.028  1.00  0.49           C  
ATOM     35  O   LYS A 511     237.243 271.883 376.010  1.00  0.49           O  
ATOM     36  CB  LYS A 511     239.664 271.247 375.081  1.00  0.49           C  
ATOM     37  CG  LYS A 511     241.052 270.915 374.505  1.00  0.49           C  
ATOM     38  CD  LYS A 511     242.217 271.069 375.506  1.00  0.49           C  
ATOM     39  CE  LYS A 511     242.228 269.987 376.601  1.00  0.49           C  
ATOM     40  NZ  LYS A 511     243.417 270.138 377.473  1.00  0.49           N  
ATOM     41  N   GLY A 512     236.661 273.398 374.471  1.00  0.52           N  
ATOM     42  CA  GLY A 512     235.253 273.513 374.853  1.00  0.52           C  
ATOM     43  C   GLY A 512     234.987 274.263 376.142  1.00  0.52           C  
ATOM     44  O   GLY A 512     233.859 274.387 376.577  1.00  0.52           O  
ATOM     45  N   ARG A 513     236.059 274.786 376.784  1.00  0.43           N  
ATOM     46  CA  ARG A 513     235.988 275.540 378.035  1.00  0.43           C  
ATOM     47  C   ARG A 513     235.290 276.885 377.916  1.00  0.43           C  
ATOM     48  O   ARG A 513     234.549 277.305 378.786  1.00  0.43           O  
ATOM     49  CB  ARG A 513     237.381 275.839 378.662  1.00  0.43           C  
ATOM     50  CG  ARG A 513     238.146 274.650 379.291  1.00  0.43           C  
ATOM     51  CD  ARG A 513     238.733 273.625 378.310  1.00  0.43           C  
ATOM     52  NE  ARG A 513     239.876 272.900 378.972  1.00  0.43           N  
ATOM     53  CZ  ARG A 513     241.162 273.233 378.887  1.00  0.43           C  
ATOM     54  NH1 ARG A 513     241.577 274.290 378.223  1.00  0.43           N  
ATOM     55  NH2 ARG A 513     242.055 272.654 379.675  1.00  0.43           N  
ATOM     56  N   LYS A 514     235.557 277.607 376.811  1.00  0.49           N  
ATOM     57  CA  LYS A 514     234.789 278.777 376.478  1.00  0.49           C  
ATOM     58  C   LYS A 514     233.706 278.335 375.536  1.00  0.49           C  
ATOM     59  O   LYS A 514     233.953 277.566 374.616  1.00  0.49           O  
ATOM     60  CB  LYS A 514     235.637 279.907 375.845  1.00  0.49           C  
ATOM     61  CG  LYS A 514     236.024 280.958 376.894  1.00  0.49           C  
ATOM     62  CD  LYS A 514     236.606 282.233 376.268  1.00  0.49           C  
ATOM     63  CE  LYS A 514     237.047 283.269 377.309  1.00  0.49           C  
ATOM     64  NZ  LYS A 514     237.398 284.549 376.651  1.00  0.49           N  
ATOM     65  N   LEU A 515     232.477 278.839 375.756  1.00  0.45           N  
ATOM     66  CA  LEU A 515     231.373 278.703 374.835  1.00  0.45           C  
ATOM     67  C   LEU A 515     231.604 279.497 373.572  1.00  0.45           C  
ATOM     68  O   LEU A 515     231.296 280.683 373.464  1.00  0.45           O  
ATOM     69  CB  LEU A 515     230.020 279.150 375.438  1.00  0.45           C  
ATOM     70  CG  LEU A 515     229.679 278.452 376.766  1.00  0.45           C  
ATOM     71  CD1 LEU A 515     229.939 279.375 377.972  1.00  0.45           C  
ATOM     72  CD2 LEU A 515     228.227 277.947 376.761  1.00  0.45           C  
ATOM     73  N   ARG A 516     232.170 278.842 372.555  1.00  0.42           N  
ATOM     74  CA  ARG A 516     232.392 279.457 371.279  1.00  0.42           C  
ATOM     75  C   ARG A 516     231.098 279.388 370.467  1.00  0.42           C  
ATOM     76  O   ARG A 516     230.771 278.353 369.888  1.00  0.42           O  
ATOM     77  CB  ARG A 516     233.535 278.711 370.538  1.00  0.42           C  
ATOM     78  CG  ARG A 516     234.890 278.571 371.277  1.00  0.42           C  
ATOM     79  CD  ARG A 516     235.790 277.447 370.728  1.00  0.42           C  
ATOM     80  NE  ARG A 516     235.921 277.645 369.237  1.00  0.42           N  
ATOM     81  CZ  ARG A 516     236.951 278.199 368.580  1.00  0.42           C  
ATOM     82  NH1 ARG A 516     238.014 278.673 369.214  1.00  0.42           N  
ATOM     83  NH2 ARG A 516     236.919 278.267 367.247  1.00  0.42           N  
ATOM     84  N   PHE A 517     230.326 280.496 370.422  1.00  0.46           N  
ATOM     85  CA  PHE A 517     229.063 280.613 369.707  1.00  0.46           C  
ATOM     86  C   PHE A 517     229.228 280.647 368.192  1.00  0.46           C  
ATOM     87  O   PHE A 517     229.154 281.697 367.558  1.00  0.46           O  
ATOM     88  CB  PHE A 517     228.275 281.879 370.147  1.00  0.46           C  
ATOM     89  CG  PHE A 517     227.882 281.804 371.594  1.00  0.46           C  
ATOM     90  CD1 PHE A 517     226.754 281.061 371.973  1.00  0.46           C  
ATOM     91  CD2 PHE A 517     228.599 282.503 372.578  1.00  0.46           C  
ATOM     92  CE1 PHE A 517     226.336 281.030 373.310  1.00  0.46           C  
ATOM     93  CE2 PHE A 517     228.188 282.471 373.917  1.00  0.46           C  
ATOM     94  CZ  PHE A 517     227.052 281.738 374.282  1.00  0.46           C  
ATOM     95  N   HIS A 518     229.430 279.471 367.574  1.00  0.49           N  
ATOM     96  CA  HIS A 518     229.561 279.312 366.142  1.00  0.49           C  
ATOM     97  C   HIS A 518     228.396 278.484 365.694  1.00  0.49           C  
ATOM     98  O   HIS A 518     228.046 277.488 366.330  1.00  0.49           O  
ATOM     99  CB  HIS A 518     230.841 278.554 365.728  1.00  0.49           C  
ATOM    100  CG  HIS A 518     232.067 279.307 366.077  1.00  0.49           C  
ATOM    101  ND1 HIS A 518     232.510 280.351 365.295  1.00  0.49           N  
ATOM    102  CD2 HIS A 518     232.859 279.158 367.156  1.00  0.49           C  
ATOM    103  CE1 HIS A 518     233.562 280.823 365.918  1.00  0.49           C  
ATOM    104  NE2 HIS A 518     233.826 280.138 367.061  1.00  0.49           N  
ATOM    105  N   VAL A 519     227.743 278.874 364.588  1.00  0.46           N  
ATOM    106  CA  VAL A 519     226.709 278.070 363.970  1.00  0.46           C  
ATOM    107  C   VAL A 519     227.298 276.776 363.411  1.00  0.46           C  
ATOM    108  O   VAL A 519     228.254 276.774 362.635  1.00  0.46           O  
ATOM    109  CB  VAL A 519     225.908 278.869 362.934  1.00  0.46           C  
ATOM    110  CG1 VAL A 519     226.808 279.401 361.791  1.00  0.46           C  
ATOM    111  CG2 VAL A 519     224.717 278.040 362.406  1.00  0.46           C  
ATOM    112  N   LEU A 520     226.775 275.617 363.846  1.00  0.41           N  
ATOM    113  CA  LEU A 520     227.337 274.330 363.508  1.00  0.41           C  
ATOM    114  C   LEU A 520     226.651 273.771 362.277  1.00  0.41           C  
ATOM    115  O   LEU A 520     225.429 273.762 362.165  1.00  0.41           O  
ATOM    116  CB  LEU A 520     227.181 273.349 364.689  1.00  0.41           C  
ATOM    117  CG  LEU A 520     227.948 273.712 365.974  1.00  0.41           C  
ATOM    118  CD1 LEU A 520     227.595 272.691 367.069  1.00  0.41           C  
ATOM    119  CD2 LEU A 520     229.469 273.799 365.755  1.00  0.41           C  
ATOM    120  N   SER A 521     227.438 273.324 361.286  1.00  0.40           N  
ATOM    121  CA  SER A 521     226.950 273.042 359.944  1.00  0.40           C  
ATOM    122  C   SER A 521     226.062 271.830 359.770  1.00  0.40           C  
ATOM    123  O   SER A 521     225.052 271.865 359.084  1.00  0.40           O  
ATOM    124  CB  SER A 521     228.124 272.907 358.960  1.00  0.40           C  
ATOM    125  OG  SER A 521     228.924 274.081 359.072  1.00  0.40           O  
ATOM    126  N   LYS A 522     226.428 270.688 360.390  1.00  0.39           N  
ATOM    127  CA  LYS A 522     225.628 269.480 360.303  1.00  0.39           C  
ATOM    128  C   LYS A 522     224.554 269.397 361.390  1.00  0.39           C  
ATOM    129  O   LYS A 522     223.611 268.634 361.269  1.00  0.39           O  
ATOM    130  CB  LYS A 522     226.510 268.208 360.401  1.00  0.39           C  
ATOM    131  CG  LYS A 522     227.714 268.176 359.443  1.00  0.39           C  
ATOM    132  CD  LYS A 522     228.421 266.801 359.389  1.00  0.39           C  
ATOM    133  CE  LYS A 522     227.585 265.603 358.905  1.00  0.39           C  
ATOM    134  NZ  LYS A 522     226.989 265.920 357.593  1.00  0.39           N  
ATOM    135  N   LEU A 523     224.670 270.240 362.441  1.00  0.38           N  
ATOM    136  CA  LEU A 523     223.804 270.332 363.608  1.00  0.38           C  
ATOM    137  C   LEU A 523     222.736 271.396 363.327  1.00  0.38           C  
ATOM    138  O   LEU A 523     222.051 271.869 364.225  1.00  0.38           O  
ATOM    139  CB  LEU A 523     224.663 270.787 364.833  1.00  0.38           C  
ATOM    140  CG  LEU A 523     225.324 269.691 365.715  1.00  0.38           C  
ATOM    141  CD1 LEU A 523     224.258 268.929 366.518  1.00  0.38           C  
ATOM    142  CD2 LEU A 523     226.280 268.742 364.963  1.00  0.38           C  
ATOM    143  N   LEU A 524     222.592 271.823 362.050  1.00  0.37           N  
ATOM    144  CA  LEU A 524     221.812 272.986 361.685  1.00  0.37           C  
ATOM    145  C   LEU A 524     220.292 272.791 361.648  1.00  0.37           C  
ATOM    146  O   LEU A 524     219.534 273.604 362.165  1.00  0.37           O  
ATOM    147  CB  LEU A 524     222.345 273.541 360.342  1.00  0.37           C  
ATOM    148  CG  LEU A 524     221.727 274.881 359.890  1.00  0.37           C  
ATOM    149  CD1 LEU A 524     221.899 276.002 360.934  1.00  0.37           C  
ATOM    150  CD2 LEU A 524     222.339 275.295 358.543  1.00  0.37           C  
ATOM    151  N   SER A 525     219.816 271.674 361.050  1.00  0.38           N  
ATOM    152  CA  SER A 525     218.408 271.487 360.691  1.00  0.38           C  
ATOM    153  C   SER A 525     217.933 270.095 361.061  1.00  0.38           C  
ATOM    154  O   SER A 525     217.328 269.396 360.255  1.00  0.38           O  
ATOM    155  CB  SER A 525     218.126 271.662 359.170  1.00  0.38           C  
ATOM    156  OG  SER A 525     218.488 272.973 358.737  1.00  0.38           O  
ATOM    157  N   PHE A 526     218.231 269.639 362.295  1.00  0.33           N  
ATOM    158  CA  PHE A 526     217.853 268.322 362.792  1.00  0.33           C  
ATOM    159  C   PHE A 526     216.367 268.070 363.012  1.00  0.33           C  
ATOM    160  O   PHE A 526     215.794 267.132 362.478  1.00  0.33           O  
ATOM    161  CB  PHE A 526     218.534 268.088 364.174  1.00  0.33           C  
ATOM    162  CG  PHE A 526     219.946 267.560 364.124  1.00  0.33           C  
ATOM    163  CD1 PHE A 526     220.675 267.285 362.948  1.00  0.33           C  
ATOM    164  CD2 PHE A 526     220.534 267.244 365.358  1.00  0.33           C  
ATOM    165  CE1 PHE A 526     221.936 266.674 363.016  1.00  0.33           C  
ATOM    166  CE2 PHE A 526     221.790 266.636 365.428  1.00  0.33           C  
ATOM    167  CZ  PHE A 526     222.497 266.356 364.256  1.00  0.33           C  
ATOM    168  N   MET A 527     215.709 268.903 363.835  1.00  0.32           N  
ATOM    169  CA  MET A 527     214.379 268.615 364.314  1.00  0.32           C  
ATOM    170  C   MET A 527     213.515 269.814 364.044  1.00  0.32           C  
ATOM    171  O   MET A 527     213.916 270.952 364.271  1.00  0.32           O  
ATOM    172  CB  MET A 527     214.408 268.317 365.835  1.00  0.32           C  
ATOM    173  CG  MET A 527     214.898 266.892 366.162  1.00  0.32           C  
ATOM    174  SD  MET A 527     213.676 265.591 365.790  1.00  0.32           S  
ATOM    175  CE  MET A 527     212.506 265.974 367.130  1.00  0.32           C  
ATOM    176  N   ALA A 528     212.291 269.582 363.531  1.00  0.42           N  
ATOM    177  CA  ALA A 528     211.290 270.612 363.419  1.00  0.42           C  
ATOM    178  C   ALA A 528     210.768 270.948 364.827  1.00  0.42           C  
ATOM    179  O   ALA A 528     210.553 270.011 365.599  1.00  0.42           O  
ATOM    180  CB  ALA A 528     210.146 270.152 362.483  1.00  0.42           C  
ATOM    181  N   PRO A 529     210.560 272.196 365.249  1.00  0.39           N  
ATOM    182  CA  PRO A 529     209.767 272.517 366.437  1.00  0.39           C  
ATOM    183  C   PRO A 529     208.371 271.893 366.362  1.00  0.39           C  
ATOM    184  O   PRO A 529     207.659 272.098 365.382  1.00  0.39           O  
ATOM    185  CB  PRO A 529     209.796 274.060 366.491  1.00  0.39           C  
ATOM    186  CG  PRO A 529     209.998 274.496 365.034  1.00  0.39           C  
ATOM    187  CD  PRO A 529     210.845 273.377 364.435  1.00  0.39           C  
ATOM    188  N   ILE A 530     207.986 271.089 367.374  1.00  0.33           N  
ATOM    189  CA  ILE A 530     206.670 270.497 367.499  1.00  0.33           C  
ATOM    190  C   ILE A 530     206.088 271.065 368.774  1.00  0.33           C  
ATOM    191  O   ILE A 530     206.490 270.696 369.879  1.00  0.33           O  
ATOM    192  CB  ILE A 530     206.704 268.964 367.553  1.00  0.33           C  
ATOM    193  CG1 ILE A 530     207.253 268.427 366.208  1.00  0.33           C  
ATOM    194  CG2 ILE A 530     205.282 268.425 367.846  1.00  0.33           C  
ATOM    195  CD1 ILE A 530     207.489 266.912 366.185  1.00  0.33           C  
ATOM    196  N   ASP A 531     205.111 271.982 368.659  1.00  0.44           N  
ATOM    197  CA  ASP A 531     204.447 272.560 369.804  1.00  0.44           C  
ATOM    198  C   ASP A 531     203.384 271.618 370.358  1.00  0.44           C  
ATOM    199  O   ASP A 531     202.268 271.509 369.850  1.00  0.44           O  
ATOM    200  CB  ASP A 531     203.809 273.924 369.438  1.00  0.44           C  
ATOM    201  CG  ASP A 531     204.871 274.891 368.945  1.00  0.44           C  
ATOM    202  OD1 ASP A 531     206.011 274.839 369.471  1.00  0.44           O  
ATOM    203  OD2 ASP A 531     204.547 275.683 368.026  1.00  0.44           O  
ATOM    204  N   HIS A 532     203.712 270.885 371.438  1.00  0.38           N  
ATOM    205  CA  HIS A 532     202.787 269.982 372.087  1.00  0.38           C  
ATOM    206  C   HIS A 532     202.498 270.493 373.482  1.00  0.38           C  
ATOM    207  O   HIS A 532     203.085 270.056 374.467  1.00  0.38           O  
ATOM    208  CB  HIS A 532     203.318 268.535 372.141  1.00  0.38           C  
ATOM    209  CG  HIS A 532     202.321 267.550 372.670  1.00  0.38           C  
ATOM    210  ND1 HIS A 532     202.762 266.266 372.915  1.00  0.38           N  
ATOM    211  CD2 HIS A 532     200.994 267.651 372.949  1.00  0.38           C  
ATOM    212  CE1 HIS A 532     201.706 265.615 373.341  1.00  0.38           C  
ATOM    213  NE2 HIS A 532     200.601 266.402 373.383  1.00  0.38           N  
ATOM    214  N   THR A 533     201.566 271.452 373.594  1.00  0.41           N  
ATOM    215  CA  THR A 533     201.210 272.090 374.852  1.00  0.41           C  
ATOM    216  C   THR A 533     199.693 271.999 374.877  1.00  0.41           C  
ATOM    217  O   THR A 533     199.035 272.397 373.924  1.00  0.41           O  
ATOM    218  CB  THR A 533     201.687 273.548 374.933  1.00  0.41           C  
ATOM    219  OG1 THR A 533     203.105 273.627 374.954  1.00  0.41           O  
ATOM    220  CG2 THR A 533     201.242 274.235 376.224  1.00  0.41           C  
ATOM    221  N   THR A 534     199.059 271.422 375.928  1.00  0.53           N  
ATOM    222  CA  THR A 534     197.612 271.184 375.960  1.00  0.53           C  
ATOM    223  C   THR A 534     196.831 272.343 376.561  1.00  0.53           C  
ATOM    224  O   THR A 534     195.614 272.306 376.662  1.00  0.53           O  
ATOM    225  CB  THR A 534     197.245 269.967 376.810  1.00  0.53           C  
ATOM    226  OG1 THR A 534     197.909 269.996 378.066  1.00  0.53           O  
ATOM    227  CG2 THR A 534     197.720 268.690 376.115  1.00  0.53           C  
ATOM    228  N   MET A 535     197.546 273.406 376.973  1.00  0.36           N  
ATOM    229  CA  MET A 535     196.981 274.592 377.581  1.00  0.36           C  
ATOM    230  C   MET A 535     197.018 275.707 376.570  1.00  0.36           C  
ATOM    231  O   MET A 535     197.882 275.755 375.711  1.00  0.36           O  
ATOM    232  CB  MET A 535     197.741 275.006 378.874  1.00  0.36           C  
ATOM    233  CG  MET A 535     197.632 273.922 379.964  1.00  0.36           C  
ATOM    234  SD  MET A 535     195.924 273.510 380.452  1.00  0.36           S  
ATOM    235  CE  MET A 535     195.570 275.039 381.361  1.00  0.36           C  
ATOM    236  N   ASN A 536     196.049 276.635 376.671  1.00  0.44           N  
ATOM    237  CA  ASN A 536     195.702 277.563 375.611  1.00  0.44           C  
ATOM    238  C   ASN A 536     196.407 278.911 375.681  1.00  0.44           C  
ATOM    239  O   ASN A 536     195.866 279.906 375.248  1.00  0.44           O  
ATOM    240  CB  ASN A 536     194.176 277.838 375.659  1.00  0.44           C  
ATOM    241  CG  ASN A 536     193.448 276.536 375.371  1.00  0.44           C  
ATOM    242  OD1 ASN A 536     193.780 275.806 374.453  1.00  0.44           O  
ATOM    243  ND2 ASN A 536     192.399 276.219 376.167  1.00  0.44           N  
ATOM    244  N   ASP A 537     197.612 278.969 376.277  1.00  0.45           N  
ATOM    245  CA  ASP A 537     198.457 280.152 376.357  1.00  0.45           C  
ATOM    246  C   ASP A 537     198.010 281.374 377.177  1.00  0.45           C  
ATOM    247  O   ASP A 537     198.846 281.995 377.827  1.00  0.45           O  
ATOM    248  CB  ASP A 537     198.972 280.526 374.946  1.00  0.45           C  
ATOM    249  CG  ASP A 537     199.781 279.330 374.469  1.00  0.45           C  
ATOM    250  OD1 ASP A 537     200.553 278.787 375.312  1.00  0.45           O  
ATOM    251  OD2 ASP A 537     199.635 278.924 373.294  1.00  0.45           O  
ATOM    252  N   ASP A 538     196.703 281.705 377.255  1.00  0.37           N  
ATOM    253  CA  ASP A 538     196.131 282.725 378.123  1.00  0.37           C  
ATOM    254  C   ASP A 538     196.194 282.417 379.638  1.00  0.37           C  
ATOM    255  O   ASP A 538     196.467 283.269 380.472  1.00  0.37           O  
ATOM    256  CB  ASP A 538     194.693 283.055 377.644  1.00  0.37           C  
ATOM    257  CG  ASP A 538     194.737 283.518 376.194  1.00  0.37           C  
ATOM    258  OD1 ASP A 538     195.515 284.460 375.897  1.00  0.37           O  
ATOM    259  OD2 ASP A 538     193.978 282.932 375.382  1.00  0.37           O  
ATOM    260  N   ALA A 539     195.947 281.141 380.033  1.00  0.58           N  
ATOM    261  CA  ALA A 539     195.910 280.726 381.432  1.00  0.58           C  
ATOM    262  C   ALA A 539     197.304 280.404 381.977  1.00  0.58           C  
ATOM    263  O   ALA A 539     197.537 280.289 383.176  1.00  0.58           O  
ATOM    264  CB  ALA A 539     195.031 279.457 381.578  1.00  0.58           C  
ATOM    265  N   ARG A 540     198.289 280.225 381.073  1.00  0.38           N  
ATOM    266  CA  ARG A 540     199.640 279.853 381.435  1.00  0.38           C  
ATOM    267  C   ARG A 540     200.487 281.019 381.963  1.00  0.38           C  
ATOM    268  O   ARG A 540     201.245 280.857 382.911  1.00  0.38           O  
ATOM    269  CB  ARG A 540     200.324 279.103 380.263  1.00  0.38           C  
ATOM    270  CG  ARG A 540     201.693 278.513 380.655  1.00  0.38           C  
ATOM    271  CD  ARG A 540     202.320 277.602 379.595  1.00  0.38           C  
ATOM    272  NE  ARG A 540     203.657 277.169 380.157  1.00  0.38           N  
ATOM    273  CZ  ARG A 540     204.531 276.382 379.507  1.00  0.38           C  
ATOM    274  NH1 ARG A 540     205.714 276.034 380.019  1.00  0.38           N  
ATOM    275  NH2 ARG A 540     204.246 275.919 378.295  1.00  0.38           N  
ATOM    276  N   THR A 541     200.388 282.240 381.387  1.00  0.54           N  
ATOM    277  CA  THR A 541     201.105 283.426 381.877  1.00  0.54           C  
ATOM    278  C   THR A 541     200.540 283.964 383.188  1.00  0.54           C  
ATOM    279  O   THR A 541     201.279 284.489 384.018  1.00  0.54           O  
ATOM    280  CB  THR A 541     201.175 284.549 380.852  1.00  0.54           C  
ATOM    281  OG1 THR A 541     199.870 284.908 380.433  1.00  0.54           O  
ATOM    282  CG2 THR A 541     201.933 284.030 379.624  1.00  0.54           C  
ATOM    283  N   GLU A 542     199.213 283.792 383.418  1.00  0.46           N  
ATOM    284  CA  GLU A 542     198.525 284.057 384.674  1.00  0.46           C  
ATOM    285  C   GLU A 542     199.045 283.183 385.813  1.00  0.46           C  
ATOM    286  O   GLU A 542     199.298 283.674 386.908  1.00  0.46           O  
ATOM    287  CB  GLU A 542     196.992 283.928 384.499  1.00  0.46           C  
ATOM    288  CG  GLU A 542     196.392 285.052 383.613  1.00  0.46           C  
ATOM    289  CD  GLU A 542     194.865 284.980 383.533  1.00  0.46           C  
ATOM    290  OE1 GLU A 542     194.289 283.949 383.965  1.00  0.46           O  
ATOM    291  OE2 GLU A 542     194.271 285.992 383.080  1.00  0.46           O  
ATOM    292  N   LEU A 543     199.315 281.885 385.532  1.00  0.46           N  
ATOM    293  CA  LEU A 543     199.858 280.896 386.452  1.00  0.46           C  
ATOM    294  C   LEU A 543     201.233 281.249 387.004  1.00  0.46           C  
ATOM    295  O   LEU A 543     201.470 281.158 388.198  1.00  0.46           O  
ATOM    296  CB  LEU A 543     199.913 279.515 385.735  1.00  0.46           C  
ATOM    297  CG  LEU A 543     200.271 278.302 386.626  1.00  0.46           C  
ATOM    298  CD1 LEU A 543     199.614 277.035 386.056  1.00  0.46           C  
ATOM    299  CD2 LEU A 543     201.788 278.066 386.782  1.00  0.46           C  
ATOM    300  N   TYR A 544     202.188 281.695 386.153  1.00  0.45           N  
ATOM    301  CA  TYR A 544     203.550 281.993 386.599  1.00  0.45           C  
ATOM    302  C   TYR A 544     203.675 283.225 387.483  1.00  0.45           C  
ATOM    303  O   TYR A 544     204.625 283.355 388.247  1.00  0.45           O  
ATOM    304  CB  TYR A 544     204.544 282.147 385.424  1.00  0.45           C  
ATOM    305  CG  TYR A 544     204.791 280.819 384.795  1.00  0.45           C  
ATOM    306  CD1 TYR A 544     205.582 279.854 385.442  1.00  0.45           C  
ATOM    307  CD2 TYR A 544     204.309 280.563 383.510  1.00  0.45           C  
ATOM    308  CE1 TYR A 544     205.890 278.646 384.796  1.00  0.45           C  
ATOM    309  CE2 TYR A 544     204.644 279.377 382.857  1.00  0.45           C  
ATOM    310  CZ  TYR A 544     205.430 278.419 383.495  1.00  0.45           C  
ATOM    311  OH  TYR A 544     205.724 277.231 382.802  1.00  0.45           O  
ATOM    312  N   ARG A 545     202.698 284.152 387.403  1.00  0.39           N  
ATOM    313  CA  ARG A 545     202.567 285.254 388.338  1.00  0.39           C  
ATOM    314  C   ARG A 545     202.201 284.779 389.740  1.00  0.39           C  
ATOM    315  O   ARG A 545     202.834 285.172 390.716  1.00  0.39           O  
ATOM    316  CB  ARG A 545     201.495 286.258 387.849  1.00  0.39           C  
ATOM    317  CG  ARG A 545     201.909 287.015 386.577  1.00  0.39           C  
ATOM    318  CD  ARG A 545     200.806 287.954 386.097  1.00  0.39           C  
ATOM    319  NE  ARG A 545     201.351 288.667 384.896  1.00  0.39           N  
ATOM    320  CZ  ARG A 545     200.629 289.483 384.119  1.00  0.39           C  
ATOM    321  NH1 ARG A 545     201.180 290.039 383.042  1.00  0.39           N  
ATOM    322  NH2 ARG A 545     199.352 289.740 384.385  1.00  0.39           N  
ATOM    323  N   SER A 546     201.200 283.880 389.862  1.00  0.44           N  
ATOM    324  CA  SER A 546     200.583 283.485 391.123  1.00  0.44           C  
ATOM    325  C   SER A 546     201.246 282.276 391.772  1.00  0.44           C  
ATOM    326  O   SER A 546     200.600 281.333 392.228  1.00  0.44           O  
ATOM    327  CB  SER A 546     199.048 283.272 390.985  1.00  0.44           C  
ATOM    328  OG  SER A 546     198.720 282.351 389.940  1.00  0.44           O  
ATOM    329  N   LEU A 547     202.592 282.309 391.869  1.00  0.34           N  
ATOM    330  CA  LEU A 547     203.390 281.369 392.644  1.00  0.34           C  
ATOM    331  C   LEU A 547     203.580 281.959 394.039  1.00  0.34           C  
ATOM    332  O   LEU A 547     202.873 282.866 394.432  1.00  0.34           O  
ATOM    333  CB  LEU A 547     204.750 281.046 391.957  1.00  0.34           C  
ATOM    334  CG  LEU A 547     204.711 279.759 391.107  1.00  0.34           C  
ATOM    335  CD1 LEU A 547     203.837 279.934 389.859  1.00  0.34           C  
ATOM    336  CD2 LEU A 547     206.140 279.323 390.740  1.00  0.34           C  
ATOM    337  N   PHE A 548     204.544 281.467 394.846  1.00  0.24           N  
ATOM    338  CA  PHE A 548     204.626 281.732 396.278  1.00  0.24           C  
ATOM    339  C   PHE A 548     204.858 283.175 396.748  1.00  0.24           C  
ATOM    340  O   PHE A 548     204.717 283.473 397.923  1.00  0.24           O  
ATOM    341  CB  PHE A 548     205.807 280.909 396.865  1.00  0.24           C  
ATOM    342  CG  PHE A 548     205.745 279.416 396.635  1.00  0.24           C  
ATOM    343  CD1 PHE A 548     204.570 278.694 396.336  1.00  0.24           C  
ATOM    344  CD2 PHE A 548     206.945 278.701 396.781  1.00  0.24           C  
ATOM    345  CE1 PHE A 548     204.609 277.303 396.167  1.00  0.24           C  
ATOM    346  CE2 PHE A 548     206.985 277.311 396.627  1.00  0.24           C  
ATOM    347  CZ  PHE A 548     205.816 276.611 396.314  1.00  0.24           C  
ATOM    348  N   GLY A 549     205.286 284.082 395.842  1.00  0.45           N  
ATOM    349  CA  GLY A 549     205.449 285.499 396.141  1.00  0.45           C  
ATOM    350  C   GLY A 549     204.183 286.331 396.055  1.00  0.45           C  
ATOM    351  O   GLY A 549     204.063 287.313 396.776  1.00  0.45           O  
ATOM    352  N   GLN A 550     203.262 285.992 395.123  1.00  0.46           N  
ATOM    353  CA  GLN A 550     201.947 286.605 394.991  1.00  0.46           C  
ATOM    354  C   GLN A 550     200.844 285.802 395.741  1.00  0.46           C  
ATOM    355  O   GLN A 550     201.141 284.742 396.348  1.00  0.46           O  
ATOM    356  CB  GLN A 550     201.490 286.740 393.501  1.00  0.46           C  
ATOM    357  CG  GLN A 550     202.242 287.820 392.683  1.00  0.46           C  
ATOM    358  CD  GLN A 550     201.519 288.232 391.391  1.00  0.46           C  
ATOM    359  OE1 GLN A 550     200.462 287.771 390.991  1.00  0.46           O  
ATOM    360  NE2 GLN A 550     202.128 289.232 390.691  1.00  0.46           N  
ATOM    361  OXT GLN A 550     199.672 286.272 395.709  1.00  0.46           O  
TER     362      GLN A 550                                                      
END   
