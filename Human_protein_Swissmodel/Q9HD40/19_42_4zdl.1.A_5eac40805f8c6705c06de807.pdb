TITLE     SWISS-MODEL SERVER (https://swissmodel.expasy.org)
TITLE    2 Untitled Project
EXPDTA    THEORETICAL MODEL (SWISS-MODEL SERVER)
AUTHOR    SWISS-MODEL SERVER (SEE REFERENCE IN JRNL Records)
REVDAT   1   01-MAY-20 1MOD    1       17:25
JRNL        AUTH   A.WATERHOUSE,M.BERTONI,S.BIENERT,G.STUDER,G.TAURIELLO,
JRNL        AUTH 2 R.GUMIENNY,F.T.HEER,T.A.P.DE BEER,C.REMPFER,L.BORDOLI,
JRNL        AUTH 3 R.LEPORE,T.SCHWEDE
JRNL        TITL   SWISS-MODEL: HOMOLOGY MODELLING OF PROTEIN STRUCTURES AND
JRNL        TITL 2 COMPLEXES
JRNL        REF    NUCLEIC.ACIDS.RES..           V.  46 W296  2018
JRNL        PMID   29788355
JRNL        DOI    10.1093/nar/gky427
REMARK   1
REMARK   1 REFERENCE 1
REMARK   1  AUTH   S.BIENERT,A.WATERHOUSE,T.A.P.DE BEER,G.TAURIELLO,G.STUDER,
REMARK   1  AUTH 2 L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   THE SWISS-MODEL REPOSITORY - NEW FEATURES AND FUNCTIONALITY
REMARK   1  REF    NUCLEIC.ACIDS.RES..           V.  22       2017
REMARK   1  REFN                   ISSN 0305-1048
REMARK   1  PMID   27899672
REMARK   1  DOI    10.1093/nar/gkw1132
REMARK   1
REMARK   1 REFERENCE 2
REMARK   1  AUTH   N.GUEX,M.C.PEITSCH,T.SCHWEDE
REMARK   1  TITL   AUTOMATED COMPARATIVE PROTEIN STRUCTURE MODELING WITH
REMARK   1  TITL 2 SWISS-MODEL AND SWISS-PDBVIEWER: A HISTORICAL PERSPECTIVE
REMARK   1  REF    ELECTROPHORESIS               V.  30       2009
REMARK   1  REFN                   ISSN 0173-0835
REMARK   1  PMID   19517507
REMARK   1  DOI    10.1002/elps.200900140
REMARK   1
REMARK   1 REFERENCE 3
REMARK   1  AUTH   P.BENKERT,M.BIASINI,T.SCHWEDE
REMARK   1  TITL   TOWARD THE ESTIMATION OF THE ABSOLUTE QUALITY OF INDIVIDUAL
REMARK   1  TITL 2 PROTEIN STRUCTURE MODELS
REMARK   1  REF    BIOINFORMATICS                V.  27       2011
REMARK   1  REFN                   ISSN 1367-4803
REMARK   1  PMID   21134891
REMARK   1  DOI    10.1093/bioinformatics/btq662
REMARK   1
REMARK   1 REFERENCE 4
REMARK   1  AUTH   M.BERTONI,F.KIEFER,M.BIASINI,L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   MODELING PROTEIN QUATERNARY STRUCTURE OF HOMO- AND
REMARK   1  TITL 2 HETERO-OLIGOMERS BEYOND BINARY INTERACTIONS BY HOMOLOGY
REMARK   1  REF    SCI.REP.                      V.   7       2017
REMARK   1  REFN                   ISSN
REMARK   1  PMID   28874689
REMARK   1  DOI    10.1038/s41598-017-09654-8
REMARK   1
REMARK   1 DISCLAIMER
REMARK   1 The SWISS-MODEL SERVER produces theoretical models for proteins.
REMARK   1 The results of any theoretical modelling procedure is
REMARK   1 NON-EXPERIMENTAL and MUST be considered with care. These models may
REMARK   1 contain significant errors. This is especially true for automated
REMARK   1 modeling since there is no human intervention during model
REMARK   1 building. Please read the header section and the logfile carefully
REMARK   1 to know what templates and alignments were used during the model
REMARK   1 building process. All information by the SWISS-MODEL SERVER is
REMARK   1 provided "AS-IS", without any warranty, expressed or implied.
REMARK   2
REMARK   2 COPYRIGHT NOTICE
REMARK   2 This SWISS-MODEL protein model is copyright. It is produced by the
REMARK   2 SWISS-MODEL server, developed by the Computational Structural
REMARK   2 Biology Group at the SIB Swiss Institute of Bioinformatics at the
REMARK   2 Biozentrum, University of Basel (https://swissmodel.expasy.org). This
REMARK   2 model is licensed under the CC BY-SA 4.0 Creative Commons
REMARK   2 Attribution-ShareAlike 4.0 International License
REMARK   2 (https://creativecommons.org/licenses/by-sa/4.0/legalcode), i.e. you
REMARK   2 can copy and redistribute the model in any medium or format,
REMARK   2 transform and build upon the model for any purpose, even
REMARK   2 commercially, under the following terms:
REMARK   2 Attribution - You must give appropriate credit, provide a link to
REMARK   2 the license, and indicate if changes were made. You may do so in any
REMARK   2 reasonable manner, but not in any way that suggests the licensor
REMARK   2 endorses you or your use. When you publish, patent or distribute
REMARK   2 results that were fully or partially based on the model, please cite
REMARK   2 the corresponding papers mentioned under JRNL.
REMARK   2 ShareAlike - If you remix, transform, or build upon the material,
REMARK   2 you must distribute your contributions under the same license as the
REMARK   2 original.
REMARK   2 No additional restrictions - you may not apply legal terms or
REMARK   2 technological measures that legally restrict others from doing
REMARK   2 anything the license permits.
REMARK   2 Find a human-readable summary of (and not a substitute for) the
REMARK   2 CC BY-SA 4.0 license at this link:
REMARK   2 https://creativecommons.org/licenses/by-sa/4.0/
REMARK   3 
REMARK   3 MODEL INFORMATION
REMARK   3  ENGIN   PROMOD3
REMARK   3  VERSN   3.0.0
REMARK   3  OSTAT   monomer
REMARK   3  OSRSN   PREDICTION
REMARK   3  QSPRD   0.691
REMARK   3  QMN4    -0.85
REMARK   3  MODT    FALSE
REMARK   3 
REMARK   3 TEMPLATE 1
REMARK   3  PDBID   4zdl
REMARK   3  CHAIN   A
REMARK   3  MMCIF   A
REMARK   3  PDBV    2020-04-24
REMARK   3  SMTLE   4zdl.1.A
REMARK   3  SMTLV   2020-04-29
REMARK   3  MTHD    X-RAY DIFFRACTION 2.26 A
REMARK   3  FOUND   HHblits
REMARK   3  SIM     0.55
REMARK   3  SID     90.48
REMARK   3  OSTAT   homo-dimer
REMARK   3  LIGND   PLR
REMARK   3  LIGND 2 PLR
REMARK   3  LIGND 3 PLR
REMARK   3  LIGND 4 PLR
REMARK   3  LIGND 5 FLC
REMARK   3  LIGND 6 FLC
REMARK   3  ALN A TRG MNRESFAAGERLVSPAYVRQGCEARRSHEHLIRLLLEKVHSWHWTIR
REMARK   3  ALN A TPL MNRESFAAGERLVSPAYVRQGCEARRSHEHLIRLLLEKGKCP-----
REMARK   3  ALN A OFF 0
ATOM      1  N   ARG A  19      28.014  41.944  61.403  1.00  0.44           N  
ATOM      2  CA  ARG A  19      27.139  40.920  62.106  1.00  0.44           C  
ATOM      3  C   ARG A  19      26.653  39.897  61.097  1.00  0.44           C  
ATOM      4  O   ARG A  19      26.859  40.124  59.911  1.00  0.44           O  
ATOM      5  CB  ARG A  19      25.870  41.601  62.727  1.00  0.44           C  
ATOM      6  CG  ARG A  19      26.152  42.550  63.909  1.00  0.44           C  
ATOM      7  CD  ARG A  19      24.875  43.130  64.542  1.00  0.44           C  
ATOM      8  NE  ARG A  19      25.321  44.032  65.653  1.00  0.44           N  
ATOM      9  CZ  ARG A  19      24.492  44.770  66.405  1.00  0.44           C  
ATOM     10  NH1 ARG A  19      24.987  45.539  67.372  1.00  0.44           N  
ATOM     11  NH2 ARG A  19      23.176  44.755  66.219  1.00  0.44           N  
ATOM     12  N   GLN A  20      25.933  38.821  61.520  1.00  0.59           N  
ATOM     13  CA  GLN A  20      25.324  37.830  60.641  1.00  0.59           C  
ATOM     14  C   GLN A  20      24.307  38.420  59.660  1.00  0.59           C  
ATOM     15  O   GLN A  20      24.113  37.925  58.562  1.00  0.59           O  
ATOM     16  CB  GLN A  20      24.648  36.710  61.487  1.00  0.59           C  
ATOM     17  CG  GLN A  20      25.623  35.566  61.875  1.00  0.59           C  
ATOM     18  CD  GLN A  20      24.974  34.199  61.619  1.00  0.59           C  
ATOM     19  OE1 GLN A  20      24.009  33.825  62.270  1.00  0.59           O  
ATOM     20  NE2 GLN A  20      25.500  33.444  60.624  1.00  0.59           N  
ATOM     21  N   GLY A  21      23.637  39.534  60.042  1.00  0.56           N  
ATOM     22  CA  GLY A  21      22.606  40.155  59.213  1.00  0.56           C  
ATOM     23  C   GLY A  21      23.082  40.768  57.915  1.00  0.56           C  
ATOM     24  O   GLY A  21      22.412  40.719  56.895  1.00  0.56           O  
ATOM     25  N   CYS A  22      24.282  41.379  57.916  1.00  0.55           N  
ATOM     26  CA  CYS A  22      24.803  42.077  56.753  1.00  0.55           C  
ATOM     27  C   CYS A  22      25.483  41.116  55.775  1.00  0.55           C  
ATOM     28  O   CYS A  22      25.478  41.311  54.561  1.00  0.55           O  
ATOM     29  CB  CYS A  22      25.798  43.204  57.182  1.00  0.55           C  
ATOM     30  SG  CYS A  22      25.404  44.018  58.777  1.00  0.55           S  
ATOM     31  N   GLU A  23      26.030  39.996  56.306  1.00  0.50           N  
ATOM     32  CA  GLU A  23      26.541  38.862  55.560  1.00  0.50           C  
ATOM     33  C   GLU A  23      25.408  38.048  54.934  1.00  0.50           C  
ATOM     34  O   GLU A  23      25.632  37.294  53.996  1.00  0.50           O  
ATOM     35  CB  GLU A  23      27.375  37.932  56.491  1.00  0.50           C  
ATOM     36  CG  GLU A  23      28.645  38.593  57.078  1.00  0.50           C  
ATOM     37  CD  GLU A  23      29.496  37.690  57.965  1.00  0.50           C  
ATOM     38  OE1 GLU A  23      29.094  36.543  58.265  1.00  0.50           O  
ATOM     39  OE2 GLU A  23      30.582  38.207  58.332  1.00  0.50           O  
ATOM     40  N   ALA A  24      24.144  38.216  55.404  1.00  0.61           N  
ATOM     41  CA  ALA A  24      22.979  37.553  54.849  1.00  0.61           C  
ATOM     42  C   ALA A  24      22.605  38.056  53.466  1.00  0.61           C  
ATOM     43  O   ALA A  24      22.375  37.270  52.555  1.00  0.61           O  
ATOM     44  CB  ALA A  24      21.750  37.755  55.763  1.00  0.61           C  
ATOM     45  N   ARG A  25      22.600  39.399  53.260  1.00  0.57           N  
ATOM     46  CA  ARG A  25      22.346  40.053  51.982  1.00  0.57           C  
ATOM     47  C   ARG A  25      23.304  39.579  50.904  1.00  0.57           C  
ATOM     48  O   ARG A  25      22.905  39.316  49.773  1.00  0.57           O  
ATOM     49  CB  ARG A  25      22.422  41.600  52.135  1.00  0.57           C  
ATOM     50  CG  ARG A  25      22.629  42.361  50.806  1.00  0.57           C  
ATOM     51  CD  ARG A  25      22.108  43.798  50.775  1.00  0.57           C  
ATOM     52  NE  ARG A  25      21.017  43.828  49.741  1.00  0.57           N  
ATOM     53  CZ  ARG A  25      20.468  44.959  49.292  1.00  0.57           C  
ATOM     54  NH1 ARG A  25      20.816  46.119  49.830  1.00  0.57           N  
ATOM     55  NH2 ARG A  25      19.610  44.942  48.275  1.00  0.57           N  
ATOM     56  N   ARG A  26      24.576  39.371  51.288  1.00  0.53           N  
ATOM     57  CA  ARG A  26      25.648  38.867  50.459  1.00  0.53           C  
ATOM     58  C   ARG A  26      25.333  37.546  49.753  1.00  0.53           C  
ATOM     59  O   ARG A  26      25.725  37.342  48.600  1.00  0.53           O  
ATOM     60  CB  ARG A  26      26.868  38.634  51.376  1.00  0.53           C  
ATOM     61  CG  ARG A  26      28.209  38.560  50.621  1.00  0.53           C  
ATOM     62  CD  ARG A  26      29.419  38.766  51.537  1.00  0.53           C  
ATOM     63  NE  ARG A  26      29.380  40.222  51.949  1.00  0.53           N  
ATOM     64  CZ  ARG A  26      29.618  40.664  53.193  1.00  0.53           C  
ATOM     65  NH1 ARG A  26      29.924  39.820  54.158  1.00  0.53           N  
ATOM     66  NH2 ARG A  26      29.540  41.960  53.493  1.00  0.53           N  
ATOM     67  N   SER A  27      24.603  36.641  50.458  1.00  0.63           N  
ATOM     68  CA  SER A  27      24.081  35.361  49.981  1.00  0.63           C  
ATOM     69  C   SER A  27      23.075  35.528  48.852  1.00  0.63           C  
ATOM     70  O   SER A  27      23.193  34.933  47.789  1.00  0.63           O  
ATOM     71  CB  SER A  27      23.357  34.580  51.122  1.00  0.63           C  
ATOM     72  OG  SER A  27      23.154  33.211  50.770  1.00  0.63           O  
ATOM     73  N   HIS A  28      22.079  36.435  49.021  1.00  0.65           N  
ATOM     74  CA  HIS A  28      21.099  36.711  47.976  1.00  0.65           C  
ATOM     75  C   HIS A  28      21.750  37.373  46.798  1.00  0.65           C  
ATOM     76  O   HIS A  28      21.547  36.984  45.658  1.00  0.65           O  
ATOM     77  CB  HIS A  28      19.938  37.615  48.436  1.00  0.65           C  
ATOM     78  CG  HIS A  28      19.268  37.056  49.633  1.00  0.65           C  
ATOM     79  ND1 HIS A  28      18.127  36.296  49.480  1.00  0.65           N  
ATOM     80  CD2 HIS A  28      19.602  37.145  50.935  1.00  0.65           C  
ATOM     81  CE1 HIS A  28      17.792  35.942  50.696  1.00  0.65           C  
ATOM     82  NE2 HIS A  28      18.654  36.427  51.632  1.00  0.65           N  
ATOM     83  N   GLU A  29      22.639  38.356  47.062  1.00  0.63           N  
ATOM     84  CA  GLU A  29      23.359  39.025  46.011  1.00  0.63           C  
ATOM     85  C   GLU A  29      24.210  38.084  45.162  1.00  0.63           C  
ATOM     86  O   GLU A  29      24.283  38.263  43.959  1.00  0.63           O  
ATOM     87  CB  GLU A  29      24.265  40.130  46.545  1.00  0.63           C  
ATOM     88  CG  GLU A  29      23.578  41.330  47.215  1.00  0.63           C  
ATOM     89  CD  GLU A  29      24.684  42.237  47.737  1.00  0.63           C  
ATOM     90  OE1 GLU A  29      25.885  41.848  47.604  1.00  0.63           O  
ATOM     91  OE2 GLU A  29      24.332  43.317  48.269  1.00  0.63           O  
ATOM     92  N   HIS A  30      24.833  37.029  45.754  1.00  0.66           N  
ATOM     93  CA  HIS A  30      25.548  35.980  45.028  1.00  0.66           C  
ATOM     94  C   HIS A  30      24.699  35.285  43.968  1.00  0.66           C  
ATOM     95  O   HIS A  30      25.125  35.182  42.818  1.00  0.66           O  
ATOM     96  CB  HIS A  30      26.086  34.912  46.026  1.00  0.66           C  
ATOM     97  CG  HIS A  30      26.636  33.677  45.382  1.00  0.66           C  
ATOM     98  ND1 HIS A  30      25.861  32.532  45.401  1.00  0.66           N  
ATOM     99  CD2 HIS A  30      27.756  33.470  44.657  1.00  0.66           C  
ATOM    100  CE1 HIS A  30      26.523  31.657  44.691  1.00  0.66           C  
ATOM    101  NE2 HIS A  30      27.690  32.165  44.208  1.00  0.66           N  
ATOM    102  N   LEU A  31      23.458  34.869  44.303  1.00  0.71           N  
ATOM    103  CA  LEU A  31      22.529  34.280  43.352  1.00  0.71           C  
ATOM    104  C   LEU A  31      22.147  35.237  42.232  1.00  0.71           C  
ATOM    105  O   LEU A  31      22.099  34.870  41.062  1.00  0.71           O  
ATOM    106  CB  LEU A  31      21.211  33.852  44.043  1.00  0.71           C  
ATOM    107  CG  LEU A  31      21.340  32.714  45.073  1.00  0.71           C  
ATOM    108  CD1 LEU A  31      19.969  32.522  45.746  1.00  0.71           C  
ATOM    109  CD2 LEU A  31      21.805  31.394  44.428  1.00  0.71           C  
ATOM    110  N   ILE A  32      21.886  36.519  42.580  1.00  0.72           N  
ATOM    111  CA  ILE A  32      21.558  37.564  41.616  1.00  0.72           C  
ATOM    112  C   ILE A  32      22.710  37.851  40.667  1.00  0.72           C  
ATOM    113  O   ILE A  32      22.517  37.926  39.456  1.00  0.72           O  
ATOM    114  CB  ILE A  32      21.135  38.871  42.290  1.00  0.72           C  
ATOM    115  CG1 ILE A  32      19.906  38.613  43.202  1.00  0.72           C  
ATOM    116  CG2 ILE A  32      20.816  39.949  41.213  1.00  0.72           C  
ATOM    117  CD1 ILE A  32      19.423  39.863  43.950  1.00  0.72           C  
ATOM    118  N   ARG A  33      23.954  37.971  41.187  1.00  0.63           N  
ATOM    119  CA  ARG A  33      25.152  38.201  40.395  1.00  0.63           C  
ATOM    120  C   ARG A  33      25.367  37.096  39.376  1.00  0.63           C  
ATOM    121  O   ARG A  33      25.545  37.363  38.195  1.00  0.63           O  
ATOM    122  CB  ARG A  33      26.414  38.281  41.306  1.00  0.63           C  
ATOM    123  CG  ARG A  33      26.552  39.604  42.094  1.00  0.63           C  
ATOM    124  CD  ARG A  33      27.909  39.780  42.805  1.00  0.63           C  
ATOM    125  NE  ARG A  33      27.930  38.973  44.088  1.00  0.63           N  
ATOM    126  CZ  ARG A  33      27.664  39.451  45.321  1.00  0.63           C  
ATOM    127  NH1 ARG A  33      27.666  38.630  46.369  1.00  0.63           N  
ATOM    128  NH2 ARG A  33      27.323  40.718  45.515  1.00  0.63           N  
ATOM    129  N   LEU A  34      25.221  35.828  39.818  1.00  0.70           N  
ATOM    130  CA  LEU A  34      25.308  34.647  38.988  1.00  0.70           C  
ATOM    131  C   LEU A  34      24.297  34.637  37.842  1.00  0.70           C  
ATOM    132  O   LEU A  34      24.636  34.291  36.708  1.00  0.70           O  
ATOM    133  CB  LEU A  34      25.070  33.403  39.890  1.00  0.70           C  
ATOM    134  CG  LEU A  34      24.957  32.048  39.154  1.00  0.70           C  
ATOM    135  CD1 LEU A  34      26.289  31.663  38.489  1.00  0.70           C  
ATOM    136  CD2 LEU A  34      24.451  30.951  40.105  1.00  0.70           C  
ATOM    137  N   LEU A  35      23.030  35.044  38.106  1.00  0.67           N  
ATOM    138  CA  LEU A  35      22.000  35.169  37.089  1.00  0.67           C  
ATOM    139  C   LEU A  35      22.362  36.190  36.025  1.00  0.67           C  
ATOM    140  O   LEU A  35      22.248  35.937  34.832  1.00  0.67           O  
ATOM    141  CB  LEU A  35      20.664  35.644  37.726  1.00  0.67           C  
ATOM    142  CG  LEU A  35      19.508  35.878  36.721  1.00  0.67           C  
ATOM    143  CD1 LEU A  35      19.095  34.571  36.021  1.00  0.67           C  
ATOM    144  CD2 LEU A  35      18.319  36.548  37.430  1.00  0.67           C  
ATOM    145  N   LEU A  36      22.826  37.383  36.444  1.00  0.71           N  
ATOM    146  CA  LEU A  36      23.215  38.444  35.542  1.00  0.71           C  
ATOM    147  C   LEU A  36      24.437  38.160  34.699  1.00  0.71           C  
ATOM    148  O   LEU A  36      24.462  38.486  33.518  1.00  0.71           O  
ATOM    149  CB  LEU A  36      23.446  39.752  36.329  1.00  0.71           C  
ATOM    150  CG  LEU A  36      22.170  40.285  37.018  1.00  0.71           C  
ATOM    151  CD1 LEU A  36      22.471  41.620  37.720  1.00  0.71           C  
ATOM    152  CD2 LEU A  36      20.982  40.444  36.044  1.00  0.71           C  
ATOM    153  N   GLU A  37      25.486  37.556  35.286  1.00  0.68           N  
ATOM    154  CA  GLU A  37      26.691  37.215  34.568  1.00  0.68           C  
ATOM    155  C   GLU A  37      26.506  36.120  33.542  1.00  0.68           C  
ATOM    156  O   GLU A  37      26.943  36.230  32.401  1.00  0.68           O  
ATOM    157  CB  GLU A  37      27.738  36.721  35.584  1.00  0.68           C  
ATOM    158  CG  GLU A  37      28.244  37.858  36.497  1.00  0.68           C  
ATOM    159  CD  GLU A  37      29.198  37.347  37.569  1.00  0.68           C  
ATOM    160  OE1 GLU A  37      28.902  36.286  38.182  1.00  0.68           O  
ATOM    161  OE2 GLU A  37      30.224  38.036  37.800  1.00  0.68           O  
ATOM    162  N   LYS A  38      25.846  35.009  33.928  1.00  0.64           N  
ATOM    163  CA  LYS A  38      25.803  33.868  33.043  1.00  0.64           C  
ATOM    164  C   LYS A  38      24.508  33.704  32.273  1.00  0.64           C  
ATOM    165  O   LYS A  38      24.491  32.937  31.316  1.00  0.64           O  
ATOM    166  CB  LYS A  38      25.979  32.565  33.840  1.00  0.64           C  
ATOM    167  CG  LYS A  38      27.390  32.347  34.389  1.00  0.64           C  
ATOM    168  CD  LYS A  38      27.491  30.978  35.078  1.00  0.64           C  
ATOM    169  CE  LYS A  38      28.880  30.738  35.675  1.00  0.64           C  
ATOM    170  NZ  LYS A  38      28.936  29.448  36.403  1.00  0.64           N  
ATOM    171  N   VAL A  39      23.403  34.379  32.680  1.00  0.63           N  
ATOM    172  CA  VAL A  39      22.117  34.441  31.976  1.00  0.63           C  
ATOM    173  C   VAL A  39      21.329  33.126  32.088  1.00  0.63           C  
ATOM    174  O   VAL A  39      20.211  32.969  31.607  1.00  0.63           O  
ATOM    175  CB  VAL A  39      22.248  35.020  30.549  1.00  0.63           C  
ATOM    176  CG1 VAL A  39      20.886  35.319  29.874  1.00  0.63           C  
ATOM    177  CG2 VAL A  39      23.063  36.337  30.611  1.00  0.63           C  
ATOM    178  N   HIS A  40      21.870  32.132  32.823  1.00  0.55           N  
ATOM    179  CA  HIS A  40      21.293  30.806  32.871  1.00  0.55           C  
ATOM    180  C   HIS A  40      20.256  30.755  33.960  1.00  0.55           C  
ATOM    181  O   HIS A  40      20.295  31.517  34.927  1.00  0.55           O  
ATOM    182  CB  HIS A  40      22.332  29.668  33.069  1.00  0.55           C  
ATOM    183  CG  HIS A  40      23.244  29.507  31.890  1.00  0.55           C  
ATOM    184  ND1 HIS A  40      22.734  29.123  30.662  1.00  0.55           N  
ATOM    185  CD2 HIS A  40      24.584  29.657  31.804  1.00  0.55           C  
ATOM    186  CE1 HIS A  40      23.776  29.061  29.862  1.00  0.55           C  
ATOM    187  NE2 HIS A  40      24.929  29.381  30.500  1.00  0.55           N  
ATOM    188  N   SER A  41      19.301  29.815  33.830  1.00  0.61           N  
ATOM    189  CA  SER A  41      18.402  29.417  34.902  1.00  0.61           C  
ATOM    190  C   SER A  41      19.222  28.892  36.073  1.00  0.61           C  
ATOM    191  O   SER A  41      20.307  28.332  35.867  1.00  0.61           O  
ATOM    192  CB  SER A  41      17.384  28.351  34.391  1.00  0.61           C  
ATOM    193  OG  SER A  41      16.253  28.167  35.232  1.00  0.61           O  
ATOM    194  N   TRP A  42      18.736  29.141  37.297  1.00  0.61           N  
ATOM    195  CA  TRP A  42      19.179  28.545  38.538  1.00  0.61           C  
ATOM    196  C   TRP A  42      18.829  27.025  38.587  1.00  0.61           C  
ATOM    197  O   TRP A  42      17.958  26.562  37.797  1.00  0.61           O  
ATOM    198  CB  TRP A  42      18.531  29.330  39.728  1.00  0.61           C  
ATOM    199  CG  TRP A  42      18.938  28.897  41.124  1.00  0.61           C  
ATOM    200  CD1 TRP A  42      20.008  29.272  41.886  1.00  0.61           C  
ATOM    201  CD2 TRP A  42      18.232  27.905  41.901  1.00  0.61           C  
ATOM    202  NE1 TRP A  42      20.011  28.600  43.102  1.00  0.61           N  
ATOM    203  CE2 TRP A  42      18.918  27.744  43.099  1.00  0.61           C  
ATOM    204  CE3 TRP A  42      17.097  27.152  41.597  1.00  0.61           C  
ATOM    205  CZ2 TRP A  42      18.490  26.820  44.059  1.00  0.61           C  
ATOM    206  CZ3 TRP A  42      16.657  26.224  42.554  1.00  0.61           C  
ATOM    207  CH2 TRP A  42      17.338  26.060  43.767  1.00  0.61           C  
ATOM    208  OXT TRP A  42      19.457  26.313  39.414  1.00  0.61           O  
TER     209      TRP A  42                                                      
END   
