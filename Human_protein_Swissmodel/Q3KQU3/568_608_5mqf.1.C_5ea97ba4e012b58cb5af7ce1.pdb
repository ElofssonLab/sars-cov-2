TITLE     SWISS-MODEL SERVER (https://swissmodel.expasy.org)
TITLE    2 Untitled Project
EXPDTA    THEORETICAL MODEL (SWISS-MODEL SERVER)
AUTHOR    SWISS-MODEL SERVER (SEE REFERENCE IN JRNL Records)
REVDAT   1   29-APR-20 1MOD    1       15:05
JRNL        AUTH   A.WATERHOUSE,M.BERTONI,S.BIENERT,G.STUDER,G.TAURIELLO,
JRNL        AUTH 2 R.GUMIENNY,F.T.HEER,T.A.P.DE BEER,C.REMPFER,L.BORDOLI,
JRNL        AUTH 3 R.LEPORE,T.SCHWEDE
JRNL        TITL   SWISS-MODEL: HOMOLOGY MODELLING OF PROTEIN STRUCTURES AND
JRNL        TITL 2 COMPLEXES
JRNL        REF    NUCLEIC.ACIDS.RES..           V.  46 W296  2018
JRNL        PMID   29788355
JRNL        DOI    10.1093/nar/gky427
REMARK   1
REMARK   1 REFERENCE 1
REMARK   1  AUTH   S.BIENERT,A.WATERHOUSE,T.A.P.DE BEER,G.TAURIELLO,G.STUDER,
REMARK   1  AUTH 2 L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   THE SWISS-MODEL REPOSITORY - NEW FEATURES AND FUNCTIONALITY
REMARK   1  REF    NUCLEIC.ACIDS.RES..           V.  22       2017
REMARK   1  REFN                   ISSN 0305-1048
REMARK   1  PMID   27899672
REMARK   1  DOI    10.1093/nar/gkw1132
REMARK   1
REMARK   1 REFERENCE 2
REMARK   1  AUTH   N.GUEX,M.C.PEITSCH,T.SCHWEDE
REMARK   1  TITL   AUTOMATED COMPARATIVE PROTEIN STRUCTURE MODELING WITH
REMARK   1  TITL 2 SWISS-MODEL AND SWISS-PDBVIEWER: A HISTORICAL PERSPECTIVE
REMARK   1  REF    ELECTROPHORESIS               V.  30       2009
REMARK   1  REFN                   ISSN 0173-0835
REMARK   1  PMID   19517507
REMARK   1  DOI    10.1002/elps.200900140
REMARK   1
REMARK   1 REFERENCE 3
REMARK   1  AUTH   P.BENKERT,M.BIASINI,T.SCHWEDE
REMARK   1  TITL   TOWARD THE ESTIMATION OF THE ABSOLUTE QUALITY OF INDIVIDUAL
REMARK   1  TITL 2 PROTEIN STRUCTURE MODELS
REMARK   1  REF    BIOINFORMATICS                V.  27       2011
REMARK   1  REFN                   ISSN 1367-4803
REMARK   1  PMID   21134891
REMARK   1  DOI    10.1093/bioinformatics/btq662
REMARK   1
REMARK   1 REFERENCE 4
REMARK   1  AUTH   M.BERTONI,F.KIEFER,M.BIASINI,L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   MODELING PROTEIN QUATERNARY STRUCTURE OF HOMO- AND
REMARK   1  TITL 2 HETERO-OLIGOMERS BEYOND BINARY INTERACTIONS BY HOMOLOGY
REMARK   1  REF    SCI.REP.                      V.   7       2017
REMARK   1  REFN                   ISSN
REMARK   1  PMID   28874689
REMARK   1  DOI    10.1038/s41598-017-09654-8
REMARK   1
REMARK   1 DISCLAIMER
REMARK   1 The SWISS-MODEL SERVER produces theoretical models for proteins.
REMARK   1 The results of any theoretical modelling procedure is
REMARK   1 NON-EXPERIMENTAL and MUST be considered with care. These models may
REMARK   1 contain significant errors. This is especially true for automated
REMARK   1 modeling since there is no human intervention during model
REMARK   1 building. Please read the header section and the logfile carefully
REMARK   1 to know what templates and alignments were used during the model
REMARK   1 building process. All information by the SWISS-MODEL SERVER is
REMARK   1 provided "AS-IS", without any warranty, expressed or implied.
REMARK   2
REMARK   2 COPYRIGHT NOTICE
REMARK   2 This SWISS-MODEL protein model is copyright. It is produced by the
REMARK   2 SWISS-MODEL server, developed by the Computational Structural
REMARK   2 Biology Group at the SIB Swiss Institute of Bioinformatics at the
REMARK   2 Biozentrum, University of Basel (https://swissmodel.expasy.org). This
REMARK   2 model is licensed under the CC BY-SA 4.0 Creative Commons
REMARK   2 Attribution-ShareAlike 4.0 International License
REMARK   2 (https://creativecommons.org/licenses/by-sa/4.0/legalcode), i.e. you
REMARK   2 can copy and redistribute the model in any medium or format,
REMARK   2 transform and build upon the model for any purpose, even
REMARK   2 commercially, under the following terms:
REMARK   2 Attribution - You must give appropriate credit, provide a link to
REMARK   2 the license, and indicate if changes were made. You may do so in any
REMARK   2 reasonable manner, but not in any way that suggests the licensor
REMARK   2 endorses you or your use. When you publish, patent or distribute
REMARK   2 results that were fully or partially based on the model, please cite
REMARK   2 the corresponding papers mentioned under JRNL.
REMARK   2 ShareAlike - If you remix, transform, or build upon the material,
REMARK   2 you must distribute your contributions under the same license as the
REMARK   2 original.
REMARK   2 No additional restrictions - you may not apply legal terms or
REMARK   2 technological measures that legally restrict others from doing
REMARK   2 anything the license permits.
REMARK   2 Find a human-readable summary of (and not a substitute for) the
REMARK   2 CC BY-SA 4.0 license at this link:
REMARK   2 https://creativecommons.org/licenses/by-sa/4.0/
REMARK   3 
REMARK   3 MODEL INFORMATION
REMARK   3  ENGIN   PROMOD3
REMARK   3  VERSN   3.0.0
REMARK   3  OSTAT   monomer
REMARK   3  OSRSN   PREDICTION
REMARK   3  QSPRD   0.000
REMARK   3  QMN4    2.00
REMARK   3  MODT    FALSE
REMARK   3 
REMARK   3 TEMPLATE 1
REMARK   3  PDBID   5mqf
REMARK   3  CHAIN   C
REMARK   3  MMCIF   C
REMARK   3  PDBV    2020-04-24
REMARK   3  SMTLE   5mqf.1.C
REMARK   3  SMTLV   2020-04-29
REMARK   3  MTHD    ELECTRON MICROSCOPY 0.00 A
REMARK   3  FOUND   HHblits
REMARK   3  SIM     0.35
REMARK   3  SID     34.15
REMARK   3  OSTAT   monomer
REMARK   3  ALN C TRG MESGPRAELGAGAPPAVVARTPPEPRPSPEGDPSPPPPPMSALVPDTPPDTPPAMKNA
REMARK   3  ALN C TRG TSSKQLPLEPESPSGQVGPRPAPPQEESPSSEAKSRGPTPPAMGPRDARPPRRSSQPS
REMARK   3  ALN C TRG PTAVPASDSPPTKQEVKKAGERHKLAKERREERAKYLAAKKAVWLEKEEKAKALREKQ
REMARK   3  ALN C TRG LQERRRRLEEQRLKAEQRRAALEERQRQKLEKNKERYEAAIQRSVKKTWAEIRQQRWS
REMARK   3  ALN C TRG WAGALHHSSPGHKTSGSRCSVSAVNLPKHVDSIINKRLSKSSATLWNSPSRNRSLQLS
REMARK   3  ALN C TRG AWESSIVDRLMTPTLSFLARSRSAVTLPRNGRDQAVPVCPRSASASPLTPCSVTRSVH
REMARK   3  ALN C TRG RCAPAGERGERRKPNAGGSPAPVRRRPEASPVQKKEKKDKERENEKEKSALARERSLK
REMARK   3  ALN C TRG KRQSLPASPRARLSASTASELSPKSKARPSSPSTSWHRPASPCPSPGPGHTLPPKPPS
REMARK   3  ALN C TRG PRGTTASPKGRVRRKEEAKESPSAAGPEDKSQSKRRASNEKESAAPASPAPSPAPSPT
REMARK   3  ALN C TRG PAPPQKEQPPAETPTDAAVLTSPPAPAPPVTPSKPMAGTTDREEATRLLAEKRRQARE
REMARK   3  ALN C TRG QREREEQERRLQAERDKRMREEQLAREAEARAEREAEARRREEQEAREKAQAEQEEQE
REMARK   3  ALN C TRG RLQKQKEEAEARSREEAERQRLEREKHFQQQEQERQERRKRLEEIMKRTRKSEVSETK
REMARK   3  ALN C TRG KQDSKEANANGSSPEPVKAVEARSPGLQKEAVQKEEPIPQEPQWSLPSKELPASLVNG
REMARK   3  ALN C TRG LQPLPAHQENGFSTNGPSGDKSLSRTPETLLPFAEAEAFLKKAVVQSPQVTEVL
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ---------------------------------------------AEALYIADRKARE
REMARK   3  ALN C TPL AVEMRAQVERKMAQKEKEKHEEKLREMA------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ----------------------------------------------------------
REMARK   3  ALN C TPL ------------------------------------------------------
REMARK   3  ALN C OFF 287
ATOM      1  N   THR C 568     257.970 294.459 235.533  1.00  0.55           N  
ATOM      2  CA  THR C 568     258.767 295.574 234.866  1.00  0.55           C  
ATOM      3  C   THR C 568     258.341 295.953 233.473  1.00  0.55           C  
ATOM      4  O   THR C 568     257.993 297.100 233.244  1.00  0.55           O  
ATOM      5  CB  THR C 568     260.269 295.332 234.946  1.00  0.55           C  
ATOM      6  OG1 THR C 568     260.589 295.081 236.307  1.00  0.55           O  
ATOM      7  CG2 THR C 568     261.076 296.566 234.510  1.00  0.55           C  
ATOM      8  N   ARG C 569     258.283 295.020 232.500  1.00  0.56           N  
ATOM      9  CA  ARG C 569     257.783 295.299 231.155  1.00  0.56           C  
ATOM     10  C   ARG C 569     256.356 295.832 231.134  1.00  0.56           C  
ATOM     11  O   ARG C 569     256.064 296.803 230.438  1.00  0.56           O  
ATOM     12  CB  ARG C 569     257.870 294.018 230.300  1.00  0.56           C  
ATOM     13  CG  ARG C 569     259.322 293.591 230.017  1.00  0.56           C  
ATOM     14  CD  ARG C 569     259.394 292.301 229.198  1.00  0.56           C  
ATOM     15  NE  ARG C 569     260.846 292.010 228.977  1.00  0.56           N  
ATOM     16  CZ  ARG C 569     261.304 290.875 228.429  1.00  0.56           C  
ATOM     17  NH1 ARG C 569     262.612 290.717 228.240  1.00  0.56           N  
ATOM     18  NH2 ARG C 569     260.483 289.899 228.057  1.00  0.56           N  
ATOM     19  N   LEU C 570     255.447 295.262 231.952  1.00  0.54           N  
ATOM     20  CA  LEU C 570     254.107 295.798 232.143  1.00  0.54           C  
ATOM     21  C   LEU C 570     254.132 297.226 232.645  1.00  0.54           C  
ATOM     22  O   LEU C 570     253.352 298.052 232.158  1.00  0.54           O  
ATOM     23  CB  LEU C 570     253.253 294.926 233.104  1.00  0.54           C  
ATOM     24  CG  LEU C 570     252.911 293.527 232.560  1.00  0.54           C  
ATOM     25  CD1 LEU C 570     252.226 292.685 233.647  1.00  0.54           C  
ATOM     26  CD2 LEU C 570     252.023 293.605 231.308  1.00  0.54           C  
ATOM     27  N   LEU C 571     255.007 297.581 233.599  1.00  0.55           N  
ATOM     28  CA  LEU C 571     255.187 298.946 234.061  1.00  0.55           C  
ATOM     29  C   LEU C 571     255.686 299.861 232.955  1.00  0.55           C  
ATOM     30  O   LEU C 571     255.203 300.984 232.840  1.00  0.55           O  
ATOM     31  CB  LEU C 571     256.107 299.047 235.308  1.00  0.55           C  
ATOM     32  CG  LEU C 571     255.557 298.359 236.575  1.00  0.55           C  
ATOM     33  CD1 LEU C 571     256.629 298.355 237.676  1.00  0.55           C  
ATOM     34  CD2 LEU C 571     254.284 299.050 237.089  1.00  0.55           C  
ATOM     35  N   ALA C 572     256.643 299.443 232.109  1.00  0.66           N  
ATOM     36  CA  ALA C 572     257.119 300.233 230.991  1.00  0.66           C  
ATOM     37  C   ALA C 572     256.065 300.505 229.914  1.00  0.66           C  
ATOM     38  O   ALA C 572     255.847 301.653 229.531  1.00  0.66           O  
ATOM     39  CB  ALA C 572     258.348 299.543 230.363  1.00  0.66           C  
ATOM     40  N   GLU C 573     255.347 299.456 229.456  1.00  0.67           N  
ATOM     41  CA  GLU C 573     254.262 299.584 228.497  1.00  0.67           C  
ATOM     42  C   GLU C 573     253.075 300.372 229.054  1.00  0.67           C  
ATOM     43  O   GLU C 573     252.575 301.303 228.419  1.00  0.67           O  
ATOM     44  CB  GLU C 573     253.839 298.192 227.959  1.00  0.67           C  
ATOM     45  CG  GLU C 573     252.766 298.259 226.837  1.00  0.67           C  
ATOM     46  CD  GLU C 573     253.120 299.113 225.604  1.00  0.67           C  
ATOM     47  OE1 GLU C 573     252.163 299.509 224.879  1.00  0.67           O  
ATOM     48  OE2 GLU C 573     254.311 299.429 225.362  1.00  0.67           O  
ATOM     49  N   LYS C 574     252.634 300.091 230.304  1.00  0.65           N  
ATOM     50  CA  LYS C 574     251.633 300.888 231.006  1.00  0.65           C  
ATOM     51  C   LYS C 574     252.074 302.324 231.230  1.00  0.65           C  
ATOM     52  O   LYS C 574     251.264 303.231 231.109  1.00  0.65           O  
ATOM     53  CB  LYS C 574     251.162 300.270 232.352  1.00  0.65           C  
ATOM     54  CG  LYS C 574     250.285 299.020 232.167  1.00  0.65           C  
ATOM     55  CD  LYS C 574     249.923 298.346 233.504  1.00  0.65           C  
ATOM     56  CE  LYS C 574     248.969 297.155 233.348  1.00  0.65           C  
ATOM     57  NZ  LYS C 574     248.663 296.555 234.669  1.00  0.65           N  
ATOM     58  N   ARG C 575     253.354 302.597 231.547  1.00  0.62           N  
ATOM     59  CA  ARG C 575     253.882 303.947 231.647  1.00  0.62           C  
ATOM     60  C   ARG C 575     253.867 304.707 230.329  1.00  0.62           C  
ATOM     61  O   ARG C 575     253.506 305.883 230.297  1.00  0.62           O  
ATOM     62  CB  ARG C 575     255.314 303.936 232.230  1.00  0.62           C  
ATOM     63  CG  ARG C 575     255.911 305.319 232.532  1.00  0.62           C  
ATOM     64  CD  ARG C 575     257.244 305.200 233.266  1.00  0.62           C  
ATOM     65  NE  ARG C 575     257.716 306.600 233.505  1.00  0.62           N  
ATOM     66  CZ  ARG C 575     258.880 306.915 234.090  1.00  0.62           C  
ATOM     67  NH1 ARG C 575     259.211 308.196 234.245  1.00  0.62           N  
ATOM     68  NH2 ARG C 575     259.718 305.981 234.526  1.00  0.62           N  
ATOM     69  N   ARG C 576     254.241 304.059 229.205  1.00  0.69           N  
ATOM     70  CA  ARG C 576     254.135 304.642 227.879  1.00  0.69           C  
ATOM     71  C   ARG C 576     252.695 304.930 227.492  1.00  0.69           C  
ATOM     72  O   ARG C 576     252.367 306.057 227.112  1.00  0.69           O  
ATOM     73  CB  ARG C 576     254.783 303.719 226.819  1.00  0.69           C  
ATOM     74  CG  ARG C 576     254.847 304.359 225.419  1.00  0.69           C  
ATOM     75  CD  ARG C 576     255.536 303.492 224.360  1.00  0.69           C  
ATOM     76  NE  ARG C 576     254.565 302.441 223.925  1.00  0.69           N  
ATOM     77  CZ  ARG C 576     253.623 302.631 222.993  1.00  0.69           C  
ATOM     78  NH1 ARG C 576     252.776 301.639 222.728  1.00  0.69           N  
ATOM     79  NH2 ARG C 576     253.479 303.766 222.308  1.00  0.69           N  
ATOM     80  N   GLN C 577     251.786 303.945 227.666  1.00  0.73           N  
ATOM     81  CA  GLN C 577     250.365 304.116 227.442  1.00  0.73           C  
ATOM     82  C   GLN C 577     249.810 305.205 228.342  1.00  0.73           C  
ATOM     83  O   GLN C 577     249.094 306.073 227.867  1.00  0.73           O  
ATOM     84  CB  GLN C 577     249.580 302.786 227.591  1.00  0.73           C  
ATOM     85  CG  GLN C 577     249.913 301.784 226.462  1.00  0.73           C  
ATOM     86  CD  GLN C 577     249.109 300.491 226.637  1.00  0.73           C  
ATOM     87  OE1 GLN C 577     248.041 300.461 227.227  1.00  0.73           O  
ATOM     88  NE2 GLN C 577     249.652 299.371 226.098  1.00  0.73           N  
ATOM     89  N   ALA C 578     250.138 305.240 229.648  1.00  0.78           N  
ATOM     90  CA  ALA C 578     249.758 306.300 230.558  1.00  0.78           C  
ATOM     91  C   ALA C 578     250.274 307.678 230.139  1.00  0.78           C  
ATOM     92  O   ALA C 578     249.524 308.649 230.169  1.00  0.78           O  
ATOM     93  CB  ALA C 578     250.203 305.967 232.004  1.00  0.78           C  
ATOM     94  N   ARG C 579     251.545 307.820 229.711  1.00  0.71           N  
ATOM     95  CA  ARG C 579     252.095 309.080 229.238  1.00  0.71           C  
ATOM     96  C   ARG C 579     251.476 309.598 227.942  1.00  0.71           C  
ATOM     97  O   ARG C 579     251.158 310.785 227.838  1.00  0.71           O  
ATOM     98  CB  ARG C 579     253.630 308.995 229.035  1.00  0.71           C  
ATOM     99  CG  ARG C 579     254.295 310.315 228.570  1.00  0.71           C  
ATOM    100  CD  ARG C 579     254.172 311.485 229.557  1.00  0.71           C  
ATOM    101  NE  ARG C 579     254.799 312.697 228.915  1.00  0.71           N  
ATOM    102  CZ  ARG C 579     254.165 313.605 228.151  1.00  0.71           C  
ATOM    103  NH1 ARG C 579     254.845 314.627 227.634  1.00  0.71           N  
ATOM    104  NH2 ARG C 579     252.875 313.551 227.840  1.00  0.71           N  
ATOM    105  N   GLU C 580     251.299 308.712 226.935  1.00  0.76           N  
ATOM    106  CA  GLU C 580     250.590 309.012 225.704  1.00  0.76           C  
ATOM    107  C   GLU C 580     249.118 309.300 225.987  1.00  0.76           C  
ATOM    108  O   GLU C 580     248.582 310.305 225.519  1.00  0.76           O  
ATOM    109  CB  GLU C 580     250.765 307.889 224.638  1.00  0.76           C  
ATOM    110  CG  GLU C 580     252.211 307.780 224.085  1.00  0.76           C  
ATOM    111  CD  GLU C 580     252.470 306.576 223.169  1.00  0.76           C  
ATOM    112  OE1 GLU C 580     251.517 305.868 222.744  1.00  0.76           O  
ATOM    113  OE2 GLU C 580     253.676 306.315 222.910  1.00  0.76           O  
ATOM    114  N   GLN C 581     248.410 308.503 226.809  1.00  0.76           N  
ATOM    115  CA  GLN C 581     247.039 308.750 227.238  1.00  0.76           C  
ATOM    116  C   GLN C 581     246.856 310.056 228.012  1.00  0.76           C  
ATOM    117  O   GLN C 581     245.883 310.777 227.814  1.00  0.76           O  
ATOM    118  CB  GLN C 581     246.450 307.520 227.983  1.00  0.76           C  
ATOM    119  CG  GLN C 581     244.925 307.543 228.232  1.00  0.76           C  
ATOM    120  CD  GLN C 581     244.138 307.551 226.920  1.00  0.76           C  
ATOM    121  OE1 GLN C 581     244.405 306.814 225.970  1.00  0.76           O  
ATOM    122  NE2 GLN C 581     243.119 308.442 226.852  1.00  0.76           N  
ATOM    123  N   ARG C 582     247.808 310.429 228.880  1.00  0.71           N  
ATOM    124  CA  ARG C 582     247.865 311.728 229.523  1.00  0.71           C  
ATOM    125  C   ARG C 582     248.041 312.901 228.549  1.00  0.71           C  
ATOM    126  O   ARG C 582     247.422 313.957 228.707  1.00  0.71           O  
ATOM    127  CB  ARG C 582     248.989 311.718 230.587  1.00  0.71           C  
ATOM    128  CG  ARG C 582     249.057 312.989 231.446  1.00  0.71           C  
ATOM    129  CD  ARG C 582     247.790 313.239 232.272  1.00  0.71           C  
ATOM    130  NE  ARG C 582     248.001 314.540 232.976  1.00  0.71           N  
ATOM    131  CZ  ARG C 582     247.738 315.731 232.421  1.00  0.71           C  
ATOM    132  NH1 ARG C 582     247.954 316.835 233.133  1.00  0.71           N  
ATOM    133  NH2 ARG C 582     247.273 315.859 231.182  1.00  0.71           N  
ATOM    134  N   GLU C 583     248.878 312.738 227.502  1.00  0.77           N  
ATOM    135  CA  GLU C 583     248.979 313.644 226.359  1.00  0.77           C  
ATOM    136  C   GLU C 583     247.695 313.710 225.534  1.00  0.77           C  
ATOM    137  O   GLU C 583     247.269 314.777 225.091  1.00  0.77           O  
ATOM    138  CB  GLU C 583     250.178 313.273 225.447  1.00  0.77           C  
ATOM    139  CG  GLU C 583     250.568 314.349 224.400  1.00  0.77           C  
ATOM    140  CD  GLU C 583     251.143 315.605 225.064  1.00  0.77           C  
ATOM    141  OE1 GLU C 583     250.801 316.734 224.610  1.00  0.77           O  
ATOM    142  OE2 GLU C 583     251.952 315.427 226.026  1.00  0.77           O  
ATOM    143  N   ARG C 584     246.991 312.584 225.323  1.00  0.73           N  
ATOM    144  CA  ARG C 584     245.680 312.538 224.690  1.00  0.73           C  
ATOM    145  C   ARG C 584     244.604 313.352 225.407  1.00  0.73           C  
ATOM    146  O   ARG C 584     243.790 314.001 224.776  1.00  0.73           O  
ATOM    147  CB  ARG C 584     245.115 311.109 224.583  1.00  0.73           C  
ATOM    148  CG  ARG C 584     245.818 310.194 223.576  1.00  0.73           C  
ATOM    149  CD  ARG C 584     245.293 308.768 223.690  1.00  0.73           C  
ATOM    150  NE  ARG C 584     245.779 308.022 222.507  1.00  0.73           N  
ATOM    151  CZ  ARG C 584     245.569 306.710 222.342  1.00  0.73           C  
ATOM    152  NH1 ARG C 584     245.974 306.145 221.208  1.00  0.73           N  
ATOM    153  NH2 ARG C 584     244.993 305.961 223.277  1.00  0.73           N  
ATOM    154  N   GLU C 585     244.595 313.350 226.749  1.00  0.77           N  
ATOM    155  CA  GLU C 585     243.775 314.220 227.574  1.00  0.77           C  
ATOM    156  C   GLU C 585     244.115 315.693 227.396  1.00  0.77           C  
ATOM    157  O   GLU C 585     243.241 316.559 227.291  1.00  0.77           O  
ATOM    158  CB  GLU C 585     243.984 313.851 229.049  1.00  0.77           C  
ATOM    159  CG  GLU C 585     243.143 314.697 230.029  1.00  0.77           C  
ATOM    160  CD  GLU C 585     243.555 314.455 231.478  1.00  0.77           C  
ATOM    161  OE1 GLU C 585     242.893 315.043 232.367  1.00  0.77           O  
ATOM    162  OE2 GLU C 585     244.571 313.743 231.699  1.00  0.77           O  
ATOM    163  N   GLU C 586     245.417 316.015 227.316  1.00  0.78           N  
ATOM    164  CA  GLU C 586     245.899 317.345 227.018  1.00  0.78           C  
ATOM    165  C   GLU C 586     245.475 317.858 225.633  1.00  0.78           C  
ATOM    166  O   GLU C 586     245.021 318.986 225.489  1.00  0.78           O  
ATOM    167  CB  GLU C 586     247.427 317.394 227.197  1.00  0.78           C  
ATOM    168  CG  GLU C 586     248.059 318.770 226.908  1.00  0.78           C  
ATOM    169  CD  GLU C 586     247.472 320.019 227.548  1.00  0.78           C  
ATOM    170  OE1 GLU C 586     247.758 321.090 226.936  1.00  0.78           O  
ATOM    171  OE2 GLU C 586     246.750 319.987 228.571  1.00  0.78           O  
ATOM    172  N   GLN C 587     245.548 317.012 224.577  1.00  0.80           N  
ATOM    173  CA  GLN C 587     245.026 317.311 223.244  1.00  0.80           C  
ATOM    174  C   GLN C 587     243.532 317.600 223.224  1.00  0.80           C  
ATOM    175  O   GLN C 587     243.102 318.584 222.619  1.00  0.80           O  
ATOM    176  CB  GLN C 587     245.263 316.143 222.250  1.00  0.80           C  
ATOM    177  CG  GLN C 587     246.741 315.809 221.960  1.00  0.80           C  
ATOM    178  CD  GLN C 587     247.441 316.967 221.259  1.00  0.80           C  
ATOM    179  OE1 GLN C 587     246.927 317.604 220.343  1.00  0.80           O  
ATOM    180  NE2 GLN C 587     248.692 317.259 221.687  1.00  0.80           N  
ATOM    181  N   GLU C 588     242.717 316.793 223.935  1.00  0.80           N  
ATOM    182  CA  GLU C 588     241.289 317.025 224.110  1.00  0.80           C  
ATOM    183  C   GLU C 588     241.018 318.377 224.764  1.00  0.80           C  
ATOM    184  O   GLU C 588     240.258 319.201 224.257  1.00  0.80           O  
ATOM    185  CB  GLU C 588     240.659 315.883 224.955  1.00  0.80           C  
ATOM    186  CG  GLU C 588     239.169 316.089 225.334  1.00  0.80           C  
ATOM    187  CD  GLU C 588     238.187 316.175 224.164  1.00  0.80           C  
ATOM    188  OE1 GLU C 588     237.113 316.801 224.394  1.00  0.80           O  
ATOM    189  OE2 GLU C 588     238.476 315.644 223.064  1.00  0.80           O  
ATOM    190  N   ARG C 589     241.725 318.704 225.860  1.00  0.75           N  
ATOM    191  CA  ARG C 589     241.608 319.979 226.544  1.00  0.75           C  
ATOM    192  C   ARG C 589     241.931 321.206 225.678  1.00  0.75           C  
ATOM    193  O   ARG C 589     241.268 322.241 225.747  1.00  0.75           O  
ATOM    194  CB  ARG C 589     242.546 319.985 227.765  1.00  0.75           C  
ATOM    195  CG  ARG C 589     242.328 321.191 228.694  1.00  0.75           C  
ATOM    196  CD  ARG C 589     243.414 321.341 229.764  1.00  0.75           C  
ATOM    197  NE  ARG C 589     244.719 321.660 229.084  1.00  0.75           N  
ATOM    198  CZ  ARG C 589     245.089 322.862 228.622  1.00  0.75           C  
ATOM    199  NH1 ARG C 589     246.261 322.999 228.007  1.00  0.75           N  
ATOM    200  NH2 ARG C 589     244.313 323.928 228.754  1.00  0.75           N  
ATOM    201  N   ARG C 590     242.967 321.112 224.822  1.00  0.76           N  
ATOM    202  CA  ARG C 590     243.296 322.103 223.808  1.00  0.76           C  
ATOM    203  C   ARG C 590     242.206 322.281 222.747  1.00  0.76           C  
ATOM    204  O   ARG C 590     241.888 323.403 222.354  1.00  0.76           O  
ATOM    205  CB  ARG C 590     244.609 321.736 223.076  1.00  0.76           C  
ATOM    206  CG  ARG C 590     245.883 321.741 223.946  1.00  0.76           C  
ATOM    207  CD  ARG C 590     247.103 321.277 223.149  1.00  0.76           C  
ATOM    208  NE  ARG C 590     248.207 321.001 224.110  1.00  0.76           N  
ATOM    209  CZ  ARG C 590     249.331 320.365 223.748  1.00  0.76           C  
ATOM    210  NH1 ARG C 590     250.148 319.880 224.684  1.00  0.76           N  
ATOM    211  NH2 ARG C 590     249.642 320.129 222.480  1.00  0.76           N  
ATOM    212  N   LEU C 591     241.593 321.177 222.269  1.00  0.82           N  
ATOM    213  CA  LEU C 591     240.457 321.197 221.360  1.00  0.82           C  
ATOM    214  C   LEU C 591     239.221 321.857 221.953  1.00  0.82           C  
ATOM    215  O   LEU C 591     238.540 322.618 221.274  1.00  0.82           O  
ATOM    216  CB  LEU C 591     240.021 319.779 220.924  1.00  0.82           C  
ATOM    217  CG  LEU C 591     241.007 318.991 220.046  1.00  0.82           C  
ATOM    218  CD1 LEU C 591     240.502 317.543 219.946  1.00  0.82           C  
ATOM    219  CD2 LEU C 591     241.186 319.621 218.657  1.00  0.82           C  
ATOM    220  N   GLN C 592     238.903 321.589 223.236  1.00  0.82           N  
ATOM    221  CA  GLN C 592     237.821 322.250 223.955  1.00  0.82           C  
ATOM    222  C   GLN C 592     238.030 323.760 224.056  1.00  0.82           C  
ATOM    223  O   GLN C 592     237.147 324.542 223.696  1.00  0.82           O  
ATOM    224  CB  GLN C 592     237.663 321.662 225.382  1.00  0.82           C  
ATOM    225  CG  GLN C 592     237.321 320.155 225.433  1.00  0.82           C  
ATOM    226  CD  GLN C 592     237.358 319.636 226.873  1.00  0.82           C  
ATOM    227  OE1 GLN C 592     237.597 320.354 227.837  1.00  0.82           O  
ATOM    228  NE2 GLN C 592     237.133 318.308 227.016  1.00  0.82           N  
ATOM    229  N   ALA C 593     239.244 324.204 224.434  1.00  0.88           N  
ATOM    230  CA  ALA C 593     239.625 325.602 224.546  1.00  0.88           C  
ATOM    231  C   ALA C 593     239.532 326.399 223.245  1.00  0.88           C  
ATOM    232  O   ALA C 593     239.106 327.552 223.213  1.00  0.88           O  
ATOM    233  CB  ALA C 593     241.078 325.681 225.054  1.00  0.88           C  
ATOM    234  N   GLU C 594     239.936 325.775 222.125  1.00  0.82           N  
ATOM    235  CA  GLU C 594     239.746 326.281 220.781  1.00  0.82           C  
ATOM    236  C   GLU C 594     238.269 326.421 220.405  1.00  0.82           C  
ATOM    237  O   GLU C 594     237.842 327.439 219.858  1.00  0.82           O  
ATOM    238  CB  GLU C 594     240.469 325.322 219.814  1.00  0.82           C  
ATOM    239  CG  GLU C 594     240.227 325.596 218.316  1.00  0.82           C  
ATOM    240  CD  GLU C 594     240.684 326.936 217.717  1.00  0.82           C  
ATOM    241  OE1 GLU C 594     240.204 327.151 216.564  1.00  0.82           O  
ATOM    242  OE2 GLU C 594     241.462 327.699 218.329  1.00  0.82           O  
ATOM    243  N   ARG C 595     237.413 325.427 220.729  1.00  0.77           N  
ATOM    244  CA  ARG C 595     235.973 325.507 220.513  1.00  0.77           C  
ATOM    245  C   ARG C 595     235.311 326.664 221.263  1.00  0.77           C  
ATOM    246  O   ARG C 595     234.535 327.414 220.673  1.00  0.77           O  
ATOM    247  CB  ARG C 595     235.250 324.197 220.914  1.00  0.77           C  
ATOM    248  CG  ARG C 595     235.545 322.969 220.032  1.00  0.77           C  
ATOM    249  CD  ARG C 595     234.885 321.715 220.613  1.00  0.77           C  
ATOM    250  NE  ARG C 595     235.199 320.580 219.687  1.00  0.77           N  
ATOM    251  CZ  ARG C 595     234.908 319.297 219.943  1.00  0.77           C  
ATOM    252  NH1 ARG C 595     235.279 318.348 219.085  1.00  0.77           N  
ATOM    253  NH2 ARG C 595     234.286 318.927 221.057  1.00  0.77           N  
ATOM    254  N   ASP C 596     235.658 326.866 222.548  1.00  0.82           N  
ATOM    255  CA  ASP C 596     235.218 327.975 223.378  1.00  0.82           C  
ATOM    256  C   ASP C 596     235.632 329.338 222.820  1.00  0.82           C  
ATOM    257  O   ASP C 596     234.868 330.305 222.810  1.00  0.82           O  
ATOM    258  CB  ASP C 596     235.832 327.838 224.793  1.00  0.82           C  
ATOM    259  CG  ASP C 596     235.306 326.636 225.563  1.00  0.82           C  
ATOM    260  OD1 ASP C 596     234.294 326.027 225.137  1.00  0.82           O  
ATOM    261  OD2 ASP C 596     235.928 326.340 226.616  1.00  0.82           O  
ATOM    262  N   LYS C 597     236.874 329.443 222.301  1.00  0.83           N  
ATOM    263  CA  LYS C 597     237.353 330.631 221.623  1.00  0.83           C  
ATOM    264  C   LYS C 597     236.555 330.988 220.375  1.00  0.83           C  
ATOM    265  O   LYS C 597     236.150 332.142 220.221  1.00  0.83           O  
ATOM    266  CB  LYS C 597     238.842 330.502 221.242  1.00  0.83           C  
ATOM    267  CG  LYS C 597     239.406 331.776 220.598  1.00  0.83           C  
ATOM    268  CD  LYS C 597     240.866 331.603 220.177  1.00  0.83           C  
ATOM    269  CE  LYS C 597     241.403 332.828 219.445  1.00  0.83           C  
ATOM    270  NZ  LYS C 597     242.797 332.581 219.046  1.00  0.83           N  
ATOM    271  N   ARG C 598     236.256 330.000 219.505  1.00  0.78           N  
ATOM    272  CA  ARG C 598     235.413 330.164 218.328  1.00  0.78           C  
ATOM    273  C   ARG C 598     234.000 330.624 218.682  1.00  0.78           C  
ATOM    274  O   ARG C 598     233.466 331.562 218.084  1.00  0.78           O  
ATOM    275  CB  ARG C 598     235.308 328.829 217.549  1.00  0.78           C  
ATOM    276  CG  ARG C 598     236.621 328.326 216.920  1.00  0.78           C  
ATOM    277  CD  ARG C 598     236.396 326.980 216.236  1.00  0.78           C  
ATOM    278  NE  ARG C 598     237.722 326.388 215.930  1.00  0.78           N  
ATOM    279  CZ  ARG C 598     237.929 325.115 215.574  1.00  0.78           C  
ATOM    280  NH1 ARG C 598     239.184 324.713 215.416  1.00  0.78           N  
ATOM    281  NH2 ARG C 598     236.925 324.272 215.380  1.00  0.78           N  
ATOM    282  N   MET C 599     233.373 330.035 219.718  1.00  0.80           N  
ATOM    283  CA  MET C 599     232.061 330.435 220.209  1.00  0.80           C  
ATOM    284  C   MET C 599     232.001 331.883 220.696  1.00  0.80           C  
ATOM    285  O   MET C 599     231.035 332.608 220.469  1.00  0.80           O  
ATOM    286  CB  MET C 599     231.623 329.571 221.411  1.00  0.80           C  
ATOM    287  CG  MET C 599     231.325 328.096 221.101  1.00  0.80           C  
ATOM    288  SD  MET C 599     230.983 327.123 222.602  1.00  0.80           S  
ATOM    289  CE  MET C 599     229.433 327.947 223.078  1.00  0.80           C  
ATOM    290  N   ARG C 600     233.060 332.329 221.401  1.00  0.76           N  
ATOM    291  CA  ARG C 600     233.247 333.706 221.815  1.00  0.76           C  
ATOM    292  C   ARG C 600     233.391 334.690 220.648  1.00  0.76           C  
ATOM    293  O   ARG C 600     232.826 335.785 220.674  1.00  0.76           O  
ATOM    294  CB  ARG C 600     234.495 333.827 222.721  1.00  0.76           C  
ATOM    295  CG  ARG C 600     234.692 335.244 223.303  1.00  0.76           C  
ATOM    296  CD  ARG C 600     236.031 335.470 224.010  1.00  0.76           C  
ATOM    297  NE  ARG C 600     237.125 335.316 222.983  1.00  0.76           N  
ATOM    298  CZ  ARG C 600     237.520 336.259 222.112  1.00  0.76           C  
ATOM    299  NH1 ARG C 600     238.455 335.966 221.205  1.00  0.76           N  
ATOM    300  NH2 ARG C 600     236.983 337.472 222.086  1.00  0.76           N  
ATOM    301  N   GLU C 601     234.147 334.328 219.592  1.00  0.81           N  
ATOM    302  CA  GLU C 601     234.282 335.094 218.360  1.00  0.81           C  
ATOM    303  C   GLU C 601     232.969 335.242 217.612  1.00  0.81           C  
ATOM    304  O   GLU C 601     232.594 336.329 217.173  1.00  0.81           O  
ATOM    305  CB  GLU C 601     235.319 334.438 217.420  1.00  0.81           C  
ATOM    306  CG  GLU C 601     236.766 334.509 217.962  1.00  0.81           C  
ATOM    307  CD  GLU C 601     237.797 333.770 217.107  1.00  0.81           C  
ATOM    308  OE1 GLU C 601     237.429 333.161 216.076  1.00  0.81           O  
ATOM    309  OE2 GLU C 601     238.986 333.831 217.524  1.00  0.81           O  
ATOM    310  N   GLU C 602     232.195 334.149 217.513  1.00  0.80           N  
ATOM    311  CA  GLU C 602     230.862 334.182 216.962  1.00  0.80           C  
ATOM    312  C   GLU C 602     229.884 335.070 217.718  1.00  0.80           C  
ATOM    313  O   GLU C 602     229.092 335.787 217.116  1.00  0.80           O  
ATOM    314  CB  GLU C 602     230.219 332.802 216.943  1.00  0.80           C  
ATOM    315  CG  GLU C 602     230.830 331.752 215.999  1.00  0.80           C  
ATOM    316  CD  GLU C 602     229.845 330.582 215.913  1.00  0.80           C  
ATOM    317  OE1 GLU C 602     228.741 330.701 216.531  1.00  0.80           O  
ATOM    318  OE2 GLU C 602     230.178 329.582 215.235  1.00  0.80           O  
ATOM    319  N   GLN C 603     229.917 335.050 219.067  1.00  0.79           N  
ATOM    320  CA  GLN C 603     229.136 335.951 219.897  1.00  0.79           C  
ATOM    321  C   GLN C 603     229.463 337.410 219.653  1.00  0.79           C  
ATOM    322  O   GLN C 603     228.565 338.223 219.445  1.00  0.79           O  
ATOM    323  CB  GLN C 603     229.336 335.619 221.399  1.00  0.79           C  
ATOM    324  CG  GLN C 603     228.658 336.613 222.371  1.00  0.79           C  
ATOM    325  CD  GLN C 603     227.138 336.660 222.171  1.00  0.79           C  
ATOM    326  OE1 GLN C 603     226.524 335.773 221.594  1.00  0.79           O  
ATOM    327  NE2 GLN C 603     226.539 337.768 222.681  1.00  0.79           N  
ATOM    328  N   LEU C 604     230.758 337.752 219.586  1.00  0.74           N  
ATOM    329  CA  LEU C 604     231.218 339.096 219.299  1.00  0.74           C  
ATOM    330  C   LEU C 604     230.776 339.610 217.929  1.00  0.74           C  
ATOM    331  O   LEU C 604     230.385 340.764 217.764  1.00  0.74           O  
ATOM    332  CB  LEU C 604     232.760 339.123 219.372  1.00  0.74           C  
ATOM    333  CG  LEU C 604     233.379 340.515 219.139  1.00  0.74           C  
ATOM    334  CD1 LEU C 604     232.947 341.525 220.213  1.00  0.74           C  
ATOM    335  CD2 LEU C 604     234.904 340.416 219.014  1.00  0.74           C  
ATOM    336  N   ALA C 605     230.814 338.743 216.902  1.00  0.78           N  
ATOM    337  CA  ALA C 605     230.306 339.021 215.574  1.00  0.78           C  
ATOM    338  C   ALA C 605     228.794 339.250 215.504  1.00  0.78           C  
ATOM    339  O   ALA C 605     228.325 340.037 214.696  1.00  0.78           O  
ATOM    340  CB  ALA C 605     230.658 337.861 214.627  1.00  0.78           C  
ATOM    341  N   ARG C 606     228.001 338.539 216.329  1.00  0.67           N  
ATOM    342  CA  ARG C 606     226.571 338.746 216.507  1.00  0.67           C  
ATOM    343  C   ARG C 606     226.166 340.023 217.249  1.00  0.67           C  
ATOM    344  O   ARG C 606     225.076 340.527 217.040  1.00  0.67           O  
ATOM    345  CB  ARG C 606     225.914 337.610 217.326  1.00  0.67           C  
ATOM    346  CG  ARG C 606     225.838 336.239 216.634  1.00  0.67           C  
ATOM    347  CD  ARG C 606     224.968 335.240 217.414  1.00  0.67           C  
ATOM    348  NE  ARG C 606     225.758 334.669 218.563  1.00  0.67           N  
ATOM    349  CZ  ARG C 606     226.508 333.553 218.511  1.00  0.67           C  
ATOM    350  NH1 ARG C 606     227.137 333.157 219.613  1.00  0.67           N  
ATOM    351  NH2 ARG C 606     226.671 332.845 217.395  1.00  0.67           N  
ATOM    352  N   GLU C 607     226.998 340.502 218.193  1.00  0.72           N  
ATOM    353  CA  GLU C 607     226.855 341.757 218.915  1.00  0.72           C  
ATOM    354  C   GLU C 607     227.127 342.993 218.063  1.00  0.72           C  
ATOM    355  O   GLU C 607     226.553 344.048 218.301  1.00  0.72           O  
ATOM    356  CB  GLU C 607     227.826 341.807 220.118  1.00  0.72           C  
ATOM    357  CG  GLU C 607     227.455 340.822 221.251  1.00  0.72           C  
ATOM    358  CD  GLU C 607     228.481 340.730 222.384  1.00  0.72           C  
ATOM    359  OE1 GLU C 607     229.508 341.451 222.358  1.00  0.72           O  
ATOM    360  OE2 GLU C 607     228.226 339.887 223.288  1.00  0.72           O  
ATOM    361  N   ALA C 608     228.057 342.869 217.103  1.00  0.72           N  
ATOM    362  CA  ALA C 608     228.441 343.874 216.135  1.00  0.72           C  
ATOM    363  C   ALA C 608     227.446 344.173 214.964  1.00  0.72           C  
ATOM    364  O   ALA C 608     226.428 343.459 214.782  1.00  0.72           O  
ATOM    365  CB  ALA C 608     229.780 343.438 215.504  1.00  0.72           C  
ATOM    366  OXT ALA C 608     227.743 345.154 214.219  1.00  0.72           O  
TER     367      ALA C 608                                                      
END   
