TITLE     SWISS-MODEL SERVER (https://swissmodel.expasy.org)
TITLE    2 Untitled Project
EXPDTA    THEORETICAL MODEL (SWISS-MODEL SERVER)
AUTHOR    SWISS-MODEL SERVER (SEE REFERENCE IN JRNL Records)
REVDAT   1   01-MAY-20 1MOD    1       15:23
JRNL        AUTH   A.WATERHOUSE,M.BERTONI,S.BIENERT,G.STUDER,G.TAURIELLO,
JRNL        AUTH 2 R.GUMIENNY,F.T.HEER,T.A.P.DE BEER,C.REMPFER,L.BORDOLI,
JRNL        AUTH 3 R.LEPORE,T.SCHWEDE
JRNL        TITL   SWISS-MODEL: HOMOLOGY MODELLING OF PROTEIN STRUCTURES AND
JRNL        TITL 2 COMPLEXES
JRNL        REF    NUCLEIC.ACIDS.RES..           V.  46 W296  2018
JRNL        PMID   29788355
JRNL        DOI    10.1093/nar/gky427
REMARK   1
REMARK   1 REFERENCE 1
REMARK   1  AUTH   S.BIENERT,A.WATERHOUSE,T.A.P.DE BEER,G.TAURIELLO,G.STUDER,
REMARK   1  AUTH 2 L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   THE SWISS-MODEL REPOSITORY - NEW FEATURES AND FUNCTIONALITY
REMARK   1  REF    NUCLEIC.ACIDS.RES..           V.  22       2017
REMARK   1  REFN                   ISSN 0305-1048
REMARK   1  PMID   27899672
REMARK   1  DOI    10.1093/nar/gkw1132
REMARK   1
REMARK   1 REFERENCE 2
REMARK   1  AUTH   N.GUEX,M.C.PEITSCH,T.SCHWEDE
REMARK   1  TITL   AUTOMATED COMPARATIVE PROTEIN STRUCTURE MODELING WITH
REMARK   1  TITL 2 SWISS-MODEL AND SWISS-PDBVIEWER: A HISTORICAL PERSPECTIVE
REMARK   1  REF    ELECTROPHORESIS               V.  30       2009
REMARK   1  REFN                   ISSN 0173-0835
REMARK   1  PMID   19517507
REMARK   1  DOI    10.1002/elps.200900140
REMARK   1
REMARK   1 REFERENCE 3
REMARK   1  AUTH   P.BENKERT,M.BIASINI,T.SCHWEDE
REMARK   1  TITL   TOWARD THE ESTIMATION OF THE ABSOLUTE QUALITY OF INDIVIDUAL
REMARK   1  TITL 2 PROTEIN STRUCTURE MODELS
REMARK   1  REF    BIOINFORMATICS                V.  27       2011
REMARK   1  REFN                   ISSN 1367-4803
REMARK   1  PMID   21134891
REMARK   1  DOI    10.1093/bioinformatics/btq662
REMARK   1
REMARK   1 REFERENCE 4
REMARK   1  AUTH   M.BERTONI,F.KIEFER,M.BIASINI,L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   MODELING PROTEIN QUATERNARY STRUCTURE OF HOMO- AND
REMARK   1  TITL 2 HETERO-OLIGOMERS BEYOND BINARY INTERACTIONS BY HOMOLOGY
REMARK   1  REF    SCI.REP.                      V.   7       2017
REMARK   1  REFN                   ISSN
REMARK   1  PMID   28874689
REMARK   1  DOI    10.1038/s41598-017-09654-8
REMARK   1
REMARK   1 DISCLAIMER
REMARK   1 The SWISS-MODEL SERVER produces theoretical models for proteins.
REMARK   1 The results of any theoretical modelling procedure is
REMARK   1 NON-EXPERIMENTAL and MUST be considered with care. These models may
REMARK   1 contain significant errors. This is especially true for automated
REMARK   1 modeling since there is no human intervention during model
REMARK   1 building. Please read the header section and the logfile carefully
REMARK   1 to know what templates and alignments were used during the model
REMARK   1 building process. All information by the SWISS-MODEL SERVER is
REMARK   1 provided "AS-IS", without any warranty, expressed or implied.
REMARK   2
REMARK   2 COPYRIGHT NOTICE
REMARK   2 This SWISS-MODEL protein model is copyright. It is produced by the
REMARK   2 SWISS-MODEL server, developed by the Computational Structural
REMARK   2 Biology Group at the SIB Swiss Institute of Bioinformatics at the
REMARK   2 Biozentrum, University of Basel (https://swissmodel.expasy.org). This
REMARK   2 model is licensed under the CC BY-SA 4.0 Creative Commons
REMARK   2 Attribution-ShareAlike 4.0 International License
REMARK   2 (https://creativecommons.org/licenses/by-sa/4.0/legalcode), i.e. you
REMARK   2 can copy and redistribute the model in any medium or format,
REMARK   2 transform and build upon the model for any purpose, even
REMARK   2 commercially, under the following terms:
REMARK   2 Attribution - You must give appropriate credit, provide a link to
REMARK   2 the license, and indicate if changes were made. You may do so in any
REMARK   2 reasonable manner, but not in any way that suggests the licensor
REMARK   2 endorses you or your use. When you publish, patent or distribute
REMARK   2 results that were fully or partially based on the model, please cite
REMARK   2 the corresponding papers mentioned under JRNL.
REMARK   2 ShareAlike - If you remix, transform, or build upon the material,
REMARK   2 you must distribute your contributions under the same license as the
REMARK   2 original.
REMARK   2 No additional restrictions - you may not apply legal terms or
REMARK   2 technological measures that legally restrict others from doing
REMARK   2 anything the license permits.
REMARK   2 Find a human-readable summary of (and not a substitute for) the
REMARK   2 CC BY-SA 4.0 license at this link:
REMARK   2 https://creativecommons.org/licenses/by-sa/4.0/
REMARK   3 
REMARK   3 MODEL INFORMATION
REMARK   3  ENGIN   PROMOD3
REMARK   3  VERSN   3.0.0
REMARK   3  OSTAT   monomer
REMARK   3  OSRSN   PREDICTION
REMARK   3  QSPRD   0.000
REMARK   3  QMN4    0.75
REMARK   3  MODT    FALSE
REMARK   3 
REMARK   3 TEMPLATE 1
REMARK   3  PDBID   2n72
REMARK   3  CHAIN   A
REMARK   3  MMCIF   A
REMARK   3  PDBV    2020-04-24
REMARK   3  SMTLE   2n72.1.A
REMARK   3  SMTLV   2020-04-29
REMARK   3  MTHD    SOLUTION NMR 0.00 A
REMARK   3  FOUND   HHblits
REMARK   3  SIM     0.35
REMARK   3  SID     34.62
REMARK   3  OSTAT   monomer
REMARK   3  ALN A TRG MACAAARSPADQDRLLKILQLQRFKMYVQQLDLTYFLRKIKCTLENGIVMSNTEAESG
REMARK   3  ALN A TRG SSSNRKMGASALYSSHHVSQ
REMARK   3  ALN A TPL ------GNYEQQQILIRQLQEQHYQQYMQQLY--------------------------
REMARK   3  ALN A TPL --------------------
REMARK   3  ALN A OFF 28
ATOM      1  N   ARG A   7       1.146  11.553 -36.260  1.00  0.59           N  
ATOM      2  CA  ARG A   7       1.551  10.518 -37.274  1.00  0.59           C  
ATOM      3  C   ARG A   7       2.488   9.413 -36.830  1.00  0.59           C  
ATOM      4  O   ARG A   7       2.509   8.369 -37.464  1.00  0.59           O  
ATOM      5  CB  ARG A   7       2.188  11.194 -38.509  1.00  0.59           C  
ATOM      6  CG  ARG A   7       1.235  12.078 -39.334  1.00  0.59           C  
ATOM      7  CD  ARG A   7       1.947  12.723 -40.530  1.00  0.59           C  
ATOM      8  NE  ARG A   7       0.922  13.563 -41.225  1.00  0.59           N  
ATOM      9  CZ  ARG A   7       1.205  14.399 -42.234  1.00  0.59           C  
ATOM     10  NH1 ARG A   7       0.225  15.092 -42.807  1.00  0.59           N  
ATOM     11  NH2 ARG A   7       2.448  14.552 -42.689  1.00  0.59           N  
ATOM     12  N   SER A   8       3.270   9.589 -35.747  1.00  0.77           N  
ATOM     13  CA  SER A   8       4.132   8.521 -35.256  1.00  0.77           C  
ATOM     14  C   SER A   8       3.471   7.916 -34.009  1.00  0.77           C  
ATOM     15  O   SER A   8       3.605   8.508 -32.951  1.00  0.77           O  
ATOM     16  CB  SER A   8       5.535   9.076 -34.890  1.00  0.77           C  
ATOM     17  OG  SER A   8       6.411   8.003 -34.557  1.00  0.77           O  
ATOM     18  N   PRO A   9       2.724   6.790 -34.071  1.00  0.78           N  
ATOM     19  CA  PRO A   9       2.103   6.166 -32.900  1.00  0.78           C  
ATOM     20  C   PRO A   9       3.161   5.620 -31.960  1.00  0.78           C  
ATOM     21  O   PRO A   9       3.038   5.750 -30.753  1.00  0.78           O  
ATOM     22  CB  PRO A   9       1.160   5.083 -33.469  1.00  0.78           C  
ATOM     23  CG  PRO A   9       1.656   4.826 -34.896  1.00  0.78           C  
ATOM     24  CD  PRO A   9       2.283   6.154 -35.313  1.00  0.78           C  
ATOM     25  N   ALA A  10       4.248   5.066 -32.518  1.00  0.76           N  
ATOM     26  CA  ALA A  10       5.354   4.520 -31.760  1.00  0.76           C  
ATOM     27  C   ALA A  10       6.072   5.534 -30.866  1.00  0.76           C  
ATOM     28  O   ALA A  10       6.387   5.251 -29.708  1.00  0.76           O  
ATOM     29  CB  ALA A  10       6.346   3.918 -32.772  1.00  0.76           C  
ATOM     30  N   ASP A  11       6.339   6.753 -31.372  1.00  0.70           N  
ATOM     31  CA  ASP A  11       6.906   7.839 -30.593  1.00  0.70           C  
ATOM     32  C   ASP A  11       5.935   8.441 -29.579  1.00  0.70           C  
ATOM     33  O   ASP A  11       6.329   8.847 -28.487  1.00  0.70           O  
ATOM     34  CB  ASP A  11       7.460   8.948 -31.502  1.00  0.70           C  
ATOM     35  CG  ASP A  11       8.604   8.399 -32.374  1.00  0.70           C  
ATOM     36  OD1 ASP A  11       9.442   7.619 -31.829  1.00  0.70           O  
ATOM     37  OD2 ASP A  11       8.615   8.757 -33.548  1.00  0.70           O  
ATOM     38  N   GLN A  12       4.625   8.484 -29.910  1.00  0.69           N  
ATOM     39  CA  GLN A  12       3.574   8.897 -28.987  1.00  0.69           C  
ATOM     40  C   GLN A  12       3.541   8.014 -27.741  1.00  0.69           C  
ATOM     41  O   GLN A  12       3.565   8.521 -26.613  1.00  0.69           O  
ATOM     42  CB  GLN A  12       2.185   8.865 -29.681  1.00  0.69           C  
ATOM     43  CG  GLN A  12       1.958  10.003 -30.704  1.00  0.69           C  
ATOM     44  CD  GLN A  12       0.709   9.747 -31.550  1.00  0.69           C  
ATOM     45  OE1 GLN A  12      -0.228   9.041 -31.211  1.00  0.69           O  
ATOM     46  NE2 GLN A  12       0.654  10.423 -32.739  1.00  0.69           N  
ATOM     47  N   ASP A  13       3.597   6.679 -27.921  1.00  0.69           N  
ATOM     48  CA  ASP A  13       3.699   5.715 -26.832  1.00  0.69           C  
ATOM     49  C   ASP A  13       4.961   5.886 -25.973  1.00  0.69           C  
ATOM     50  O   ASP A  13       4.924   5.866 -24.745  1.00  0.69           O  
ATOM     51  CB  ASP A  13       3.667   4.277 -27.412  1.00  0.69           C  
ATOM     52  CG  ASP A  13       2.322   3.933 -28.049  1.00  0.69           C  
ATOM     53  OD1 ASP A  13       1.322   4.648 -27.788  1.00  0.69           O  
ATOM     54  OD2 ASP A  13       2.295   2.912 -28.782  1.00  0.69           O  
ATOM     55  N   ARG A  14       6.134   6.112 -26.617  1.00  0.66           N  
ATOM     56  CA  ARG A  14       7.386   6.390 -25.924  1.00  0.66           C  
ATOM     57  C   ARG A  14       7.344   7.652 -25.079  1.00  0.66           C  
ATOM     58  O   ARG A  14       7.843   7.687 -23.957  1.00  0.66           O  
ATOM     59  CB  ARG A  14       8.571   6.547 -26.905  1.00  0.66           C  
ATOM     60  CG  ARG A  14       8.968   5.255 -27.636  1.00  0.66           C  
ATOM     61  CD  ARG A  14      10.067   5.513 -28.669  1.00  0.66           C  
ATOM     62  NE  ARG A  14      10.392   4.175 -29.263  1.00  0.66           N  
ATOM     63  CZ  ARG A  14      11.226   4.030 -30.303  1.00  0.66           C  
ATOM     64  NH1 ARG A  14      11.445   2.808 -30.807  1.00  0.66           N  
ATOM     65  NH2 ARG A  14      11.810   5.093 -30.878  1.00  0.66           N  
ATOM     66  N   LEU A  15       6.733   8.722 -25.620  1.00  0.74           N  
ATOM     67  CA  LEU A  15       6.510   9.956 -24.897  1.00  0.74           C  
ATOM     68  C   LEU A  15       5.578   9.784 -23.702  1.00  0.74           C  
ATOM     69  O   LEU A  15       5.914  10.183 -22.586  1.00  0.74           O  
ATOM     70  CB  LEU A  15       5.951  11.022 -25.864  1.00  0.74           C  
ATOM     71  CG  LEU A  15       5.747  12.428 -25.266  1.00  0.74           C  
ATOM     72  CD1 LEU A  15       7.040  13.012 -24.676  1.00  0.74           C  
ATOM     73  CD2 LEU A  15       5.167  13.365 -26.334  1.00  0.74           C  
ATOM     74  N   LEU A  16       4.420   9.108 -23.876  1.00  0.75           N  
ATOM     75  CA  LEU A  16       3.459   8.846 -22.808  1.00  0.75           C  
ATOM     76  C   LEU A  16       4.061   8.065 -21.648  1.00  0.75           C  
ATOM     77  O   LEU A  16       3.805   8.364 -20.483  1.00  0.75           O  
ATOM     78  CB  LEU A  16       2.219   8.072 -23.316  1.00  0.75           C  
ATOM     79  CG  LEU A  16       1.256   8.862 -24.226  1.00  0.75           C  
ATOM     80  CD1 LEU A  16       0.236   7.895 -24.843  1.00  0.75           C  
ATOM     81  CD2 LEU A  16       0.522   9.997 -23.495  1.00  0.75           C  
ATOM     82  N   LYS A  17       4.924   7.078 -21.958  1.00  0.74           N  
ATOM     83  CA  LYS A  17       5.676   6.319 -20.976  1.00  0.74           C  
ATOM     84  C   LYS A  17       6.550   7.193 -20.073  1.00  0.74           C  
ATOM     85  O   LYS A  17       6.538   7.066 -18.854  1.00  0.74           O  
ATOM     86  CB  LYS A  17       6.570   5.297 -21.723  1.00  0.74           C  
ATOM     87  CG  LYS A  17       7.369   4.354 -20.810  1.00  0.74           C  
ATOM     88  CD  LYS A  17       8.219   3.348 -21.606  1.00  0.74           C  
ATOM     89  CE  LYS A  17       9.040   2.429 -20.695  1.00  0.74           C  
ATOM     90  NZ  LYS A  17       9.829   1.465 -21.496  1.00  0.74           N  
ATOM     91  N   ILE A  18       7.303   8.148 -20.667  1.00  0.74           N  
ATOM     92  CA  ILE A  18       8.104   9.133 -19.939  1.00  0.74           C  
ATOM     93  C   ILE A  18       7.251  10.073 -19.107  1.00  0.74           C  
ATOM     94  O   ILE A  18       7.565  10.362 -17.947  1.00  0.74           O  
ATOM     95  CB  ILE A  18       9.019   9.920 -20.873  1.00  0.74           C  
ATOM     96  CG1 ILE A  18      10.077   8.955 -21.454  1.00  0.74           C  
ATOM     97  CG2 ILE A  18       9.694  11.103 -20.131  1.00  0.74           C  
ATOM     98  CD1 ILE A  18      10.902   9.581 -22.580  1.00  0.74           C  
ATOM     99  N   LEU A  19       6.119  10.556 -19.665  1.00  0.74           N  
ATOM    100  CA  LEU A  19       5.208  11.440 -18.949  1.00  0.74           C  
ATOM    101  C   LEU A  19       4.677  10.802 -17.666  1.00  0.74           C  
ATOM    102  O   LEU A  19       4.667  11.413 -16.601  1.00  0.74           O  
ATOM    103  CB  LEU A  19       3.980  11.863 -19.802  1.00  0.74           C  
ATOM    104  CG  LEU A  19       4.270  12.668 -21.088  1.00  0.74           C  
ATOM    105  CD1 LEU A  19       2.967  12.894 -21.872  1.00  0.74           C  
ATOM    106  CD2 LEU A  19       4.993  13.998 -20.846  1.00  0.74           C  
ATOM    107  N   GLN A  20       4.270   9.520 -17.732  1.00  0.72           N  
ATOM    108  CA  GLN A  20       3.846   8.735 -16.586  1.00  0.72           C  
ATOM    109  C   GLN A  20       4.923   8.571 -15.510  1.00  0.72           C  
ATOM    110  O   GLN A  20       4.658   8.737 -14.323  1.00  0.72           O  
ATOM    111  CB  GLN A  20       3.354   7.355 -17.072  1.00  0.72           C  
ATOM    112  CG  GLN A  20       2.067   7.437 -17.932  1.00  0.72           C  
ATOM    113  CD  GLN A  20       1.809   6.109 -18.661  1.00  0.72           C  
ATOM    114  OE1 GLN A  20       2.653   5.244 -18.788  1.00  0.72           O  
ATOM    115  NE2 GLN A  20       0.554   5.959 -19.172  1.00  0.72           N  
ATOM    116  N   LEU A  21       6.183   8.300 -15.911  1.00  0.74           N  
ATOM    117  CA  LEU A  21       7.324   8.242 -15.002  1.00  0.74           C  
ATOM    118  C   LEU A  21       7.631   9.563 -14.302  1.00  0.74           C  
ATOM    119  O   LEU A  21       7.954   9.614 -13.117  1.00  0.74           O  
ATOM    120  CB  LEU A  21       8.601   7.793 -15.749  1.00  0.74           C  
ATOM    121  CG  LEU A  21       8.563   6.361 -16.313  1.00  0.74           C  
ATOM    122  CD1 LEU A  21       9.776   6.139 -17.231  1.00  0.74           C  
ATOM    123  CD2 LEU A  21       8.506   5.293 -15.210  1.00  0.74           C  
ATOM    124  N   GLN A  22       7.546  10.694 -15.026  1.00  0.72           N  
ATOM    125  CA  GLN A  22       7.672  12.012 -14.430  1.00  0.72           C  
ATOM    126  C   GLN A  22       6.528  12.380 -13.479  1.00  0.72           C  
ATOM    127  O   GLN A  22       6.741  12.928 -12.400  1.00  0.72           O  
ATOM    128  CB  GLN A  22       7.862  13.093 -15.510  1.00  0.72           C  
ATOM    129  CG  GLN A  22       8.394  14.418 -14.925  1.00  0.72           C  
ATOM    130  CD  GLN A  22       8.694  15.418 -16.038  1.00  0.72           C  
ATOM    131  OE1 GLN A  22       7.824  16.145 -16.501  1.00  0.72           O  
ATOM    132  NE2 GLN A  22       9.970  15.454 -16.496  1.00  0.72           N  
ATOM    133  N   ARG A  23       5.279  12.025 -13.850  1.00  0.70           N  
ATOM    134  CA  ARG A  23       4.100  12.126 -12.996  1.00  0.70           C  
ATOM    135  C   ARG A  23       4.211  11.297 -11.723  1.00  0.70           C  
ATOM    136  O   ARG A  23       3.739  11.702 -10.662  1.00  0.70           O  
ATOM    137  CB  ARG A  23       2.816  11.701 -13.746  1.00  0.70           C  
ATOM    138  CG  ARG A  23       2.362  12.689 -14.837  1.00  0.70           C  
ATOM    139  CD  ARG A  23       1.206  12.126 -15.666  1.00  0.70           C  
ATOM    140  NE  ARG A  23       0.923  13.119 -16.756  1.00  0.70           N  
ATOM    141  CZ  ARG A  23       0.024  12.931 -17.728  1.00  0.70           C  
ATOM    142  NH1 ARG A  23      -0.168  13.879 -18.644  1.00  0.70           N  
ATOM    143  NH2 ARG A  23      -0.706  11.816 -17.804  1.00  0.70           N  
ATOM    144  N   PHE A  24       4.865  10.120 -11.782  1.00  0.73           N  
ATOM    145  CA  PHE A  24       5.182   9.318 -10.608  1.00  0.73           C  
ATOM    146  C   PHE A  24       6.028  10.088  -9.581  1.00  0.73           C  
ATOM    147  O   PHE A  24       5.736  10.070  -8.391  1.00  0.73           O  
ATOM    148  CB  PHE A  24       5.869   7.988 -11.026  1.00  0.73           C  
ATOM    149  CG  PHE A  24       6.037   7.042  -9.865  1.00  0.73           C  
ATOM    150  CD1 PHE A  24       7.297   6.865  -9.268  1.00  0.73           C  
ATOM    151  CD2 PHE A  24       4.936   6.335  -9.354  1.00  0.73           C  
ATOM    152  CE1 PHE A  24       7.453   6.001  -8.177  1.00  0.73           C  
ATOM    153  CE2 PHE A  24       5.090   5.469  -8.262  1.00  0.73           C  
ATOM    154  CZ  PHE A  24       6.349   5.301  -7.674  1.00  0.73           C  
ATOM    155  N   LYS A  25       7.055  10.851 -10.021  1.00  0.73           N  
ATOM    156  CA  LYS A  25       7.856  11.685  -9.128  1.00  0.73           C  
ATOM    157  C   LYS A  25       7.049  12.750  -8.389  1.00  0.73           C  
ATOM    158  O   LYS A  25       7.236  12.994  -7.201  1.00  0.73           O  
ATOM    159  CB  LYS A  25       9.000  12.393  -9.888  1.00  0.73           C  
ATOM    160  CG  LYS A  25      10.051  11.436 -10.466  1.00  0.73           C  
ATOM    161  CD  LYS A  25      11.130  12.211 -11.244  1.00  0.73           C  
ATOM    162  CE  LYS A  25      12.220  11.301 -11.813  1.00  0.73           C  
ATOM    163  NZ  LYS A  25      13.211  12.089 -12.579  1.00  0.73           N  
ATOM    164  N   MET A  26       6.107  13.395  -9.103  1.00  0.74           N  
ATOM    165  CA  MET A  26       5.136  14.304  -8.520  1.00  0.74           C  
ATOM    166  C   MET A  26       4.188  13.632  -7.529  1.00  0.74           C  
ATOM    167  O   MET A  26       3.895  14.165  -6.465  1.00  0.74           O  
ATOM    168  CB  MET A  26       4.274  14.963  -9.614  1.00  0.74           C  
ATOM    169  CG  MET A  26       5.045  15.881 -10.578  1.00  0.74           C  
ATOM    170  SD  MET A  26       4.002  16.525 -11.926  1.00  0.74           S  
ATOM    171  CE  MET A  26       2.926  17.582 -10.911  1.00  0.74           C  
ATOM    172  N   TYR A  27       3.702  12.423  -7.872  1.00  0.73           N  
ATOM    173  CA  TYR A  27       2.842  11.599  -7.039  1.00  0.73           C  
ATOM    174  C   TYR A  27       3.505  11.212  -5.719  1.00  0.73           C  
ATOM    175  O   TYR A  27       2.897  11.288  -4.661  1.00  0.73           O  
ATOM    176  CB  TYR A  27       2.448  10.332  -7.840  1.00  0.73           C  
ATOM    177  CG  TYR A  27       1.221   9.663  -7.294  1.00  0.73           C  
ATOM    178  CD1 TYR A  27      -0.046  10.097  -7.709  1.00  0.73           C  
ATOM    179  CD2 TYR A  27       1.319   8.581  -6.405  1.00  0.73           C  
ATOM    180  CE1 TYR A  27      -1.202   9.453  -7.251  1.00  0.73           C  
ATOM    181  CE2 TYR A  27       0.160   7.934  -5.946  1.00  0.73           C  
ATOM    182  CZ  TYR A  27      -1.100   8.371  -6.374  1.00  0.73           C  
ATOM    183  OH  TYR A  27      -2.270   7.722  -5.934  1.00  0.73           O  
ATOM    184  N   VAL A  28       4.803  10.828  -5.763  1.00  0.74           N  
ATOM    185  CA  VAL A  28       5.615  10.569  -4.569  1.00  0.74           C  
ATOM    186  C   VAL A  28       5.737  11.805  -3.671  1.00  0.74           C  
ATOM    187  O   VAL A  28       5.443  11.738  -2.486  1.00  0.74           O  
ATOM    188  CB  VAL A  28       6.991  10.004  -4.942  1.00  0.74           C  
ATOM    189  CG1 VAL A  28       7.874   9.809  -3.692  1.00  0.74           C  
ATOM    190  CG2 VAL A  28       6.798   8.630  -5.618  1.00  0.74           C  
ATOM    191  N   GLN A  29       6.062  12.987  -4.242  1.00  0.69           N  
ATOM    192  CA  GLN A  29       6.138  14.249  -3.504  1.00  0.69           C  
ATOM    193  C   GLN A  29       4.812  14.681  -2.871  1.00  0.69           C  
ATOM    194  O   GLN A  29       4.767  15.316  -1.830  1.00  0.69           O  
ATOM    195  CB  GLN A  29       6.600  15.408  -4.429  1.00  0.69           C  
ATOM    196  CG  GLN A  29       8.073  15.338  -4.903  1.00  0.69           C  
ATOM    197  CD  GLN A  29       9.036  15.797  -3.798  1.00  0.69           C  
ATOM    198  OE1 GLN A  29       8.669  16.190  -2.709  1.00  0.69           O  
ATOM    199  NE2 GLN A  29      10.349  15.836  -4.147  1.00  0.69           N  
ATOM    200  N   GLN A  30       3.692  14.393  -3.555  1.00  0.69           N  
ATOM    201  CA  GLN A  30       2.350  14.604  -3.027  1.00  0.69           C  
ATOM    202  C   GLN A  30       1.962  13.683  -1.865  1.00  0.69           C  
ATOM    203  O   GLN A  30       1.146  14.059  -1.006  1.00  0.69           O  
ATOM    204  CB  GLN A  30       1.311  14.412  -4.152  1.00  0.69           C  
ATOM    205  CG  GLN A  30      -0.106  14.829  -3.704  1.00  0.69           C  
ATOM    206  CD  GLN A  30      -1.114  14.664  -4.833  1.00  0.69           C  
ATOM    207  OE1 GLN A  30      -1.006  13.823  -5.710  1.00  0.69           O  
ATOM    208  NE2 GLN A  30      -2.168  15.521  -4.793  1.00  0.69           N  
ATOM    209  N   LEU A  31       2.440  12.440  -1.866  1.00  0.75           N  
ATOM    210  CA  LEU A  31       2.252  11.458  -0.804  1.00  0.75           C  
ATOM    211  C   LEU A  31       3.031  11.742   0.489  1.00  0.75           C  
ATOM    212  O   LEU A  31       2.613  11.270   1.554  1.00  0.75           O  
ATOM    213  CB  LEU A  31       2.649  10.043  -1.297  1.00  0.75           C  
ATOM    214  CG  LEU A  31       1.634   9.355  -2.230  1.00  0.75           C  
ATOM    215  CD1 LEU A  31       2.266   8.095  -2.842  1.00  0.75           C  
ATOM    216  CD2 LEU A  31       0.341   9.001  -1.481  1.00  0.75           C  
ATOM    217  N   ASP A  32       4.164  12.456   0.404  1.00  0.71           N  
ATOM    218  CA  ASP A  32       4.960  12.930   1.530  1.00  0.71           C  
ATOM    219  C   ASP A  32       4.289  14.088   2.369  1.00  0.71           C  
ATOM    220  O   ASP A  32       3.215  14.614   1.975  1.00  0.71           O  
ATOM    221  CB  ASP A  32       6.349  13.412   1.003  1.00  0.71           C  
ATOM    222  CG  ASP A  32       7.279  12.307   0.503  1.00  0.71           C  
ATOM    223  OD1 ASP A  32       7.064  11.104   0.807  1.00  0.71           O  
ATOM    224  OD2 ASP A  32       8.283  12.674  -0.171  1.00  0.71           O  
ATOM    225  OXT ASP A  32       4.867  14.448   3.438  1.00  0.71           O  
TER     226      ASP A  32                                                      
END   
