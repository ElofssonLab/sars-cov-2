TITLE     SWISS-MODEL SERVER (https://swissmodel.expasy.org)
TITLE    2 Untitled Project
EXPDTA    THEORETICAL MODEL (SWISS-MODEL SERVER)
AUTHOR    SWISS-MODEL SERVER (SEE REFERENCE IN JRNL Records)
REVDAT   1   29-APR-20 1MOD    1       10:54
JRNL        AUTH   A.WATERHOUSE,M.BERTONI,S.BIENERT,G.STUDER,G.TAURIELLO,
JRNL        AUTH 2 R.GUMIENNY,F.T.HEER,T.A.P.DE BEER,C.REMPFER,L.BORDOLI,
JRNL        AUTH 3 R.LEPORE,T.SCHWEDE
JRNL        TITL   SWISS-MODEL: HOMOLOGY MODELLING OF PROTEIN STRUCTURES AND
JRNL        TITL 2 COMPLEXES
JRNL        REF    NUCLEIC.ACIDS.RES..           V.  46 W296  2018
JRNL        PMID   29788355
JRNL        DOI    10.1093/nar/gky427
REMARK   1
REMARK   1 REFERENCE 1
REMARK   1  AUTH   S.BIENERT,A.WATERHOUSE,T.A.P.DE BEER,G.TAURIELLO,G.STUDER,
REMARK   1  AUTH 2 L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   THE SWISS-MODEL REPOSITORY - NEW FEATURES AND FUNCTIONALITY
REMARK   1  REF    NUCLEIC.ACIDS.RES..           V.  22       2017
REMARK   1  REFN                   ISSN 0305-1048
REMARK   1  PMID   27899672
REMARK   1  DOI    10.1093/nar/gkw1132
REMARK   1
REMARK   1 REFERENCE 2
REMARK   1  AUTH   N.GUEX,M.C.PEITSCH,T.SCHWEDE
REMARK   1  TITL   AUTOMATED COMPARATIVE PROTEIN STRUCTURE MODELING WITH
REMARK   1  TITL 2 SWISS-MODEL AND SWISS-PDBVIEWER: A HISTORICAL PERSPECTIVE
REMARK   1  REF    ELECTROPHORESIS               V.  30       2009
REMARK   1  REFN                   ISSN 0173-0835
REMARK   1  PMID   19517507
REMARK   1  DOI    10.1002/elps.200900140
REMARK   1
REMARK   1 REFERENCE 3
REMARK   1  AUTH   P.BENKERT,M.BIASINI,T.SCHWEDE
REMARK   1  TITL   TOWARD THE ESTIMATION OF THE ABSOLUTE QUALITY OF INDIVIDUAL
REMARK   1  TITL 2 PROTEIN STRUCTURE MODELS
REMARK   1  REF    BIOINFORMATICS                V.  27       2011
REMARK   1  REFN                   ISSN 1367-4803
REMARK   1  PMID   21134891
REMARK   1  DOI    10.1093/bioinformatics/btq662
REMARK   1
REMARK   1 REFERENCE 4
REMARK   1  AUTH   M.BERTONI,F.KIEFER,M.BIASINI,L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   MODELING PROTEIN QUATERNARY STRUCTURE OF HOMO- AND
REMARK   1  TITL 2 HETERO-OLIGOMERS BEYOND BINARY INTERACTIONS BY HOMOLOGY
REMARK   1  REF    SCI.REP.                      V.   7       2017
REMARK   1  REFN                   ISSN
REMARK   1  PMID   28874689
REMARK   1  DOI    10.1038/s41598-017-09654-8
REMARK   1
REMARK   1 DISCLAIMER
REMARK   1 The SWISS-MODEL SERVER produces theoretical models for proteins.
REMARK   1 The results of any theoretical modelling procedure is
REMARK   1 NON-EXPERIMENTAL and MUST be considered with care. These models may
REMARK   1 contain significant errors. This is especially true for automated
REMARK   1 modeling since there is no human intervention during model
REMARK   1 building. Please read the header section and the logfile carefully
REMARK   1 to know what templates and alignments were used during the model
REMARK   1 building process. All information by the SWISS-MODEL SERVER is
REMARK   1 provided "AS-IS", without any warranty, expressed or implied.
REMARK   2
REMARK   2 COPYRIGHT NOTICE
REMARK   2 This SWISS-MODEL protein model is copyright. It is produced by the
REMARK   2 SWISS-MODEL server, developed by the Computational Structural
REMARK   2 Biology Group at the SIB Swiss Institute of Bioinformatics at the
REMARK   2 Biozentrum, University of Basel (https://swissmodel.expasy.org). This
REMARK   2 model is licensed under the CC BY-SA 4.0 Creative Commons
REMARK   2 Attribution-ShareAlike 4.0 International License
REMARK   2 (https://creativecommons.org/licenses/by-sa/4.0/legalcode), i.e. you
REMARK   2 can copy and redistribute the model in any medium or format,
REMARK   2 transform and build upon the model for any purpose, even
REMARK   2 commercially, under the following terms:
REMARK   2 Attribution - You must give appropriate credit, provide a link to
REMARK   2 the license, and indicate if changes were made. You may do so in any
REMARK   2 reasonable manner, but not in any way that suggests the licensor
REMARK   2 endorses you or your use. When you publish, patent or distribute
REMARK   2 results that were fully or partially based on the model, please cite
REMARK   2 the corresponding papers mentioned under JRNL.
REMARK   2 ShareAlike - If you remix, transform, or build upon the material,
REMARK   2 you must distribute your contributions under the same license as the
REMARK   2 original.
REMARK   2 No additional restrictions - you may not apply legal terms or
REMARK   2 technological measures that legally restrict others from doing
REMARK   2 anything the license permits.
REMARK   2 Find a human-readable summary of (and not a substitute for) the
REMARK   2 CC BY-SA 4.0 license at this link:
REMARK   2 https://creativecommons.org/licenses/by-sa/4.0/
REMARK   3 
REMARK   3 MODEL INFORMATION
REMARK   3  ENGIN   PROMOD3
REMARK   3  VERSN   3.0.0
REMARK   3  OSTAT   monomer
REMARK   3  OSRSN   PREDICTION
REMARK   3  QSPRD   0.000
REMARK   3  QMN4    -0.75
REMARK   3  MODT    FALSE
REMARK   3 
REMARK   3 TEMPLATE 1
REMARK   3  PDBID   1pd3
REMARK   3  CHAIN   A
REMARK   3  MMCIF   A
REMARK   3  PDBV    2020-04-17
REMARK   3  SMTLE   1pd3.1.A
REMARK   3  SMTLV   2020-04-22
REMARK   3  MTHD    X-RAY DIFFRACTION 2.60 A
REMARK   3  FOUND   HHblits
REMARK   3  SIM     0.36
REMARK   3  SID     34.62
REMARK   3  OSTAT   monomer
REMARK   3  ALN A TRG MEDERKNGAYGTPQKYDPTFKGPIYNRGCTDIICCVFLLLAIVGYVAVGIIAWTHGDP
REMARK   3  ALN A TRG RKVIYPTDSRGEFCGQKGTKNENKPYLFYFNIVKCASPLVLLEFQCPTPQICVEKCPD
REMARK   3  ALN A TRG RYLTYLNARSSRDFEYYKQFCVPGFKNNKGVAEVLQDGDCPAVLIPSKPLARRCFPAI
REMARK   3  ALN A TRG HAYKGVLMVGNETTYEDGHGSRKNITDLVEGAKKANGVLEARQLAMRIFEDYTVSWYW
REMARK   3  ALN A TRG IIIGLVIAMAMSLLFIILLRFLAGIMVWVMIIMVILVLGYGIFHCYMEYSRLRGEAGS
REMARK   3  ALN A TRG DVSLVDLGFQTDFRVYLHLRQTWLAFMIILSILEVIIILLLIFLRKRILIAIALIKEA
REMARK   3  ALN A TRG SRAVGYVMCSLLYPLVTFFLLCLCIAYWASTAVFLSTSNEAVYKIFDDSPCPFTAKTC
REMARK   3  ALN A TRG NPETFPSSNESRQCPNARCQFAFYGGESGYHRALLGLQIFNAFMFFWLANFVLALGQV
REMARK   3  ALN A TRG TLAGAFASYYWALRKPDDLPAFPLFSAFGRALRYHTGSLAFGALILAIVQIIRVILEY
REMARK   3  ALN A TRG LDQRLKAAENKFAKCLMTCLKCCFWCLEKFIKFLNRNAYIMIAIYGTNFCTSARNAFF
REMARK   3  ALN A TRG LLMRNIIRVAVLDKVTDFLFLLGKLLIVGSVGILAFFFFTHRIRIVQDTAPPLNYYWV
REMARK   3  ALN A TRG PILTVIVGSYLIAHGFFSVYGMCVDTLFLCFLEDLERNDGSAERPYFMSSTLKKLLNK
REMARK   3  ALN A TRG TNKKAAES
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL -------------------------------------------------EEIRWLIEE
REMARK   3  ALN A TPL VRHRLKITENSFEQITF-----------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL ----------------------------------------------------------
REMARK   3  ALN A TPL --------
REMARK   3  ALN A OFF 15
ATOM      1  N   GLN A 514      37.789  15.608   4.029  1.00  0.23           N  
ATOM      2  CA  GLN A 514      37.034  16.492   3.081  1.00  0.23           C  
ATOM      3  C   GLN A 514      36.325  17.614   3.802  1.00  0.23           C  
ATOM      4  O   GLN A 514      36.719  18.758   3.625  1.00  0.23           O  
ATOM      5  CB  GLN A 514      36.070  15.682   2.173  1.00  0.23           C  
ATOM      6  CG  GLN A 514      35.407  16.473   1.010  1.00  0.23           C  
ATOM      7  CD  GLN A 514      36.439  17.073   0.050  1.00  0.23           C  
ATOM      8  OE1 GLN A 514      37.555  16.565  -0.136  1.00  0.23           O  
ATOM      9  NE2 GLN A 514      36.092  18.233  -0.545  1.00  0.23           N  
ATOM     10  N   ILE A 515      35.334  17.336   4.671  1.00  0.33           N  
ATOM     11  CA  ILE A 515      34.456  18.273   5.387  1.00  0.33           C  
ATOM     12  C   ILE A 515      35.222  19.331   6.166  1.00  0.33           C  
ATOM     13  O   ILE A 515      34.964  20.524   6.027  1.00  0.33           O  
ATOM     14  CB  ILE A 515      33.533  17.457   6.295  1.00  0.33           C  
ATOM     15  CG1 ILE A 515      32.578  16.609   5.418  1.00  0.33           C  
ATOM     16  CG2 ILE A 515      32.746  18.364   7.269  1.00  0.33           C  
ATOM     17  CD1 ILE A 515      31.799  15.553   6.207  1.00  0.33           C  
ATOM     18  N   ILE A 516      36.261  18.923   6.927  1.00  0.28           N  
ATOM     19  CA  ILE A 516      37.145  19.839   7.650  1.00  0.28           C  
ATOM     20  C   ILE A 516      37.809  20.858   6.739  1.00  0.28           C  
ATOM     21  O   ILE A 516      37.821  22.058   7.012  1.00  0.28           O  
ATOM     22  CB  ILE A 516      38.255  19.083   8.394  1.00  0.28           C  
ATOM     23  CG1 ILE A 516      37.668  18.072   9.405  1.00  0.28           C  
ATOM     24  CG2 ILE A 516      39.226  20.067   9.097  1.00  0.28           C  
ATOM     25  CD1 ILE A 516      38.736  17.142   9.998  1.00  0.28           C  
ATOM     26  N   ARG A 517      38.352  20.402   5.594  1.00  0.26           N  
ATOM     27  CA  ARG A 517      38.997  21.266   4.636  1.00  0.26           C  
ATOM     28  C   ARG A 517      38.028  22.270   4.020  1.00  0.26           C  
ATOM     29  O   ARG A 517      38.326  23.453   3.949  1.00  0.26           O  
ATOM     30  CB  ARG A 517      39.718  20.439   3.552  1.00  0.26           C  
ATOM     31  CG  ARG A 517      40.653  21.301   2.686  1.00  0.26           C  
ATOM     32  CD  ARG A 517      41.466  20.539   1.636  1.00  0.26           C  
ATOM     33  NE  ARG A 517      40.542  20.271   0.478  1.00  0.26           N  
ATOM     34  CZ  ARG A 517      39.929  19.121   0.181  1.00  0.26           C  
ATOM     35  NH1 ARG A 517      40.141  17.999   0.860  1.00  0.26           N  
ATOM     36  NH2 ARG A 517      39.109  19.033  -0.865  1.00  0.26           N  
ATOM     37  N   VAL A 518      36.808  21.823   3.650  1.00  0.42           N  
ATOM     38  CA  VAL A 518      35.716  22.659   3.144  1.00  0.42           C  
ATOM     39  C   VAL A 518      35.288  23.746   4.138  1.00  0.42           C  
ATOM     40  O   VAL A 518      35.108  24.905   3.764  1.00  0.42           O  
ATOM     41  CB  VAL A 518      34.505  21.802   2.758  1.00  0.42           C  
ATOM     42  CG1 VAL A 518      33.274  22.667   2.429  1.00  0.42           C  
ATOM     43  CG2 VAL A 518      34.837  20.944   1.524  1.00  0.42           C  
ATOM     44  N   ILE A 519      35.155  23.409   5.443  1.00  0.40           N  
ATOM     45  CA  ILE A 519      34.884  24.367   6.525  1.00  0.40           C  
ATOM     46  C   ILE A 519      35.969  25.441   6.622  1.00  0.40           C  
ATOM     47  O   ILE A 519      35.694  26.642   6.702  1.00  0.40           O  
ATOM     48  CB  ILE A 519      34.709  23.676   7.889  1.00  0.40           C  
ATOM     49  CG1 ILE A 519      33.375  22.895   7.928  1.00  0.40           C  
ATOM     50  CG2 ILE A 519      34.769  24.693   9.059  1.00  0.40           C  
ATOM     51  CD1 ILE A 519      33.238  21.985   9.156  1.00  0.40           C  
ATOM     52  N   LEU A 520      37.250  25.025   6.585  1.00  0.43           N  
ATOM     53  CA  LEU A 520      38.398  25.911   6.647  1.00  0.43           C  
ATOM     54  C   LEU A 520      38.501  26.866   5.468  1.00  0.43           C  
ATOM     55  O   LEU A 520      38.713  28.062   5.670  1.00  0.43           O  
ATOM     56  CB  LEU A 520      39.705  25.116   6.897  1.00  0.43           C  
ATOM     57  CG  LEU A 520      39.828  24.533   8.326  1.00  0.43           C  
ATOM     58  CD1 LEU A 520      41.082  23.650   8.447  1.00  0.43           C  
ATOM     59  CD2 LEU A 520      39.854  25.637   9.399  1.00  0.43           C  
ATOM     60  N   GLU A 521      38.250  26.372   4.234  1.00  0.48           N  
ATOM     61  CA  GLU A 521      38.167  27.163   3.010  1.00  0.48           C  
ATOM     62  C   GLU A 521      37.066  28.211   3.110  1.00  0.48           C  
ATOM     63  O   GLU A 521      37.236  29.382   2.784  1.00  0.48           O  
ATOM     64  CB  GLU A 521      37.845  26.275   1.769  1.00  0.48           C  
ATOM     65  CG  GLU A 521      38.904  25.208   1.388  1.00  0.48           C  
ATOM     66  CD  GLU A 521      40.150  25.728   0.673  1.00  0.48           C  
ATOM     67  OE1 GLU A 521      40.411  26.953   0.687  1.00  0.48           O  
ATOM     68  OE2 GLU A 521      40.848  24.845   0.100  1.00  0.48           O  
ATOM     69  N   TYR A 522      35.889  27.818   3.638  1.00  0.44           N  
ATOM     70  CA  TYR A 522      34.769  28.709   3.853  1.00  0.44           C  
ATOM     71  C   TYR A 522      35.110  29.899   4.771  1.00  0.44           C  
ATOM     72  O   TYR A 522      34.836  31.052   4.429  1.00  0.44           O  
ATOM     73  CB  TYR A 522      33.574  27.866   4.391  1.00  0.44           C  
ATOM     74  CG  TYR A 522      32.451  28.719   4.891  1.00  0.44           C  
ATOM     75  CD1 TYR A 522      32.378  29.003   6.259  1.00  0.44           C  
ATOM     76  CD2 TYR A 522      31.530  29.307   4.015  1.00  0.44           C  
ATOM     77  CE1 TYR A 522      31.380  29.845   6.754  1.00  0.44           C  
ATOM     78  CE2 TYR A 522      30.495  30.110   4.520  1.00  0.44           C  
ATOM     79  CZ  TYR A 522      30.417  30.373   5.897  1.00  0.44           C  
ATOM     80  OH  TYR A 522      29.401  31.175   6.456  1.00  0.44           O  
ATOM     81  N   LEU A 523      35.733  29.660   5.946  1.00  0.44           N  
ATOM     82  CA  LEU A 523      36.152  30.737   6.830  1.00  0.44           C  
ATOM     83  C   LEU A 523      37.289  31.578   6.249  1.00  0.44           C  
ATOM     84  O   LEU A 523      37.259  32.786   6.444  1.00  0.44           O  
ATOM     85  CB  LEU A 523      36.392  30.316   8.312  1.00  0.44           C  
ATOM     86  CG  LEU A 523      35.126  29.965   9.145  1.00  0.44           C  
ATOM     87  CD1 LEU A 523      35.515  29.527  10.568  1.00  0.44           C  
ATOM     88  CD2 LEU A 523      34.120  31.123   9.272  1.00  0.44           C  
ATOM     89  N   ASP A 524      38.270  31.011   5.492  1.00  0.44           N  
ATOM     90  CA  ASP A 524      39.247  31.810   4.739  1.00  0.44           C  
ATOM     91  C   ASP A 524      38.596  32.707   3.691  1.00  0.44           C  
ATOM     92  O   ASP A 524      38.907  33.891   3.561  1.00  0.44           O  
ATOM     93  CB  ASP A 524      40.303  30.960   3.986  1.00  0.44           C  
ATOM     94  CG  ASP A 524      41.488  31.838   3.633  1.00  0.44           C  
ATOM     95  OD1 ASP A 524      41.913  31.803   2.449  1.00  0.44           O  
ATOM     96  OD2 ASP A 524      41.985  32.614   4.491  1.00  0.44           O  
ATOM     97  N   GLN A 525      37.637  32.171   2.918  1.00  0.45           N  
ATOM     98  CA  GLN A 525      37.009  32.849   1.801  1.00  0.45           C  
ATOM     99  C   GLN A 525      36.276  34.125   2.187  1.00  0.45           C  
ATOM    100  O   GLN A 525      36.064  35.038   1.385  1.00  0.45           O  
ATOM    101  CB  GLN A 525      36.027  31.892   1.082  1.00  0.45           C  
ATOM    102  CG  GLN A 525      35.427  32.441  -0.232  1.00  0.45           C  
ATOM    103  CD  GLN A 525      36.536  32.713  -1.245  1.00  0.45           C  
ATOM    104  OE1 GLN A 525      37.379  31.866  -1.533  1.00  0.45           O  
ATOM    105  NE2 GLN A 525      36.549  33.934  -1.827  1.00  0.45           N  
ATOM    106  N   ARG A 526      35.886  34.223   3.463  1.00  0.26           N  
ATOM    107  CA  ARG A 526      35.233  35.378   4.015  1.00  0.26           C  
ATOM    108  C   ARG A 526      36.177  36.372   4.650  1.00  0.26           C  
ATOM    109  O   ARG A 526      35.763  37.415   5.152  1.00  0.26           O  
ATOM    110  CB  ARG A 526      34.273  34.889   5.100  1.00  0.26           C  
ATOM    111  CG  ARG A 526      33.137  34.074   4.484  1.00  0.26           C  
ATOM    112  CD  ARG A 526      31.829  34.420   5.161  1.00  0.26           C  
ATOM    113  NE  ARG A 526      30.756  33.679   4.435  1.00  0.26           N  
ATOM    114  CZ  ARG A 526      29.463  33.771   4.764  1.00  0.26           C  
ATOM    115  NH1 ARG A 526      29.052  34.637   5.686  1.00  0.26           N  
ATOM    116  NH2 ARG A 526      28.576  32.960   4.182  1.00  0.26           N  
ATOM    117  N   LEU A 527      37.479  36.083   4.618  1.00  0.26           N  
ATOM    118  CA  LEU A 527      38.479  36.999   5.084  1.00  0.26           C  
ATOM    119  C   LEU A 527      39.032  37.795   3.933  1.00  0.26           C  
ATOM    120  O   LEU A 527      39.126  37.382   2.777  1.00  0.26           O  
ATOM    121  CB  LEU A 527      39.584  36.303   5.903  1.00  0.26           C  
ATOM    122  CG  LEU A 527      39.015  35.501   7.090  1.00  0.26           C  
ATOM    123  CD1 LEU A 527      40.124  34.703   7.785  1.00  0.26           C  
ATOM    124  CD2 LEU A 527      38.187  36.325   8.092  1.00  0.26           C  
ATOM    125  N   LYS A 528      39.399  39.036   4.252  1.00  0.29           N  
ATOM    126  CA  LYS A 528      40.165  39.870   3.370  1.00  0.29           C  
ATOM    127  C   LYS A 528      41.484  40.083   4.072  1.00  0.29           C  
ATOM    128  O   LYS A 528      41.624  40.759   5.095  1.00  0.29           O  
ATOM    129  CB  LYS A 528      39.410  41.160   2.972  1.00  0.29           C  
ATOM    130  CG  LYS A 528      38.186  40.916   2.056  1.00  0.29           C  
ATOM    131  CD  LYS A 528      38.483  40.261   0.693  1.00  0.29           C  
ATOM    132  CE  LYS A 528      37.197  39.865  -0.043  1.00  0.29           C  
ATOM    133  NZ  LYS A 528      37.477  39.640  -1.478  1.00  0.29           N  
ATOM    134  N   ALA A 529      42.521  39.428   3.544  1.00  0.28           N  
ATOM    135  CA  ALA A 529      43.781  39.341   4.197  1.00  0.28           C  
ATOM    136  C   ALA A 529      44.833  39.372   3.118  1.00  0.28           C  
ATOM    137  O   ALA A 529      44.565  39.052   1.966  1.00  0.28           O  
ATOM    138  CB  ALA A 529      43.777  38.078   5.075  1.00  0.28           C  
ATOM    139  N   ALA A 530      46.037  39.859   3.467  1.00  0.34           N  
ATOM    140  CA  ALA A 530      47.102  40.068   2.516  1.00  0.34           C  
ATOM    141  C   ALA A 530      47.966  38.825   2.460  1.00  0.34           C  
ATOM    142  O   ALA A 530      48.005  38.038   3.404  1.00  0.34           O  
ATOM    143  CB  ALA A 530      47.978  41.290   2.881  1.00  0.34           C  
ATOM    144  N   GLU A 531      48.700  38.656   1.353  1.00  0.33           N  
ATOM    145  CA  GLU A 531      49.348  37.419   0.964  1.00  0.33           C  
ATOM    146  C   GLU A 531      50.517  36.963   1.834  1.00  0.33           C  
ATOM    147  O   GLU A 531      50.865  35.788   1.895  1.00  0.33           O  
ATOM    148  CB  GLU A 531      49.742  37.544  -0.533  1.00  0.33           C  
ATOM    149  CG  GLU A 531      49.945  36.197  -1.268  1.00  0.33           C  
ATOM    150  CD  GLU A 531      48.672  35.331  -1.334  1.00  0.33           C  
ATOM    151  OE1 GLU A 531      47.590  35.757  -0.844  1.00  0.33           O  
ATOM    152  OE2 GLU A 531      48.777  34.217  -1.905  1.00  0.33           O  
ATOM    153  N   ASN A 532      51.116  37.909   2.581  1.00  0.36           N  
ATOM    154  CA  ASN A 532      52.190  37.683   3.530  1.00  0.36           C  
ATOM    155  C   ASN A 532      51.752  38.069   4.929  1.00  0.36           C  
ATOM    156  O   ASN A 532      52.560  38.288   5.829  1.00  0.36           O  
ATOM    157  CB  ASN A 532      53.463  38.476   3.129  1.00  0.36           C  
ATOM    158  CG  ASN A 532      54.553  37.489   2.734  1.00  0.36           C  
ATOM    159  OD1 ASN A 532      54.856  36.549   3.473  1.00  0.36           O  
ATOM    160  ND2 ASN A 532      55.184  37.682   1.555  1.00  0.36           N  
ATOM    161  N   LYS A 533      50.436  38.163   5.195  1.00  0.34           N  
ATOM    162  CA  LYS A 533      49.976  38.135   6.566  1.00  0.34           C  
ATOM    163  C   LYS A 533      50.248  36.786   7.189  1.00  0.34           C  
ATOM    164  O   LYS A 533      50.090  35.752   6.544  1.00  0.34           O  
ATOM    165  CB  LYS A 533      48.461  38.393   6.775  1.00  0.34           C  
ATOM    166  CG  LYS A 533      48.015  39.835   6.493  1.00  0.34           C  
ATOM    167  CD  LYS A 533      46.565  40.127   6.942  1.00  0.34           C  
ATOM    168  CE  LYS A 533      46.032  41.527   6.579  1.00  0.34           C  
ATOM    169  NZ  LYS A 533      44.598  41.706   6.945  1.00  0.34           N  
ATOM    170  N   PHE A 534      50.542  36.780   8.503  1.00  0.20           N  
ATOM    171  CA  PHE A 534      50.675  35.581   9.319  1.00  0.20           C  
ATOM    172  C   PHE A 534      49.434  34.682   9.178  1.00  0.20           C  
ATOM    173  O   PHE A 534      49.544  33.468   9.041  1.00  0.20           O  
ATOM    174  CB  PHE A 534      50.926  36.027  10.797  1.00  0.20           C  
ATOM    175  CG  PHE A 534      50.677  34.953  11.832  1.00  0.20           C  
ATOM    176  CD1 PHE A 534      51.704  34.090  12.246  1.00  0.20           C  
ATOM    177  CD2 PHE A 534      49.385  34.770  12.356  1.00  0.20           C  
ATOM    178  CE1 PHE A 534      51.442  33.048  13.146  1.00  0.20           C  
ATOM    179  CE2 PHE A 534      49.116  33.723  13.242  1.00  0.20           C  
ATOM    180  CZ  PHE A 534      50.146  32.863  13.638  1.00  0.20           C  
ATOM    181  N   ALA A 535      48.233  35.313   9.143  1.00  0.32           N  
ATOM    182  CA  ALA A 535      46.919  34.712   8.965  1.00  0.32           C  
ATOM    183  C   ALA A 535      46.830  33.896   7.675  1.00  0.32           C  
ATOM    184  O   ALA A 535      46.453  32.731   7.689  1.00  0.32           O  
ATOM    185  CB  ALA A 535      45.839  35.836   8.978  1.00  0.32           C  
ATOM    186  N   LYS A 536      47.248  34.479   6.535  1.00  0.29           N  
ATOM    187  CA  LYS A 536      47.270  33.829   5.238  1.00  0.29           C  
ATOM    188  C   LYS A 536      48.264  32.689   5.133  1.00  0.29           C  
ATOM    189  O   LYS A 536      47.971  31.657   4.544  1.00  0.29           O  
ATOM    190  CB  LYS A 536      47.607  34.839   4.119  1.00  0.29           C  
ATOM    191  CG  LYS A 536      47.446  34.319   2.677  1.00  0.29           C  
ATOM    192  CD  LYS A 536      46.030  34.502   2.092  1.00  0.29           C  
ATOM    193  CE  LYS A 536      45.015  33.438   2.536  1.00  0.29           C  
ATOM    194  NZ  LYS A 536      43.703  33.615   1.880  1.00  0.29           N  
ATOM    195  N   CYS A 537      49.467  32.878   5.702  1.00  0.23           N  
ATOM    196  CA  CYS A 537      50.518  31.876   5.792  1.00  0.23           C  
ATOM    197  C   CYS A 537      50.194  30.698   6.703  1.00  0.23           C  
ATOM    198  O   CYS A 537      50.723  29.604   6.512  1.00  0.23           O  
ATOM    199  CB  CYS A 537      51.830  32.513   6.313  1.00  0.23           C  
ATOM    200  SG  CYS A 537      52.542  33.734   5.162  1.00  0.23           S  
ATOM    201  N   LEU A 538      49.393  30.926   7.761  1.00  0.20           N  
ATOM    202  CA  LEU A 538      48.881  29.914   8.671  1.00  0.20           C  
ATOM    203  C   LEU A 538      47.725  29.069   8.131  1.00  0.20           C  
ATOM    204  O   LEU A 538      47.583  27.908   8.525  1.00  0.20           O  
ATOM    205  CB  LEU A 538      48.474  30.582  10.004  1.00  0.20           C  
ATOM    206  CG  LEU A 538      47.996  29.633  11.119  1.00  0.20           C  
ATOM    207  CD1 LEU A 538      49.032  28.543  11.430  1.00  0.20           C  
ATOM    208  CD2 LEU A 538      47.656  30.428  12.384  1.00  0.20           C  
ATOM    209  N   MET A 539      46.874  29.635   7.258  1.00  0.18           N  
ATOM    210  CA  MET A 539      45.857  28.937   6.481  1.00  0.18           C  
ATOM    211  C   MET A 539      46.376  27.976   5.366  1.00  0.18           C  
ATOM    212  O   MET A 539      47.588  27.992   5.027  1.00  0.18           O  
ATOM    213  CB  MET A 539      44.893  29.970   5.834  1.00  0.18           C  
ATOM    214  CG  MET A 539      43.901  30.634   6.810  1.00  0.18           C  
ATOM    215  SD  MET A 539      42.859  29.518   7.799  1.00  0.18           S  
ATOM    216  CE  MET A 539      42.293  28.468   6.437  1.00  0.18           C  
ATOM    217  OXT MET A 539      45.530  27.190   4.848  1.00  0.18           O  
TER     218      MET A 539                                                      
END   
