TITLE     SWISS-MODEL SERVER (https://swissmodel.expasy.org)
TITLE    2 Untitled Project
EXPDTA    THEORETICAL MODEL (SWISS-MODEL SERVER)
AUTHOR    SWISS-MODEL SERVER (SEE REFERENCE IN JRNL Records)
REVDAT   1   19-APR-18 1MOD    1       11:46
JRNL        AUTH   M.BIASINI,S.BIENERT,A.WATERHOUSE,K.ARNOLD,G.STUDER,
JRNL        AUTH 2 T.SCHMIDT,F.KIEFER,T.G.CASSARINO,M.BERTONI,L.BORDOLI,
JRNL        AUTH 3 T.SCHWEDE
JRNL        TITL   SWISS-MODEL: MODELLING PROTEIN TERTIARY AND QUATERNARY
JRNL        TITL 2 STRUCTURE USING EVOLUTIONARY INFORMATION
JRNL        REF    NUCLEIC.ACIDS.RES..                        2014
JRNL        REFN                   ISSN 0305-1048
JRNL        PMID   24782522
JRNL        DOI    10.1093/nar/gku340
REMARK   1
REMARK   1 REFERENCE 1
REMARK   1  AUTH   S.BIENERT,A.WATERHOUSE,T.A.DE BEER,G.TAURIELLO,G.STUDER,
REMARK   1  AUTH 2 L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   THE SWISS-MODEL REPOSITORY - NEW FEATURES AND FUNCTIONALITY
REMARK   1  REF    NUCLEIC.ACIDS.RES..           V.  22       2017
REMARK   1  REFN                   ISSN 0305-1048
REMARK   1  PMID   27899672
REMARK   1  DOI    10.1093/nar/gkw1132
REMARK   1
REMARK   1 REFERENCE 2
REMARK   1  AUTH   N.GUEX,M.C.PEITSCH,T.SCHWEDE
REMARK   1  TITL   AUTOMATED COMPARATIVE PROTEIN STRUCTURE MODELING WITH
REMARK   1  TITL 2 SWISS-MODEL AND SWISS-PDBVIEWER: A HISTORICAL PERSPECTIVE
REMARK   1  REF    ELECTROPHORESIS               V.30         2009
REMARK   1  REFN                   ISSN 0173-0835
REMARK   1  PMID   19517507
REMARK   1  DOI    10.1002/elps.200900140
REMARK   1
REMARK   1 REFERENCE 3
REMARK   1  AUTH   P.BENKERT,M.BIASINI,T.SCHWEDE
REMARK   1  TITL   TOWARD THE ESTIMATION OF THE ABSOLUTE QUALITY OF INDIVIDUAL
REMARK   1  TITL 2 PROTEIN STRUCTURE MODELS
REMARK   1  REF    BIOINFORMATICS                V.27         2011
REMARK   1  REFN                   ISSN 1367-4803
REMARK   1  PMID   21134891
REMARK   1  DOI    10.1093/bioinformatics/btq662
REMARK   1
REMARK   1 REFERENCE 4
REMARK   1  AUTH   M.BERTONI,F.KIEFER,M.BIASINI,L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   MODELING PROTEIN QUATERNARY STRUCTURE OF HOMO- AND
REMARK   1  TITL 2 HETERO-OLIGOMERS BEYOND BINARY INTERACTIONS BY HOMOLOGY
REMARK   1  REF    SCI.REP.                      V. 7         2017
REMARK   1  REFN                   ISSN
REMARK   1  PMID   28874689
REMARK   1  DOI    10.1038/s41598-017-09654-8
REMARK   1
REMARK   1 DISCLAIMER
REMARK   1 The SWISS-MODEL SERVER produces theoretical models for proteins.
REMARK   1 The results of any theoretical modelling procedure is
REMARK   1 NON-EXPERIMENTAL and MUST be considered with care. These models may
REMARK   1 contain significant errors. This is especially true for automated
REMARK   1 modeling since there is no human intervention during model
REMARK   1 building. Please read the header section and the logfile carefully
REMARK   1 to know what templates and alignments were used during the model
REMARK   1 building process. All information by the SWISS-MODEL SERVER is
REMARK   1 provided "AS-IS", without any warranty, expressed or implied.
REMARK   2
REMARK   2 COPYRIGHT NOTICE
REMARK   2 This SWISS-MODEL protein model is copyright. It is produced by the
REMARK   2 SWISS-MODEL server, developed by the Computational Structural
REMARK   2 Biology Group at the SIB Swiss Institute of Bioinformatics at the
REMARK   2 Biozentrum, University of Basel (https://swissmodel.expasy.org). This
REMARK   2 model is licensed under the CC BY-SA 4.0 Creative Commons
REMARK   2 Attribution-ShareAlike 4.0 International License
REMARK   2 (https://creativecommons.org/licenses/by-sa/4.0/legalcode), i.e. you
REMARK   2 can copy and redistribute the model in any medium or format,
REMARK   2 transform and build upon the model for any purpose, even
REMARK   2 commercially, under the following terms:
REMARK   2 Attribution - You must give appropriate credit, provide a link to
REMARK   2 the license, and indicate if changes were made. You may do so in any
REMARK   2 reasonable manner, but not in any way that suggests the licensor
REMARK   2 endorses you or your use. When you publish, patent or distribute
REMARK   2 results that were fully or partially based on the model, please cite
REMARK   2 the corresponding papers mentioned under JRNL.
REMARK   2 ShareAlike - If you remix, transform, or build upon the material,
REMARK   2 you must distribute your contributions under the same license as the
REMARK   2 original.
REMARK   2 No additional restrictions - you may not apply legal terms or
REMARK   2 technological measures that legally restrict others from doing
REMARK   2 anything the license permits.
REMARK   2 Find a human-readable summary of (and not a substitute for) the
REMARK   2 CC BY-SA 4.0 license at this link:
REMARK   2 https://creativecommons.org/licenses/by-sa/4.0/
REMARK   3 
REMARK   3 MODEL INFORMATION
REMARK   3  ENGIN   PROMOD3
REMARK   3  VERSN   1.1.0
REMARK   3  OSTAT   monomer
REMARK   3  OSRSN   PREDICTION
REMARK   3  QSPRD   0.000
REMARK   3  GMQE    0.02
REMARK   3  QMN4    -2.32
REMARK   3  MODT    FALSE
REMARK   3 
REMARK   3 MODEL LIGAND 1
REMARK   3  NAME    ZN
REMARK   3  BIND    A.224
REMARK   3  BIND 2  A.232
REMARK   3  BIND 3  A.238
REMARK   3  BIND 4  A.242
REMARK   3  BIND 5  _.1
REMARK   3 
REMARK   3 TEMPLATE 1
REMARK   3  PDBID   2e5s
REMARK   3  CHAIN   A
REMARK   3  MMCIF   A
REMARK   3  PDBV    2018-04-13
REMARK   3  SMTLE   2e5s.1.A
REMARK   3  SMTLV   2018-04-18
REMARK   3  MTHD    SOLUTION NMR 0.00 A
REMARK   3  FOUND   HHblits
REMARK   3  GMQE    0.02
REMARK   3  SIM     0.43
REMARK   3  SID     42.86
REMARK   3  OSTAT   monomer
REMARK   3  LIGND   ZN
REMARK   3  LIGND 2 ZN
ATOM      1  N   VAL A 218      -6.158   7.314 -10.964  1.00  0.53           N  
ATOM      2  CA  VAL A 218      -5.855   6.883  -9.528  1.00  0.53           C  
ATOM      3  C   VAL A 218      -5.878   5.387  -9.205  1.00  0.53           C  
ATOM      4  O   VAL A 218      -5.342   4.961  -8.200  1.00  0.53           O  
ATOM      5  CB  VAL A 218      -6.797   7.583  -8.556  1.00  0.53           C  
ATOM      6  CG1 VAL A 218      -6.726   9.118  -8.735  1.00  0.53           C  
ATOM      7  CG2 VAL A 218      -8.238   7.033  -8.690  1.00  0.53           C  
ATOM      8  N   ARG A 219      -6.445   4.544 -10.099  1.00  0.39           N  
ATOM      9  CA  ARG A 219      -6.257   3.112 -10.108  1.00  0.39           C  
ATOM     10  C   ARG A 219      -4.998   2.592 -10.871  1.00  0.39           C  
ATOM     11  O   ARG A 219      -5.038   1.403 -11.170  1.00  0.39           O  
ATOM     12  CB  ARG A 219      -7.566   2.518 -10.714  1.00  0.39           C  
ATOM     13  CG  ARG A 219      -8.841   2.938  -9.941  1.00  0.39           C  
ATOM     14  CD  ARG A 219     -10.121   2.332 -10.526  1.00  0.39           C  
ATOM     15  NE  ARG A 219     -11.257   2.819  -9.672  1.00  0.39           N  
ATOM     16  CZ  ARG A 219     -12.540   2.491  -9.872  1.00  0.39           C  
ATOM     17  NH1 ARG A 219     -13.479   2.945  -9.046  1.00  0.39           N  
ATOM     18  NH2 ARG A 219     -12.904   1.708 -10.882  1.00  0.39           N  
ATOM     19  N   PRO A 220      -3.872   3.286 -11.227  1.00  0.41           N  
ATOM     20  CA  PRO A 220      -2.620   2.613 -11.589  1.00  0.41           C  
ATOM     21  C   PRO A 220      -1.639   2.509 -10.418  1.00  0.41           C  
ATOM     22  O   PRO A 220      -1.350   3.501  -9.754  1.00  0.41           O  
ATOM     23  CB  PRO A 220      -2.047   3.514 -12.713  1.00  0.41           C  
ATOM     24  CG  PRO A 220      -2.470   4.938 -12.330  1.00  0.41           C  
ATOM     25  CD  PRO A 220      -3.810   4.707 -11.620  1.00  0.41           C  
ATOM     26  N   ARG A 221      -1.095   1.308 -10.145  1.00  0.56           N  
ATOM     27  CA  ARG A 221      -0.059   1.103  -9.161  1.00  0.56           C  
ATOM     28  C   ARG A 221       1.244   0.830  -9.916  1.00  0.56           C  
ATOM     29  O   ARG A 221       1.276  -0.147 -10.659  1.00  0.56           O  
ATOM     30  CB  ARG A 221      -0.422  -0.113  -8.279  1.00  0.56           C  
ATOM     31  CG  ARG A 221      -1.635   0.201  -7.382  1.00  0.56           C  
ATOM     32  CD  ARG A 221      -2.009  -1.004  -6.525  1.00  0.56           C  
ATOM     33  NE  ARG A 221      -3.151  -0.649  -5.624  1.00  0.56           N  
ATOM     34  CZ  ARG A 221      -3.696  -1.486  -4.733  1.00  0.56           C  
ATOM     35  NH1 ARG A 221      -4.742  -1.089  -4.014  1.00  0.56           N  
ATOM     36  NH2 ARG A 221      -3.233  -2.719  -4.531  1.00  0.56           N  
ATOM     37  N   PRO A 222       2.332   1.587  -9.820  1.00  0.58           N  
ATOM     38  CA  PRO A 222       3.628   1.196 -10.384  1.00  0.58           C  
ATOM     39  C   PRO A 222       4.124  -0.144  -9.839  1.00  0.58           C  
ATOM     40  O   PRO A 222       4.286  -0.320  -8.630  1.00  0.58           O  
ATOM     41  CB  PRO A 222       4.543   2.395 -10.068  1.00  0.58           C  
ATOM     42  CG  PRO A 222       3.940   2.983  -8.786  1.00  0.58           C  
ATOM     43  CD  PRO A 222       2.442   2.690  -8.880  1.00  0.58           C  
ATOM     44  N   THR A 223       4.344  -1.119 -10.740  1.00  0.65           N  
ATOM     45  CA  THR A 223       4.995  -2.393 -10.473  1.00  0.65           C  
ATOM     46  C   THR A 223       6.440  -2.241 -10.065  1.00  0.65           C  
ATOM     47  O   THR A 223       7.221  -1.551 -10.723  1.00  0.65           O  
ATOM     48  CB  THR A 223       4.900  -3.326 -11.670  1.00  0.65           C  
ATOM     49  OG1 THR A 223       3.545  -3.721 -11.781  1.00  0.65           O  
ATOM     50  CG2 THR A 223       5.711  -4.612 -11.472  1.00  0.65           C  
ATOM     51  N   CYS A 224       6.864  -2.890  -8.958  1.00  0.64           N  
ATOM     52  CA  CYS A 224       8.262  -2.858  -8.547  1.00  0.64           C  
ATOM     53  C   CYS A 224       9.239  -3.520  -9.524  1.00  0.64           C  
ATOM     54  O   CYS A 224       8.975  -4.571 -10.106  1.00  0.64           O  
ATOM     55  CB  CYS A 224       8.488  -3.503  -7.153  1.00  0.64           C  
ATOM     56  SG  CYS A 224      10.001  -2.963  -6.277  1.00  0.64           S  
ATOM     57  N   ARG A 225      10.440  -2.939  -9.695  1.00  0.61           N  
ATOM     58  CA  ARG A 225      11.460  -3.487 -10.569  1.00  0.61           C  
ATOM     59  C   ARG A 225      12.253  -4.615  -9.938  1.00  0.61           C  
ATOM     60  O   ARG A 225      12.614  -5.585 -10.593  1.00  0.61           O  
ATOM     61  CB  ARG A 225      12.435  -2.364 -10.976  1.00  0.61           C  
ATOM     62  CG  ARG A 225      11.814  -1.430 -12.038  1.00  0.61           C  
ATOM     63  CD  ARG A 225      11.861  -1.968 -13.482  1.00  0.61           C  
ATOM     64  NE  ARG A 225      13.316  -2.069 -13.884  1.00  0.61           N  
ATOM     65  CZ  ARG A 225      14.090  -1.037 -14.258  1.00  0.61           C  
ATOM     66  NH1 ARG A 225      13.615   0.199 -14.336  1.00  0.61           N  
ATOM     67  NH2 ARG A 225      15.374  -1.237 -14.557  1.00  0.61           N  
ATOM     68  N   PHE A 226      12.582  -4.489  -8.637  1.00  0.58           N  
ATOM     69  CA  PHE A 226      13.373  -5.476  -7.929  1.00  0.58           C  
ATOM     70  C   PHE A 226      12.700  -6.811  -7.682  1.00  0.58           C  
ATOM     71  O   PHE A 226      13.303  -7.851  -7.909  1.00  0.58           O  
ATOM     72  CB  PHE A 226      13.746  -4.935  -6.539  1.00  0.58           C  
ATOM     73  CG  PHE A 226      14.848  -3.942  -6.661  1.00  0.58           C  
ATOM     74  CD1 PHE A 226      14.688  -2.691  -6.061  1.00  0.58           C  
ATOM     75  CD2 PHE A 226      16.065  -4.252  -7.301  1.00  0.58           C  
ATOM     76  CE1 PHE A 226      15.723  -1.753  -6.096  1.00  0.58           C  
ATOM     77  CE2 PHE A 226      17.110  -3.325  -7.318  1.00  0.58           C  
ATOM     78  CZ  PHE A 226      16.936  -2.072  -6.721  1.00  0.58           C  
ATOM     79  N   PHE A 227      11.424  -6.785  -7.222  1.00  0.58           N  
ATOM     80  CA  PHE A 227      10.532  -7.917  -6.979  1.00  0.58           C  
ATOM     81  C   PHE A 227      10.413  -8.843  -8.187  1.00  0.58           C  
ATOM     82  O   PHE A 227      10.421 -10.046  -8.029  1.00  0.58           O  
ATOM     83  CB  PHE A 227       9.114  -7.397  -6.537  1.00  0.58           C  
ATOM     84  CG  PHE A 227       7.970  -8.362  -6.818  1.00  0.58           C  
ATOM     85  CD1 PHE A 227       7.347  -8.331  -8.082  1.00  0.58           C  
ATOM     86  CD2 PHE A 227       7.655  -9.410  -5.937  1.00  0.58           C  
ATOM     87  CE1 PHE A 227       6.510  -9.374  -8.485  1.00  0.58           C  
ATOM     88  CE2 PHE A 227       6.724 -10.395  -6.302  1.00  0.58           C  
ATOM     89  CZ  PHE A 227       6.173 -10.388  -7.586  1.00  0.58           C  
ATOM     90  N   MET A 228      10.320  -8.267  -9.410  1.00  0.56           N  
ATOM     91  CA  MET A 228      10.163  -8.975 -10.665  1.00  0.56           C  
ATOM     92  C   MET A 228      11.236 -10.040 -10.888  1.00  0.56           C  
ATOM     93  O   MET A 228      10.978 -11.146 -11.335  1.00  0.56           O  
ATOM     94  CB  MET A 228      10.298  -7.905 -11.778  1.00  0.56           C  
ATOM     95  CG  MET A 228      10.262  -8.462 -13.213  1.00  0.56           C  
ATOM     96  SD  MET A 228      10.574  -7.212 -14.500  1.00  0.56           S  
ATOM     97  CE  MET A 228      12.350  -6.995 -14.176  1.00  0.56           C  
ATOM     98  N   LYS A 229      12.495  -9.696 -10.548  1.00  0.63           N  
ATOM     99  CA  LYS A 229      13.588 -10.648 -10.546  1.00  0.63           C  
ATOM    100  C   LYS A 229      13.822 -11.297  -9.184  1.00  0.63           C  
ATOM    101  O   LYS A 229      14.507 -12.313  -9.101  1.00  0.63           O  
ATOM    102  CB  LYS A 229      14.884  -9.897 -10.919  1.00  0.63           C  
ATOM    103  CG  LYS A 229      14.896  -9.368 -12.354  1.00  0.63           C  
ATOM    104  CD  LYS A 229      16.229  -8.674 -12.675  1.00  0.63           C  
ATOM    105  CE  LYS A 229      16.291  -8.169 -14.118  1.00  0.63           C  
ATOM    106  NZ  LYS A 229      17.591  -7.509 -14.386  1.00  0.63           N  
ATOM    107  N   GLY A 230      13.261 -10.737  -8.091  1.00  0.63           N  
ATOM    108  CA  GLY A 230      13.350 -11.270  -6.732  1.00  0.63           C  
ATOM    109  C   GLY A 230      14.401 -10.630  -5.850  1.00  0.63           C  
ATOM    110  O   GLY A 230      15.079 -11.296  -5.086  1.00  0.63           O  
ATOM    111  N   ASN A 231      14.542  -9.290  -5.914  1.00  0.58           N  
ATOM    112  CA  ASN A 231      15.563  -8.534  -5.193  1.00  0.58           C  
ATOM    113  C   ASN A 231      14.945  -7.667  -4.096  1.00  0.58           C  
ATOM    114  O   ASN A 231      15.586  -6.762  -3.575  1.00  0.58           O  
ATOM    115  CB  ASN A 231      16.341  -7.585  -6.145  1.00  0.58           C  
ATOM    116  CG  ASN A 231      17.165  -8.360  -7.159  1.00  0.58           C  
ATOM    117  OD1 ASN A 231      18.290  -8.771  -6.931  1.00  0.58           O  
ATOM    118  ND2 ASN A 231      16.584  -8.537  -8.369  1.00  0.58           N  
ATOM    119  N   CYS A 232      13.672  -7.893  -3.711  1.00  0.58           N  
ATOM    120  CA  CYS A 232      13.073  -7.199  -2.580  1.00  0.58           C  
ATOM    121  C   CYS A 232      12.108  -8.186  -1.951  1.00  0.58           C  
ATOM    122  O   CYS A 232      11.352  -8.842  -2.670  1.00  0.58           O  
ATOM    123  CB  CYS A 232      12.330  -5.878  -2.994  1.00  0.58           C  
ATOM    124  SG  CYS A 232      11.645  -4.893  -1.609  1.00  0.58           S  
ATOM    125  N   THR A 233      12.125  -8.362  -0.617  1.00  0.57           N  
ATOM    126  CA  THR A 233      11.444  -9.470   0.059  1.00  0.57           C  
ATOM    127  C   THR A 233      10.655  -8.973   1.230  1.00  0.57           C  
ATOM    128  O   THR A 233      10.346  -9.723   2.155  1.00  0.57           O  
ATOM    129  CB  THR A 233      12.389 -10.563   0.542  1.00  0.57           C  
ATOM    130  OG1 THR A 233      13.514 -10.014   1.210  1.00  0.57           O  
ATOM    131  CG2 THR A 233      12.922 -11.285  -0.698  1.00  0.57           C  
ATOM    132  N   TRP A 234      10.243  -7.697   1.218  1.00  0.54           N  
ATOM    133  CA  TRP A 234       9.343  -7.226   2.241  1.00  0.54           C  
ATOM    134  C   TRP A 234       8.049  -6.789   1.606  1.00  0.54           C  
ATOM    135  O   TRP A 234       7.881  -5.625   1.233  1.00  0.54           O  
ATOM    136  CB  TRP A 234       9.957  -6.054   3.024  1.00  0.54           C  
ATOM    137  CG  TRP A 234      11.224  -6.360   3.789  1.00  0.54           C  
ATOM    138  CD1 TRP A 234      12.520  -6.120   3.422  1.00  0.54           C  
ATOM    139  CD2 TRP A 234      11.289  -6.949   5.109  1.00  0.54           C  
ATOM    140  NE1 TRP A 234      13.392  -6.520   4.416  1.00  0.54           N  
ATOM    141  CE2 TRP A 234      12.639  -7.033   5.456  1.00  0.54           C  
ATOM    142  CE3 TRP A 234      10.284  -7.390   5.977  1.00  0.54           C  
ATOM    143  CZ2 TRP A 234      13.042  -7.564   6.681  1.00  0.54           C  
ATOM    144  CZ3 TRP A 234      10.684  -7.907   7.223  1.00  0.54           C  
ATOM    145  CH2 TRP A 234      12.039  -7.998   7.567  1.00  0.54           C  
ATOM    146  N   GLY A 235       7.094  -7.737   1.487  1.00  0.57           N  
ATOM    147  CA  GLY A 235       5.865  -7.606   0.703  1.00  0.57           C  
ATOM    148  C   GLY A 235       5.024  -6.368   0.931  1.00  0.57           C  
ATOM    149  O   GLY A 235       4.540  -5.759  -0.007  1.00  0.57           O  
ATOM    150  N   MET A 236       4.851  -5.976   2.213  1.00  0.51           N  
ATOM    151  CA  MET A 236       4.006  -4.868   2.632  1.00  0.51           C  
ATOM    152  C   MET A 236       4.819  -3.679   3.127  1.00  0.51           C  
ATOM    153  O   MET A 236       4.264  -2.719   3.654  1.00  0.51           O  
ATOM    154  CB  MET A 236       3.028  -5.329   3.752  1.00  0.51           C  
ATOM    155  CG  MET A 236       2.013  -6.381   3.254  1.00  0.51           C  
ATOM    156  SD  MET A 236       0.970  -5.832   1.861  1.00  0.51           S  
ATOM    157  CE  MET A 236       0.036  -4.566   2.769  1.00  0.51           C  
ATOM    158  N   ASN A 237       6.157  -3.674   2.931  1.00  0.61           N  
ATOM    159  CA  ASN A 237       6.988  -2.541   3.325  1.00  0.61           C  
ATOM    160  C   ASN A 237       7.303  -1.720   2.090  1.00  0.61           C  
ATOM    161  O   ASN A 237       8.021  -0.721   2.167  1.00  0.61           O  
ATOM    162  CB  ASN A 237       8.345  -2.995   3.928  1.00  0.61           C  
ATOM    163  CG  ASN A 237       8.133  -3.718   5.253  1.00  0.61           C  
ATOM    164  OD1 ASN A 237       7.047  -3.820   5.808  1.00  0.61           O  
ATOM    165  ND2 ASN A 237       9.232  -4.296   5.793  1.00  0.61           N  
ATOM    166  N   CYS A 238       6.780  -2.103   0.909  1.00  0.60           N  
ATOM    167  CA  CYS A 238       7.069  -1.423  -0.332  1.00  0.60           C  
ATOM    168  C   CYS A 238       5.791  -0.783  -0.825  1.00  0.60           C  
ATOM    169  O   CYS A 238       4.725  -1.389  -0.817  1.00  0.60           O  
ATOM    170  CB  CYS A 238       7.636  -2.385  -1.416  1.00  0.60           C  
ATOM    171  SG  CYS A 238       8.462  -1.509  -2.794  1.00  0.60           S  
ATOM    172  N   ARG A 239       5.872   0.492  -1.248  1.00  0.56           N  
ATOM    173  CA  ARG A 239       4.764   1.231  -1.822  1.00  0.56           C  
ATOM    174  C   ARG A 239       4.333   0.710  -3.188  1.00  0.56           C  
ATOM    175  O   ARG A 239       3.168   0.758  -3.562  1.00  0.56           O  
ATOM    176  CB  ARG A 239       5.156   2.716  -1.993  1.00  0.56           C  
ATOM    177  CG  ARG A 239       5.587   3.410  -0.686  1.00  0.56           C  
ATOM    178  CD  ARG A 239       5.951   4.872  -0.953  1.00  0.56           C  
ATOM    179  NE  ARG A 239       6.715   5.391   0.234  1.00  0.56           N  
ATOM    180  CZ  ARG A 239       7.526   6.458   0.184  1.00  0.56           C  
ATOM    181  NH1 ARG A 239       7.693   7.137  -0.946  1.00  0.56           N  
ATOM    182  NH2 ARG A 239       8.184   6.860   1.269  1.00  0.56           N  
ATOM    183  N   PHE A 240       5.318   0.247  -3.986  1.00  0.57           N  
ATOM    184  CA  PHE A 240       5.107  -0.358  -5.284  1.00  0.57           C  
ATOM    185  C   PHE A 240       4.404  -1.691  -5.158  1.00  0.57           C  
ATOM    186  O   PHE A 240       4.751  -2.506  -4.304  1.00  0.57           O  
ATOM    187  CB  PHE A 240       6.454  -0.539  -6.040  1.00  0.57           C  
ATOM    188  CG  PHE A 240       7.263   0.739  -6.035  1.00  0.57           C  
ATOM    189  CD1 PHE A 240       6.659   1.986  -6.273  1.00  0.57           C  
ATOM    190  CD2 PHE A 240       8.643   0.711  -5.768  1.00  0.57           C  
ATOM    191  CE1 PHE A 240       7.414   3.162  -6.317  1.00  0.57           C  
ATOM    192  CE2 PHE A 240       9.402   1.890  -5.774  1.00  0.57           C  
ATOM    193  CZ  PHE A 240       8.789   3.114  -6.069  1.00  0.57           C  
ATOM    194  N   ILE A 241       3.397  -1.958  -6.010  1.00  0.56           N  
ATOM    195  CA  ILE A 241       2.707  -3.238  -6.031  1.00  0.56           C  
ATOM    196  C   ILE A 241       3.601  -4.363  -6.547  1.00  0.56           C  
ATOM    197  O   ILE A 241       4.431  -4.178  -7.446  1.00  0.56           O  
ATOM    198  CB  ILE A 241       1.372  -3.178  -6.777  1.00  0.56           C  
ATOM    199  CG1 ILE A 241       0.527  -4.457  -6.529  1.00  0.56           C  
ATOM    200  CG2 ILE A 241       1.631  -2.884  -8.277  1.00  0.56           C  
ATOM    201  CD1 ILE A 241      -0.929  -4.346  -6.993  1.00  0.56           C  
ATOM    202  N   HIS A 242       3.487  -5.573  -5.967  1.00  0.58           N  
ATOM    203  CA  HIS A 242       4.273  -6.722  -6.366  1.00  0.58           C  
ATOM    204  C   HIS A 242       3.352  -7.721  -7.087  1.00  0.58           C  
ATOM    205  O   HIS A 242       2.482  -8.292  -6.431  1.00  0.58           O  
ATOM    206  CB  HIS A 242       4.983  -7.337  -5.140  1.00  0.58           C  
ATOM    207  CG  HIS A 242       6.086  -6.442  -4.616  1.00  0.58           C  
ATOM    208  ND1 HIS A 242       7.062  -6.981  -3.787  1.00  0.58           N  
ATOM    209  CD2 HIS A 242       6.343  -5.129  -4.811  1.00  0.58           C  
ATOM    210  CE1 HIS A 242       7.865  -5.988  -3.503  1.00  0.58           C  
ATOM    211  NE2 HIS A 242       7.478  -4.825  -4.091  1.00  0.58           N  
ATOM    212  N   PRO A 243       3.433  -7.939  -8.408  1.00  0.57           N  
ATOM    213  CA  PRO A 243       2.441  -8.737  -9.129  1.00  0.57           C  
ATOM    214  C   PRO A 243       2.899 -10.179  -9.093  1.00  0.57           C  
ATOM    215  O   PRO A 243       3.883 -10.503  -9.737  1.00  0.57           O  
ATOM    216  CB  PRO A 243       2.497  -8.235 -10.600  1.00  0.57           C  
ATOM    217  CG  PRO A 243       3.848  -7.522 -10.714  1.00  0.57           C  
ATOM    218  CD  PRO A 243       4.066  -6.981  -9.305  1.00  0.57           C  
ATOM    219  N   GLY A 244       2.223 -11.063  -8.328  1.00  0.46           N  
ATOM    220  CA  GLY A 244       2.654 -12.456  -8.179  1.00  0.46           C  
ATOM    221  C   GLY A 244       2.523 -13.353  -9.390  1.00  0.46           C  
ATOM    222  O   GLY A 244       3.259 -14.325  -9.511  1.00  0.46           O  
ATOM    223  N   VAL A 245       1.538 -13.036 -10.253  1.00  0.37           N  
ATOM    224  CA  VAL A 245       1.298 -13.601 -11.570  1.00  0.37           C  
ATOM    225  C   VAL A 245       2.256 -12.960 -12.611  1.00  0.37           C  
ATOM    226  O   VAL A 245       2.698 -11.798 -12.409  1.00  0.37           O  
ATOM    227  CB  VAL A 245      -0.181 -13.389 -11.971  1.00  0.37           C  
ATOM    228  CG1 VAL A 245      -0.499 -13.974 -13.365  1.00  0.37           C  
ATOM    229  CG2 VAL A 245      -1.101 -14.068 -10.932  1.00  0.37           C  
ATOM    230  OXT VAL A 245       2.554 -13.638 -13.631  1.00  0.37           O  
TER     231      VAL A 245                                                      
HETATM  232 ZN    ZN _   1       9.030  -3.549  -4.032  1.00  0.00          ZN  
END   
