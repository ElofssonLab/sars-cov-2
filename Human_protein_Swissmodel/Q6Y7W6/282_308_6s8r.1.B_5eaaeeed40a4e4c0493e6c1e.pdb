TITLE     SWISS-MODEL SERVER (https://swissmodel.expasy.org)
TITLE    2 Untitled Project
EXPDTA    THEORETICAL MODEL (SWISS-MODEL SERVER)
AUTHOR    SWISS-MODEL SERVER (SEE REFERENCE IN JRNL Records)
REVDAT   1   30-APR-20 1MOD    1       17:25
JRNL        AUTH   A.WATERHOUSE,M.BERTONI,S.BIENERT,G.STUDER,G.TAURIELLO,
JRNL        AUTH 2 R.GUMIENNY,F.T.HEER,T.A.P.DE BEER,C.REMPFER,L.BORDOLI,
JRNL        AUTH 3 R.LEPORE,T.SCHWEDE
JRNL        TITL   SWISS-MODEL: HOMOLOGY MODELLING OF PROTEIN STRUCTURES AND
JRNL        TITL 2 COMPLEXES
JRNL        REF    NUCLEIC.ACIDS.RES..           V.  46 W296  2018
JRNL        PMID   29788355
JRNL        DOI    10.1093/nar/gky427
REMARK   1
REMARK   1 REFERENCE 1
REMARK   1  AUTH   S.BIENERT,A.WATERHOUSE,T.A.P.DE BEER,G.TAURIELLO,G.STUDER,
REMARK   1  AUTH 2 L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   THE SWISS-MODEL REPOSITORY - NEW FEATURES AND FUNCTIONALITY
REMARK   1  REF    NUCLEIC.ACIDS.RES..           V.  22       2017
REMARK   1  REFN                   ISSN 0305-1048
REMARK   1  PMID   27899672
REMARK   1  DOI    10.1093/nar/gkw1132
REMARK   1
REMARK   1 REFERENCE 2
REMARK   1  AUTH   N.GUEX,M.C.PEITSCH,T.SCHWEDE
REMARK   1  TITL   AUTOMATED COMPARATIVE PROTEIN STRUCTURE MODELING WITH
REMARK   1  TITL 2 SWISS-MODEL AND SWISS-PDBVIEWER: A HISTORICAL PERSPECTIVE
REMARK   1  REF    ELECTROPHORESIS               V.  30       2009
REMARK   1  REFN                   ISSN 0173-0835
REMARK   1  PMID   19517507
REMARK   1  DOI    10.1002/elps.200900140
REMARK   1
REMARK   1 REFERENCE 3
REMARK   1  AUTH   P.BENKERT,M.BIASINI,T.SCHWEDE
REMARK   1  TITL   TOWARD THE ESTIMATION OF THE ABSOLUTE QUALITY OF INDIVIDUAL
REMARK   1  TITL 2 PROTEIN STRUCTURE MODELS
REMARK   1  REF    BIOINFORMATICS                V.  27       2011
REMARK   1  REFN                   ISSN 1367-4803
REMARK   1  PMID   21134891
REMARK   1  DOI    10.1093/bioinformatics/btq662
REMARK   1
REMARK   1 REFERENCE 4
REMARK   1  AUTH   M.BERTONI,F.KIEFER,M.BIASINI,L.BORDOLI,T.SCHWEDE
REMARK   1  TITL   MODELING PROTEIN QUATERNARY STRUCTURE OF HOMO- AND
REMARK   1  TITL 2 HETERO-OLIGOMERS BEYOND BINARY INTERACTIONS BY HOMOLOGY
REMARK   1  REF    SCI.REP.                      V.   7       2017
REMARK   1  REFN                   ISSN
REMARK   1  PMID   28874689
REMARK   1  DOI    10.1038/s41598-017-09654-8
REMARK   1
REMARK   1 DISCLAIMER
REMARK   1 The SWISS-MODEL SERVER produces theoretical models for proteins.
REMARK   1 The results of any theoretical modelling procedure is
REMARK   1 NON-EXPERIMENTAL and MUST be considered with care. These models may
REMARK   1 contain significant errors. This is especially true for automated
REMARK   1 modeling since there is no human intervention during model
REMARK   1 building. Please read the header section and the logfile carefully
REMARK   1 to know what templates and alignments were used during the model
REMARK   1 building process. All information by the SWISS-MODEL SERVER is
REMARK   1 provided "AS-IS", without any warranty, expressed or implied.
REMARK   2
REMARK   2 COPYRIGHT NOTICE
REMARK   2 This SWISS-MODEL protein model is copyright. It is produced by the
REMARK   2 SWISS-MODEL server, developed by the Computational Structural
REMARK   2 Biology Group at the SIB Swiss Institute of Bioinformatics at the
REMARK   2 Biozentrum, University of Basel (https://swissmodel.expasy.org). This
REMARK   2 model is licensed under the CC BY-SA 4.0 Creative Commons
REMARK   2 Attribution-ShareAlike 4.0 International License
REMARK   2 (https://creativecommons.org/licenses/by-sa/4.0/legalcode), i.e. you
REMARK   2 can copy and redistribute the model in any medium or format,
REMARK   2 transform and build upon the model for any purpose, even
REMARK   2 commercially, under the following terms:
REMARK   2 Attribution - You must give appropriate credit, provide a link to
REMARK   2 the license, and indicate if changes were made. You may do so in any
REMARK   2 reasonable manner, but not in any way that suggests the licensor
REMARK   2 endorses you or your use. When you publish, patent or distribute
REMARK   2 results that were fully or partially based on the model, please cite
REMARK   2 the corresponding papers mentioned under JRNL.
REMARK   2 ShareAlike - If you remix, transform, or build upon the material,
REMARK   2 you must distribute your contributions under the same license as the
REMARK   2 original.
REMARK   2 No additional restrictions - you may not apply legal terms or
REMARK   2 technological measures that legally restrict others from doing
REMARK   2 anything the license permits.
REMARK   2 Find a human-readable summary of (and not a substitute for) the
REMARK   2 CC BY-SA 4.0 license at this link:
REMARK   2 https://creativecommons.org/licenses/by-sa/4.0/
REMARK   3 
REMARK   3 MODEL INFORMATION
REMARK   3  ENGIN   PROMOD3
REMARK   3  VERSN   3.0.0
REMARK   3  OSTAT   monomer
REMARK   3  OSRSN   PREDICTION
REMARK   3  QSPRD   0.000
REMARK   3  QMN4    -0.53
REMARK   3  MODT    FALSE
REMARK   3 
REMARK   3 TEMPLATE 1
REMARK   3  PDBID   6s8r
REMARK   3  CHAIN   B
REMARK   3  MMCIF   B
REMARK   3  PDBV    2020-04-24
REMARK   3  SMTLE   6s8r.1.B
REMARK   3  SMTLV   2020-04-29
REMARK   3  MTHD    X-RAY DIFFRACTION 2.41 A
REMARK   3  FOUND   HHblits
REMARK   3  SIM     0.43
REMARK   3  SID     44.44
REMARK   3  OSTAT   monomer
REMARK   3  ALN B TRG MAAETQTLNFGPEWLRALSSGGSITSPPLSPALPKYKLADYRYGREEMLALFLKDNKI
REMARK   3  ALN B TRG PSDLLDKEFLPILQEEPLPPLALVPFTEEEQRNFSMSVNSAAVLRLTGRGGGGTVVGA
REMARK   3  ALN B TRG PRGRSSSRGRGRGRGECGFYQRSFDEVEGVFGRGGGREMHRSQSWEERGDRRFEKPGR
REMARK   3  ALN B TRG KDVGRPNFEEGGPTSVGRKHEFIRSESENWRIFREEQNGEDEDGGWRLAGSRRDGERW
REMARK   3  ALN B TRG RPHSPDGPRSAGWREHMERRRRFEFDFRDRDDERGYRRVRSGSGSIDDDRDSLPEWCL
REMARK   3  ALN B TRG EDAEEEMGTFDSSGAFLSLKKVQKEPIPEEQEMDFRPVDEGEECSDSEGSHNEEAKEP
REMARK   3  ALN B TRG DKTNKKEGEKTDRVGVEASEETPQTSSSSARPGTPSDHQSQEASQFERKDEPKTEQTE
REMARK   3  ALN B TRG KAEEETRMENSLPAKVPSRGDEMVADVQQPLSQIPSDTASPLLILPPPVPNPSPTLRP
REMARK   3  ALN B TRG VETPVVGAPGMGSVSTEPDDEEGLKHLEQQAEKMVAYLQDSALDDERLASKLQEHRAK
REMARK   3  ALN B TRG GVSIPLMHEAMQKWYYKDPQGEIQGPFNNQEMAEWFQAGYFTMSLLVKRACDESFQPL
REMARK   3  ALN B TRG GDIMKMWGRVPFSPGPAPPPHMGELDQERLTRQQELTALYQMQHLQYQQFLIQQQYAQ
REMARK   3  ALN B TRG VLAQQQKAALSSQQQQQLALLLQQFQTLKMRISDQNIIPSVTRSVSVPDTGSIWELQP
REMARK   3  ALN B TRG TASQPTVWEGGSVWDLPLDTTTPGPALEQLQQLEKAKAAKLEQERREAEMRAKREEEE
REMARK   3  ALN B TRG RKRQEELRRQQEEILRRQQEEERKRREEEELARRKQEEALRRQREQEIALRRQREEEE
REMARK   3  ALN B TRG RQQQEEALRRLEERRREEEERRKQEELLRKQEEEAAKWAREEEEAQRRLEENRLRMEE
REMARK   3  ALN B TRG EAARLRHEEEERKRKELEVQRQKELMRQRQQQQEALRRLQQQQQQQQLAQMKLPSSST
REMARK   3  ALN B TRG WGQQSNTTACQSQATLSLAEIQKLEEERERQLREEQRRQQRELMKALQQQQQQQQQKL
REMARK   3  ALN B TRG SGWGNVSKPSGTTKSLLEIQQEEARQMQKQQQQQQQHQQPNRARNNTHSNLHTSIGNS
REMARK   3  ALN B TRG VWGSINTGPPNQWASDLVSSIWSNADTKNSNMGFWDDAVKEVGPRNSTNKNKNNASLS
REMARK   3  ALN B TRG KSVGVSNRQNKKVEEEEKLLKLFQGVNKAQDGFTQWCEQMLHALNTANNLDVPTFVSF
REMARK   3  ALN B TRG LKEVESPYEVHDYIRAYLGDTSEAKEFAKQFLERRAKQKANQQRQQQQLPQQQQQQPP
REMARK   3  ALN B TRG QQPPQQPQQQDSVWGMNHSTLHSVFQTNQSNNQQSNFEAVQSGKKKKKQKMVRADPSL
REMARK   3  ALN B TRG LGFSVNASSERLNMGEIETLDDY
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL -------------------------------------------------DENLPEWAI
REMARK   3  ALN B TPL ENPSKLGGSFDASGAFHG----------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL ----------------------------------------------------------
REMARK   3  ALN B TPL -----------------------
REMARK   3  ALN B OFF 0
ATOM      1  N   ARG B 282     -23.344   9.930  21.128  1.00  0.10           N  
ATOM      2  CA  ARG B 282     -22.872  10.819  22.257  1.00  0.10           C  
ATOM      3  C   ARG B 282     -21.645  10.191  22.884  1.00  0.10           C  
ATOM      4  O   ARG B 282     -21.600   8.968  22.929  1.00  0.10           O  
ATOM      5  CB  ARG B 282     -23.999  10.970  23.331  1.00  0.10           C  
ATOM      6  CG  ARG B 282     -25.199  11.837  22.888  1.00  0.10           C  
ATOM      7  CD  ARG B 282     -26.266  12.014  23.981  1.00  0.10           C  
ATOM      8  NE  ARG B 282     -27.346  12.883  23.399  1.00  0.10           N  
ATOM      9  CZ  ARG B 282     -28.485  13.200  24.035  1.00  0.10           C  
ATOM     10  NH1 ARG B 282     -29.379  13.989  23.442  1.00  0.10           N  
ATOM     11  NH2 ARG B 282     -28.751  12.751  25.257  1.00  0.10           N  
ATOM     12  N   ASP B 283     -20.648  10.978  23.345  1.00  0.17           N  
ATOM     13  CA  ASP B 283     -19.469  10.482  24.014  1.00  0.17           C  
ATOM     14  C   ASP B 283     -19.837  10.406  25.499  1.00  0.17           C  
ATOM     15  O   ASP B 283     -20.195  11.402  26.127  1.00  0.17           O  
ATOM     16  CB  ASP B 283     -18.296  11.442  23.643  1.00  0.17           C  
ATOM     17  CG  ASP B 283     -16.923  10.934  24.057  1.00  0.17           C  
ATOM     18  OD1 ASP B 283     -16.848   9.809  24.601  1.00  0.17           O  
ATOM     19  OD2 ASP B 283     -15.941  11.671  23.783  1.00  0.17           O  
ATOM     20  N   SER B 284     -19.889   9.174  26.044  1.00  0.50           N  
ATOM     21  CA  SER B 284     -20.191   8.895  27.434  1.00  0.50           C  
ATOM     22  C   SER B 284     -18.896   8.594  28.136  1.00  0.50           C  
ATOM     23  O   SER B 284     -17.886   8.290  27.515  1.00  0.50           O  
ATOM     24  CB  SER B 284     -21.165   7.691  27.645  1.00  0.50           C  
ATOM     25  OG  SER B 284     -20.687   6.477  27.057  1.00  0.50           O  
ATOM     26  N   LEU B 285     -18.860   8.652  29.480  1.00  0.45           N  
ATOM     27  CA  LEU B 285     -17.676   8.213  30.189  1.00  0.45           C  
ATOM     28  C   LEU B 285     -17.539   6.681  30.045  1.00  0.45           C  
ATOM     29  O   LEU B 285     -18.497   6.000  30.415  1.00  0.45           O  
ATOM     30  CB  LEU B 285     -17.765   8.614  31.682  1.00  0.45           C  
ATOM     31  CG  LEU B 285     -16.487   8.334  32.499  1.00  0.45           C  
ATOM     32  CD1 LEU B 285     -15.295   9.187  32.021  1.00  0.45           C  
ATOM     33  CD2 LEU B 285     -16.757   8.574  33.993  1.00  0.45           C  
ATOM     34  N   PRO B 286     -16.470   6.060  29.528  1.00  0.45           N  
ATOM     35  CA  PRO B 286     -16.403   4.609  29.352  1.00  0.45           C  
ATOM     36  C   PRO B 286     -16.364   3.861  30.671  1.00  0.45           C  
ATOM     37  O   PRO B 286     -15.705   4.344  31.589  1.00  0.45           O  
ATOM     38  CB  PRO B 286     -15.062   4.373  28.623  1.00  0.45           C  
ATOM     39  CG  PRO B 286     -14.762   5.697  27.920  1.00  0.45           C  
ATOM     40  CD  PRO B 286     -15.349   6.737  28.876  1.00  0.45           C  
ATOM     41  N   GLU B 287     -16.995   2.664  30.783  1.00  0.42           N  
ATOM     42  CA  GLU B 287     -17.001   1.859  32.000  1.00  0.42           C  
ATOM     43  C   GLU B 287     -15.578   1.532  32.485  1.00  0.42           C  
ATOM     44  O   GLU B 287     -15.203   1.788  33.619  1.00  0.42           O  
ATOM     45  CB  GLU B 287     -17.878   0.590  31.783  1.00  0.42           C  
ATOM     46  CG  GLU B 287     -19.383   0.932  31.573  1.00  0.42           C  
ATOM     47  CD  GLU B 287     -20.312  -0.279  31.723  1.00  0.42           C  
ATOM     48  OE1 GLU B 287     -20.302  -0.896  32.815  1.00  0.42           O  
ATOM     49  OE2 GLU B 287     -21.058  -0.567  30.752  1.00  0.42           O  
ATOM     50  N   TRP B 288     -14.675   1.140  31.560  1.00  0.34           N  
ATOM     51  CA  TRP B 288     -13.310   0.733  31.859  1.00  0.34           C  
ATOM     52  C   TRP B 288     -12.400   1.879  32.315  1.00  0.34           C  
ATOM     53  O   TRP B 288     -11.319   1.638  32.841  1.00  0.34           O  
ATOM     54  CB  TRP B 288     -12.670   0.006  30.628  1.00  0.34           C  
ATOM     55  CG  TRP B 288     -12.952   0.638  29.262  1.00  0.34           C  
ATOM     56  CD1 TRP B 288     -13.958   0.324  28.389  1.00  0.34           C  
ATOM     57  CD2 TRP B 288     -12.213   1.713  28.639  1.00  0.34           C  
ATOM     58  NE1 TRP B 288     -13.912   1.136  27.274  1.00  0.34           N  
ATOM     59  CE2 TRP B 288     -12.843   1.998  27.415  1.00  0.34           C  
ATOM     60  CE3 TRP B 288     -11.088   2.425  29.048  1.00  0.34           C  
ATOM     61  CZ2 TRP B 288     -12.375   3.014  26.581  1.00  0.34           C  
ATOM     62  CZ3 TRP B 288     -10.592   3.419  28.190  1.00  0.34           C  
ATOM     63  CH2 TRP B 288     -11.239   3.731  26.989  1.00  0.34           C  
ATOM     64  N   CYS B 289     -12.815   3.160  32.143  1.00  0.44           N  
ATOM     65  CA  CYS B 289     -12.076   4.313  32.651  1.00  0.44           C  
ATOM     66  C   CYS B 289     -12.365   4.554  34.126  1.00  0.44           C  
ATOM     67  O   CYS B 289     -11.704   5.374  34.757  1.00  0.44           O  
ATOM     68  CB  CYS B 289     -12.425   5.643  31.904  1.00  0.44           C  
ATOM     69  SG  CYS B 289     -11.577   5.883  30.313  1.00  0.44           S  
ATOM     70  N   LEU B 290     -13.383   3.878  34.699  1.00  0.43           N  
ATOM     71  CA  LEU B 290     -13.781   4.061  36.075  1.00  0.43           C  
ATOM     72  C   LEU B 290     -13.108   3.115  37.066  1.00  0.43           C  
ATOM     73  O   LEU B 290     -12.606   3.558  38.097  1.00  0.43           O  
ATOM     74  CB  LEU B 290     -15.305   3.870  36.159  1.00  0.43           C  
ATOM     75  CG  LEU B 290     -15.912   4.134  37.548  1.00  0.43           C  
ATOM     76  CD1 LEU B 290     -15.634   5.558  38.062  1.00  0.43           C  
ATOM     77  CD2 LEU B 290     -17.412   3.824  37.503  1.00  0.43           C  
ATOM     78  N   GLU B 291     -13.104   1.790  36.799  1.00  0.38           N  
ATOM     79  CA  GLU B 291     -12.500   0.805  37.686  1.00  0.38           C  
ATOM     80  C   GLU B 291     -10.976   0.742  37.634  1.00  0.38           C  
ATOM     81  O   GLU B 291     -10.325   1.223  36.704  1.00  0.38           O  
ATOM     82  CB  GLU B 291     -13.103  -0.611  37.481  1.00  0.38           C  
ATOM     83  CG  GLU B 291     -14.558  -0.716  38.012  1.00  0.38           C  
ATOM     84  CD  GLU B 291     -15.673  -0.579  36.976  1.00  0.38           C  
ATOM     85  OE1 GLU B 291     -15.398  -0.669  35.755  1.00  0.38           O  
ATOM     86  OE2 GLU B 291     -16.829  -0.396  37.440  1.00  0.38           O  
ATOM     87  N   ASP B 292     -10.359   0.140  38.681  1.00  0.35           N  
ATOM     88  CA  ASP B 292      -8.933  -0.089  38.787  1.00  0.35           C  
ATOM     89  C   ASP B 292      -8.384  -0.897  37.617  1.00  0.35           C  
ATOM     90  O   ASP B 292      -8.816  -2.000  37.297  1.00  0.35           O  
ATOM     91  CB  ASP B 292      -8.565  -0.764  40.142  1.00  0.35           C  
ATOM     92  CG  ASP B 292      -8.823   0.153  41.330  1.00  0.35           C  
ATOM     93  OD1 ASP B 292      -9.044   1.371  41.112  1.00  0.35           O  
ATOM     94  OD2 ASP B 292      -8.771  -0.351  42.480  1.00  0.35           O  
ATOM     95  N   ALA B 293      -7.402  -0.313  36.905  1.00  0.38           N  
ATOM     96  CA  ALA B 293      -6.891  -0.905  35.690  1.00  0.38           C  
ATOM     97  C   ALA B 293      -5.828  -1.966  35.947  1.00  0.38           C  
ATOM     98  O   ALA B 293      -5.584  -2.837  35.114  1.00  0.38           O  
ATOM     99  CB  ALA B 293      -6.292   0.202  34.802  1.00  0.38           C  
ATOM    100  N   GLU B 294      -5.165  -1.903  37.118  1.00  0.47           N  
ATOM    101  CA  GLU B 294      -4.091  -2.796  37.496  1.00  0.47           C  
ATOM    102  C   GLU B 294      -4.581  -3.832  38.485  1.00  0.47           C  
ATOM    103  O   GLU B 294      -4.497  -3.661  39.699  1.00  0.47           O  
ATOM    104  CB  GLU B 294      -2.906  -2.009  38.101  1.00  0.47           C  
ATOM    105  CG  GLU B 294      -2.285  -1.018  37.088  1.00  0.47           C  
ATOM    106  CD  GLU B 294      -1.141  -0.188  37.669  1.00  0.47           C  
ATOM    107  OE1 GLU B 294      -0.854  -0.307  38.886  1.00  0.47           O  
ATOM    108  OE2 GLU B 294      -0.560   0.597  36.876  1.00  0.47           O  
ATOM    109  N   GLU B 295      -5.091  -4.959  37.968  1.00  0.34           N  
ATOM    110  CA  GLU B 295      -5.434  -6.118  38.753  1.00  0.34           C  
ATOM    111  C   GLU B 295      -4.610  -7.237  38.149  1.00  0.34           C  
ATOM    112  O   GLU B 295      -4.394  -7.280  36.938  1.00  0.34           O  
ATOM    113  CB  GLU B 295      -6.958  -6.425  38.752  1.00  0.34           C  
ATOM    114  CG  GLU B 295      -7.784  -5.303  39.443  1.00  0.34           C  
ATOM    115  CD  GLU B 295      -9.290  -5.569  39.527  1.00  0.34           C  
ATOM    116  OE1 GLU B 295      -9.772  -6.560  38.925  1.00  0.34           O  
ATOM    117  OE2 GLU B 295      -9.973  -4.773  40.223  1.00  0.34           O  
ATOM    118  N   GLU B 296      -4.073  -8.128  39.007  1.00  0.34           N  
ATOM    119  CA  GLU B 296      -3.328  -9.307  38.620  1.00  0.34           C  
ATOM    120  C   GLU B 296      -4.324 -10.399  38.254  1.00  0.34           C  
ATOM    121  O   GLU B 296      -5.471 -10.337  38.684  1.00  0.34           O  
ATOM    122  CB  GLU B 296      -2.445  -9.836  39.785  1.00  0.34           C  
ATOM    123  CG  GLU B 296      -1.367  -8.838  40.270  1.00  0.34           C  
ATOM    124  CD  GLU B 296      -0.358  -8.577  39.157  1.00  0.34           C  
ATOM    125  OE1 GLU B 296      -0.082  -9.530  38.381  1.00  0.34           O  
ATOM    126  OE2 GLU B 296       0.132  -7.425  39.073  1.00  0.34           O  
ATOM    127  N   MET B 297      -3.885 -11.419  37.487  1.00  0.31           N  
ATOM    128  CA  MET B 297      -4.665 -12.531  36.941  1.00  0.31           C  
ATOM    129  C   MET B 297      -5.099 -12.232  35.514  1.00  0.31           C  
ATOM    130  O   MET B 297      -4.624 -11.302  34.869  1.00  0.31           O  
ATOM    131  CB  MET B 297      -5.806 -13.181  37.805  1.00  0.31           C  
ATOM    132  CG  MET B 297      -5.342 -13.730  39.169  1.00  0.31           C  
ATOM    133  SD  MET B 297      -6.668 -14.155  40.346  1.00  0.31           S  
ATOM    134  CE  MET B 297      -7.124 -12.443  40.744  1.00  0.31           C  
ATOM    135  N   GLY B 298      -5.978 -13.093  34.972  1.00  0.43           N  
ATOM    136  CA  GLY B 298      -6.604 -12.946  33.675  1.00  0.43           C  
ATOM    137  C   GLY B 298      -6.405 -14.184  32.855  1.00  0.43           C  
ATOM    138  O   GLY B 298      -5.296 -14.683  32.687  1.00  0.43           O  
ATOM    139  N   THR B 299      -7.495 -14.735  32.306  1.00  0.44           N  
ATOM    140  CA  THR B 299      -7.408 -15.894  31.429  1.00  0.44           C  
ATOM    141  C   THR B 299      -8.683 -15.916  30.612  1.00  0.44           C  
ATOM    142  O   THR B 299      -9.573 -15.098  30.834  1.00  0.44           O  
ATOM    143  CB  THR B 299      -7.153 -17.240  32.132  1.00  0.44           C  
ATOM    144  OG1 THR B 299      -6.885 -18.281  31.199  1.00  0.44           O  
ATOM    145  CG2 THR B 299      -8.349 -17.660  33.000  1.00  0.44           C  
ATOM    146  N   PHE B 300      -8.802 -16.826  29.631  1.00  0.38           N  
ATOM    147  CA  PHE B 300      -9.975 -16.973  28.791  1.00  0.38           C  
ATOM    148  C   PHE B 300     -10.400 -18.421  28.890  1.00  0.38           C  
ATOM    149  O   PHE B 300      -9.554 -19.316  28.933  1.00  0.38           O  
ATOM    150  CB  PHE B 300      -9.740 -16.629  27.289  1.00  0.38           C  
ATOM    151  CG  PHE B 300      -9.290 -15.203  27.130  1.00  0.38           C  
ATOM    152  CD1 PHE B 300      -7.929 -14.869  27.224  1.00  0.38           C  
ATOM    153  CD2 PHE B 300     -10.223 -14.180  26.900  1.00  0.38           C  
ATOM    154  CE1 PHE B 300      -7.510 -13.537  27.118  1.00  0.38           C  
ATOM    155  CE2 PHE B 300      -9.803 -12.852  26.751  1.00  0.38           C  
ATOM    156  CZ  PHE B 300      -8.447 -12.527  26.874  1.00  0.38           C  
ATOM    157  N   ASP B 301     -11.715 -18.693  28.958  1.00  0.44           N  
ATOM    158  CA  ASP B 301     -12.251 -20.039  28.992  1.00  0.44           C  
ATOM    159  C   ASP B 301     -12.399 -20.665  27.591  1.00  0.44           C  
ATOM    160  O   ASP B 301     -11.957 -20.132  26.573  1.00  0.44           O  
ATOM    161  CB  ASP B 301     -13.526 -20.111  29.887  1.00  0.44           C  
ATOM    162  CG  ASP B 301     -14.659 -19.219  29.406  1.00  0.44           C  
ATOM    163  OD1 ASP B 301     -15.044 -19.336  28.211  1.00  0.44           O  
ATOM    164  OD2 ASP B 301     -15.172 -18.440  30.244  1.00  0.44           O  
ATOM    165  N   SER B 302     -13.004 -21.872  27.526  1.00  0.46           N  
ATOM    166  CA  SER B 302     -13.302 -22.644  26.319  1.00  0.46           C  
ATOM    167  C   SER B 302     -14.239 -21.939  25.341  1.00  0.46           C  
ATOM    168  O   SER B 302     -14.173 -22.180  24.138  1.00  0.46           O  
ATOM    169  CB  SER B 302     -13.905 -24.046  26.652  1.00  0.46           C  
ATOM    170  OG  SER B 302     -15.070 -23.947  27.474  1.00  0.46           O  
ATOM    171  N   SER B 303     -15.126 -21.046  25.838  1.00  0.52           N  
ATOM    172  CA  SER B 303     -16.118 -20.313  25.060  1.00  0.52           C  
ATOM    173  C   SER B 303     -15.573 -18.960  24.651  1.00  0.52           C  
ATOM    174  O   SER B 303     -16.263 -18.165  24.017  1.00  0.52           O  
ATOM    175  CB  SER B 303     -17.415 -20.057  25.886  1.00  0.52           C  
ATOM    176  OG  SER B 303     -18.382 -21.094  25.690  1.00  0.52           O  
ATOM    177  N   GLY B 304     -14.298 -18.657  24.983  1.00  0.49           N  
ATOM    178  CA  GLY B 304     -13.655 -17.411  24.595  1.00  0.49           C  
ATOM    179  C   GLY B 304     -14.001 -16.266  25.493  1.00  0.49           C  
ATOM    180  O   GLY B 304     -13.665 -15.119  25.207  1.00  0.49           O  
ATOM    181  N   ALA B 305     -14.684 -16.537  26.620  1.00  0.49           N  
ATOM    182  CA  ALA B 305     -15.064 -15.507  27.546  1.00  0.49           C  
ATOM    183  C   ALA B 305     -13.875 -15.217  28.455  1.00  0.49           C  
ATOM    184  O   ALA B 305     -13.100 -16.095  28.826  1.00  0.49           O  
ATOM    185  CB  ALA B 305     -16.372 -15.887  28.280  1.00  0.49           C  
ATOM    186  N   PHE B 306     -13.640 -13.925  28.753  1.00  0.37           N  
ATOM    187  CA  PHE B 306     -12.605 -13.489  29.667  1.00  0.37           C  
ATOM    188  C   PHE B 306     -12.991 -13.712  31.125  1.00  0.37           C  
ATOM    189  O   PHE B 306     -14.118 -13.455  31.549  1.00  0.37           O  
ATOM    190  CB  PHE B 306     -12.266 -11.999  29.384  1.00  0.37           C  
ATOM    191  CG  PHE B 306     -11.152 -11.445  30.240  1.00  0.37           C  
ATOM    192  CD1 PHE B 306      -9.832 -11.921  30.158  1.00  0.37           C  
ATOM    193  CD2 PHE B 306     -11.445 -10.454  31.184  1.00  0.37           C  
ATOM    194  CE1 PHE B 306      -8.828 -11.401  30.986  1.00  0.37           C  
ATOM    195  CE2 PHE B 306     -10.438  -9.919  31.990  1.00  0.37           C  
ATOM    196  CZ  PHE B 306      -9.127 -10.393  31.905  1.00  0.37           C  
ATOM    197  N   LEU B 307     -12.012 -14.158  31.925  1.00  0.22           N  
ATOM    198  CA  LEU B 307     -12.108 -14.281  33.351  1.00  0.22           C  
ATOM    199  C   LEU B 307     -11.113 -13.283  33.926  1.00  0.22           C  
ATOM    200  O   LEU B 307      -9.909 -13.418  33.707  1.00  0.22           O  
ATOM    201  CB  LEU B 307     -11.782 -15.741  33.746  1.00  0.22           C  
ATOM    202  CG  LEU B 307     -11.918 -16.058  35.246  1.00  0.22           C  
ATOM    203  CD1 LEU B 307     -13.345 -15.811  35.767  1.00  0.22           C  
ATOM    204  CD2 LEU B 307     -11.480 -17.507  35.512  1.00  0.22           C  
ATOM    205  N   SER B 308     -11.649 -12.251  34.612  1.00  0.28           N  
ATOM    206  CA  SER B 308     -10.951 -11.166  35.302  1.00  0.28           C  
ATOM    207  C   SER B 308     -10.661 -11.652  36.748  1.00  0.28           C  
ATOM    208  O   SER B 308     -11.334 -12.628  37.193  1.00  0.28           O  
ATOM    209  CB  SER B 308     -11.860  -9.881  35.270  1.00  0.28           C  
ATOM    210  OG  SER B 308     -11.219  -8.613  35.147  1.00  0.28           O  
ATOM    211  OXT SER B 308      -9.746 -11.098  37.402  1.00  0.28           O  
TER     212      SER B 308                                                      
END   
