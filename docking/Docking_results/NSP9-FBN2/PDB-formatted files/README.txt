Complete docking model consists of two files, receptor01* or receptor02* file
(larger protein PKP2 in a pair) and corresponding ligand (smaller protein nsp9)
file. Chain IDs are set to 'A' and 'B' in the receptor and ligand files, 
respectively.

Folder CLUSTER contains ligand files, which were generated using different 
templates, but form tight cluster.
