All full- (FSA) and interface- (PSA) structure alignment docking models which 
do not have severe clashes and form an interface. FSA models were generated 
utilizing custom template library based on interacting structures of proteins
homologous to nsp9. PSA models were built using standard interface template 
library (v.1.1) from DOCKGROUND.