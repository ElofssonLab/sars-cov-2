import sys
import subprocess
from Bio.PDB import *
#from Bio.PDB.PDBParser import PDBParser
#from Bio.PDB.PDBIO import PDBIO
#from Bio.PDB.PDBIO import Select

three2one = {'ALA':'A','ARG':'R','ASN':'N','ASP':'D',
             'CYS':'C','GLN':'Q','GLU':'E','GLY':'G',
             'HIS':'H','ILE':'I','LEU':'L','LYS':'K',
             'MET':'M','PHE':'F','PRO':'P','SER':'S',
             'THR':'T','TRP':'W','TYR':'Y','VAL':'V',
             'MSE':'M','UNK':'X'}

def extinterface(tchain, ichain):

    int_res = []
    iatoms = []
    for residue in ichain:
        int_res.append(residue)
        for atom in residue: iatoms.append(atom)

    ns = NeighborSearch(iatoms)
    for tresidue in tchain: 
        for atom in tresidue:
            neighbours = ns.search(atom.coord, 12, level='R')
            if len(list(neighbours)) != 0:
                int_res.append(tresidue)
                break

    return int_res
 
class RSelect(Select):
     def accept_model(self, model):
         if model.get_id() == 0:
             return True
         else: 
             return False

     def accept_chain(self, chain):
         if chain.get_id() == chainR:
             return True
         else:
             return False  

class LSelect(Select):
     def accept_model(self, model):
         if model.get_id() == int(ch[0]):
             return True
         else:
             return False

     def accept_chain(self, chain):
         if chain.get_id() == ch[1]:
             return True
         else:
             return False     

hitlist = 'n'
p = PDBParser()
outpath = sys.argv[2].rstrip('/')+'/'
for line in open(sys.argv[1],'r'):
    if line.rstrip() == '': continue
    if line.startswith('Match_columns '): ql80 = (float(line.split()[1])/100)*80
    if line.startswith(' No Hit'): 
        hitlist = 'y'
        continue
    if line.startswith('No '): break

    if hitlist == 'y':
        download = []
        code = line[4:8].lower()
        chainR = line[9]
        if float(line[35:40]) < 90: continue
        if int(line[70:74].lstrip()) < ql80: continue

        subprocess.run('wget https://files.rcsb.org/header/'+code+'.pdb', shell=True)

        assembly = ''
        author = ''
        software = ''
        for line in open(code+'.pdb'):
            if line.startswith('REMARK 350 BIOMOLECULE:'): assembly = line.split()[3]
            if line.startswith('REMARK 350 AUTHOR DETERMINED BIOLOGICAL UNIT:'): author = line.split()[6]
            if line.startswith('REMARK 350 SOFTWARE DETERMINED QUATERNARY STRUCTURE:'): software = line.split()[6]
            if assembly != '' and (author != 'MONOMER' and software != 'MONOMER') and (author != '' or software != ''): 
                if assembly not in download: download.append(assembly)
        subprocess.run('rm '+code+'.pdb', shell=True)

        for assembly in download: 
            subprocess.run('wget https://files.rcsb.org/download/'+code+'.pdb'+assembly, shell=True)

            match = 'n'
            chains = []
            structure = p.get_structure('', code+'.pdb'+assembly)
            for mod in structure:
                for ch in mod:

                    if mod.get_id() == 0 and ch.get_id() == chainR:
                        match = 'y'
                        continue

                    prot = 'n'
                    for res in ch:
                        if is_aa(res.get_resname()):
                            prot = 'y'
                            break
                    if prot == 'y': chains.append(str(mod.get_id())+ch.get_id())

            if match == 'n': pass
            else: break

            subprocess.run('rm '+code+'.pdb'+assembly, shell=True)

        rres = Selection.unfold_entities(structure[0][chainR], 'R')
        
        for ch in chains:
            lres = Selection.unfold_entities(structure[int(ch[0])][ch[1]], 'R')
            interface = extinterface(rres, lres)
            if len(interface) <= 3: continue
            io=PDBIO()
            int_id = code+'_0'+chainR+ch
            io.set_structure(structure)
            io.save(outpath+int_id+'_1.pdb', RSelect())
            io.save(outpath+int_id+'_2.pdb', LSelect())

            subprocess.run('grep ATOM '+outpath+int_id+'_1.pdb > '+outpath+int_id+'_1.pdb_c' , shell=True)
            subprocess.run('mv '+outpath+int_id+'_1.pdb_c '+outpath+int_id+'_1.pdb' , shell=True)
            subprocess.run('grep ATOM '+outpath+int_id+'_2.pdb > '+outpath+int_id+'_2.pdb_c' , shell=True)
            subprocess.run('mv '+outpath+int_id+'_2.pdb_c '+outpath+int_id+'_2.pdb' , shell=True)

        subprocess.run('rm '+code+'.pdb'+assembly, shell=True)
