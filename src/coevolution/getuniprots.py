#!/usr/bin/env python3

import urllib.parse
import urllib.request

url = 'https://www.uniprot.org/uploadlists/'

params = {
'from': 'ACC+ID',
'to': 'ENSEMBL_ID',
'format': 'tab',
#'query': 'P40925 P40926 O43175 Q9UM73 P97793'
    'taxonomy':'Eukaryota',
    'taxonomy':'Vespertilio'
    
}

data = urllib.parse.urlencode(params)
data = data.encode('utf-8')
req = urllib.request.Request(url, data)
with urllib.request.urlopen(req) as f:
   response = f.read()
print(response.decode('utf-8'))

#https://www.uniprot.org/uniprot/?query=taxonomy%3A%22Eukaryota+%5B2759%5D%22+taxonomy%3A%22Vespertilio+%5B59484%5D%22&sort=score

https://www.uniprot.org/uniprot/?query=insulin&sort=score&columns=id,entry name,reviewed,protein names,genes,organism,length&format=tab
