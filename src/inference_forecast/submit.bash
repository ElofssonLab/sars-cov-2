#!/bin/bash -x
#SBATCH -A SNIC2019-3-319
#SBATCH -t 01:00:00
#SBATCH -n 1
#SBATCH --output=pymc3.%A_%a.out
#SBATCH --error=pymc3.%A_%a.out
#SBATCH --array=1-5
#SBATCH -c 8 # Or 14, or any other number of cores to use for tblastn
# -c can also be specified as arg to "sbatch" and overrides the value
# specified here.

#source /pfs/nobackup/home/w/wbasile/venv-work/bin/activate

unset PYTHONPATH

$offset=0

pos=$(($SLURM_ARRAY_TASK_ID + $offset))

country=`gawk -F "," '{print $2}' /pfs/nobackup/home/a/arnee/Corona/data/countries_regions.csv  | sed "s/ /_/g" | sort -u   | head -n $pos | tail -1`


echo $country

#python ./do_blast.py
#python create_final_dataset_single.py
if [ ! -s /pfs/nobackup/home/a/arnee/Corona/R0-breakpoints/3par/${i}-3par.out ]
then
   singularity exec /pfs/nobackup/home/a/arnee/singularity-images/pymc3.simg python3 example_script_covid19_inference.py -c "${country}" -i /pfs/nobackup/home/a/arnee/Corona/data/countries_regions.csv  -o  /pfs/nobackup/home/a/arnee/Corona/R0-breakpoints/3par/
fi
if [ ! -s /pfs/nobackup/home/a/arnee/Corona/R0-breakpoints/2par/${i}-2par.out ]
then
   singularity exec /pfs/nobackup/home/a/arnee/singularity-images/pymc3.simg python3 example_script_covid19_inference-2par.py -c "${country}" -i /pfs/nobackup/home/a/arnee/Corona/data/countries_regions.csv  -o  /pfs/nobackup/home/a/arnee/Corona/R0-breakpoints/2par/
fi
