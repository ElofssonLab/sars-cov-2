#!/bin/bash -x


countries="Sweden Norway Denmark Germany Spain France UK Italy Singapore Japan USA Belgium Netherlands Switzerland Austria Finland"
#for i in  `gawk -F "," '{print $3}' ../../../Corona/data/merged_ECCDC-R0-mobility.csv  | sort -u `
#for i in  `gawk -F "," '{print $4}' ../../../Corona/data/merged_JHS.csv  | sort -u `
#for i in  `gawk -F "," '{print $4}' ../../../Corona/data/merged_JHS.csv  | sort -u `

dir=/home/arnee/Desktop/Corona/


#countries=`gawk -F "," '{print $3}' $dir/data/countries_regions.csv  | sed "s/ /_/g" | sort -u `

countries=`cat ~/git/sars-cov-2/src/inference_forecast/countries`

for i in ${countries}
do
    echo $i
    if [ ! -s $dir/R0-breakpoints/3par/${i}-3par-R0.png ]
    then
	name=`echo $i | sed "s/_/ /g"`
	#name=`echo $i`
	python3 example_script_covid19_inference.py -c "$name" -i $dir/data/countries_regions.csv -o $dir/R0-breakpoints/3par/ > $dir/R0-breakpoints/3par/${i}-3par.out
    fi
    if [ ! -s $dir/R0-breakpoints/3par/${i}-3par-R0.png ]
    then
	python3 example_script_covid19_inference-2par.py -c "$name" -i $dir/data/countries_regions.csv -o $dir/R0-breakpoints/2par/ > $dir/R0-breakpoints/2par/${i}-2par.out
    fi
done
    
