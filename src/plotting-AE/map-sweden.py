#!/usr/bin/env python3

import pandas as pd
#import folium
import argparse
from argparse import RawTextHelpFormatter
import re
import numpy as np
import plotly.express as px
from urllib.request import urlopen
import json
import plotly
import csv
import math
import preprocess as pp
import os
from datetime import  timedelta, date
import plotly.io as pio
pio.orca.config.use_xvfb = True

p = argparse.ArgumentParser(description = 
             '- Make a map over R0 values -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-data','--input','-i', required= True, help='Input dir formatted CSV file')
p.add_argument('-o','-out','--output_folder', required= False, help='output folder')
#parser.add_argument('--nargs', nargs='+')
ns = p.parse_args()
if ns.output_folder:
    out_dir = ns.output_folder
else:
    out_dir=home = str(Path.home())+"/Desktop/Corona/R-values/"
    #out_dir=re.sub("/.*","",ns.input)
    if out_dir == ns.input:
        out_dir=home = str(Path.home())+"/Desktop/Corona/R-values"        

if not os.path.exists(out_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + out_dir)


f=open(os.getcwd()+"/data/world+regions.json","r")
#f=open("laen-kustlinjer.geo.json","r")
SE_regioner=json.load(f)    


#concat_df,lastdate,firstdate=pp.get_r0_countries(ns.input)

# Still does not work as I do not have a good way to get last dates
concat_df = pd.read_csv(ns.input)
pp.map_names(concat_df)
lastdate=pp.FormatDateMerged(concat_df.date.dropna().max())

#lastdate=date.today()
for source in concat_df.datasource.drop_duplicates():
    lastdate=min(lastdate,pp.FormatDateMerged(
        concat_df.loc[concat_df.datasource==source]["date"].max()))

concat_df=concat_df[ (  ( concat_df['country'] != "Spain" )
                     & ( concat_df['country'] != "Sweden" )
                     & ( concat_df['country'] != "United States" )
                     & ( concat_df['country'] != "Italy" )
                     & ( concat_df['country'] != "United States of America" )
                     & ( concat_df['country'] != "USA" )
                     & ( concat_df['country'] != "America" )
                     & ( concat_df['country'] != "China" )
                     & ( concat_df['country'] != "Canada" ))]

concat_df=concat_df.loc[concat_df.date==str(lastdate.date())]

#concat_df.to_csv("foo.csv",sep=',')
#sys.exit()
fig = px.choropleth_mapbox(concat_df, geojson=SE_regioner,
                           locations='country', color='R0',
                           featureidkey='properties.name',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.diverging.Tealrose,
                           color_continuous_midpoint=1,
                           # range_color=[0,2],
                            #color_continuous_scale="Viridis",
                               range_color=(0, 2),
                           mapbox_style="carto-positron",
                           title='Estimated R0 as of '+str(lastdate.date()),
                           #zoom=4, center = {"lat": 62.0902, "lon": 15.7129}, # This is for Sweden
                           zoom=3, center = {"lat": 52.0902, "lon": 5.7129},  # This is for Europe
                           opacity=0.5,
                           labels={'R0':'Estimated R0'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
#plotly.offline.plot(fig, filename=out_dir+'/index.html')
plotly.offline.plot(fig, filename=out_dir+'/R0.html')

#sys.exit()

# We can make several ma

# Deaths per million

fig = px.choropleth_mapbox(concat_df, geojson=SE_regioner,
                           locations='country', color='deaths_per_million',
                           featureidkey='properties.name',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.sequential.Sunsetdark,
                           #color_continuous_midpoint=1,
                           # range_color=[0,2],
                            #color_continuous_scale="Viridis",
                           range_color=(0, 1500),
                           mapbox_style="carto-positron",
                           title='Total number of deaths per millsion people '+str(lastdate.date()),
                           zoom=3, center = {"lat": 52.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'deaths/millsion':'Total deaths'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
plotly.offline.plot(fig, filename=out_dir+'/deaths.html')
#fig.write_image(out_dir+'/deaths.png')  # This crashes

fig = px.choropleth_mapbox(concat_df, geojson=SE_regioner,
                           locations='country', color='cases_per_million',
                           featureidkey='properties.name',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.sequential.Sunsetdark,
                           #color_continuous_midpoint=1,
                           # range_color=[0,2],
                            #color_continuous_scale="Viridis",
                           range_color=(0, 20000),
                           mapbox_style="carto-positron",
                           title='Total number of cases per million of people '+str(lastdate.date()),
                           zoom=3, center = {"lat": 52.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'cases_per_million':'Total cases'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
plotly.offline.plot(fig, filename=out_dir+'/cases.html')
fig = px.choropleth_mapbox(concat_df, geojson=SE_regioner,
                           locations='country', color='Current-deaths_per_million',
                           featureidkey='properties.name',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.sequential.Sunsetdark,
                           #color_continuous_midpoint=1,
                           # range_color=[0,2],
                            #color_continuous_scale="Viridis",
                           title='Number of deaths per million of people last week'+str(lastdate.date()),
                            range_color=(0, 25),
                           mapbox_style="carto-positron",
                           zoom=3, center = {"lat": 52.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'Current-deaths_per_million':'Death per day'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
plotly.offline.plot(fig, filename=out_dir+'/currentdeaths.html')



fig = px.choropleth_mapbox(concat_df, geojson=SE_regioner,
                           locations='country', color='Current-cases_per_million',
                           featureidkey='properties.name',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.sequential.Sunsetdark,
                           #color_continuous_midpoint=1,
                           #range_color=[0,2],
                           #color_continuous_scale="Viridis",
                           range_color=(0, 300),
                           mapbox_style="carto-positron",
                           title='Number of cases per million of people last week'+str(lastdate.date()),
                           zoom=3, center = {"lat": 52.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'Current-cases_per_million':'Cases per day'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
plotly.offline.plot(fig, filename=out_dir+'/currentcases.html')


cfr=0.0025  # From Gisecka
herd=0.6
minnum=1

concat_df["days_left"]= concat_df.apply(lambda x:((herd*cfr*x.popData2019-x.deaths)/(x.deaths_7days+minnum)),axis=1)
concat_df["log_days_left"]= np.log10(concat_df['days_left']+minnum)


fig = px.choropleth_mapbox(concat_df, geojson=SE_regioner,
                           locations='country', color='days_left',
                           featureidkey='properties.name',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.sequential.Sunsetdark,
                           #color_continuous_midpoint=1,
                           #range_color=[0,2],
                           #color_continuous_scale="Viridis",
                           range_color=(0, 3650),
                           mapbox_style="carto-positron",
                           title='Number of days left until herd immunity as of '+str(lastdate.date()),
                           zoom=3, center = {"lat": 52.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'days_left':'Number of days left'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
plotly.offline.plot(fig, filename=out_dir+'/days_left.html')


#
#fig = px.choropleth_mapbox(concat_df, geojson=world,
#                           locations='country', color='ratio',
#                           #locations='fips', color='unemp',
#                           #color_continuous_scale=px.colors.diverging.BrBG,
#                           color_continuous_scale=px.colors.sequential.Sunsetdark,
#                           #color_continuous_midpoint=1,
#                           # range_color=[0,2],
#                            #color_continuous_scale="Viridis",
#                           #    range_color=(0, 2),
#                           mapbox_style="carto-positron",
#                           zoom=3, center = {"lat": 52.0902, "lon": 5.7129},
#                           opacity=0.5,
#                           labels={'R0':'Estimated R0'},
#                           hover_name="country"
#                          )
#fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
##fig.show()
#plotly.offline.plot(fig, filename=out_dir+'/ratio.html')
#
#

#df = px.data.gapminder().query("year==2007")
#fig = px.scatter_geo(df, locations="iso_alpha", color="continent",
#                     hover_name="country", size="pop",
#                     projection="natural earth")



#fig = px.choropleth_mapbox(concat_df, geojson=SE_regioner,
#                           locations='country', color='retail_and_recreation',
#                           featureidkey='properties.name',
#                           #locations='fips', color='unemp',
#                           #color_continuous_scale=px.colors.diverging.BrBG,
#                           color_continuous_scale=px.colors.sequential.Sunsetdark,
#                           #color_continuous_midpoint=1,
#                           #range_color=[0,2],
#                           #color_continuous_scale="Viridis",
#                           range_color=(0, 3650),
#                           mapbox_style="carto-positron",
#                           title='Mobility changes as of '+str(lastdate.date()),
#                           zoom=3, center = {"lat": 52.0902, "lon": 5.7129},
#                           opacity=0.5,
#                           labels={'days_left':'Number of days left'},
#                           hover_name="country"
#                          )
#fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
##fig.show()
#plotly.offline.plot(fig, filename=out_dir+'/retail_and_recreation.html')
