#!/usr/bin/env python3

import pandas as pd
#import folium
import argparse
from argparse import RawTextHelpFormatter
import re
import numpy as np
import plotly.express as px
from urllib.request import urlopen
import json
import plotly
import csv
import preprocess as pp
import os

p = argparse.ArgumentParser(description = 
             '- Make a map over R0 values -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-data','--input','-i', required= True, help='Input formatted CSV file')
p.add_argument('-o','-out','--output_folder', required= False, help='output folder')
#parser.add_argument('--nargs', nargs='+')
ns = p.parse_args()
if ns.output_folder:
    out_dir = ns.output_folder
else:
    out_dir=home = str(Path.home())+"/Desktop/Corona/R-values/"
    #out_dir=re.sub("/.*","",ns.input)
    if out_dir == ns.input:
        out_dir=home = str(Path.home())+"/Desktop/Corona/R-values"        

if not os.path.exists(out_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + out_dir)
        
# Swedish regions
#https://github.com/deldersveld/topojson/countries/sweden/sweden-counties.json
#url = 'https://raw.githubusercontent.com/python-visualization/folium/master/examples/data'
#country_shapes = f'{url}/world-countries.json'

#country_codes = csv.DictReader(open("country-threeletter.csv"))
reader = csv.reader(open('country-threeletter.csv', 'r'))
country_codes = {}
for row in reader:
   k, v = row
   country_codes[k] = v
   
#country_codes=pd.read_csv("country-threeletter.csv")
R_df = pd.read_csv(ns.input)

#with urlopen('https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json') as response:
#    counties = json.load(response)
with urlopen('https://raw.githubusercontent.com/python-visualization/folium/master/examples/data/world-countries.json') as response:
    world = json.load(response)


# We need to avoid some strange values
maxval=3
tiny=1.e-20
#R_df['R0']=R_df['R0'].apply(lambda x:min(x,maxval))
# also try with log
R_df['LogR0']=R_df['R0'].apply(lambda x:(np.log2(max(x,tiny))))

pp.map_names(R_df)

R_df['code']=R_df['country'].apply(lambda x:(country_codes[x]))


#df = pd.read_csv("https://raw.githubusercontent.com/plotly/datasets/master/fips-unemp-16.csv",    dtype={"fips": str})

fig = px.choropleth_mapbox(R_df, geojson=world,
                           locations='code', color='R0',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.diverging.Tealrose,
                           color_continuous_midpoint=1,
                           # range_color=[0,2],
                            #color_continuous_scale="Viridis",
                               range_color=(0, 2),
                           mapbox_style="carto-positron",
                           zoom=3, center = {"lat": 37.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'R0':'Estimated R0'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
#plotly.offline.plot(fig, filename=out_dir+'/index.html')
plotly.offline.plot(fig, filename=out_dir+'/R0.html')


# We can make several maps at the same time..
fig = px.choropleth_mapbox(R_df, geojson=world,
                           locations='code', color='R0',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.diverging.Tealrose,
                           color_continuous_midpoint=1,
                           # range_color=[0,2],
                            #color_continuous_scale="Viridis",
                               range_color=(0, 2),
                           mapbox_style="carto-positron",
                           zoom=3, center = {"lat": 37.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'R0':'Estimated R0'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
#plotly.offline.plot(fig, filename=out_dir+'/index.html')
plotly.offline.plot(fig, filename=out_dir+'/R0.html')


# Deaths per million

R_df['deaths/million']=1000000*R_df['deaths']/R_df['popData2019']
R_df['cases/million']=1000000*R_df['confirmed']/R_df['popData2019']
R_df['Current deaths/million']=1000000*R_df['deaths_7days']/R_df['popData2019']
R_df['Current cases/million']=1000000*R_df['cases_7days']/R_df['popData2019']

fig = px.choropleth_mapbox(R_df, geojson=world,
                           locations='code', color='deaths/million',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.sequential.Sunsetdark,
                           #color_continuous_midpoint=1,
                           # range_color=[0,2],
                            #color_continuous_scale="Viridis",
                           range_color=(0, 1200),
                           mapbox_style="carto-positron",
                           zoom=3, center = {"lat": 37.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'R0':'Estimated R0'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
plotly.offline.plot(fig, filename=out_dir+'/deaths.html')

fig = px.choropleth_mapbox(R_df, geojson=world,
                           locations='code', color='cases/million',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.sequential.Sunsetdark,
                           #color_continuous_midpoint=1,
                           # range_color=[0,2],
                            #color_continuous_scale="Viridis",
                           range_color=(0, 10000),
                           mapbox_style="carto-positron",
                           zoom=3, center = {"lat": 37.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'R0':'Estimated R0'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
plotly.offline.plot(fig, filename=out_dir+'/cases.html')
fig = px.choropleth_mapbox(R_df, geojson=world,
                           locations='code', color='Current deaths/million',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.sequential.Sunsetdark,
                           #color_continuous_midpoint=1,
                           # range_color=[0,2],
                            #color_continuous_scale="Viridis",
                            range_color=(0, 10),
                           mapbox_style="carto-positron",
                           zoom=3, center = {"lat": 37.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'R0':'Estimated R0'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
plotly.offline.plot(fig, filename=out_dir+'/currentdeaths.html')
fig = px.choropleth_mapbox(R_df, geojson=world,
                           locations='code', color='Current cases/million',
                           #locations='fips', color='unemp',
                           #color_continuous_scale=px.colors.diverging.BrBG,
                           color_continuous_scale=px.colors.sequential.Sunsetdark,
                           #color_continuous_midpoint=1,
                           #range_color=[0,2],
                           #color_continuous_scale="Viridis",
                           range_color=(0, 200),
                           mapbox_style="carto-positron",
                           zoom=3, center = {"lat": 37.0902, "lon": 5.7129},
                           opacity=0.5,
                           labels={'R0':'Estimated R0'},
                           hover_name="country"
                          )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
#fig.show()
plotly.offline.plot(fig, filename=out_dir+'/currentcases.html')
#
#fig = px.choropleth_mapbox(R_df, geojson=world,
#                           locations='code', color='ratio',
#                           #locations='fips', color='unemp',
#                           #color_continuous_scale=px.colors.diverging.BrBG,
#                           color_continuous_scale=px.colors.sequential.Sunsetdark,
#                           #color_continuous_midpoint=1,
#                           # range_color=[0,2],
#                            #color_continuous_scale="Viridis",
#                           #    range_color=(0, 2),
#                           mapbox_style="carto-positron",
#                           zoom=3, center = {"lat": 37.0902, "lon": 5.7129},
#                           opacity=0.5,
#                           labels={'R0':'Estimated R0'},
#                           hover_name="country"
#                          )
#fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
##fig.show()
#plotly.offline.plot(fig, filename=out_dir+'/ratio.html')
#
#

#df = px.data.gapminder().query("year==2007")
#fig = px.scatter_geo(df, locations="iso_alpha", color="continent",
#                     hover_name="country", size="pop",
#                     projection="natural earth")


