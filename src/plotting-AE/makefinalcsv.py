#!/usr/bin/env python3

import pandas as pd
import argparse
from argparse import RawTextHelpFormatter
import re,sys
import preprocess as pp
import os
from pathlib import Path

p = argparse.ArgumentParser(description = 
             '- Make a map over R0 values -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-data','--input','-i', required= True, help='Input dir formatted CSV file')
p.add_argument('-o','-out','--output', required= False, help='output file')
#parser.add_argument('--nargs', nargs='+')
ns = p.parse_args()
if ns.output:
    concatfile = ns.output
else:
    concatfile = str(Path.home())+"/Desktop/Corona/data/countries_regions.csv"


concat_df,lastdate,firstdate=pp.get_r0_countries(ns.input)

pp.fix_country_names(concat_df)
concat_df=pp.fix_end_dates_etc(concat_df)



concat_df.to_csv(concatfile)
