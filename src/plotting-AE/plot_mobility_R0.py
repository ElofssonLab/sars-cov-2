#!/usr/bin/env python
# coding: utf-8

# In[6]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import math
import seaborn as sns
from datetime import datetime,date,time, timedelta
from pathlib import Path
import argparse
from argparse import RawTextHelpFormatter
import re
import os
import preprocess as pp

p = argparse.ArgumentParser(description = 
                                     '- AE-analysis.py - Extract data from date range and create models -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-f','--force', required= False, help='Force', action='store_true')
p.add_argument('-o','-out','--output_folder', required= False, help='output folder')
p.add_argument('-data','--input','-i', required= True, help='Input formatted CSV file')
ns = p.parse_args()
# In[8]:


#data_df=pd.read_csv(str(Path.home())+"/Desktop/Corona/data/countries_regions.csv")

data_df=pd.read_csv(ns.input)
if ns.output_folder:
    out_dir = ns.output_folder
else:
    #out_dir=home = str(Path.home())+"/Desktop/Corona/"
    out_dir=re.sub("data/.*","Mobility/",ns.input)
    if out_dir == ns.input:
        out_dir=home = str(Path.home())+"/Desktop/Corona/Mobility/"        
if not os.path.exists(out_dir):
    print('Creating output folder...')
    os.system('mkdir -p ' + out_dir)




# In[9]:




# In[10]:




countries=data_df.country.drop_duplicates()
data_df.date=data_df.apply(lambda x:datetime.strptime(x.date,"%Y-%m-%d"), axis=1)
#data=data.loc[data.date>datetime.strptime("2020-02-01","%Y-%m-%d")]
#countries


# In[11]:



# Now we need to fix mobility before and after the first dates

mobilitycolumns=["retail_and_recreation","grocery_and_pharmacy","parks","transit_stations","workplaces","residential"]
R0columns=["R0"] #,"R0-FixSI"]

    
columns=["country","R0","new_confirmed_cases","new_deaths","date","deaths","confirmed","Ratio",
         "deaths_7days","cases_7days","popData2019","code","retail_and_recreation",
         "grocery_and_pharmacy","parks","transit_stations","workplaces","residential",
         "date_sec","deaths/million","cases/million","Current deaths/million",
         "Current cases/million","log_deaths","log_cases","log_deaths_7days","log_cases_7days",
         "log_deaths/million","log_cases/million","log_current_deaths/million",
         "log_current_cases/million","days_left","log_days_left"]

data_df.rename(columns={
    "deaths/million":"deaths_per_million",
    "cases/million":"cases_per_million",
    "Current deaths/million":"Current-deaths_per_million",
    "Current cases/million":"Current-cases_per_million",
    "log_deaths":"log_deaths",
    "log_deaths/million":"log_deaths_per_million",
    "log_cases/million":"log_cases_per_million",
    "log_current_deaths/million":"log_current_deaths_per_million",
    "log_current_cases/million":"log_current_cases_per_million",
 },inplace=True)


# In[12]:



def plot_time_new(df,outname,name,columns): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)
    '''Plot with shaded 95 % CI (plots both 1 and 2 std, where 2 = 95 % interval)
    '''
    #extra=["new_deaths","new_confirmed_cases"]
    extra=["R0"] #,"R0_7days"]
    #extra=[]
    fig, ax1 = plt.subplots(figsize=(16,10))
    #tmp_df=df.loc[df.date>=startdate]
    tmp_df=df.groupby(['date'])[columns+extra].mean()
    std_df=df.groupby(['date'])[columns+extra].std()
    N_df=df.groupby(['date'])[columns+extra].size()
    N_df=N_df.apply(lambda x: np.sqrt(x))
    stderr_df=pd.DataFrame([])
    lower_bound=pd.DataFrame([])
    higher_bound=pd.DataFrame([])
    lower_bound25=pd.DataFrame([])
    higher_bound75=pd.DataFrame([])

    for c in columns+extra:
        stderr_df[c]=std_df[c]/N_df
        higher_bound[c]=tmp_df[c]+1.96*stderr_df[c]
        lower_bound[c]=tmp_df[c]-1.96*stderr_df[c]
        higher_bound75[c]=tmp_df[c]+1.15*stderr_df[c]
        lower_bound25[c]=tmp_df[c]-1.15*stderr_df[c]


    tmp_df["date"]=df.groupby(['date'])["date"].first()
    #tmp_df=df
    #print (tmp_df)
    x=np.arange(0,len(tmp_df))
    #print (tmp_df)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    #shift=-0.2
    #for c in extra:
    #    ax2.bar(x+shift,tmp_df[c],yerr=stderr_df[c], alpha = 0.5,width=0.4,label=c)
    #    shift+=0.4
    for c in extra:
        y=tmp_df[c]
        yerr=std_df[c]
        ax2.plot(x,y,label=c, linewidth = 1.0,color="g")
        ax2.fill_between(x, lower_bound[c], higher_bound[c], color='lightgreen', alpha=0.4)
        ax2.fill_between(x, lower_bound25[c], higher_bound75[c], color='lightgreen', alpha=0.6)
        
    ax2.set_ylabel('R0',color='g')
    ax2.tick_params(axis='y', labelcolor='g')
    ax2.legend()
    for c in columns:
        y=tmp_df[c]
        yerr=std_df[c]
        ax1.plot(x,y,label=c, linewidth = 1.0)
        ax1.fill_between(x, lower_bound[c], higher_bound[c], color='cornflowerblue', alpha=0.4)
        ax1.fill_between(x, lower_bound25[c], higher_bound75[c], color='cornflowerblue', alpha=0.6)
    dates = tmp_df['date'].to_list()
    #ax1.legend(loc='lower left', frameon=False, markerscale=2)
    ax1.set_ylabel('Value')
    #ax1.set_yscale('log')
    ax1.set(title="R0 and mobility "+ name )
    ax1.grid(color='b',linestyle="-",linewidth=1)
    ax1.tick_params(axis='x', labelrotation=45 )    
    #ax1.set_ylim([0,max(higher_bound)])
    xticks=np.arange(0,len(tmp_df),7)
    #yticks=[0.1,0.5,1,2,5,10]
    
    #ax1.set_xticklabels(dates)
    #plt.xticks(rotation=45, ha='right')
    ax1.set_xticklabels(dates[0::7],rotation=45)
    #ax1.set_yticklabels(["0.1","0.5","1","2","5","10"])
    ax1.set_xticks(xticks)
    #ax1.set_yticks(yticks)
    plt.gcf().subplots_adjust(bottom=0.25)
    plt.tight_layout()
    ax1.legend()
    #fig.savefig(outname, format = 'png', dpi=300)
    #plt.close(fig="all")


# In[13]:



def plot_date(df,outname,name,columns): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)
    '''Plot with shaded 95 % CI (plots both 1 and 2 std, where 2 = 95 % interval)
    '''
    #extra=["new_deaths","new_confirmed_cases"]
    extra=["R0"] #,"R0_7days"]
    #extra=[]
    fig, ax1 = plt.subplots(figsize=(16,10))
    #tmp_df=df.loc[df.date>=startdate]
    tmp_df=df.groupby(['date'])[columns+extra].mean()
    std_df=df.groupby(['date'])[columns+extra].std()
    N_df=df.groupby(['date'])[columns+extra].size()
    N_df=N_df.apply(lambda x: np.sqrt(x))
    stderr_df=pd.DataFrame([])
    lower_bound=pd.DataFrame([])
    higher_bound=pd.DataFrame([])
    lower_bound25=pd.DataFrame([])
    higher_bound75=pd.DataFrame([])

    for c in columns+extra:
        stderr_df[c]=std_df[c]/N_df
        higher_bound[c]=tmp_df[c]+1.96*stderr_df[c]
        lower_bound[c]=tmp_df[c]-1.96*stderr_df[c]
        higher_bound75[c]=tmp_df[c]+1.15*stderr_df[c]
        lower_bound25[c]=tmp_df[c]-1.15*stderr_df[c]


    tmp_df["date"]=df.groupby(['date'])["date"].first()
    #tmp_df=df
    #print (tmp_df)
    x=np.arange(0,len(tmp_df))
    #print (tmp_df)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    #shift=-0.2
    #for c in extra:
    #    ax2.bar(x+shift,tmp_df[c],yerr=stderr_df[c], alpha = 0.5,width=0.4,label=c)
    #    shift+=0.4
    for c in extra:
        y=tmp_df[c]
        yerr=std_df[c]
        ax2.plot(x,y,label=c, linewidth = 1.0,color="g")
        ax2.fill_between(x, lower_bound[c], higher_bound[c], color='lightgreen', alpha=0.4)
        ax2.fill_between(x, lower_bound25[c], higher_bound75[c], color='lightgreen', alpha=0.6)
        
    ax2.set_ylabel('R0',color='g')
    ax2.tick_params(axis='y', labelcolor='g')
    ax2.legend()
    for c in columns:
        y=tmp_df[c]
        yerr=std_df[c]
        ax1.plot(x,y,label=c, linewidth = 1.0)
        ax1.fill_between(x, lower_bound[c], higher_bound[c], color='cornflowerblue', alpha=0.4)
        ax1.fill_between(x, lower_bound25[c], higher_bound75[c], color='cornflowerblue', alpha=0.6)
    dates = tmp_df['date'].to_list()
    #ax1.legend(loc='lower left', frameon=False, markerscale=2)
    ax1.set_ylabel('Value')
    #ax1.set_yscale('log')
    ax1.set(title="R0 and mobility "+ name )
    ax1.grid(color='b',linestyle="-",linewidth=1)
    ax1.tick_params(axis='x', labelrotation=45 )    
    #ax1.set_ylim([0,max(higher_bound)])
    xticks=np.arange(0,len(tmp_df),7)
    #yticks=[0.1,0.5,1,2,5,10]
    
    #ax1.set_xticklabels(dates)
    #plt.xticks(rotation=45, ha='right')
    ax1.set_xticklabels(dates[0::7],rotation=45)
    #ax1.set_yticklabels(["0.1","0.5","1","2","5","10"])
    ax1.set_xticks(xticks)
    #ax1.set_yticks(yticks)
    plt.gcf().subplots_adjust(bottom=0.25)
    plt.tight_layout()
    ax1.legend()
    #fig.savefig(outname, format = 'png', dpi=300)
    #plt.close(fig="all")


# In[14]:

def plot_time_triplett(df,outname,name,columns,columns2): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)
    '''Plot with shaded 95 % CI (plots both 1 and 2 std, where 2 = 95 % interval)
    '''
    #extra=["new_deaths","new_confirmed_cases"]
    extra=["R0_7days"]
    #extra=[]
    fig, ax1 = plt.subplots(figsize=(16,10))
    #tmp_df=df.loc[df.date>=startdate]
    tmp_df=df.groupby(['date'])[columns+extra+columns2].mean()
    std_df=df.groupby(['date'])[columns+extra+columns2].std()
    N_df=df.groupby(['date'])[columns+extra+columns2].size()
    N_df=N_df.apply(lambda x: np.sqrt(x))
    stderr_df=pd.DataFrame([])
    lower_bound=pd.DataFrame([])
    higher_bound=pd.DataFrame([])
    lower_bound25=pd.DataFrame([])
    higher_bound75=pd.DataFrame([])

    for c in columns+extra+columns2:
        stderr_df[c]=std_df[c]/N_df
        higher_bound[c]=tmp_df[c]+1.96*stderr_df[c]
        lower_bound[c]=tmp_df[c]-1.96*stderr_df[c]
        higher_bound75[c]=tmp_df[c]+1.15*stderr_df[c]
        lower_bound25[c]=tmp_df[c]-1.15*stderr_df[c]


    tmp_df["date"]=df.groupby(['date'])["date"].first()
    #tmp_df=df
    #print (tmp_df)
    x=np.arange(0,len(tmp_df))
    #print (tmp_df)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax3 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    #shift=-0.2
    #for c in extra:
    #    ax2.bar(x+shift,tmp_df[c],yerr=stderr_df[c], alpha = 0.5,width=0.4,label=c)
    #    shift+=0.4
    for c in extra:
        y=tmp_df[c]
        yerr=std_df[c]
        ax2.plot(x,y,label=c, linewidth = 2.0,color="g",linestyle="-")
        ax2.fill_between(x, lower_bound[c], higher_bound[c], color='lightgreen', alpha=0.4)
        ax2.fill_between(x, lower_bound25[c], higher_bound75[c], color='lightgreen', alpha=0.6)        
    ax2.set_ylabel('R0',color='g')
    ax2.tick_params(axis='y', labelcolor='g')
    ax2.legend()
    
    for c in columns2:
        y=tmp_df[c]
        yerr=std_df[c]
        ax3.plot(x,y,label=c, linewidth = 4.0,linestyle="dotted")
        ax3.fill_between(x, lower_bound[c], higher_bound[c], color='pink', alpha=0.4)
        ax3.fill_between(x, lower_bound25[c], higher_bound75[c], color='pink', alpha=0.6)        
    ax3.set_ylabel('mobility',color='r')
    ax3.tick_params(axis='y', labelcolor='r')
    ax3.legend()
    
     
    
    for c in columns:
        y=tmp_df[c]
        yerr=std_df[c]
        ax1.plot(x,y,label=c, linewidth = 4.0,linestyle="--")
        ax1.fill_between(x, lower_bound[c], higher_bound[c], color='cornflowerblue', alpha=0.4)
        ax1.fill_between(x, lower_bound25[c], higher_bound75[c], color='cornflowerblue', alpha=0.6)
    dates = tmp_df['date'].to_list()
    #ax1.legend(loc='lower left', frameon=False, markerscale=2)
    ax1.set_ylabel('Value')
    #ax1.set_yscale('log')
    ax1.set(title="R0 and mobility "+ name )
    ax1.grid(color='b',linestyle="-",linewidth=1)
    ax1.tick_params(axis='x', labelrotation=45 )    
    #ax1.set_ylim([0,max(higher_bound)])
    xticks=np.arange(0,len(tmp_df),7)
    #yticks=[0.1,0.5,1,2,5,10]
    
    #ax1.set_xticklabels(dates)
    #plt.xticks(rotation=45, ha='right')
    ax1.set_xticklabels(dates[0::7],rotation=45)
    #ax1.set_yticklabels(["0.1","0.5","1","2","5","10"])
    ax1.set_xticks(xticks)
    #ax1.set_yticks(yticks)
    plt.gcf().subplots_adjust(bottom=0.25)
    plt.tight_layout()
    ax1.legend()
    fig.savefig(outname, format = 'png', dpi=300)
    plt.close(fig="all")

#data_df=backup
#backup=data_df
#data_df.confirmed.max()


# In[15]:


#data_df=data_df[data_df.confirmed>10]
#data_df
#fig,ax=plt.subplots(figsize=(10,5))
#ax.hist(data_df.confirmed)


# In[16]:




# In[17]:
#data_df.date=data_df.apply(lambda x:datetime.strptime(x.date,"%Y-%m-%d"), axis=1)


mobilitycolumns7=["retail_and_recreation_7days","grocery_and_pharmacy_7days","parks_7days",
                 "transit_stations_7days","workplaces_7days","residential_7days"]



#data_df=data_df.dropna()
pp.fix_country_names(data_df)
countries=data_df.country.drop_duplicates()

plot_time_triplett(data_df,out_dir+"/All.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)

merged_df=data_df.loc[data_df.datasource=="ECDC"]
plot_time_triplett(merged_df,out_dir+"/ECDC.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)

merged_df=data_df.loc[data_df.datasource=="Sweden"]
plot_time_triplett(merged_df,out_dir+"/Sweden-regions.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)
merged_df=data_df.loc[data_df.datasource=="Italy"]
plot_time_triplett(merged_df,out_dir+"/Italy-regions.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)
merged_df=data_df.loc[data_df.datasource=="Spain"]
plot_time_triplett(merged_df,out_dir+"/Spain-regions.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)
merged_df=data_df.loc[data_df.datasource=="JHS-regions"]
plot_time_triplett(merged_df,out_dir+"/USA-regions.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)

scandinavia=["Sweden","Norway","Denmark","Finland","Iceland"]
southeurope=["Spain","Italy","Portugal","France","Greece"]
easteurope=["Poland","Slovakia","Check Reublic","Bulgaria","Romania","Estonia","Latvia","Litthuania","Hungary"]
centraleurope=["UK","Netherlands","France","Switzerland","Austria","Germany","Belgium"]
europe=scandinavia+southeurope+easteurope+centraleurope
        
merged_df=data_df.loc[data_df.country.isin(scandinavia)]
plot_time_triplett(merged_df,out_dir+"/Scandinavia.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)
merged_df=data_df.loc[data_df.country.isin(southeurope)]
plot_time_triplett(merged_df,out_dir+"/Southeurope.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)
merged_df=data_df.loc[data_df.country.isin(easteurope)]
plot_time_triplett(merged_df,out_dir+"/Easteurope.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)
merged_df=data_df.loc[data_df.country.isin(centraleurope)]
plot_time_triplett(merged_df,out_dir+"/Centraleurope.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)
merged_df=data_df.loc[data_df.country.isin(europe)]
plot_time_triplett(merged_df,out_dir+"/Europe.png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)

for country in countries:
    merged_df=data_df.loc[data_df.country==country]
    plot_time_triplett(merged_df,out_dir+country+".png","Deaths",["deaths_ratio","confirmed_ratio"],mobilitycolumns7)


