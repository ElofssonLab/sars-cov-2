#!/usr/bin/env python3

import pandas as pd
import folium
import argparse
from argparse import RawTextHelpFormatter
import re
import numpy as np
import preprocess as pp

p = argparse.ArgumentParser(description = 
             '- Make a map over R0 values -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-data','--input','-i', required= True, help='Input formatted CSV file')
p.add_argument('-o','-out','--output_folder', required= False, help='output folder')
#parser.add_argument('--nargs', nargs='+')
ns = p.parse_args()
if ns.output_folder:
    out_dir = ns.output_folder
else:
    out_dir=home = str(Path.home())+"/Desktop/Corona/R-values/"
    #out_dir=re.sub("/.*","",ns.input)
    if out_dir == ns.input:
        out_dir=home = str(Path.home())+"/Desktop/Corona/R-values"        

# Swedish regions
#https://github.com/deldersveld/topojson/countries/sweden/sweden-counties.json
url = 'https://raw.githubusercontent.com/python-visualization/folium/master/examples/data'
country_shapes = f'{url}/world-countries.json'
R_df = pd.read_csv(ns.input)

# We need to avoid some strange values
maxval=3
tiny=1.e-20
R_df['R0']=R_df['R0'].apply(lambda x:min(x,maxval))
# also try with log
R_df['LogR0']=R_df['R0'].apply(lambda x:(np.log2(max(x,tiny))))

pp.map_names(R_df)

m = folium.Map(location=[48, -102], zoom_start=3)

#print (R_df)
folium.Choropleth(
    geo_data=country_shapes,
    name='choropleth',
    data=R_df,
    columns=['country', 'LogR0'],
    key_on='feature.properties.name',
    fill_color='YlGn',
    fill_opacity=0.7,
    line_opacity=0.2,
    legend_name='Log(R0)'
).add_to(m)

folium.LayerControl().add_to(m)

m.save(out_dir+'/index.html')


