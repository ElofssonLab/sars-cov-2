
# coding: utf-8

# In[304]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import math
from datetime import datetime,date,time, timedelta
from pathlib import Path
import sys

# In[305]:


data_df=pd.read_csv( str(Path.home())+"/Desktop/Corona/data/countries_regions.csv")


# In[306]:


#data_df=data_df.dropna()
data_df=data_df.loc[data_df.datasource=="ECDC"]


# In[307]:




countries=data_df.country.drop_duplicates()
countries=["Greece","Austria","France","Denmark","Norway","Spain",
          "United Kingdom","Sweden","Germany","Italy","Belgium","Netherlands",
           "Switzerland","Finland","Portugal","Ireland","Iceland"]

data_df.date=data_df.apply(lambda x:datetime.strptime(x.date,"%Y-%m-%d"), axis=1)
#data=data.loc[data.date>datetime.strptime("2020-02-01","%Y-%m-%d")]
#countries


# In[308]:



# Now we need to fix mobility before and after the first dates

mobilitycolumns=["retail_and_recreation","grocery_and_pharmacy","parks","transit_stations","workplaces","residential"]
R0columns=["R0"] #,"R0-FixSI"]

    
columns=["country","R0","new_confirmed_cases","new_deaths","date","deaths","confirmed","Ratio",
         "deaths_7days","cases_7days","popData2019","code","retail_and_recreation",
         "grocery_and_pharmacy","parks","transit_stations","workplaces","residential",
         "date_sec","deaths/million","cases/million","Current deaths/million",
         "Current cases/million","log_deaths","log_cases","log_deaths_7days","log_cases_7days",
         "log_deaths/million","log_cases/million","log_current_deaths/million",
         "log_current_cases/million","days_left","log_days_left"]

data_df.rename(columns={
    "deaths/million":"deaths_per_million",
    "cases/million":"cases_per_million",
    "Current deaths/million":"Current-deaths_per_million",
    "Current cases/million":"Current-cases_per_million",
    "log_deaths":"log_deaths",
    "log_deaths/million":"log_deaths_per_million",
    "log_cases/million":"log_cases_per_million",
    "log_current_deaths/million":"log_current_deaths_per_million",
    "log_current_cases/million":"log_current_cases_per_million",
 },inplace=True)


# In[309]:


merged_df = pd.DataFrame([])
for country in countries:
    temp_df=data_df.loc[data_df.country==country]
    for key in mobilitycolumns:
        try:
            firstpos=temp_df[key].notna().idxmax()
            temp_df[key].loc[:firstpos-1]=0
            lastpos=temp_df[key].isna().idxmax()
            lastvalue=temp_df[key].loc[lastpos-8:lastpos-1].mean()
            temp_df[key].loc[lastpos:]=lastvalue
            #print (country,key,firstpos,lastpos)
        except:
            print ("No Nan in beginning for: ",country,key)

    for key in mobilitycolumns+R0columns:
        # First we set it all to 1 until the first date
        # Then we do a rolling mean
        temp_df[key+'_7days']=temp_df[key].rolling(7).mean()
        temp_df[key+'_10days']=temp_df[key].rolling(10).mean()

    # Finally we add the first 6 days.
    for key in mobilitycolumns:
        firstpos=temp_df[key+'_7days'].notna().idxmax()
        firstvalue=temp_df[key+'_7days'].loc[firstpos]
        temp_df[key+'_7days'].loc[:firstpos-1]=firstvalue
        temp_df[key+'_10days'].loc[:firstpos-1]=firstvalue

    merged_df=pd.concat([merged_df, temp_df], ignore_index=True,sort=False)
        # 


# In[310]:


#merged_df


# In[327]:



def plot_time_new(df,outname,name,columns): #x,end,start_dat,end_date,R,cases, lower_bound, higher_bound,lower_bound25, higher_bound75,ylabel,outname)
    '''Plot with shaded 95 % CI (plots both 1 and 2 std, where 2 = 95 % interval)
    '''
    #extra=["new_deaths","new_confirmed_cases"]
    extra=["R0"] #,"R0_7days"]
    #extra=[]
    fig, ax1 = plt.subplots(figsize=(16,10))
    #tmp_df=df.loc[df.date>=startdate]
    tmp_df=df.groupby(['date'])[columns+extra].mean()
    std_df=df.groupby(['date'])[columns+extra].std()
    N_df=df.groupby(['date'])[columns+extra].size()
    N_df=N_df.apply(lambda x: np.sqrt(x))
    stderr_df=pd.DataFrame([])
    lower_bound=pd.DataFrame([])
    higher_bound=pd.DataFrame([])
    lower_bound25=pd.DataFrame([])
    higher_bound75=pd.DataFrame([])

    for c in columns+extra:
        stderr_df[c]=std_df[c]/N_df
        higher_bound[c]=tmp_df[c]+1.96*stderr_df[c]
        lower_bound[c]=tmp_df[c]-1.96*stderr_df[c]
        higher_bound75[c]=tmp_df[c]+1.15*stderr_df[c]
        lower_bound25[c]=tmp_df[c]-1.15*stderr_df[c]


    tmp_df["date"]=df.groupby(['date'])["date"].first()
    #tmp_df=df
    #print (tmp_df)
    x=np.arange(0,len(tmp_df))
    #print (tmp_df)
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    #shift=-0.2
    #for c in extra:
    #    ax2.bar(x+shift,tmp_df[c],yerr=stderr_df[c], alpha = 0.5,width=0.4,label=c)
    #    shift+=0.4
    for c in extra:
        y=tmp_df[c]
        yerr=std_df[c]
        ax2.plot(x,y,label=c, linewidth = 1.0,color="g")
        ax2.fill_between(x, lower_bound[c], higher_bound[c], color='lightgreen', alpha=0.4)
        ax2.fill_between(x, lower_bound25[c], higher_bound75[c], color='lightgreen', alpha=0.6)
        
    ax2.set_ylabel('R0',color='g')
    ax2.tick_params(axis='y', labelcolor='g')
    ax2.legend()
    for c in columns:
        y=tmp_df[c]
        yerr=std_df[c]
        ax1.plot(x,y,label=c, linewidth = 1.0)
        ax1.fill_between(x, lower_bound[c], higher_bound[c], color='cornflowerblue', alpha=0.4)
        ax1.fill_between(x, lower_bound25[c], higher_bound75[c], color='cornflowerblue', alpha=0.6)
    dates = tmp_df['date'].to_list()
    #ax1.legend(loc='lower left', frameon=False, markerscale=2)
    ax1.set_ylabel('Value')
    #ax1.set_yscale('log')
    ax1.set(title="R0 and mobility "+ name )
    ax1.grid(color='b',linestyle="-",linewidth=1)
    ax1.tick_params(axis='x', labelrotation=45 )    
    #ax1.set_ylim([0,max(higher_bound)])
    xticks=np.arange(0,len(tmp_df),7)
    #yticks=[0.1,0.5,1,2,5,10]
    
    #ax1.set_xticklabels(dates)
    #plt.xticks(rotation=45, ha='right')
    ax1.set_xticklabels(dates[0::7],rotation=45)
    #ax1.set_yticklabels(["0.1","0.5","1","2","5","10"])
    ax1.set_xticks(xticks)
    #ax1.set_yticks(yticks)
    plt.gcf().subplots_adjust(bottom=0.25)
    plt.tight_layout()
    ax1.legend()
    fig.savefig(outname, format = 'png', dpi=300)
    plt.close(fig="all")


# In[328]:


merged_df=merged_df.dropna()
merged_df


# In[329]:


plot_time_new(merged_df,"foo.png","R0",["grocery_and_pharmacy","grocery_and_pharmacy_7days","grocery_and_pharmacy_10days"])


# In[330]:


plot_time_new(merged_df,"foo.png","R0",["grocery_and_pharmacy_7days"])


# In[331]:


plot_time_new(merged_df,"foo.png","R0",["retail_and_recreation_7days",
                                        "grocery_and_pharmacy_7days","parks_7days",
                                        "transit_stations_7days","workplaces_7days",
                                        "residential_7days"])


# In[319]:


plot_time_new(merged_df,"foo.png","R0",["R0_7days"])


# In[320]:


print (countries)


# In[ ]:


sys.exit()


# In[152]:


countries=["Greece","Austria","France","Denmark","Norway","Spain",
          "United Kingdom","Sweden","Germany","Italy","Belgium","Netherlands",
         "Switzerland","Finland","Portugal","Ireland","Iceland","USA"]
for c in countries:
    temp_df=data.loc[(data.country==c) & (data.confirmed>100)]
    #print(temp_df)
    temp_df["deltaGrocery"]=temp_df.grocery_and_pharmacy.diff(-7).fillna(0)
    temp_df["deltaTR"]=temp_df.transit_stations.diff(-7).fillna(0)
    temp_df["deltaR0"]=temp_df["R0-FixSI"].diff(-7).dropna()
    fig, ax=plt.subplots(figsize=(10,5))
    #ax.bar(temp_df.date,temp_df.deltaR0,color="b",width=0.2)
    #ax.bar(temp_df.date,temp_df.deltaGrocery/100,color="r",width=0.5,alpha=0.5)
    ax2 = ax.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.bar(temp_df.date,temp_df.new_confirmed_cases/100,color="r",width=0.5,alpha=0.5,label="new cases")
    ax.plot(temp_df.date,temp_df["R0-FixSI"],label="R0")
    ax.plot(temp_df.date,temp_df.grocery_and_pharmacy/100,label="Grocery")
    ax.set_title(c)
    ax.legend()
    fig.show()


# In[172]:


x=[]
y=[]
new_df=pd.DataFrame([])
columns=["deaths_7days","cases_7days"]
R0cols=["R0","R0-FixSI"]
mobcolumns=["retail_and_recreation","grocery_and_pharmacy","parks","transit_stations","workplaces","residential"]
start_trans=datetime.strptime("2020-03-15","%Y-%m-%d")
stop_trans=datetime.strptime("2020-04-15","%Y-%m-%d")
endrecovery=datetime.strptime("2020-05-08","%Y-%m-%d") # We need latest mobility data
lenrecovery=30
countries
country_df=pd.DataFrame([])
mincases=1000
mindeaths=100
countries=["Greece","Austria","France","Denmark","Norway","Spain",
          "United Kingdom","Sweden","Germany","Italy","Belgium","Netherlands",
          "Switzerland","Finland","Portugal","Ireland","Iceland","USA"]
for c in countries:
    temp_df=data.loc[(data.country==c)]
    if (temp_df.confirmed.max()<mincases): continue
    if (temp_df.deaths.max()<mindeaths): continue
    #print(temp_df)
    # Find max value of cases/detas
    newdata={}
    breaktest=False
    if not c in newdata:
        newdata[c]={}
        newdata[c]["country"]=c
    for col in R0cols:
        newdata[c][col+"-max"]=temp_df.loc[(data.date>start_trans)&
                                          (data.date<stop_trans)][col].mean()
        newdata[c][col+"-end"]=temp_df.loc[(data.date>endrecovery-timedelta(lenrecovery))&
                                          (data.date<endrecovery)][col].min()
        #print (c,col,newdata[c][col+"-max"],newdata[c][col+"-end"])
    
    for col in columns+mobcolumns:
        newdata[c][col+"-max"]=temp_df.loc[(data.date>start_trans)&
                                          (data.date<stop_trans) ][col].max()
        if newdata[c][col+"-max"]==0: 
            breaktest=True
    if breaktest:        continue
    #for col in mobcolumns:
    #    newdata[c][col+"-max"]=1
    for col in columns+mobcolumns:
        newdata[c][col+"-end"]=temp_df.loc[(data.date>endrecovery-timedelta(lenrecovery))&
                                          (data.date<endrecovery) ][col].mean()

        #print (c,col,newdata[c][col+"-max"],newdata[c][col+"-end"])
    #fig, ax=plt.subplots(figsize=(10,5))
    #ax.bar(temp_df.date,temp_df.deltaR0,color="b",width=0.2)
    #ax.bar(temp_df.date,temp_df.deltaGrocery/100,color="r",width=0.5,alpha=0.5)
    #ax2 = ax.twinx()  # instantiate a second axes that shares the same x-axis
    #ax2.bar(temp_df.date,temp_df.new_confirmed_cases/100,color="r",width=0.5,alpha=0.5)
    #ax.plot(temp_df.date,temp_df["R0-FixSI"])
    #ax.plot(temp_df.date,temp_df.grocery_and_pharmacy/100)
    #ax.set_title(c)
    tmp_df=pd.DataFrame.from_dict(newdata).transpose()
    tmp_df=tmp_df.dropna()
    #print (newdata)
    #fig.show()
    new_df=new_df.append(tmp_df)
    
#new_df    
#mar=new_df[new_df.date==datetime.strptime("2020-03-01","%Y-%m-%d")]
#apr=new_df[new_df.date==datetime.strptime("2020-04-01","%Y-%m-%d")]
#may=new_df[new_df.date==datetime.strptime("2020-05-15","%Y-%m-%d")]
#new_df.transit_stations30days

for col in columns:
    #print (col,new_df[col+"-end"],new_df[col+"-max"])
    new_df[col+"-diff"]=new_df[col+"-end"]/(new_df[col+"-max"])
for col in mobcolumns+R0cols:
    #print (col,new_df[col+"-end"],new_df[col+"-max"])
    new_df[col+"-diff"]=new_df[col+"-end"] # -new_df[col+"-max"]


    
for mobi in mobcolumns:
    fig, ax=plt.subplots(figsize=(10,5))
    corr={}
    title="Rs: "
    for col in R0cols:
        #print(col,new_df[col+"-diff"],new_df[mobi+"-diff"])
        ax.scatter(new_df[col+"-diff"],new_df[mobi+"-diff"],label=col)
        for i, txt in enumerate(new_df[col+"-diff"]):
            ax.annotate(new_df["country"][i], (new_df[col+"-diff"][i], new_df[mobi+"-diff"][i]))
        #print (new_df[col+"-diff"].to_list(),new_df[mobi+"-diff"].to_list())
        corr[col]=np.corrcoef(new_df[col+"-diff"].to_list(),new_df[mobi+"-diff"].to_list())[0,1]
        #print (col,corr[col])
        title=title+" "+col+" "+str(round(corr[col],3))                  
    ax.set(title=title)
    ax.set(xlabel="Final R0")
    ax.set(ylabel="Change in : "+mobi)
    ax.legend()    
    
    
for mobi in mobcolumns:
    fig, ax=plt.subplots(figsize=(10,5))
    corr={}
    title="Rs: "
    for col in columns:
        #print(col,new_df[ocl+"-diff"],new_df[mobi+"-diff"])
        ax.scatter(new_df[col+"-diff"],new_df[mobi+"-diff"],label=col)
        for i, txt in enumerate(new_df[col+"-diff"]):
            ax.annotate(new_df["country"][i], (new_df[col+"-diff"][i], new_df[mobi+"-diff"][i]))
        #print (new_df[col+"-diff"].to_list(),new_df[mobi+"-diff"].to_list())
        corr[col]=np.corrcoef(new_df[col+"-diff"].to_list(),new_df[mobi+"-diff"].to_list())[0,1]
        #print (col,corr[col])
        title=title+" "+col+" "+str(round(corr[col],3))                  
    ax.set(title=title)
    ax.set(xlabel="Change in cases")
    ax.set(ylabel="End value for: "+mobi)
    ax.legend()    


# In[135]:


#countries

