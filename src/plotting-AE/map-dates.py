#!/usr/bin/env python3

import pandas as pd
import argparse
import branca.colormap as cm
from argparse import RawTextHelpFormatter
import re,sys
from pathlib import Path
import numpy as np
import plotly.express as px
from urllib.request import urlopen
import json
import plotly
import csv
import math
import preprocess as pp
import os
import plotly.io as pio
import geopandas as gpd
from folium.features import GeoJsonPopup
from folium.features import GeoJson
from folium.plugins import TimeSliderChoropleth
import folium
from datetime import date,time, timedelta


pio.orca.config.use_xvfb = True


def world_map(joined_df, metric,outfile,cmap):
    #temp_df['log_'+metric] = np.log10(temp_df[metric])
    #We can now select the columns needed for the map and discard the others:
    temp_df = joined_df[['country', 'date_sec', metric, 'geometry']]
    #print (cmap)
    #print (temp_df[metric])
    #temp_df.to_csv("foo.csv",sep=",")
    #temp_df['color'] = temp_df[metric].map(cmap)
    #for i,j in temp_df.iterrows():
    #    print (i,j["country"],j["date_sec"],j[metric])
    #    print (cmap(j[metric]))
    temp_df['color'] = temp_df[metric].apply(lambda x:cmap(x))
    #temp_df['color'] = temp_df.apply(lambda x:cmap(x[metric]),axis=1)
    #temp_df['color'] = temp_df.loc[.apply(lambda x:cmap(x[metric]),axis=1)
    country_list = temp_df['country'].drop_duplicates().tolist()
    country_idx = range(len(country_list))
    #create dict with all geometry_df
    style_dict = {}
    for i in country_idx:
        country = country_list[i]
        result = temp_df[temp_df['country'] == country]
        inner_dict = {}
        for _, r in result.iterrows():
            inner_dict[r['date_sec']] = {'color': r['color'], 'opacity': 0.9}
        style_dict[str(i)] = inner_dict
    #select country and geometry coloumns
    countries_df = temp_df[['country','geometry']] #,metric]]  # To include metric makes it very very slow
    countries_gdf = gpd.GeoDataFrame(countries_df)
    countries_gdf = countries_gdf.drop_duplicates().reset_index()
    slider_map = folium.Map(min_zoom=2, max_bounds=True,tiles='cartodbpositron')
    #convert to json file
    data=countries_gdf.to_json()
    #create TimeSliderChoropleth for dynamic map depend on the time
    _ = TimeSliderChoropleth(
        data=data,
        styledict=style_dict,
        ).add_to(slider_map)
    #other layer for the popup country info
    a=GeoJson(data, popup=(folium.GeoJsonPopup(fields=['country'])),
                style_function=lambda feature: {'fillColor': 'white',
            'color': 'black', 'fillOpacity': 0.1, 'weight': 0.5})
    a.add_to(slider_map)
    _ = cmap.add_to(slider_map)
    slider_map.save(outfile=outfile)


p = argparse.ArgumentParser(description = 
             '- Make a map over R0 values -',
            formatter_class=RawTextHelpFormatter)
p.add_argument('-data','--input','-i', required= True, help='Input dir formatted CSV file')
p.add_argument('-o','-out','--output_folder', required= False, help='output folder')
#parser.add_argument('--nargs', nargs='+')
ns = p.parse_args()
if ns.output_folder:
    out_dir = ns.output_folder
else:
    out_dir=home = str(Path.home())+"/Desktop/Corona/R-values/"
    #out_dir=re.sub("/.*","",ns.input)
    if out_dir == ns.input:
        out_dir=home = str(Path.home())+"/Desktop/Corona/R-values"        

if not os.path.exists(out_dir):
    print('Creating reports folder...')
    os.system('mkdir -p ' + out_dir)


geometry_df = gpd.read_file(os.getcwd()+'/data/world+regions.json')
geometry_df = geometry_df.rename(columns={'name': 'country'})
#print (geometry_df.country.drop_duplicates())

#concat_df,lastdate,firstdate=pp.get_r0_countries(ns.input)
concatfile=ns.input+"/countries_regions.csv"
# and my calculations on how to normalise deaths and cases
# Now lets save the files
#print (concat_df)
#concat_df.to_csv("concat.csv")
#concat_df=pp.fix_end_dates_etc(concat_df)
#concat_df.to_csv(concatfile)
#sys.exit()

concat_df = pd.read_csv(concatfile)
pp.map_names(concat_df)
# Before merging we need to exclude countries where we have regions 
concat_df=concat_df[ (    # ( concat_df['country'] != "Spain" ) &
                      ( concat_df['country'] != "Sweden" )
                     & ( concat_df['country'] != "United States" )
                     & ( concat_df['country'] != "Italy" )
                     & ( concat_df['country'] != "United States of America" )
                     & ( concat_df['country'] != "USA" )
                     & ( concat_df['country'] != "America" )
                     & ( concat_df['country'] != "China" )
                     & ( concat_df['country'] != "Canada" ))]


#print (firstdate,lastdate)

#sys.exit()
# Delete dates too 
concat_df['date']=concat_df.apply(lambda x:pp.FormatDateMerged(x.date), axis=1)
#We do not want to plot the last dates as they are unreliable for R0
#concat_df=concat_df[ concat_df.date <= lastdate]
minnum=1

joined_df = concat_df.merge(geometry_df, on='country')

#max_color = 2
#min_color = 0
#cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
##cmap = cm.linear.RdBu.scale(min_color, max_color) # this does not work
#metric="R0"
#cmap.caption = metric
#joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
#joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
#world_map(joined_df,metric,out_dir+"/R0.html",cmap)


#print (min(joined_df["deaths_per_million"]),max(joined_df["deaths_per_million"]))
max_color = max(joined_df.loc[joined_df.date> pd.to_datetime(date.today()-timedelta(7))]["deaths"])
min_color = 0 #min(joined_df["deaths"])
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "deaths"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/totaldeaths.html",cmap)



#print (min(joined_df["deaths_per_million"]),max(joined_df["deaths_per_million"]))
max_color = 200
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "deaths_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/deaths.html",cmap)

max_color = 2000
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "cases_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/cases.html",cmap)

max_color = 2
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "Current-deaths_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/currentdeaths.html",cmap)

max_color = 50
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "Current-cases_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/currentcases.html",cmap)


#print (min(joined_df["deaths_per_million"]),max(joined_df["deaths_per_million"]))
max_color = np.log10(1500)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_deaths_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_deaths.html",cmap)

max_color = np.log10(20000)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_cases_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_cases.html",cmap)

minnum=0.1
max_color = np.log10(25)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_current_deaths_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_currentdeaths.html",cmap)

max_color = np.log10(5000)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_current_cases_per_million"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_currentcases.html",cmap)


max_color = 365
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "days_left"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/days_left.html",cmap)

#joined_df.to_csv("joined.csv",sep=",")


max_color = np.log10(3650)
min_color = np.log10(minnum)
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
metric = "log_days_left"
cmap.caption = metric
joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
world_map(joined_df,metric,out_dir+"/log_days_left.html",cmap)


# Try to plot mobility, but we need to delete rows with empty values


max_color = +100
min_color = -100
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
for c in ["retail_and_recreation","grocery_and_pharmacy","parks","transit_stations","workplaces","residential"]:
    joined_df=joined_df.loc[joined_df[c].notna()]
    print (c)
    metric = c 
    cmap.caption = metric
    #joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
    #joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
    world_map(joined_df,metric,out_dir+c+".html",cmap)

 


max_color = +1
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
for c in ["deaths_ratio","confirmed_ratio"]:
    joined_df=joined_df.loc[joined_df[c].notna()]
    print (c)
    metric = c 
    cmap.caption = metric
    #joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
    #joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
    world_map(joined_df,metric,out_dir+c+".html",cmap)


max_color = +0.3
min_color = 0
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
for c in ["deaths_slope","confirmed_slope"]:
    joined_df=joined_df.loc[joined_df[c].notna()]
    print (c)
    metric = c 
    cmap.caption = metric
    #joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
    #joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
    world_map(joined_df,metric,out_dir+c+".html",cmap)

 


# Delete dates too 
#We do not want to plot the last dates as they are unreliable for R0
concat_df=concat_df[ concat_df.date <= pd.to_datetime(date.today()-timedelta(3))]

#print(concat_df)
joined_df = concat_df.merge(geometry_df, on='country')

#max_color = 5
#min_color = 0
min_color=min(joined_df[metric])
max_color=max(joined_df[metric])

# I keep getting an "thresholds are not sorted error
cmap = cm.linear.YlOrRd_09.scale(min_color, max_color) 
#cmap = cm.linear.Purples_09.scale(min_color, max_color) # this does not work


metric="R0"
cmap.caption = metric
#joined_df[metric]= joined_df.apply(lambda x:min(x[metric],max_color),axis=1)
#joined_df[metric]= joined_df.apply(lambda x:max(x[metric],min_color),axis=1)
joined_df=joined_df.loc[joined_df[c].notna()]
#print (min(joined_df[metric]),max(joined_df[metric]))
world_map(joined_df,metric,out_dir+"/R0.html",cmap)

