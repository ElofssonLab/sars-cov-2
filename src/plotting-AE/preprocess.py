import numpy as np
#from dateutil.parser import parse
from datetime import datetime # ,date,time, timedelta
#from dateutil import parser
import csv
import os
import numpy as np
import pandas as pd
import math
from datetime import datetime,date,time, timedelta

def fix_country_names(df):
    translations = {'United_States_of_America':'USA',
                    'United States of America':'USA',
                    #'Central_African_Republic':'CAR',
                    'United Arab Emirates':'United_Arab_Emirates',
                    #'United Arab Emirates':'UAE',
                    "Myanmar (Burma)":"Myanmar",
                    'United_Republic_of_Tanzania':'Tanzania',
                    'Democratic_Republic_of_the_Congo':'Congo',
                    'Iran (Islamic Republic of)':"Iran, Islamic Republic of",
                    'Bosnia and Herzegovina':'Bosnia_and_Herzegovina',
                    'Bosnia':'Bosnia_and_Herzegovina',
                    'Mainland China':'China',
                    'Korea, South':'South Korea',
                    'Republic of Korea':'South Korea',
                    'Cases_on_an_international_conveyance_Japan':'Others',
                    'Mainland China':'China',
                    'Korea, South':'South Korea',
                    'Republic of Korea':'South Korea',
                    'Hong Kong SAR':'Hong Kong',
                    'Taipei and environs':'Taiwan',
                    'Taiwan*':'Taiwan',
                    'Macao SAR':'Macau',
                        'Viet Nam':'Vietnam',
                        ' Azerbaijan':'Azerbaijan',
                        'Czech Republic':'Czechia',
                        'Republic of Ireland':'Ireland',
                        'North Ireland':'Ireland',
                        'Republic of Moldova':'Moldova',
                        'Congo (Brazzaville:)':'Congo',
                        'Congo (Kinshasa)':'Congo',
                        'Republic of the Congo':'Congo',
                        'Gambia, The':'Gambia',
                        'The Gambia':'Gambia',
                        'Unites States':"USA",
                        'US':'USA',
                        "United Kindom":'UK',
                        "United Kingdom":'UK',
                        'Bahamas, The':'Bahamas',
                        'Bahamas':'Bahamas',
                        'Cruise Ship':'Others',
                        'America-Cruise Ship Princess':'Others',
                        'America-Cruise Ship Princess':'Others',
                        'America-Cruise Ship Princess)':'Others',
                        'Australia-Cruise Ship Princess':'Others',
                        'Canada-Cruise Ship Princess':'Others',
                        'Cruise Ship Princess-Cruise Ship Princess':'Others',
                        'Israel-Cruise Ship Princess':'Others',
                        'Others-Cruise Ship Princess cruise ship':'Others',
                        'Others-Cruise Ship Princess':'Others',
                        'Others-Cruise Ship':'Others',
                        'America-American Samoa':'US-American_Samoa',
                        'America-District of Columbia':'US-District_of_Columbia',
                        'America-Grand Princess':'Others',
                        'US-Grand Princess Cruise Ship':'Others',
                        'America-New Hampshire':'US-New_Hampshire',
                        'America-New Jersey':'US-New_Jersey',
                        'America-New Mexico':'US-New_Mexico',
                        'America-New York':'US-New_York',
                        'America-North Carolina':'US-North_Carolina',
                        'America-North Dakota':'US-North_Dakota',
                        'America-Northern Mariana Islands':'US-Northern_Mariana_Islands',
                        'America-Oregon ':'US-Oregon',
                        'America-Puerto Rico':'US-Puerto_Rico',
                        'America-Rhode Island':'US-Rhode_Island',
                        'America-South Carolina':'US-South_Carolina',
                        'America-South Dakota':'US-South_Dakota',
                        'America-United States Virgin Islands':'US-Virgin_Islands',
                        'America-Virgin Islands':'US-Virgin_Islands',
                        'America-Washington, D.C.':'US-Washington,_D.C.',
                        'America-West Virginia':'US-West_Virginia',
                        'America-Wuhan Evacuee':'US-Wuhan_Evacuee',
                        'US-New Hampshire':'US-New_Hampshire',
                        'US-New Jersey':'US-New_Jersey',
                        'US-New Mexico':'US-New_Mexico',
                        'US-New York':'US-New_York',
                        'US-North Carolina':'US-North_Carolina',
                        'US-North Dakota':'US-North_Dakota',
                        'US-Northern Mariana Islands':'US-Northern_Mariana_Islands',
                        'US-Oregon ':'US-Oregon',
                        'US-Puerto Rico':'US-Puerto_Rico',
                        'US-Rhode Island':'US-Rhode_Island',
                        'US-South Carolina':'US-South_Carolina',
                        'US-South Dakota':'US-South_Dakota',
                        'US-United States Virgin Islands':'US-Virgin_Islands',
                        'US-Virgin Islands':'US-Virgin_Islands',
                        'US-Washington, D.C.':'US-Washington,_D.C.',
                        'US-West Virginia':'US-West_Virginia',
                        'US-Wuhan Evacuee':'US-Wuhan_Evacuee',
                        'Antigua and Barbuda-Antigua and Barbuda':'Antigua_and_Barbuda',
                        'Australia-Australian Capital Territory':'Australia-Australian_Capital_Territory',
                        'Australia-External territories':'Australia-External_territories',
                        'Australia-Jervis Bay Territory':'Australia-Jervis_Bay_Territory',
                        'Australia-New South Wales':'Australia-New_South_Wales',
                        'Australia-Northern Territory':'Australia-Northern_Territory',
                        'Australia-South Australia':'Australia-South_Australia',
                        'Australia-Western Australia':'Australia-Western_Australia',
                        'Burkina Faso-Burkina Faso':'Burkina_Faso',
                        'Cabo Verde-Cabo Verde':'Cabo_Verde',
                        'Canada-British Columbia':'Canada-British_Columbia',
                        'Canada-Calgary, Alberta':'Canada-Calgary_Alberta',
                        'Canada-Edmonton, Alberta':'Canada-Edmonton_Alberta',
                        'Canada-Grand Princess':'Others',
                        'Canada-London, ON':'Canada-London_ON',
                        'Canada- Montreal, QC':'Canada-_Montreal_QC',
                        'Canada-New Brunswick':'Canada-New_Brunswick',
                        'Canada-Newfoundland and Labrador':'Canada-Newfoundland_and_Labrador',
                        'Canada-Northwest Territories':'Canada-Northwest_Territories',
                        'Canada-Nova Scotia':'Canada-Nova_Scotia',
                        'Canada-Prince Edward Island':'Canada-Prince_Edward_Island',
                        'Canada-Toronto, ON':'Canada-Toronto_ON',
                        'Cape Verde-Cape Verde':'Cape_Verde',
                        'Cayman Islands-Cayman Islands':'Cayman_Islands',
                        'Central African Republic-Central African Republic':'Central_African_Republic',
                        'Channel Islands-Channel Islands':'Channel_Islands',
                        'China-Hong Kong':'China-Hong_Kong',
                        'China-Inner Mongolia':'China-Inner_Mongolia',
                        'Congo-Congo (Brazzaville)':'Congo',
                        'Costa Rica-Costa Rica':'Costa_Rica',
                        'Cote d\'Ivoire-Cote d\'Ivoire':'Cote_dIvoire',
                        'Denmark-Faroe Islands':'Denmark-Faroe_Islands',
                        'Dominican Republic-Dominican Republic':'Dominican_Republic',
                        'East Timor-East Timor':'East_Timor',
                        'El Salvador-El Salvador':'El_Salvador',
                        'Equatorial Guinea-Equatorial Guinea':'Equatorial_Guinea',
                        'Faroe Islands-Faroe Islands':'Faroe_Islands',
                        'France-Fench Guiana':'France-French_Guiana',
                        'France-French Guiana':'France-French_Guiana',
                        'France-French Polynesia':'France-French_Polynesia',
                        'France-New Caledonia':'France-New_Caledonia',
                        'France-Saint Barthelemy':'France-Saint_Barthelemy',
                        'France-Saint Pierre and Miquelon':'France-Saint_Pierre_and_Miquelon',
                        'France-St Martin':'France-St_Martin',
                        'French Guiana-French Guiana':'France-French_Guiana-',
                        'Holy See-Holy See':'Holy_See',
                        'Hong Kong-Hong Kong':'China-Hong_Kong',
                        'Ivory Coast-Ivory Coast':'Ivory_Coast',
                        'Mississippi Zaandam-Mississippi Zaandam':'Mississippi_Zaandam',
                        'Netherlands-Bonaire, Sint Eustatius and Saba':'Netherlands-Bonaire_Sint_Eustatius_and_Saba',
                        'Netherlands-Sint Maarten':'Netherlands-Sint_Maarten',
                        'New Zealand-New Zealand':'New_Zealand-New_Zealand',
                        'North Macedonia-North Macedonia':'North_Macedonia-North_Macedonia',
                        'occupied Palestinian territory-occupied Palestinian territory':'Palestine',
                        'Papua New Guinea-Papua New Guinea':'Papua_New_Guinea',
                        'Puerto Rico-Puerto Rico':'Puerto_Rico',
                        'Russian Federation-Russian Federation':'Russia',
                        'Saint Barthelemy-Saint Barthelemy':'Saint_Barthelemy',
                        'Saint Kitts and Nevis-Saint Kitts and Nevis':'Saint_Kitts_and_Nevis',
                        'Saint Lucia-Saint Lucia':'Saint_Lucia',
                        'Saint Martin-Saint Martin':'Saint_Martin',
                        'Saint Vincent and the Grenadines-Saint Vincent and the Grenadines':'Saint_Vincent_and_the_Grenadines',
                        'San Marino-San Marino':'San_Marino',
                        'Sao Tome and Principe-Sao Tome and Principe':'Sao_Tome_and_Principe',
                        'Saudi Arabia-Saudi Arabia':'Saudi_Arabia',
                        'Sierra Leone-Sierra Leone':'Sierra_Leone',
                        'South Africa-South Africa':'South_Africa',
                        'South Korea-South Korea':'South_Korea',
                        'South Sudan-South Sudan':'South_Sudan',
                        'Sri Lanka-Sri Lanka':'Sri_Lanka',
                        'St. Martin-St. Martin':'St_Martin._Martin',
                        'The Bahamas':'Bahamas',
                        'The Bahamas-The Bahamas':'Bahamas',
                        'Trinidad and Tobago-Trinidad and Tobago':'Trinidad_and_Tobago',
                        'United Arab Emirates-United Arab Emirates':'United_Arab_Emirates',
                    "Others Princess":"Others",
                    "United Kingdom-Anguilla":"United Kingdom-Anguilla",
                    "United Kingdom-Bermuda":"United Kingdom-Bermuda",
                    "United Kingdom-British Virgin Islands":"United Kingdom-British_Virgin_Islands",
                    "United Kingdom-Cayman Islands":"United Kingdom-Cayman Islands",
                    "United Kingdom-Channel Islands":"United Kingdom-Channel Islands",
                    "United Kingdom-Falkland Islands (Malvinas)":"United Kingdom-Falkland Islands Malvinas",
                    "United Kingdom-Gibraltar":"United Kingdom-Gibraltar",
                    "United Kingdom-Isle of Man":"United_Kingdom-Isle_of_Man",
                    "United Kingdom-Montserrat":"United_Kingdom-Montserrat",
                    "United Kingdom-Turks and Caicos Islands":"United_Kingdom-Turks_and_Caicos_Islands",
                    'UK-Anguilla':'United_Kingdom-Anguilla',
                        'UK-Bermuda':'United_Kingdom-Bermuda',
                        'UK-British Virgin Islands':'United_Kingdom-British_Virgin_Islands',
                        'UK-Cayman Islands':'United_Kingdom-Cayman_Islands',
                        'UK-Channel Islands':'United_Kingdom-Channel_Islands',
                        'UK-Falkland Islands (Islas Malvinas)':'United_Kingdom-Falkland_Islands',
                        'UK-Falkland Islands (Malvinas)':'United_Kingdom-Falkland_Islands',
                        'UK-Falkland Islands Malvinas':'United_Kingdom-Falkland_Islands',
                        'UK-Gibraltar':'United_Kingdom-Gibraltar',
                        'UK-Isle of Man':'United_Kingdom-Isle_of_Man',
                        'UK-Montserrat':'United_Kingdom-Montserrat',
                        'UK-Turks and Caicos Islands':'United_Kingdom-Turks_and_Caicos_Islands',
                        'UK-UK':'UK',
                        'UK-Anguilla':'United_Kingdom-Anguilla',
                        'UK-Bermuda':'United_Kingdom-Bermuda',
                        'UK-British Virgin Islands':'United_Kingdom-British_Virgin_Islands',
                        'UK-Cayman Islands':'United_Kingdom-Cayman_Islands',
                        'UK-Channel Islands':'United_Kingdom-Channel_Islands',
                        'UK-Falkland Islands (Islas Malvinas)':'United_Kingdom-Falkland_Islands',
                        'UK-Falkland Islands (Malvinas)':'United_Kingdom-Falkland_Islands',
                        'UK-Falkland Islands Malvinas':'United_Kingdom-Falkland_Islands',
                        'UK-Gibraltar':'United_Kingdom-Gibraltar',
                        'UK-Isle of Man':'United_Kingdom-Isle_of_Man',
                        'UK-Montserrat':'United_Kingdom-Montserrat',
                        'UK-Turks and Caicos Islands':'United_Kingdom-Turks_and_Caicos_Islands',
                        'UK-United Kingdom':'UK',
                        'United_Kingdom':'UK',
                        'Vatican City-Vatican City':'Vatican_City-Vatican_City',
                        'West Bank and Gaza-West Bank and Gaza':'West_Bank_and_Gaza',
                        'Western Sahara-Western Sahara':'Western_Sahara',
                        'Antigua and Barbuda':'Antigua_and_Barbuda',
                'Burkina Faso':'Burkina_Faso',
                'Cabo Verde':'Cape_Verde',
                'Cape Verde':'Cape_Verde',
                'Cayman Islands':'Cayman_Islands',
                'Central African Republic':'Central_African_Republic',
                'Channel Islands':'Channel_Islands',
                'Costa Rica':'Costa_Rica',
                'Cote d\'Ivoire':'Cote_dIvoire',
                'Cruise Ship':'Others',
                'Cruise Ship Princess':'Others',
                'Dominican Republic':'Dominican_Republic',
                'East Timor':'East_Timor',
                'El Salvador':'El_Salvador',
                'Equatorial Guinea':'Equatorial_Guinea',
                'Faroe Islands':'Faroe_Islands',
                'French Guiana':'French_Guiana',
                'Holy See':'Holy_See',
                'Hong Kong':'Hong_Kong',
                'Ivory Coast':'Ivory_Coast',
                'Mississippi Zaandam':'Mississippi_Zaandam',
                'New Zealand':'New_Zealand',
                'North Macedonia':'North_Macedonia',
                'occupied Palestinian territory':'Palestine',
                'Papua New Guinea':'Papua_New_Guinea',
                'Puerto Rico':'Puerto_Rico',
                'Russian Federation':'Russian_Federation',
                'Saint Barthelemy':'Saint_Barthelemy',
                'Saint Kitts and Nevis':'Saint_Kitts_and_Nevis',
                'Saint Lucia':'Saint_Lucia',
                'Saint Martin':'Saint_Martin',
                'Saint Vincent and the Grenadines':'Saint_Vincent_and_the_Grenadines',
                'San Marino':'San_Marino',
                'Sao Tome and Principe':'Sao_Tome_and_Principe',
                'Saudi Arabia':'Saudi_Arabia',
                'Sierra Leone':'Sierra_Leone',
                'South Africa':'South_Africa',
                'South Korea':'South_Korea',
                'South Sudan':'South_Sudan',
                'Sri Lanka':'Sri_Lanka',
                'St. Martin':'St._Martin',
                'The Bahamas':'Bahamas',
                'Trinidad and Tobago':'Trinidad_and_Tobago',
                'United Arab Emirates':'United_Arab_Emirates',
                'United Kingdom':'UK',
                'Vatican City':'Vatican_City',
                'West Bank and Gaza':'West_Bank_and_Gaza',
                'Western Sahara':'Western_Sahara',
                        "Blekinge County":"Blekinge",
                "Dalarna County":"Dalarna",
                "Gavleborg County":"Gävleborg",
                "Gotland County":"Gotland",
                "Halland County":"Halland",
                "Jamtland County":"Jämtland_Härjedalen",
                "Jämtland Härjedalen":"Jämtland_Härjedalen",
                "Jonkoping County":"Jönköping",
                "Kalmar County":"Kalmar",
                "Kronoberg County":"Kronoberg",
                "Norrbotten County":"Norrbotten",
                "Örebro County":"Örebro",
                "Östergötland County":"Östergötland",
                "Skåne County":"Skåne",
                "Södermanland County":"Sörmland",
                "Stockholm County":"Stockholm",
                "Uppsala County":"Uppsala",
                "Varmland County":"Värmland",
                "Västerbotten County":"Västerbotten",
                "Västernorrland County":"Västernorrland",
                "Västmanland County":"Västmanland",
                "Västra Götaland County":"Västra_Götaland",
                "Västra Götaland":"Västra_Götaland",
                "Valle d'Aosta":"Valle_d_Aosta",
                "Valle D'Aosta/Vallée D'Aoste":"Valle_D_Aosta_Vallee_D_Aoste",
                "Valle_D_Aosta/Vallee_D_Aoste":"Valle_D_Aosta_Vallee_D_Aoste",
                    "Friuli Venezia Giulia":"Friuli_Venezia_Giulia",
                "P.A. Bolzano":"P.A._Bolzano",
                "Trentino-Alto:Adige/Sudtirol":"Trentino-Alto_Adige_Sudtirol",
                "Trentino-Alto Adige/Sudtirol":"Trentino-Alto_Adige_Sudtirol",
                "P.A. Trento":"Trentino-Alto_Adige_Sudtirol",
    'Castile-La Mancha':'Castilla-La Mancha',
    'Castile and León':'Castilla-Leon',
    'Canary Islands':'Canarias',
    'Catalonia':'Cataluña',
    'Balearic Islands':'Baleares',
    'Region of Murcia':'Murcia',
    'Community of Madrid':'Madrid',
    'Basque Country':'Pais Vasco',
    'Valencian Community':'Valencia',
    'Navarre':'Navarra',
    'Friuli-Venezia Giulia':'Friuli Venezia Giulia',
    'Aosta':"Valle D'Aosta/Vallée D'Aoste",
    'Lombardy':'Lombardia',
    'Apulia':'Puglia',
    'Piedmont':'Piemonte',
    'Sardinia':'Sardegna',
    'Trentino-South Tyrol':'Trentino-Alto_Adige_Sudtirol',
    'Tuscany':'Toscana',
                        }
    df.replace(translations, inplace=True)


def fix_states(df):
    df.replace(['.*AL'],['Alabama'],regex=True,inplace=True)
    df.replace(['.*AK'],['Alaska'],regex=True,inplace=True)
    df.replace(['.*AZ'],['Arizona'],regex=True,inplace=True)
    df.replace(['.*AR'],['Arkansas'],regex=True,inplace=True)
    df.replace(['.*CA'],['California'],regex=True,inplace=True)
    df.replace(['.*CO'],['Colorado'],regex=True,inplace=True)
    df.replace(['.*CT'],['Connecticut'],regex=True,inplace=True)
    df.replace(['.*DE'],['Delaware'],regex=True,inplace=True)
    df.replace(['.*DC'],['District of Columbia'],regex=True,inplace=True)
    df.replace(['.*FL'],['Florida'],regex=True,inplace=True)
    df.replace(['.*GA'],['Georgia'],regex=True,inplace=True)
    df.replace(['.*HI'],['Hawaii'],regex=True,inplace=True)
    df.replace(['.*ID'],['Idaho'],regex=True,inplace=True)
    df.replace(['.*IL'],['Illinois'],regex=True,inplace=True)
    df.replace(['.*IN'],['Indiana'],regex=True,inplace=True)
    df.replace(['.*IA'],['Iowa'],regex=True,inplace=True)
    df.replace(['.*KS'],['Kansas'],regex=True,inplace=True)
    df.replace(['.*KY'],['Kentucky'],regex=True,inplace=True)
    df.replace(['.*LA'],['Louisiana'],regex=True,inplace=True)
    df.replace(['.*ME'],['Maine'],regex=True,inplace=True)
    df.replace(['.*MD'],['Maryland'],regex=True,inplace=True)
    df.replace(['.*MA'],['Massachusetts'],regex=True,inplace=True)
    df.replace(['.*MI'],['Michigan'],regex=True,inplace=True)
    df.replace(['.*MN'],['Minnesota'],regex=True,inplace=True)
    df.replace(['.*MS'],['Mississippi'],regex=True,inplace=True)
    df.replace(['.*MO'],['Missouri'],regex=True,inplace=True)
    df.replace(['.*MT'],['Montana'],regex=True,inplace=True)
    df.replace(['.*NE'],['Nebraska'],regex=True,inplace=True)
    df.replace(['.*NV'],['Nevada'],regex=True,inplace=True)
    df.replace(['.*NH'],['New Hampshire'],regex=True,inplace=True)	
    df.replace(['.*NJ'],['New Jersey'],regex=True,inplace=True)	
    df.replace(['.*NM'],['New Mexico'],regex=True,inplace=True)	
    df.replace(['.*NY'],['New York'],regex=True,inplace=True)	
    df.replace(['.*NC'],['North Carolina'],regex=True,inplace=True)	
    df.replace(['.*ND'],['North Dakota'],regex=True,inplace=True)	
    df.replace(['.*OH'],['Ohio'],regex=True,inplace=True)
    df.replace(['.*OK'],['Oklahoma'],regex=True,inplace=True)
    df.replace(['.*OR'],['Oregon'],regex=True,inplace=True)
    df.replace(['.*PA'],['Pennsylvania'],regex=True,inplace=True)
    df.replace(['.*RI'],['Rhode Island'],regex=True,inplace=True)	
    df.replace(['.*SC'],['South Carolina'],regex=True,inplace=True)	
    df.replace(['.*SD'],['South Dakota'],regex=True,inplace=True)	
    df.replace(['.*TN'],['Tennessee'],regex=True,inplace=True)
    df.replace(['.*TX'],['Texas'],regex=True,inplace=True)
    df.replace(['.*UT'],['Utah'],regex=True,inplace=True)
    df.replace(['.*VT'],['Vermont'],regex=True,inplace=True)
    df.replace(['.*VA'],['Virginia'],regex=True,inplace=True)
    df.replace(['.*WA'],['Washington'],regex=True,inplace=True)
    df.replace(['.*WV'],['West Virginia'],regex=True,inplace=True)
    df.replace(['.*WI'],['Wisconsin'],regex=True,inplace=True)
    df.replace(['.*WY'],['Wyoming'],regex=True,inplace=True)
    df.replace(['.*WY'],['Wyoming'], regex=True, inplace=True)
    df.replace(['.*Diamond*'],['Others'], regex=True, inplace=True)
    df.replace(['Virgin Islands, U.S.'],['Virgin Islands'], regex=True, inplace=True)
    
def sigmoidalfunction(x,y,k,m,a,b):
    f=[]
    for i in x:
        f+=[(m-b) / (1 + np.exp(-k*(i-a))) + b ]
    return f
def sigmoidalfunction0(x,y,k,m,a):
    f=[]
    b=0
    for i in x:
        f+=[(m-b) / (1 + np.exp(-k*(i-a))) + b ]
    return f

def FormatDate(x):
    #return (parser.parse(x))
    return (datetime.strptime(x,"%d/%m/%Y"))

def FormatUSDate(x):
    #return (parser.parse(x))
    return (datetime.strptime(x,"%m/%d/%y"))

def FormatDateMerged(x):
    #return (parser.parse(x))
    return (datetime.strptime(x,"%Y-%m-%d"))

def replace_arg_space(country_str):
    return country_str.replace(' ', '_')

def replace_arg_score(country_str):
    return country_str.replace('_', ' ')
def map_names(df):
    translations = {'Antigua_and_Barbuda':'Antigua and Barbuda',
                    'Bosnia_and_Herzegovina':'Bosnia and Herzegovina',
                    'British_Virgin_Islands':'British Virgin Islands',
                    'Brunei_Darussalam':'Brunei Darussalam',
                    'Burkina_Faso':'Burkina Faso',
                    'CAR':'Central African Republic',
                    'Central_African_Republic':'Central African Republic',
                    'Cape_Verde':'Cape Verde',
                    'Cayman_Islands':'Cayman Islands',
                    'Congo':"Republic of the Congo",
                    'Costa_Rica':'Costa Rica',
                    'Cote_dIvoire':"Côte d'Ivoire",
                    'Czechia':'Czech Republic',
                    'Dominican_Republic':'Dominican Republic',
                    'El_Salvador':'El Salvador',
                    #'Vietnam':'Viet Nam',
                    'Equatorial_Guinea':'Equatorial Guinea',
                    'Faroe_Islands':'Faroe Islands',
                    'French_Polynesia':'French Polynesia',
                    'Guinea_Bissau':'Guinea-Bissau',
                    'Holy_See':'Holy See (Vatican City State)',
                    'Isle_of_Man':'Isle of Man',
                    #'Ivore Cost':'Ivore Coast',
                    'Ivory_Coast':'Ivory Coast',
                    'New_Caledonia':'New Caledonia',
                    'New_Zealand':'New Zealand',
                    'North_Macedonia':'Macedonia, the former Yugoslav Republic of',
                    'Northern_Mariana_Islands':'Northern Mariana Islands',
                    'Papua_New_Guinea':'Papua New Guinea',
                    'Puerto_Rico':'Puerto Rico',
                    'Saint_Kitts_and_Nevis':'Saint Kitts and Nevis',
                    'Saint_Lucia':'Saint Lucia',
                    'Sint_Maarten':'Sint Maarten (Dutch part)',
                    'Saint_Vincent_and_the_Grenadines':'Saint Vincent and the Grenadines',
                    'San_Marino':'San Marino',
                    'Sao_Tome_and_Principe':'Sao Tome and Principe',
                    'Saudi_Arabia':'Saudi Arabia',
                    'Sierra_Leone':'Sierra Leone',
                    'South_Africa':'South Africa',
                    'South_Korea':'South Korea',
                    'South_Sudan':'South Sudan',
                    #'Syria':'Syrian Arab Republic',
                    'Taiwan':'Taiwan, Province of China',
                    #'Iran':'Iran, Islamic Republic of',
                    'Tanzania':'Tanzania, United Republic of',
                    #'Tanzania':'United Republic of Tanzania',
                    'Eswatini':'Swaziland',
                    'Sri_Lanka':'Sri Lanka',
                    'UK':'United Kingdom',
                    #'Russia':'Russian Federation',
                    'Palestine':'Palestine, State of',
                    'Moldova':'Moldova, Republic of',
                    'USA':'United States',
                    'UAE':'United Arab Emirates',
                    'Timor_Leste':'Timor-Leste',
                    #'Laos':"Lao People's Democratic Republic",
                    'Trinidad_and_Tobago':'Trinidad and Tobago',
                    'Turks_and_Caicos_islands':'Turks and Caicos Islands',
                    'United_States_Virgin_Islands':'Virgin Islands, U.S.',
                    'United_Arab_Emirates':'United Arab Emirates',
                    'Venezuela':'Venezuela, Bolivarian Republic of',
                    'Trentino-Alto_Adige_Sudtirol':"Trentino-Alto Adige/Sudtirol",
                    "Friuli_Venezia_Giulia":"Friuli Venezia Giulia",
        "US-District_of_Columbia":"US-District of Columbia",
        "US-New_Hampshire":"US-New Hampshire",
        "US-New_Jersey":"US-New Jersey",
        "US-New_Mexico":"US-New Mexico",
        "US-New_York":"US-New York",
        "US-North_Carolina":"US-North Carolina",
        "US-North_Dakota":"US-North Dakota",
        "US-Rhode_Island":"US-Rhode Island",
        "US-South_Carolina":"US-South Carolina",
        "US-South_Dakota":"US-South Dakota",
        "US-West_Virginia":"US-West Virginia",
        "US-Puerto_Rico":"US-Puerto Rico",
                        }
    df.replace(translations, inplace=True)
def svenska_regioner(df):
    translations = {
      "Gotland":"Gotlands län",
      "Kalmar":"Kalmar län",
      "Blekinge":"Blekinge län",
      "Jämtland_Härjedalen":"Jämtlands län",
      "Västerbotten":"Västerbottens län",
      "Kronoberg":"Kronobergs län",
      "Västra_Götaland":"Västra Götalands län",
      "Värmland":"Värmlands län",
      "Dalarna":"Dalarnas län",
      "Sörmland":"Södermanlands län",
      "Jönköping":"Jönköpings län",
      "Östergötland":"Östergötlands län",
      "Stockholm":"Stockholms län",
      "Västmanland":"Västmanlands län",
      "Uppsala":"Uppsala län",
      "Gävleborg":"Gävleborgs län",
      "Västernorrland":"Västernorrlands län",
      "Örebro":"Örebro län",
      "Norrbotten":"Norrbottens län",
      "Halland":"Hallands län",
      "Skåne":"Skåne län"
                        }
    df.replace(translations, inplace=True)
def spain_communities(df):
    translations = {
        "AN":"Andalucia",
        "AR":"Aragon",
        "AS":"Asturias",
        "IB":"Baleares",
        "CN":"Canarias",
        "CB":"Cantabria",
        "CM":"Castilla-La Mancha",
        "CL":"Castilla-Leon",
        "CT":"Cataluña",
        "CE":"Ceuta",
        "EX":"Extremadura", 
        "GA":"Galicia",
        "RI":"La Rioja",
        "MD":"Madrid",
        "ML":"Melilla",
        "MC":"Murcia",
        "NC":"Navarra",
        "VC":"Valencia",
        "PV":"Pais Vasco"
                        }
    df.replace(translations, inplace=True)
def italy_regions(df):
    translations = {
        'Abruzzo':'Abruzzo',
        #"Valle d'Aosta":"Valle D'Aosta/Vallée D'Aoste", # Problems
        #'Apulia':'Puglia',
        'Puglia':'Puglia',
        'Basilicata':'Basilicata',
        'Calabria':'Calabria',
        'Campania':'Campania',
        'Emilia-Romagna':'Emilia-Romagna',
        #'Friuli Venezia Giulia':'Friuli Venezia Giulia',  # Also Udine
        'Lazio':'Lazio',
        'Liguria':'Liguria',
        'Lombardia':'Lombardia',
        'Marche':'Marche',
        'Molise':'Molise',
        'Piemonte':'Piemonte',
        'Sardegna':'Sardegna',
        'Sicilia':'Sicilia',
        #'P.A. Bolzano':'P.A. Bolzano',
        #'P.A. Trento':'Trentino-Alto Adige/Sudtirol', # PRoblens
        'Toscana':'Toscana',
        'Umbria':'Umbria',
        'Veneto':'Veneto',
        "Valle_d_Aosta":"Valle_D_Aosta_Vallee_D_Aoste",
        "#Valle_D_Aosta/Vallee_D_Aoste":"Valle_D_Aosta_Vallee_D_Aoste",
        "Friuli_Venezia_Giulia":"Friuli Venezia Giulia",
        "P.A._Bolzano":"P.A. Bolzano",
        "P.A._Trento":"Trentino-Alto_Adige_Sudtirol",
        "P.A. Bolzano":"Trentino-Alto Adige/Sudtirol"
                        }
    df.replace(translations, inplace=True)
def spain_communities_nogaps(df):
    translations = {
        "AN":"Andalucia",
        "AR":"Aragon",
        "AS":"Asturias",
        "IB":"Baleares",
        "CN":"Canarias",
        "CB":"Cantabria",
        "CM":"Castilla-La_Mancha",
        "CL":"Castilla-Leon",
        "CT":"Cataluña",
        "CE":"Ceuta",
        "EX":"Extremadura", 
        "GA":"Galicia",
        "RI":"La_Rioja",
        "MD":"Madrid",
        "ML":"Melilla",
        "MC":"Murcia",
        "NC":"Navarra",
        "VC":"Valencia",
        "PV":"Pais_Vasco"
                        }
    df.replace(translations, inplace=True)

def fix_regions(df):
    translations = {
        "America-Alabama":"US-Alabama",
        "America-Alaska":"US-Alaska",
        "America-Arizona":"US-Arizona",
        "America-Arkansas":"US-Arkansas",
        "America-California":"US-California",
        "America-Colorado":"US-Colorado",
        "America-Connecticut":"US-Connecticut",
        "America-Delaware":"US-Delaware",
        "America-District_of_Columbia":"US-District of Columbia",
        "America-Florida":"US-Florida",
        "America-Georgia":"US-Georgia",
        "America-Hawaii":"US-Hawaii",
        "America-Idaho":"US-Idaho",
        "America-Illinois":"US-Illinois",
        "America-Indiana":"US-Indiana",
        "America-Iowa":"US-Iowa",
        "America-Kansas":"US-Kansas",
        "America-Kentucky":"US-Kentucky",
        "America-Louisiana":"US-Louisiana",
        "America-Maine":"US-Maine",
        "America-Maryland":"US-Maryland",
        "America-Massachusetts":"US-Massachusetts",
        "America-Michigan":"US-Michigan",
        "America-Minnesota":"US-Minnesota",
        "America-Mississippi":"US-Mississippi",
        "America-Missouri":"US-Missouri",
        "America-Montana":"US-Montana",
        "America-Nebraska":"US-Nebraska",
        "America-Nevada":"US-Nevada",
        "America-New_Hampshire":"US-New Hampshire",
        "America-New_Jersey":"US-New Jersey",
        "America-New_Mexico":"US-New Mexico",
        "America-New_York":"US-New York",
        "America-North_Carolina":"US-North Carolina",
        "America-North_Dakota":"US-North Dakota",
        "America-Ohio":"US-Ohio",
        "America-Oklahoma":"US-Oklahoma",
        "America-Oregon":"US-Oregon",
        "America-Pennsylvania":"US-Pennsylvania",
        "America-Rhode_Island":"US-Rhode Island",
        "America-South_Carolina":"US-South Carolina",
        "America-South_Dakota":"US-South Dakota",
        "America-Tennessee":"US-Tennessee",
        "America-Texas":"US-Texas",
        "America-Utah":"US-Utah",
        "America-Vermont":"US-Vermont",
        "America-Virginia":"US-Virginia",
        "America-Washington":"US-Washington",
        "America-West_Virginia":"US-West Virginia",
        "America-Wisconsin":"US-Wisconsin",
        "America-Wyoming":"US-Wyoming",
        "America-Puerto_Rico":"US-Puerto Rico",
        "US-District_of_Columbia":"US-District of Columbia",
        "US-New_Hampshire":"US-New Hampshire",
        "US-New_Jersey":"US-New Jersey",
        "US-New_Mexico":"US-New Mexico",
        "US-New_York":"US-New York",
        "US-North_Carolina":"US-North Carolina",
        "US-North_Dakota":"US-North Dakota",
        "US-Rhode_Island":"US-Rhode Island",
        "US-South_Carolina":"US-South Carolina",
        "US-South_Dakota":"US-South Dakota",
        "US-West_Virginia":"US-West Virginia",
        "US-Puerto_Rico":"US-Puerto Rico",
        "Canada-Alberta":"CA-Alberta",
        "Canada-British_Columbia":"CA-British Columbia",
        "Canada-Calgary_Alberta":"CA-Alberta",
        "Canada-Edmonton_Alberta":"CA-Edmonton_Alberta",
        "Canada-London_ON":"CA-London_ON",
        "Canada-Manitoba":"CA-Manitoba",
        "Canada-New_Brunswick":"CA-New Brunswick",
        #"Canada-Newfoundland_and_Labrador":"CA-Newfoundland",
        "Canada-Newfoundland_and_Labrador":"CA-Newfoundland and Labrador",
        "Canada-Northwest_Territories":"CA-Northwest Territory",
        "Canada-Nova_Scotia":"CA-Nova Scotia",
        "Canada-Ontario":"CA-Ontario",
        "Canada-Prince_Edward_Island":"CA-Prince Edward Island",
        "Canada-Quebec":"CA-Quebec",
        "Canada-Saskatchewan":"CA-Saskatchewan",
        "Canada-Toronto_ON":"CA-Toronto",
        "Canada-Yukon":"CA-Yukon",
        "Canada-_Montreal_QC":"CA-Montreal",
        #"Australia-Australia":"AU-Australia",
        #"Australia-Australian_Capital_Territory":"AU-Australian Capital Territory",
        #"Australia-New_South_Wales":"AU-New South Wales",
        #"Australia-Northern_Territory":"AU-Northern_Territory",
        #"Australia-Queensland":"AU-Queensland",
        #"Australia-South_Australia":"AU-South Australia",
        #"Australia-Tasmania":"AU-Tasmania",
        #"Australia-Victoria":"AU-Victoria",
        #"Australia-Western_Australia":"AU-Western Australia",
        "China-Anhui":"CH-Anhui",
        "China-Beijing":"CH-Beijing",
        "China-Chongqing":"CH-Chongqing",
        "China-Fujian":"CH-Fujian",
        "China-Gansu":"CH-Gansu",
        "China-Guangdong":"CH-Guangdong",
        "China-Guangxi":"CH-Guangxi",
        "China-Guizhou":"CH-Guizhou",
        "China-Hainan":"CH-Hainan",
        "China-Hebei":"CH-Hebei",
        "China-Heilongjiang":"CH-Heilongjiang",
        "China-Henan":"CH-Henan",
        "China-Hong_Kong":"CH-Hong Kong",
        "China-Hubei":"CH-Hubei",
        "China-Hunan":"CH-Hunan",
        "China-Inner_Mongolia":"CH-Inner Mongolia",
        "China-Jiangsu":"CH-Jiangsu",
        "China-Jiangxi":"CH-Jiangxi",
        "China-Jilin":"CH-Jilin",
        "China-Liaoning":"CH-Liaoning",
        "China-Macau":"CH-Macau",
        "China-Ningxia":"CH-Ningxia",
        "China-Qinghai":"CH-Qinghai",
        "China-Shaanxi":"CH-Shaanxi",
        "China-Shandong":"CH-Shandong",
        "China-Shanghai":"CH-Shanghai",
        "China-Shanxi":"CH-Shanxi",
        "China-Sichuan":"CH-Sichuan",
        "China-Tianjin":"CH-Tianjin",
        "China-Tibet":"CH-Tibet",
        "China-Xinjiang":"CH-Xinjiang",
        "China-Yunnan":"CH-Yunnan",
        "China-Zhejiang":"CH-Zhejiang"
     }
    df.replace(translations, inplace=True)



def get_r0_countries(input):

    ##Good place for geojson: https://github.com/codeforamerica/click_that_hood/tree/master/public/data
    # Swedish regions
    #https://github.com/deldersveld/topojson/countries/sweden/sweden-counties.json
    #url = 'https://raw.githubusercontent.com/python-visualization/folium/master/examples/data'
    #country_shapes = f'{url}/world-countries.json'

    #country_codes = csv.DictReader(open("country-threeletter.csv"))
    #print (os.getcwd()+'/data/country-threeletter.csv')
    reader = csv.reader(open(os.getcwd()+'/data/country-threeletter.csv', 'r'))
    country_codes = {}
    for row in reader:
        k, v = row
        country_codes[k] = v
    ###print (country_codes)
    #country_codes=pd.read_csv("country-threeletter.csv")
    r_df = pd.read_csv(input+"/merged_FHM_xls-R0-mobility.csv") # Sweden dataq
    r2_df = pd.read_csv(input+"/merged_ECDC-R0-mobility.csv") # ECDC data
    #r3_df = pd.read_csv(input+"/merged_CCAA-R0-mobility.csv") # ECDC data
    #r4_df = pd.read_csv(input+"/merged_province_JHS-R0-mobility.csv") # ECDC data
    #r5_df = pd.read_csv(input+"/merged_italy-R0-mobility.csv") # ECDC data
    pop_df=pd.read_csv(os.getcwd()+"/data/regioner-befolknings.csv") # ECDC data

    r_df["datasource"]="Sweden"
    r2_df["datasource"]="ECDC"
    #r3_df["datasource"]="Spain"
    #r4_df["datasource"]="JHS-regions"
    #r5_df["datasource"]="Italy"
    
    # We do need to find the last date where R0 is not NaN
    lastlist=[]
    firstlist=[]
    if (math.isnan(r_df.R0.tail(1).item())):
        last=r_df[r_df["R0"].notna()].index[-1]
    else:
        last=r_df.index[-1]
    last_r=r_df.date[last]
    first=r_df[r_df.R0.notna()].index[0]
    first_r=r_df.date[first]
    lastlist+=[FormatDateMerged(last_r)]
    firstlist+=[FormatDateMerged(first_r)]

    if (math.isnan(r2_df.R0.tail(1).item())):
        last=r2_df[r2_df["R0"].notna()].index[-1]
    else:
        last=r2_df.index[-1]
    last_r2=r2_df.date[last]
    first=r2_df[r2_df.R0.notna()].index[0]
    first_r2=r2_df.date[first]
    lastlist+=[FormatDateMerged(last_r2)]
    firstlist+=[FormatDateMerged(first_r2)]


    #if (math.isnan(r3_df.R0.tail(1).item())):
    #    last=r3_df[r3_df["R0"].notna()].index[-1]
    #else:
    #    last=r3_df.index[-1]
    #last_r3=r3_df.date[last]
    #first=r3_df[r3_df.R0.notna()].index[0]
    #first_r3=r3_df.date[first]
    #lastlist+=[FormatDateMerged(last_r3)]
    #firstlist+=[FormatDateMerged(first_r3)]

    #if (math.isnan(r4_df.R0.tail(1).item())):
    #    last=r4_df[r4_df["R0"].notna()].index[-1]
    #else:
    #    last=r4_df.index[-1]
    #last_r4=r4_df.date[last]
    #first=r4_df[r4_df.R0.notna()].index[0]
    #first_r4=r4_df.date[first]
    #lastlist+=[FormatDateMerged(last_r4)]
    #firstlist+=[FormatDateMerged(first_r4)]
    #
    #
    #if (math.isnan(r5_df.R0.tail(1).item())):
    #    last=r5_df[r5_df["R0"].notna()].index[-1]
    #else:
    #    last=r5_df.index[-1]
    #last_r5=r5_df.date[last]
    #first=r5_df[r5_df.R0.notna()].index[0]
    #first_r5=r5_df.date[first]
    #lastlist+=[FormatDateMerged(last_r5)]
    #firstlist+=[FormatDateMerged(first_r5)]

    #print(firstlist,lastlist)
    #print(max(firstlist),min(lastlist))
    lastdate=min(lastlist)
    firstdate=max(firstlist)
    #sys.exit()
    #print (r_df)
    #R_df=r_df.loc[r_df.date==str(lastdate.date())]
    #R2_df=r2_df.loc[r2_df.date==str(lastdate.date())]
    #R3_df=r3_df.loc[r3_df.date==str(lastdate.date())]
    #R4_df=r4_df.loc[r4_df.date==str(lastdate.date())]
    #R5_df=r5_df.loc[r5_df.date==str(lastdate.date())]

    # in map-dates
    r_df['daterep']=r_df.apply(lambda x:FormatDateMerged(x.date), axis=1)
    r2_df['daterep']=r2_df.apply(lambda x:FormatDateMerged(x.date), axis=1)
    #r3_df['daterep']=r3_df.apply(lambda x:FormatDateMerged(x.date), axis=1)
    r4_df['daterep']=r4_df.apply(lambda x:FormatDateMerged(x.date), axis=1)
    r5_df['daterep']=r5_df.apply(lambda x:FormatDateMerged(x.date), axis=1)


    if (r_df.loc[r_df.daterep==lastdate]["R0"].isnull().values.any()):
            lastdate=lastdate-timedelta(1)
    if (r2_df.loc[r2_df.daterep==lastdate]["R0"].isnull().values.any()):
            lastdate=lastdate-timedelta(1)
    #if (r3_df.loc[r3_df.daterep==lastdate]["R0"].isnull().values.any()):
    #        lastdate=lastdate-timedelta(1)
    #if (r4_df.loc[r4_df.daterep==lastdate]["R0"].isnull().values.any()):
    #        lastdate=lastdate-timedelta(1)
    #if (r5_df.loc[r5_df.daterep==lastdate]["R0"].isnull().values.any()):
    #        lastdate=lastdate-timedelta(1)
            

    # This deletes to much
            
    #R_df=r_df.loc[   (r_df.daterep<=lastdate) & (  r_df.daterep >=firstdate)]
    #R2_df=r2_df.loc[(r2_df.daterep<=lastdate) & ( r2_df.daterep >=firstdate)]
    # R3_df=r3_df.loc[(r3_df.daterep<=lastdate) & ( r3_df.daterep >=firstdate)]
    #R4_df=r4_df.loc[(r4_df.daterep<=lastdate) & ( r4_df.daterep >=firstdate)]
    #R5_df=r5_df.loc[(r5_df.daterep<=lastdate) & ( r5_df.daterep >=firstdate)]

    R_df=r_df
    R2_df=r2_df
    #R3_df=r3_df
    #R4_df=r4_df
    #R5_df=r5_df
    

    #print (R_df,r_df)


    #sys.exit()


    # We need to avoid some strange values
    maxval=3
    tiny=1.e-20
    #R_df['R0']=R_df['R0'].apply(lambda x:min(x,maxval))
    # also try with log
    R_df['LogR0']=R_df['R0'].apply(lambda x:(np.log2(max(x,tiny))))


    #R_df['code']=R_df['country'].apply(lambda x:(country_codes[x]))
    #print (SE_regioner)
    #print (world)
    #print (US_counties)
    # Good place for maps: https://github.com/codeforamerica/click_that_hood/tree/master/public/data
    #df = pd.read_csv("https://raw.githubusercontent.com/plotly/datasets/master/fips-unemp-16.csv",    dtype={"fips": str})

    pop_df=pop_df.rename(columns={
        'Namn':'country',
        'Pop2020':'popData2019'
        })
    sweden_df=pd.merge(R_df, pop_df, how='left', on=['country'])
    svenska_regioner(sweden_df)
    svenska_regioner(R_df)
    sweden_df["code"]=sweden_df["country"]


    map_names(R2_df)
    #print (R2_df)
    R2_df['code']=R2_df['country'].apply(lambda x:(country_codes[x]))
    #exclude_countries=["Spain","Sweden","United Stated of America"]
    #world_df=R2_df[ ((R2_df['country'] != "Spain" ) & (R2_df['country'] != "Sweden" ) & (R2_df['country'] != "United States of America" ))]
    #world_df=R2_df[ R2_df['country'] in exclude_countries)]
    # We skip this only when we make the maps
    #world_df=R2_df[ (  ( R2_df['country'] != "Spain" )
    #                 & ( R2_df['country'] != "Sweden" )
    #                 & ( R2_df['country'] != "United States" )
    #                 & ( R2_df['country'] != "Italy" )
    #                 & ( R2_df['country'] != "United States of America" )
    #                 & ( R2_df['country'] != "USA" )
    #                 & ( R2_df['country'] != "America" )
    #                 & ( R2_df['country'] != "China" )
    #                 & ( R2_df['country'] != "Canada" ))]
    world_df=R2_df


    #spain_communities(R3_df)
    #spain_df=pd.merge(R3_df, pop_df, how='left', on=['country'])
    #spain_df["code"]=spain_df["country"]


    #print (R5_df.country.drop_duplicates())
    #italy_regions(R5_df)
    #italy_df=pd.merge(R5_df, pop_df, how='left', on=['country'])
    #italy_df["code"]=italy_df["country"]
    #print (R5_df.country.drop_duplicates())


    #fix_regions(R4_df)
    # We only include regons we have populations data for.
    regions_df=pd.merge(R2_df, pop_df, how='inner', on=['country'])
    regions_df["code"]=regions_df["country"]

    #common_cols = list(set.intersection(*(set(df.columns) for df in frames)))

    #print (regions_df)
    columns=["country","R0","sdR0","R0-FixSI","sdR0-FixSI","new_confirmed_cases",
                 "new_deaths","date","deaths","confirmed",
                 "Ratio","deaths_7days","cases_7days","popData2019",
                 "code","retail_and_recreation","grocery_and_pharmacy",
                 "parks","transit_stations","workplaces","residential",
             "datasource","Days","DeathsDays","LogCases","LogDeaths","LinCases","LinDeaths"]
    concat_df=pd.concat([sweden_df[columns],spain_df[columns],italy_df[columns],regions_df[columns],world_df[columns]])
    
    #print (concat_df)

    concat_df.replace({
        "United States":"United States of America",
        "Côte d'Ivoire":"Ivory Coast",
        "Guinea-Bissau":"Guinea",
        "Congo":"Republic of the Congo",
        "Iran, Islamic Republic of":"Iran",
        "Syrian Arab Republic":"Syria",
        "Russian Federation":"Russia",
        "Lao People's Democratic Republic":"Laos",
        "Viet Nam":"Vietnam",
        "Taiwan, Province of China":"Taiwan",
        "Korea, Republic of":"South Korea",
        "Tanzania, United Republic of":"Tanzania",
        "Moldova, Republic of":"Moldova",
        "Serbia":"Republic of Serbia",
        "Macedonia, the former Yugoslav Republic of":"Macedonia",
        "Faroe Islands":"Faroarna",
        'Venezuela, Bolivarian Republic of':'Venezuela',
        'Moldova, Republic of':'Moldova',
        'Tanzania, United Republic of':'United Republic of Tanzania',
        "Newfoundland":"Newfoundland and Labrador",
                       }, inplace=True)



    #concat_df=concat_df.fillna(0)
    concat_df['date_sec'] = pd.to_datetime(concat_df['date']).astype(int) / 10**9
    concat_df['date_sec'] = concat_df['date_sec'].astype(int).astype(str)


    # We need to set population to something in countries with missing data (set it to 100K)
    #concat_df['popData2019']= concat_df.apply(lambda x:max(x['popData2019'],100000),axis=1)
    # Just deal with some missing values
    concat_df['popData2019'].fillna(100000, inplace=True)
    #concat_df.R0.fillna(, inplace=True)
    concat_df["new_confirmed_cases"].fillna(0,inplace=True)
    concat_df["new_deaths"].fillna(0,inplace=True)
    concat_df["deaths"].fillna(0,inplace=True)
    concat_df["confirmed"].fillna(0,inplace=True)
    concat_df["Ratio"].fillna(0,inplace=True)
    concat_df["deaths_7days"].fillna(0,inplace=True)
    concat_df["cases_7days"].fillna(0,inplace=True)
    # JHS data has sometimes negative numbers
    concat_df['deaths_7days']= concat_df.apply(lambda x:max(x['deaths_7days'],0),axis=1)
    concat_df['cases_7days']= concat_df.apply(lambda x:max(x['cases_7days'],0),axis=1)
    

    minnum=1

    concat_df['deaths_per_million']=1000000*concat_df['deaths']/concat_df['popData2019']
    concat_df['cases_per_million']=1000000*concat_df['confirmed']/concat_df['popData2019']
    concat_df['Current-deaths_per_million']=1000000*concat_df['deaths_7days']/concat_df['popData2019']
    concat_df['Current-cases_per_million']=1000000*concat_df['cases_7days']/concat_df['popData2019']


    # We need to to use log to see anything I think 
    concat_df['log_deaths'] = np.log10(concat_df['deaths']+minnum)
    concat_df['log_cases'] = np.log10(concat_df['confirmed']+minnum)
    concat_df['log_deaths_7days'] = np.log10(concat_df['deaths_7days']+minnum)
    concat_df['log_cases_7days'] = np.log10(concat_df['cases_7days']+minnum)
    concat_df['log_deaths_per_million'] = np.log10(concat_df['deaths_per_million']+minnum)
    concat_df['log_cases_per_million'] = np.log10(concat_df['cases_per_million']+minnum)
    minnum=0.1
    concat_df['log_current_deaths_per_million'] = np.log10(concat_df['Current-deaths_per_million']+minnum)
    concat_df['log_current_cases_per_million'] = np.log10(concat_df['Current-cases_per_million']+minnum)
    minnum=1
    #sys.exit()
    #concat_df.to_csv("concat.csv",sep=",")


    # Eriks beräkning av dagar kvar

    cfr=0.00025  # From Gisecke
    herd=0.6
    minnum=1

    concat_df["days_left"]= concat_df.apply(lambda x:((herd*cfr*x.popData2019-x.deaths)/(x.deaths_7days+minnum)),axis=1)
    concat_df["log_days_left"]= concat_df.apply(lambda x:(np.log10(max(x.days_left,minnum))),axis=1)

    #concat_df['date']=concat_df.apply(lambda x:FormatDateMerged(x.date), axis=1)
    return(concat_df,lastdate,firstdate)
    

def fix_end_dates_etc(data_df):
    casecolumns=["confirmed","deaths"]
    mobilitycolumns=["retail_and_recreation","grocery_and_pharmacy","parks",
                 "transit_stations","workplaces","residential"]
    R0columns=["R0","R0-FixSI"]
    R0sdcolumns=["sdR0","sdR0-FixSI"]

    
    #columns=["country","R0","new_confirmed_cases","new_deaths","date","deaths","confirmed","Ratio",
    #     "deaths_7days","cases_7days","popData2019","code","retail_and_recreation",
    #     "grocery_and_pharmacy","parks","transit_stations","workplaces","residential",
    #     "date_sec","deaths/million","cases/million","Current deaths/million",
    #     "Current cases/million","log_deaths","log_cases","log_deaths_7days","log_cases_7days",
    #     "log_deaths/million","log_cases/million","log_current_deaths/million",
    #     "log_current_cases/million","days_left","log_days_left"]


    tiny=0.0002
    merged_df = pd.DataFrame([])
    countries=data_df.country.drop_duplicates()
    for country in countries:
        #print (country)
        temp_df=data_df.loc[data_df.country==country].reset_index()
        # The last three days 
        for key in R0columns+R0sdcolumns:
            temp_df[key].iloc[-3:]=temp_df[key].iloc[-11:-4].mean()
        for key in mobilitycolumns:
            firstpos=temp_df[key].isna().idxmin()
            if (firstpos>0):
                temp_df[key].iloc[:firstpos]=0
                
            lastpos=temp_df[key].isna().idxmax()
            if (lastpos>0):
                lastvalue=temp_df[key].loc[lastpos-8:lastpos-1].mean()
                temp_df[key].iloc[lastpos:]=lastvalue
            #print (country,key,firstpos,lastpos)
        for key in R0columns+mobilitycolumns:
            #print (key)
            temp_df[key+"_diff7"]=temp_df[key].diff(7)
        for key in casecolumns:
            #print (key)
            temp_df[key+"_diff7"]=temp_df[key].diff(7)
            temp_df[key+"_ratio"]=(tiny+temp_df[key+"_diff7"])/(temp_df[key]+tiny)
            temp_df[key+"_slope"]=temp_df[key+"_diff7"]/(temp_df[key].max())
            # Fix first 7 values
            temp_df[key+"_ratio"].fillna(1)
            temp_df[key+"_slope"].fillna(0)

        for key in mobilitycolumns+R0columns+casecolumns:
            # Then we do a rolling mean
            temp_df[key+'_7days']=temp_df[key].rolling(7).mean()


        #firstpos=temp_df[key].isna().idxmin()
        #if (firstpos>0):
        #    temp_df[key].iloc[:firstpos]=0
            
        # Finally we add the first 6 days.
        for key in mobilitycolumns+R0columns:
            firstpos=temp_df[key+'_7days'].isna().idxmin()
            if (firstpos>0):
                firstvalue=temp_df[key+'_7days'].iloc[firstpos+1]
                temp_df[key+'_7days'].iloc[:firstpos]=firstvalue
            firstpos=temp_df[key+'_diff7'].isna().idxmin()
            if (firstpos>0):
                #firstvalue=temp_df[key+'_diff7'].iloc[firstpos+1]
                temp_df[key+'_diff7'].loc[:firstpos]=0 # firstvalue
        # Fix a few more beginnings
        for key in casecolumns:
            firstpos=temp_df[key+'_7days'].isna().idxmin()
            if (firstpos>0):
                #print ("test",key,firstpos)
                temp_df[key+'_7days'].iloc[:firstpos]=0
            firstpos=temp_df[key+'_diff7'].isna().idxmin()
            if (firstpos>0):
                temp_df[key+'_diff7'].iloc[:firstpos]=0
            firstpos=temp_df[key+'_ratio'].isna().idxmin()
            if (firstpos>0):
                temp_df[key+'_ratio'].iloc[:firstpos]=1
            firstpos=temp_df[key+'_slope'].isna().idxmin()
            if (firstpos>0):
                temp_df[key+'_slope'].iloc[:firstpos]=0

        #for key in mobilitycolumns+R0columns:
        #    firstpos=temp_df[key+'_7days'].notna().idxmax()
        #    firstvalue=temp_df[key+'_7days'].iloc[firstpos]
        #    temp_df[key+'_7days'].iloc[:firstpos]=firstvalue
        #    firstpos=temp_df[key+'_diff7'].notna().idxmax()
        #    firstvalue=temp_df[key+'_diff7'].iloc[firstpos]
        #    temp_df[key+'_diff7'].loc[:firstpos]=0 # firstvalue
        # Fix a few more beginnings
        #for key in casecolumns:
        #    firstpos=temp_df[key+'_7days'].notna().idxmax()
        #    #print ("test",key,firstpos)
        #    temp_df[key+'_7days'].iloc[:firstpos]=0
        #    firstpos=temp_df[key+'_diff7'].notna().idxmax()
        #    temp_df[key+'_diff7'].iloc[:firstpos]=0
        #    firstpos=temp_df[key+'_ratio'].notna().idxmax()
        #    temp_df[key+'_ratio'].iloc[:firstpos]=1
        #    firstpos=temp_df[key+'_slope'].notna().idxmax()
        #    temp_df[key+'_slope'].iloc[:firstpos]=0
        #
        #
        #    #print (country,key,firstpos,lastpos)
        merged_df=pd.concat([merged_df, temp_df], ignore_index=True,sort=False)
        # 

    return merged_df
