#!/bin/bash -x

homedir=$HOME/git/sars-cov-2/src/plotting-AE/

outdir=${HOME}/Desktop/Hugo/
hugodir=${homedir}/HugoTheme/
URL=ae.scilifelab.se:/var/www/html/covid19.bioinfo.se/docroot/Corona/
for dir in $*
do
    echo ${dir}
    base=`basename ${dir}`
    lcbase=`echo "${base}" | tr '[:upper:]' '[:lower:]'`
    basedir=${outdir}/${base}/
    rm -rf ${basedir}
    #subdir=${base}-nations
    mkdir -p ${basedir}/content/photo/${base}
    rsync -arv ${hugodir}/ ${basedir}/
    rsync ${dir}/*.png ${basedir}/content/photo/${base}/
    pushd ${basedir}/
    #hugo new content/photo/${base}/index.md
    index=${basedir}/content/photo/${base}/index.md
    cat >${index} <<EOF
---
title: Different data series for corona using $base

resources:

EOF
    for i in content/photo/${base}/*png
    do
	j=`basename "${i}" .png`
    cat >>${index} <<EOF
- src: "${j}.png"
  name: Data from ${j}
  params:
    order: ${j}
    description: ${j}
    button_text: ${j}
    button_url: "photo/${lcbase}/$j.png"

EOF
    done
    cat >>${index} <<EOF
---
EOF
    /snap/bin/hugo -D -b "https://covid19.bioinfo.se/Corona/${base}/"
    rsync -arv --delete  -e "ssh -i ~/.ssh/id2_rsa"  public/ ${URL}/$base/
    #rsync -arv    public/ $URL/$base/
    popd
done


