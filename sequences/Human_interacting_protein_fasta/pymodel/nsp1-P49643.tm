
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/nsp1.pdb                              
Name of Chain_2: pymodel/nsp1-P49643.pdb_A                         
Length of Chain_1:  180 residues
Length of Chain_2:  277 residues

Aligned length=   85, RMSD=   5.15, Seq_ID=n_identical/n_aligned= 0.376
TM-score= 0.28559 (if normalized by length of Chain_1)
TM-score= 0.20770 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MESLVPGFNEKTHVQLSLPVLQV-------------------RDVL--VR--GFGDSVEEVLSEARQHLKDGTCGLVEVEKG-VLPQLEQ-PYVFIKRSDARTAPHGHVMVELVAELEG----IQYGRSGE---------TLGVLVPHVGEIPVAYRKVLLR-----------------------------------------------------------------------------------------------------------KN--GNKGAGGH--SYGADLKSFDLGD-----------------------ELGTDPYEDFQ--EN-WNTK--------HSSGVTRELMRELNGG---------
                                          ..::  .:  .::::::::::::::::::::::::::... :.:.    ..::::.             ::::::.     .::.             ...:::.       .:.                                                                                                                .   :..  :::  .                                   .:: :.       .  .:          .                        
-----------------------MESLVPGFNEKTHVQLSLPVLQVRDVLVRGFGDSVEEVLSEARQHLKDGTCGLVEVEKGVLPQL---EQPYVFIK-------------RSDARTA-PHGHVMVE----LVAELEGIQYGRSGET-------LGV-----LVPHVGEIPVAYRKVLLRKNGNKGAGGHSYGADLKSFDLGDELGTDPYEDFQENWNTKHSSGVTRELMRELNGGRRRADRNANNADRNRRNDRRRDRARRDRRDDDAD-RAADA--DRRDAD------------ANRAAAARADRNDNDDRARNRRADDD-NR-----RDN-DRD--DANDDNNRN---------------DDAAANDDD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P49643.pdb                            
Name of Chain_2: pymodel/nsp1-P49643.pdb_B                         
Length of Chain_1:  509 residues
Length of Chain_2:  509 residues

Aligned length=  209, RMSD=   7.61, Seq_ID=n_identical/n_aligned= 0.091
TM-score= 0.24615 (if normalized by length of Chain_1)
TM-score= 0.24615 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MEFSGRKWR--------------------------------------------------------------------------------------------------------KLRL-----------AGDQRNASYPHCL--Q---FYLQPPSENISLIEFE-----------NL-AIDRVKLLKSVENLGVSYVKGTEQYQSKLESELRKLKFSYRENLEDEYEPRRRDHISHFILRLAYCQSEELRRWFIQQEMDLLRFRFSILPKDKIQDFLKDSQLQFEAISDEEKTLREQEIVASSPSLSGLKLGFESIYKIPFADALDLFRGRKVYLEDGFAYVPLKDIVAIILNEFRAKLSKALALTARSLP-AV---------QSDERLQPLLNHLSHSYTGQDYSTQGNVGKISLDQIDLLSTKSFPPCMRQLHKALRENHHLRHGGRMQYGLFLKGIGLTLEQALQFW-----------KQEFIKGKMDPDKFDKGYSYNIRHSFGKEGKRTDYTPFSCLKIILSNPPSQGD--YHG---C-PFRHSDPELLKQKLQSYKISPGGISQ----ILDLVKGTHYQVACQKYFEMIHNVDD--CGFSLNHPNQFFCESQRILNGGKDIKK----------------EPIQPETPQPKPSV-----------QKTKDASS----------------------------------------------------ALA----S----------------------------------------------------LNSSLEMDMEGLEDYFSEDS
                                                                                                                 .:..           :.::::...::..  .   .....                      .  ....:......:.::.::  . ::. .: ..:.  .                        .  ..                                                                                                                        ...::..: :.         ::::..:..::..:..... ..        ::::....                                                            ....::.......:.  .....                ..:...:...:..    .     . ....:.:..:..:....             ...:...:...............     ..::......                                 .....                    ..                                                          ...    .                                                    .::::::::::::...:.  
---------MEFSGRKWRKLRLAGDQRNASYPHCLQFYLQPPSENISLIEFENLAIDRVKLLKSVENLGVSYVKGTEQYQSKLESELRKLKFSYRENLEDEYEPRRRDHISHFILRLAYCQSEELRRWFIQQEMDLLRFRFSILPKDKIQDF-----------LKDSQLQFEAIS-DEEKTLREQEIVASSPSLS--G-LKL-GF-ESIY--K------------------------I--PF------------------------------------------------------------------------------------------------------------------------ADALDLFRGRKVYLEDGFAYVPLKDIVAIILNEFRAKLS-KA--------LALTARSL-------------------------------------------------PAVQSDERLQPLLNHLSHSYTGQDYS--TQGNV----------------GKISLDQIDLLST--KSF--PPCMRQLHKALRENHHLRHGGR---------MQYGLFLKGIGLTLEQALQFWKQEFIK---GKMDPDKFDKGY-----------------SYNIRHSFGKEGKRTDYTPFS---------CLKIILSNPPSQG------DYHGCPFRHSDPELLKQKLQSYKISPGGISQILDLVKGTHYQVACQKYFEMIHNVDDCGFSLNHPNQFFCESQRILNGGKDIKKEPIQPETPQPKPSVQKTKDASSALASLNSSLEMDMEGLEDYFSEDS--

