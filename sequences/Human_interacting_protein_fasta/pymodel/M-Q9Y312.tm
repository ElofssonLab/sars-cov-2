
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-Q9Y312.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  295 residues

Aligned length=  106, RMSD=   3.48, Seq_ID=n_identical/n_aligned= 0.717
TM-score= 0.36170 (if normalized by length of Chain_1)
TM-score= 0.28647 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYR----INWITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMWSFNPETNILLNVPLH--GTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
.:.::.::::::::::::::::::::::::::.:::::::::::::::::::::::::::::::::::::::    :::::::::::::::::::::::::::::: :. .                    .                                                                                                                                                                                                                                                                                       
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLMWLSYFIASFRLFART-RS-M------------------WSF------------------------------------------------------------------------------------------------NPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQAADARANDNRDANRRRRDAARANDANANRADDRNRARRARAADANDANARNRAANADDDNAADARAARDAADA


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q9Y312.pdb                            
Name of Chain_2: pymodel/M-Q9Y312.pdb_B                            
Length of Chain_1:  384 residues
Length of Chain_2:  384 residues

Aligned length=  217, RMSD=   6.56, Seq_ID=n_identical/n_aligned= 0.138
TM-score= 0.34901 (if normalized by length of Chain_1)
TM-score= 0.34901 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MAAVQMDP-------------------------------------------------ELAKRLFFEGAT---------------------VVILNMPKGTEFGIDYNSWEVGPKFRGVKMIPPGIHFLHYSSVDKANPKEVGPRMGFFLSLHQRGLTVLRWSTLREEVDLSPAPESEVEAMRANLQELDQFLGPYPYATLKKW-IS-------------LTNFISEAT---VEKLQPENR---------------QICAFSDVLPVLSMKHTKDRVGQ------NLPRCGIECKSYQEGLARLPEMKPR--------AGTEIRFSELPTQMFPEGATPAEITKHSMDLSYALETVLNKQ-----------------------------FPSSPQDVLGELQFAFVCFLLGNVYEAFEHWKRLLNLLCRS-EAAMMKHHTLYINLISILYHQLGE-IPA--DFFVDIVSQDNFLTSTLQVFFSSACSIAVDATLRKKAEKFQAHLTKKFR---W--DF--AAEPEDCAPVVVELPEG-----------IEMG
                                                         ...::..                          ..::       .               :: .     ::                  ::.      .                     .  :  ::::::. :::::::..... ..             ..::..      .                       ...:::::.:::..:.             ..:::::.:::.::..:..              .....:::::::....                                                       ...:::::::::... ..    .  .. ..........::: ::..::.:::::::::::...... ..   ....::..::.  .::.::...  .......::.:::::::::.:....   .  ..  ....                        ....
--------MAAVQMDPELAKRLFFEGATVVILNMPKGTEFGIDYNSWEVGPKFRGVKMIPPGIH-----FLHYSSVDKANPKEVGPRMGFFLSL-------H---------------QR-G-----LT------------------VLR------W---------------------S--T--LREEVDL-SPAPESEVEAMRANLQELDQFLGPYPYATLKKWI---SLTN--------FISEATVEKLQPENRQICAFSDVLPVLSMKH-------TKDRVGQNLPRCGIECKSYQEGLAR------LPEMKPRAGTEIRFSELPTQMFPE--------------------------GATPAEITKHSMDLSYALETVLNKQFPSSPQDVLGELQFAFVCF-LL----G--NV-YEAFEHWKRLLNLLCRSEAAMMKHHTLYINLISILYHQLGE-IPADFFVDIVSQD--NFLTSTLQV--FFSSACSIAVDATLRKKAEKFQAHLTKKFRWDFAAEPED-------------CAPVVVELPEGIEMG

