
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-O75439.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  346 residues

Aligned length=   89, RMSD=   5.29, Seq_ID=n_identical/n_aligned= 0.112
TM-score= 0.25663 (if normalized by length of Chain_1)
TM-score= 0.18198 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
M-ADSN--GTITVEELKKLLEQWNLVIGFLFLTWICLLQF--AYANRNR-FLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLM--------------------WLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLLESE-------------------------------------------------------------------------------------------------------------------------------------------------LVIGAVILRGHLRIA------------GHHLGRCDIKDLPKEITV----------------------------------------ATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQ----------------------------------
  .     ...:::::::::::::::::::::::::::::  ::::..  ......:...::::::::::::::::.                                        .  .....:::::: :             .      ....                                                                                                                                                       ..                         .                                                         . :                                                                                   
-MA---DSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNR-FLYIIKLIFLWLLWPVTLACFVLAAVYR--------------------INWITGGIAIAMACLVGLMWL--SYFIASFRLFA-R-------------T------RSMW------SFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQAAAAARAARRRRAARNRRAANRRADDARNNNAARD-------------NANARAADRAADN-----------------ARRRNDAANARNRDDRAAADDADARRDDAAAAADANNDRNA-N-------------------------------------------------NDADRARARNNDDRNRRARDANARNRAAADRNRD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/O75439.pdb                            
Name of Chain_2: pymodel/M-O75439.pdb_B                            
Length of Chain_1:  489 residues
Length of Chain_2:  489 residues

Aligned length=  177, RMSD=   6.76, Seq_ID=n_identical/n_aligned= 0.096
TM-score= 0.23872 (if normalized by length of Chain_1)
TM-score= 0.23872 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MAAAAAR----------------------------------------------------VVLSSAARRRLWGFSESL---LIRGAAGRSLYFGENRLR---------STQAATQVVLNVPETR----VTCLESGLRVASEDSGLSTCTVGLWIDAGSRYENEKNNGTAHFLEHMAFKGTKKRSQLDLELEIENMGAHLNAYTSREQTVYYAKAFSKDLPRAVEILADIIQNSTLGEAEIERERGVILREMQEVETNLQEVVFDYLHATAYQNTALGRTILGPTENIKSISRKDLVDYITTHY--KGPRIVLA-AAG-GVS-HDELLDLAKFHFGDSLCTHKGEIPALPP-CKFTG-SEIRVRDDKMPLAHL----------AIA-VEAVGWAHPDTICLMVANTLIGNWDRSFGGGMNLSSKLAQLTCHGNLCHSFQSFNTSYTDTGLWGLYMVCESSTVADMLHVVQKEWMRLCTSVTESEVARARNLLKTNMLLQLDGSTPICEDIGRQMLCYNRRIPIPELEARI--DAVNAETIREVCTKYIYNRSPAIAAVGPIKQLPDFKQIRSNMCWLRD--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                                           ...... :: :.:..      .                          ..::::...           .........::::...:::..                                             .                  ....:::::::....... .                            .                                           ..:::::: .:: ::: :::::::::.. :: :     ......  . .   .::..                    ... .:::::::::.::::                                                 . :.      ..  :  ... ..  .          .:  :..:::::.:.:: : :: :: :::::.......:::..  ....                                                                                                                                                                                                                                                                           
-------MAAAAARVVLSSAARRRLWGFSESLLIRGAAGRSLYFGENRLRSTQAATQVVLNVPET-RV-TCLES---GLRV-----------------ASEDSGLSTCTVGLWIDA-------GSRYENEKNNGTAHFLEHMAFKGTK---------------------------------------------K------------------RSQLDLELEIENMGAHLN-A----------------------------Y-----------------------------------------TSREQTVYYAKAFSKDLPRAVEILADIIQN-ST-L-----GEAEIE-RE-R--GVILRE----------MQEVETNLQEVVFDYLHATAYQNTALGRT-------------------------------------------------I-LG------PT--E--NIK-SI--S----------RK--DLVDYITTHYKGP-R-IV-LA-AAGGVSHDELLDLAKFHFGDSLC-------------------------------------------THKGEIPALPPCKFTGSEIRVRDDKMPLAHLAIAVEAVGWAHPDTICLMVANTLIGNWDRSFGGGMNLSSKLAQLTCHGNLCHSFQSFNTSYTDTGLWGLYMVCESSTVADMLHVVQKEWMRLCTSVTESEVARARNLLKTNMLLQLDGSTPICEDIGRQMLCYNRRIPIPELEARIDAVNAETIREVCTKYIYNRSPAIAAVGPIKQLPDFKQIRSNMCWLRD

