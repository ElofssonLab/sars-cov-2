
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/nsp15.pdb                             
Name of Chain_2: pymodel/nsp15-P61970.pdb_A                        
Length of Chain_1:  346 residues
Length of Chain_2:  371 residues

Aligned length=  232, RMSD=   6.55, Seq_ID=n_identical/n_aligned= 0.315
TM-score= 0.41016 (if normalized by length of Chain_1)
TM-score= 0.38953 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
S--L-ENVAFNVVNKGHFDGQQGEVPVSIINNTVYTKVDGVDVE---LFENKTTLPVNVAF-----------ELWAKRNIKPV--P---------------EVKILNNLG---VDIAANTVIWDYKRDAPAHI-STIGVCSMTDIAKKPTETICAPLTVFFDGRVDGQVDLFRNARNGVLITEGSV--KGLQPSVGPKQASLNGVTLIGE-A--VKTQFNYYKKVDGVVQQLPETYFTQSRNLQ--EFKP------RS-QME-IDFLELAMDEFIERYKLEGYAFEHIVYGDFSHSQLGGLHLLIGLAKRFKESPFELED-FIPMDST---VKNYFITDAQTGSSKCVCSVIDLLLDDF-VEIIKSQDLSVVSKVV---------KVTIDYTEISF-MLWCKDGH-----VETF------------------YPKLQ------------------------------------------------
   . ...:.::::.                                ....:::::.....           ....:......  .               ..::::.::   :.::::::::      .:.  ... ..::::::::..:::::::::::::::::::::::::::::::::...  ..:::....:::.    ....  .  ...:::::::::::::::::.  ::...    ..        .. ... ...:::.:..::.   ..   .                      .::     ...   .         ..:::.        :. .........   .. ..   ..:....          .....:..... ...          ...                   ...                                                  
-SLENVAFNVVNKGH-----------------------------FDGQQGEVPVSIINNTVYTKVDGVDVELFENKTTLPVNVAFELWAKRNIKPVPEVKILNNLGVDIAANTVIWDYKRDAP------AHI-STIG-VCSMTDIAKKPTETICAPLTVFFDGRVDGQVDLFRNARNGVLITEGSVKGLQPSVGPKQASLN----GVTL-IGEAVKTQFNYYKKVDGVVQQLPET--YFTQS--RNLQ--EFKPRSQMEIDFLELAMDEFIERYKL---EG---Y----------------------AFE-----HIV--YG------DFSHSQLGG--------LH-LLIGLAKRF--KES-PF---ELEDFIP-MDSTVKNYFITDAQTGSSKCVCSV-----IDLLLDDF-VEIIKSQDLSVVSKVVKVTID--YTEISFMLWCKDGHVETFYPKLQDDNDRADAAAADDADDNNDANDRAN


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P61970.pdb                            
Name of Chain_2: pymodel/nsp15-P61970.pdb_B                        
Length of Chain_1:  127 residues
Length of Chain_2:  127 residues

Aligned length=   69, RMSD=   5.11, Seq_ID=n_identical/n_aligned= 0.072
TM-score= 0.30410 (if normalized by length of Chain_1)
TM-score= 0.30410 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MGDKPIWEQIGSSFIQH----------YYQLFDNDR------------TQLGAIYIDASCLTWEGQQFQGKAAIVEKLSSLP-FQKIQHSITAQDHQPTPD----------SCII-SM-VVGQLKADE-DPI-M--GF--HQM--FL-LKNINDA-WV-CTNDMFRLALHNFG------------
                           ...:::::.            .:::::::::..        ::: :::::::::: ::::...                     .::. .. ....:..   .:: :  ::  :::  :: ..      .  .                         
-----------------MGDKPIWEQIGSSFIQHYYQLFDNDRTQLGAIYIDASCLTWEG--------QQF-QGKAAIVEKLSSLPFQKI-----------QHSITAQDHQPTPDSCIISMVVGQL--KADEDPIMGFHQMFLLKNINDA-----WV-CT-------------NDMFRLALHNFG

