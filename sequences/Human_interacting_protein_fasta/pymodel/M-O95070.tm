
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-O95070.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  284 residues

Aligned length=  107, RMSD=   2.78, Seq_ID=n_identical/n_aligned= 0.720
TM-score= 0.41884 (if normalized by length of Chain_1)
TM-score= 0.33428 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYR----INWITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATS----------------------------------------------------------------------------------------------------------------------------------------------------------------RTLSYYKLGASQRVAGDSGFAAYSRY-------------RIGNYKLNTDHSSSSDNIALLVQ
:::::.::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::    :::::::::::::::::::::::::::  :.....                                                                                                                                                                                                                                  .                                      .                      
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLMWLSYFIASFRLF--ARTRSM------------------------------------------------------------------WSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQAARARAADDDAADANDANAAADRADAANRDARDNADAAARAAAARDAD-------------------------AAARRAADRRAAAR----------------------


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/O95070.pdb                            
Name of Chain_2: pymodel/M-O95070.pdb_B                            
Length of Chain_1:  293 residues
Length of Chain_2:  293 residues

Aligned length=  119, RMSD=   5.48, Seq_ID=n_identical/n_aligned= 0.134
TM-score= 0.27581 (if normalized by length of Chain_1)
TM-score= 0.27581 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MAYHSGYGAHGSKHRARAAPDPPPLFDDTSGGYSSQPGGYPATGADV------------------------------------------------------------------------------------------------------------------------------AF-SVNHLLGDPMANVAMAYGSSIASHGKDMVHKELHRFVSVSKLKYFFAVDTAYVAKKLGLLVF--------------PYTH---QNWE--VQ-YSR-DA---P--LPPRQDLNAPDLYIPTMAFITYVLLAGMALGIQKRFSPEVLGLCASTALVWVVMEVLALLLGLYLATVRSDLSTFHLLAYSGYKYVGMILSVLTGLLF---GSDGYYVALAWTSSALMY--FIVRSLRTAALGPDSMGGPVPRQRLQLYLTLGAAAFQPLIIYWLTFHLVR----------------
                                                                                                                                                                             .  ..                                                                          .      ..    .. ... ..   :  ::.::.::...:..::..::.:::::::  ::        :.....:::::.:..:..::::..:.....::::::::::::::::::::::::::::   :::::::::..:....    .                                                                 
-----------------------------------------------MAYHSGYGAHGSKHRARAAPDPPPLFDDTSGGYSSQPGGYPATGADVAFSVNHLLGDPMANVAMAYGSSIASHGKDMVHKELHRFVSVSKLKYFFAVDTAYVAKKLGLLVFPYTHQNWEVQYSRDAP-LPP------------------------------------------------------------RQDLNAPDLYIPTMA---FITYV--LLAGMALGIQKRFSPEVLGLCASTALVWVVMEVLALLLGLYLATV--RS--------DLSTFHLLAYSGYKYVGMILSVLTGLLFGSDGYYVALAWTSSALMYFIVRSLRTAALGPDSMGGPVPRQRLQLYLTL--GAA-------------------------------------------------AFQPLIIYWLTFHLVR

