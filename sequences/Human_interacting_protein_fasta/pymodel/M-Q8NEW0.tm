
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-Q8NEW0.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  290 residues

Aligned length=   75, RMSD=   3.71, Seq_ID=n_identical/n_aligned= 0.547
TM-score= 0.26526 (if normalized by length of Chain_1)
TM-score= 0.21052 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFA-YANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMWSFN---------------------------------------------------------------------------------------------------------------------------------------------------------------PETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRY-----------------------------------RIGNYKLNTDHSSSSDNIALLVQ--------------------
.:...::::::::::::::::::::::::::::::::: :::::::::::::::::::::::::..::....                                                                                                                                                                                                         .  :                .                                                                                                    .                                          
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYR------------------------------------------INWITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQDDNRDRNNAN--D----------------D-----------------------------------------------------------------AAAARDNDARAANARAADRNNADADAADADRADAAN----------------------ADARRNRDADADARNARDAA


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q8NEW0.pdb                            
Name of Chain_2: pymodel/M-Q8NEW0.pdb_B                            
Length of Chain_1:  376 residues
Length of Chain_2:  376 residues

Aligned length=  229, RMSD=   6.57, Seq_ID=n_identical/n_aligned= 0.240
TM-score= 0.38024 (if normalized by length of Chain_1)
TM-score= 0.38024 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MLPLSIKDDEYKPPKFNLFGKISGWFRSILSDKTSRNLFFFLCLNLSFAFVELLY-------------------------------------------------------GIWSNCLGLISDSFHMFFDSTAILAGLAASVISKWRD-NDAFSYGYVRAEVLAGFVNGLFLIFTAFFIFSEGVERALAPPDVHHERL------L-LVSILGFVVNLIGIFVFKHGGHGHSHGSGHGHSHSLFNGALDQAHGHVDHCHSHEVKHGAAHSHDHAHG-H-GHFH---------SHDGPSLKETT-G----------P--SRQILQGVFLHI-LADTLGSIGVIASA-IMMQNFGLMI--ADPICSILIAILIVVSVIPLLRESVGILMQRTPPLLENSLPQCYQRVQQLQGVYS------------------------LQEQHFWTLCSDVYV-GTLKLIVA-PDADARWILSQ-------------------THNIFTQAGVRQLYVQID--------FA-AM--
                                                                                                              .. ..::..::.:.::::::::::::::::::::::: ...::::::::::::................ ..         ...::.      . ....:..::::::::::::::::.:.:::::.::.                  .:...:::::::.:.. . .:.:         .:::::::..: .          .  ..:::::::::: :............  .....       ... ... ::. .: ... .:.:.......                                                 .....           .......  ...:::...                     .....                     .. .   
-------------------------------------------------------MLPLSIKDDEYKPPKFNLFGKISGWFRSILSDKTSRNLFFFLCLNLSFAFVELLYGI-WSNCLGLISDSFHMFFDSTAILAGLAASVISKWRDNDAFSYGYVRAEVLAGFVNGLFLIFTAFFIF-SE---------GVERALAPPDVHHERLLLVSILGFVVNLIGIFVFKHGGHGHSHGSGHGH------------------SHSLFNGALDQAHGHVDHCHSHEVKHGAAHSHDHAHGHGHFHSHDGPSLKETTGPSRQILQGVFLHILADTLGSIGVIASAIMM-QNFGLM-----IADPI-CSI-LIA-IL-IVV-SVIPLLRESVG-------------------------ILMQRTPPLLENSLPQCYQRVQQLQGVYS----------LQEQHFWT-LCSDVYVGTL--KLIVAPDADARWILSQTHNIFTQA-------------GVRQLYVQIDFA-AM

