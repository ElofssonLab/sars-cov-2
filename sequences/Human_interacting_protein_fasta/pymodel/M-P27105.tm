
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-P27105.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  293 residues

Aligned length=   99, RMSD=   5.12, Seq_ID=n_identical/n_aligned= 0.626
TM-score= 0.30061 (if normalized by length of Chain_1)
TM-score= 0.24107 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNG----TITVE--ELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIA----------------MACLVGLMWLSYFIASFRLFA--RTRSMWSFN----------------------------------------------------------------------------------------------------------------------------------------PETNILLNVPLHGTILTRPLLESEL--------------------VIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQ--------------
          ..:::  ::::::::::::::::::::::::::::::::::::::::::::::::::::...:..:.                            ..:::::::::: :.. ..    .                                                                                                                                                :....                                        ..: :      .:. ....                                                                               
------MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVY------------RINWITGGIAIAMACLVGLMWLSYFIAS-FRL-FA--RTR--------SMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQARRDARDDARARRADDRDDDRNAAAN--------------------NADARARNNDRANDDADARDRAA-A------AAR-ARAA-----------------------------------------------------------------ANARAAAARAANDA


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P27105.pdb                            
Name of Chain_2: pymodel/M-P27105.pdb_B                            
Length of Chain_1:  288 residues
Length of Chain_2:  288 residues

Aligned length=  116, RMSD=   6.69, Seq_ID=n_identical/n_aligned= 0.431
TM-score= 0.22157 (if normalized by length of Chain_1)
TM-score= 0.22157 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MAEKRHTRDSEAQRLPDSFK-----------------------------DSPSKGLGPCGWILVAFSFLFTVITFPISIWMCI-KIIKEYERAIIFRLGRILQGGAKGPGLFFILPCTDSFIKVDMRTISFDIPPQE-ILT---KDS-V-----------------------TISV--D---GV--------VYYRVQNATLAVANITNAD-SATRLLAQTTLRNVLGTKNLSQILSDREEIAHNMQSTLDDATDAWGIKVER----VE-IKDVKLPVQLQRAMAAEAEASREARAKVIAAEGEMNASRALKEASMVITESPAALQLRYLQTLTTIAAEKNSTIVFPLPIDMLQGIIGAKHSHLG-----------------------------------------------------------------------------------------------
                                                 .:...:.. ..                        ....::...          .....               :::....:...... ..    ..: .                       .:::  :   ::        .:.::. . .::::.:... .......:::::::..:::.:::::::::::.::............         .  .                                                                                                                                                                                             
--------------------MAEKRHTRDSEAQRLPDSFKDSPSKGLGPCGWILVAF-SF-----------------------LFTVITFPIS----------IWMCI---------------KIIKEYERAIIFRLGRI-LQGGAKGPGLFFILPCTDSFIKVDMRTISFDIPPQEILTKDSVTISVDGVVYYRVQN-A-TLAVANITNADSATRLLAQTTLRNVLGTKNLSQILSDREEIAHNMQSTLDDATDAWG-----IKVER-VE----------------------------------------------------------------------------------------------IKDVKLPVQLQRAMAAEAEASREARAKVIAAEGEMNASRALKEASMVITESPAALQLRYLQTLTTIAAEKNSTIVFPLPIDMLQGIIGAKHSHLG

