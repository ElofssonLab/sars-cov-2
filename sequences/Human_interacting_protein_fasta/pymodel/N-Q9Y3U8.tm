
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/N.pdb                                 
Name of Chain_2: pymodel/N-Q9Y3U8.pdb_A                            
Length of Chain_1:  419 residues
Length of Chain_2:  452 residues

Aligned length=  174, RMSD=   7.17, Seq_ID=n_identical/n_aligned= 0.057
TM-score= 0.24657 (if normalized by length of Chain_1)
TM-score= 0.23347 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MSDNGPQNQRNAPRITFGGPSDSTGSNQNGERSGARSKQRRPQGLPNNTA---------------------SWFTALTQHGKEDLKFPRGQGVPINTNSSPDDQ-------------IGYYRRATRRIRGGDGKMKDLSPRWYFYYLGTGPEAGLPYGANKD-GI-IWVATEGALNTPKDHIGTRNPANNAAIVLQLPQGTT-------------L-PKGFYAEGS---RGGSQA-S-SRSSSRSRNSSRNSTPG--SSRGTSP-ARMAGNGGDAALALLLLDRLNQLESKMSGKGQQQQGQTVTKKSAAEASKKPRQKRTATKAYNVTQAFGRRGPEQTQ----------------------------------------------------------------GNFGDQELIR----QGTDYKHWPQIAQFAPSASAFFGMSRIGM---------EVTPSGTWLTYTGAIKL---------DDK---DPN-FK----DQVILLNKHIDAYKTFPPTEPKKDKKKKADETQALPQRQKKQQTVTLLPAADLD------------DFSKQLQQSMSSADSTQA------------------------------------------------------------------------------------------------------------------
                                                                       ...:..        ......                          ..:.::::::...     :::::..                     .  .....    . .  .:::      :::::::.....             . .....::::   ::::.. . .....:........:..  ..:...  ....      .  ..  :: ...                                                                                                                     ..:.::::..    ...:.:.:::.:..                        ....::..                  ...   ::. ..    .:...::.  .                             ..::::::...               .::...                                                                                                                              
--------------------------------------------------MSDNGPQNQRNAPRITFGGPSDSTGSN--------QNGERS-------------GARSKQRRPQGLPNNTASWFTALTQH-----GKEDLKF--------------------PR-GQGVPI----N-T--NSSP------DDQIGYYRRATRRIRGGDGKMKDLSPRWYFYYLGTGPEAGLPYGANKDGIIWVATEGALNTPKDHIGTRNPAN-NAAIV------L--QL--PQ-GTT-----------------------------------------------------LPKGFYAEGSRGGSQASSRSSSRSRNSSRNSTPGSSRGTSPARMAGNGGDAALALLLLDRLNQLESKMSGKGQQQQGQTVTKKSAAEASKKP---------------RQKRTATKAYNVTQAFG---------RRGPEQTQGNFGDQELIRQGTDYKHWPQIAQFA--P-----------------------------SASAFFGMSRI---GMEVTPSGTWLTYTGAIK------------LDDKDPNFKDQVILLNKHIDAYKTFPPTEPKKDKKKKADETQALPQRQKKQQTVTLLPAADLDDFSKQLQQSMSSADSTQAARANNRRRRRRDRARRADRARRARRNAARAAAD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q9Y3U8.pdb                            
Name of Chain_2: pymodel/N-Q9Y3U8.pdb_B                            
Length of Chain_1:  105 residues
Length of Chain_2:  105 residues

Aligned length=   61, RMSD=   3.78, Seq_ID=n_identical/n_aligned= 0.328
TM-score= 0.35695 (if normalized by length of Chain_1)
TM-score= 0.35695 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MALRYPMAV----GLNKGHKVTKNVSKPRHSRR------------RGRL------TKHTKFVRDMIREVCGFA-PY--E---------------RRA--MELLKVSKD--KRALKFIKKRVGTHIRAKRKREELSNVLAAMRKAAAKKD
             .     .::.:.                    ..::      :::::::::::::::::: ::  :               .    ::..:::.:  .:::::::::::::::::.                    
---------MALRY-----PMAVGL--------NKGHKVTKNVSKPRHSRRRGRLTKHTKFVRDMIREVCGFAPYERRAMELLKVSKDKRALKFI--KKRVGTHIRAKRKREELSNVLAAMRKAAAKKD--------------------

