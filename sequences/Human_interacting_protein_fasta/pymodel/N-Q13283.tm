
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/N.pdb                                 
Name of Chain_2: pymodel/N-Q13283.pdb_A                            
Length of Chain_1:  419 residues
Length of Chain_2:  523 residues

Aligned length=  146, RMSD=   7.28, Seq_ID=n_identical/n_aligned= 0.068
TM-score= 0.20446 (if normalized by length of Chain_1)
TM-score= 0.17410 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MSDNGPQNQRNAPRITFGGPSDSTGSNQNGERSGARSKQRRPQGLPNNTASWFT----------------------------------------------------ALTQHGKEDLKFPRGQGVPINTNSSPDDQIGYYRRATRRIRGGDGKMKDLSPRWYFYYLGTGPEAGL---PYGANKDGIIWVATEGALNTPKDHIGTRNPANNAAIVLQLPQGTTLPKGFYAEGSRGGSQASSRSSSRSRNSSRNSTPGSSRGTSPARMAGNGGDAALALLLLDRLNQLESKMSGKGQQQQGQTVTKKSAAEASKKPRQKRT---------------------------ATKAYNVTQAFGRRGPEQTQGNFGDQELIRQ--------------------------------------------GTDYKHWPQI-------AQFAPS-A-SA--FFGMS--RIG--MEV--TPSGTWLT-YTGAIKLD--DKDPNFKDQVILLNKHIDAYKTFPPTEPKKDKKKKADETQ------------------------------------------------------------------ALPQRQKKQQTVT---L-L----PAADLDD-FS--K----------------------QLQQSMSSADSTQA------------------------------------------------------------------------------------------------------------------------------------
                                                                                                          ...::::.......::.                 . .:::.   :...                      ..:::::.:::::.  ::                   .::   :::..                                                                                                                         ... . .  :......                                                           .....:...        ..:::: . ::  :::::  :::  ..   .......  .  ...    ...:..                                                                                                    ....::::::...   . .    .::..:. .:  .                      ..:::..:::...                                                                                                                                     
------------------------------------------------------MSDNGPQNQRNAPRITFGGPSDSTGSNQNGERSGARSKQRRPQGLPNNTASWFTALTQHGKEDLKFPRG-----------------Q-GVPIN---TNSS-------------------PDDQIGYYRRATRRIRG--GD-------------------GKM---KDLSP----------------------------------------------------------------------------------------------RWYFYYLGTGPEAGLPYGANKDGIIWVATE-G-A--LNTPKDH---------------IGTRNPANNAAIVLQLPQGTTLPKGFYAEGSRGGSQASSRSSSRSRNSSRNST-PGSSRGTSPARMAGNGGDAALALLLLDRLNQLES-KMSGKGQQQ-QG--QTV--TKKSAAEA----------------------------------SKKPRQKRTATKAYNVTQAFGRRGPEQTQGNFGDQELIRQGTDYKHWPQIAQFAPSASAFFGMSRIGMEVTPSGTWLTYTGAIKLDDKDPNFKDQVILLNKHIDAYKTFPPTEPKKDKKKKADETQALPQRQKKQQT-VTLLPAADLDDFSKQLQQSMSSADSTQARRNADRNDNADARNNRDAANDNNNARRAANNDRDRDDDANDADADAAADADRANAARRRDRRRNRRRADRRRDNDDNRNNDDNRRRNRAARDRRDNRRRRRRAR


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q13283.pdb                            
Name of Chain_2: pymodel/N-Q13283.pdb_B                            
Length of Chain_1:  466 residues
Length of Chain_2:  466 residues

Aligned length=  164, RMSD=   7.77, Seq_ID=n_identical/n_aligned= 0.049
TM-score= 0.20617 (if normalized by length of Chain_1)
TM-score= 0.20617 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
M---------------------------------------------------------------------------VMEKPSPLLVGREF-----------------------------------------------------------------------------------------------V-RQYYTLLNQAPDMLHRFYGKNSSYVHGGLDSNGKPADAVYGQKEIHRKVMSQNFT--NCHTKIRHVDAHATLNDGVVVQVMGLLSNNNQALRRFM-QTFVLAPEGSVANKFYVH-NDIFR---Y--QDEVFGG-FVTEP------QEESEEEVEEPEERQQTPEVVPDDSGTF-YDQAVVSNDMEEHLEEPVAEPEPDPEPEPEQEPVSEIQEEKPEPVLE----------------------------------------ETAPEDAQKSSSPAPADIAQ-----------------------T-------------------------VQEDLRTFSWASVTSKNLPPSGAVPVTGIPPHVVKVPASQPRPESKPE------SQIPPQRPQRDQRVREQRINIPPQRGPRPIREAGEQGDIEPRRMVRHPDSHQLFIGNLPHEVDKSELKDFFQSYGNVVELRINSGGKLPNFGFVVFDDSEPVQKVLSNRPIMFRGEVRLNVEEKKTRAAREGDRRDNRLRGPGGPRGGLGGGMRGPPRGGMVQKPGFGVGRGLAPRQ--------------------
                                                                            ...:::.......                                                                                                . .....::::.                                               ...:::::::::.     ...... .    .:..:..  ........::::...:.. :....   .  ...:::. .          ....                 ......  .                                                                                      ....:::::::::::....                        .                         ......:..                  ..:..                      ..          ...  .       ..: .:::. ::.::.:.:.:::.: .               .       .::..                                                                                                                     
-MVMEKPSPLLVGREFVRQYYTLLNQAPDMLHRFYGKNSSYVHGGLDSNGKPADAVYGQKEIHRKVMSQNFTNCHTKIRHVDAHATLND-GVVVQVMGLLSNNNQALRRFMQTFVLAPEGSVANKFYVHNDIFRYQDEVFGGFVTEPQEESEEEVEEPEERQQTPEVVPDDSGTFYDQAVVSNDMEEHLEEPVAEPE---------------------------------------------PDPEPEPEQEPVSEI-----QEEKPE-P----VLEETAP-EDAQKSSSPAPADIAQTVQEDLRTFSWASVTSKNLPPSGA----VPVTGIPPHV-----------------VKVPAS-QP----------------------------------------------RPESKPESQIPPQRPQRDQRVREQRINIPPQRGPRPIREAGEQGDIEPRRMVRHPDSHQ-LFIGNLPHEVDKSELKDFFQSYGNVVELRINSGGKLPNFGFVVFDDSEPVQKVLSNRP------------------IMFRG----------------EVRLNVEE----------KKT--R-------AAR-EGDRR-DNRLRGPGGPRGGLG-G---------------G-------MRGPP-------------------------------------------------------------------------------------------------RGGMVQKPGFGVGRGLAPRQ

