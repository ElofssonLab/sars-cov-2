
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/nsp7.pdb                              
Name of Chain_2: pymodel/nsp7-P61586.pdb_A                         
Length of Chain_1:   83 residues
Length of Chain_2:  130 residues

Aligned length=   83, RMSD=   1.23, Seq_ID=n_identical/n_aligned= 1.000
TM-score= 0.88467 (if normalized by length of Chain_1)
TM-score= 0.59117 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
SKMSDVKCTSVVLLSVLQQLRVESSSKLWAQCVQLHNDILLAKDTTEAFEKMVSLLSVLLSMQGAVDINKLCEEMLDNRATLQ-----------------------------------------------
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::                                               
SKMSDVKCTSVVLLSVLQQLRVESSSKLWAQCVQLHNDILLAKDTTEAFEKMVSLLSVLLSMQGAVDINKLCEEMLDNRATLQAARDADNADDADADDRRDDDDNNNDRNDRRARDANRAADRARAAARR


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P61586.pdb                            
Name of Chain_2: pymodel/nsp7-P61586.pdb_B                         
Length of Chain_1:  193 residues
Length of Chain_2:  193 residues

Aligned length=  188, RMSD=   2.53, Seq_ID=n_identical/n_aligned= 0.862
TM-score= 0.82721 (if normalized by length of Chain_1)
TM-score= 0.82721 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
M-AAIRKKLVIVGDGACGKTCLLIVFSKDQFPEVYVPT-VFENYVADIEVDGKQVELALWDTA-GQEDYDRLRPLSYPDTDVILMCFSIDSPDSLENIPEKWTPEVKHFCPNVPIILVGNKKDLRNDEHTRRELAKMKQEPVKPEEGRDMANRIGAFGYMECSAKTKDGVREVFEMATRAALQARRGKKKSGCLVL--
  .:::::::::::::::::::::::::......:::: ::::::::::  :::::::::::: :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  ::.:.  
-MAAIRKKLVIVGDGACGKTCLLIVFSKDQFPEVYVPTVFENYVADIEV--DGKQVELALWDTAGQEDYDRLRPLSYPDTDVILMCFSIDSPDSLENIPEKWTPEVKHFCPNVPIILVGNKKDLRNDEHTRRELAKMKQEPVKPEEGRDMANRIGAFGYMECSAKTKDGVREVFEMATRAALQARRGKK--KSGCLVL

