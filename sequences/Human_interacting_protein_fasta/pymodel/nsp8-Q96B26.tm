
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/nsp8.pdb                              
Name of Chain_2: pymodel/nsp8-Q96B26.pdb_A                         
Length of Chain_1:  198 residues
Length of Chain_2:  257 residues

Aligned length=  154, RMSD=   3.70, Seq_ID=n_identical/n_aligned= 0.740
TM-score= 0.60583 (if normalized by length of Chain_1)
TM-score= 0.48327 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
AIASEFSS--------LPSYAAFATAQEAYEQAVANGDSEVVLKKLKKSLNVAKSEFDRDAAMQRKLEKMADQAMTQMYKQARSEDKRAKVTSAMQTMLFTMLRKLDNDALNNIINNARDGCVPLNIIPL-TTAAKLMV-------VIPDYNTYKNTCDGTTFTYASALWEIQQVVD-A----DSKIVQLSEISMDNSPN--LAW-PLIVTALRANS-----------------------------------------------------AVKLQ--------------------------
                ..::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: :.::::..:: .:::::..       :::.  .:  :.    :......:.:.:..  .    .:...              .:: .                                                               .                              
--------AIASEFSSLPSYAAFATAQEAYEQAVANGDSEVVLKKLKKSLNVAKSEFDRDAAMQRKLEKMADQAMTQMYKQARSEDKRAKVTSAMQTMLFTMLRKLDNDALNNIINNAR-DGCVPLNIIPLTTAAKLMVVIPDYNTYKNT--CD--GT----TFTYASALWEIQQV-VDADSKIVQLS------------EISMDNS----------PNLAWPLIVTALRANSAVKLQAARRNRDRRNADANAAADADNDRRAAADNDDDD----NDAAAANNAANNRAADDDADADRARD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q96B26.pdb                            
Name of Chain_2: pymodel/nsp8-Q96B26.pdb_B                         
Length of Chain_1:  276 residues
Length of Chain_2:  276 residues

Aligned length=  108, RMSD=   5.40, Seq_ID=n_identical/n_aligned= 0.537
TM-score= 0.27442 (if normalized by length of Chain_1)
TM-score= 0.27442 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MAAGFKTVEPLEYYRRFLKENCRPDGRELGEFRTTTVNIGSISTADGSALVKLGNTTVICGVKAEFAAP----------------------------------------------------------------------------------------------------------------------------------------STDAPDKGYVVPNVDLPPLCSSRFRSGPPGEEAQVASQFIADVIENSQII---QKEDLCISPGKLVWVLYCDLICLDYDGNILDACTFALLAALKNVQLPEV----------TINEETALAEVNLKKKSYLNIRTHPV--------ATSFAVFDDTLLIVDPTGEEEHLA--------T-GTLTIVMDE-EGKLCCLHKPGG-SGLTGAKLQDCMSRAVTRHKEVKKLMDEVIKSMKPK
                                                                                                                                                                                                             ..:.::.                                              ........:...                                               ..::.....                         ...::::.:.:::::.::::::::        . ........  . .::::::::. ::::::::::::::::::::::::::::::::::::
---------------------------------------------------------------------MAAGFKTVEPLEYYRRFLKENCRPDGRELGEFRTTTVNIGSISTADGSALVKLGNTTVICGVKAEFAAPSTDAPDKGYVVPNVDLPPLCSSRFRSGPPGEEAQVASQFIADVIENSQIIQKEDLCISPGKLVWVLYCDLICLD-------------------------------------------YDGNILDACTFALLA-------------------------------------ALKNVQLPEVTINEETALA-----------------EVNLKKKSYLNIRTHPVATSFAVFDDTLLIVDPTGEEEHLATGTLTIVMD-EE-GKLCCLHKPGGSGLTGAKLQDCMSRAVTRHKEVKKLMDEVIKSMKPK

