
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-P38606.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  350 residues

Aligned length=   89, RMSD=   5.56, Seq_ID=n_identical/n_aligned= 0.607
TM-score= 0.24319 (if normalized by length of Chain_1)
TM-score= 0.17302 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGT-------ITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLW-PVTLACFVLAAVYRI-NWITGGIAIAMACLVGLMWLSYFIA--------------------------------------------------------------------------------------------------------------------------------------------------------------------SFRL--FARTRSMW--SF--NPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQ----------------------------------------------------------------------------------
..            .:.::::::::::  .::.::::::::::::.:.:::::::::::..:..: ::.........:..  .. .::::::..:....                                                                                                                                                                            .:    . : .     .   :.                                                                                                                                                                                              
MA-----DSNGTITVEELKKLLEQWNL--VIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRI-NWI-TGGIAIAMACLVGL--------MWLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQDDDADAAARRDADRADRDRNARDDNRDNRNRAND--DRR-A-N---RDA-AAND------------------------------------------------------------------------------------------------------------RNRDDRAANNAARARDADRAARRAADAARARARNRADDADARNRADDRADAAADADDNDRNADARRAADNRDDDADADNARD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P38606.pdb                            
Name of Chain_2: pymodel/M-P38606.pdb_B                            
Length of Chain_1:  617 residues
Length of Chain_2:  617 residues

Aligned length=  179, RMSD=   7.25, Seq_ID=n_identical/n_aligned= 0.229
TM-score= 0.19360 (if normalized by length of Chain_1)
TM-score= 0.19360 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MDFSKLPKILDEDKESTFGYVHGVSGPVVTACDMAGAAMYELVRVGHSELVGEIIRLEGDMATIQVYEETSGVSVGDPVLRTGKPLSVELGPGIMGAIFDGIQRPLSDISSQTQSIYIPRGVNVSALSRDIKWDFTPCKNLRVGSHITGGDIYGIVSENSLIKHKIMLPPRNRGTVTYIAPPGNYDTSDVVLELEFEGVKEKFTMVQVWPVRQVRPVTEKLPANHPLLTGQRVLDALFPC---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------VQGGTTAIPGAFGCGKTVI----------------------------------SQSLSKYSNSDVIIYVGCGERGNEMSEVLRDFPELTMEVDGKVESI------------MKR--TALVANTSN---MPVAAREASIYTGITLSEYFRDMGYHVSMMA--D--STSRWAEALREISGRLAEMPADSGYPAYLGARLASFYERAGRVKCLGNPEREGSVSIVGA-VSPPGGDFSDPVTSAT-------LGIVQVFWGLDKKLAQRKHFPSVNWLISYSKYMRALDEYYDKHFTEFVPLRTKA-----------KEILQEEEDLAEIVQLVGKASLAETDKITLEVAKLIKDDFLQQNGYTPYDRFCPFYKTVGML--------------------------------SNMIAFYDMARRAVETTAQSDNKITWSIIREHMGDILYKLSSMKFKDPLKDGEAKIKSDYAQLLEDMQNAFRSLED-----------------------------------------------------------------------------------------------------------------
                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ...:...                                              ....:.:.:........::........... ....                       .    ......      .. ..:.:::::...::::::::::::::..  .  ::::::::::::::  .        :::. ...::.:....               .... .::::..                .. .........        ::.......::::::.::..:..                      ..                                                                                            ...:::...:::::::::::...                                                                                                                                                                      
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------MDFSKLPKILDEDKESTFGYVHGVSGPVVTACDMAGAAMYELVRVGHSELVGEIIRLEGDMATIQVYEETSGVSVGDPVLRTGKPLSVELGPGIMGAIFDGIQRPLSDISSQTQSIYIPRGVNVSALSRDIKWDFTPCKNLRVGSHITGGDIYGIVSENSLIKHKIMLPPRNRGTVTYIAPPGNYDTSDVVLELEFEGVKEKFTMVQVWPVRQVRPVTEKLPANHP------------LLTGQRVLDALFPCVQGGTTAIPGAFGCGKTVISQSLSKYSNSDVIIYVGCGERGNEMSEVLRD-FPEL-----------TMEVDGKVESIMK--RTALVANT---SNMPV-AAREASIYTGITLSEYFRDMGYHVSMMADSTSRWAEALREISGRLAE--M--------PADS-GYPAYLGARLA---------------SFYERAGRVKCL---------GNPEREGSV-SIVGAVSPP--------GGDFSDPVTSATLGIVQVFWGLD-----------KKLAQRKHFPSVN------------------------------------------------------------WLISYSKYMRALDEYYDKHFTEFVPLRTKAKEILQEEEDLAEIVQLVGKASLAET-----------------------------------------------------DKITLEVAKLIKDDFLQQNGYTPYDRFCPFYKTVGMLSNMIAFYDMARRAVETTAQSDNKITWSIIREHMGDILYKLSSMKFKDPLKDGEAKIKSDYAQLLEDMQNAFRSLED

