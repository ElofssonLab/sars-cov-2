
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-Q9BQT8.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  271 residues

Aligned length=   92, RMSD=   2.97, Seq_ID=n_identical/n_aligned= 0.793
TM-score= 0.34179 (if normalized by length of Chain_1)
TM-score= 0.28690 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVY--R--INWITGGIAIAMACLVGLMWLSY-------------------------------------------------------------------------------------------------------------------------------------------------------------------FIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLL---ESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRY---------RIGNYKLNTDHSSSSDNIALLVQ
.......::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  :  ::::::::::::::: :                                                                                                                                                                         : :                                       .                                                                         :                      
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLM-W------LSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQARARAADRRADNDRARAAAAANANRNAARNARNN-N------------------------------------NDRA----------------------------------------------------------------NDARRAARAN----------------------


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q9BQT8.pdb                            
Name of Chain_2: pymodel/M-Q9BQT8.pdb_B                            
Length of Chain_1:  299 residues
Length of Chain_2:  299 residues

Aligned length=  177, RMSD=   5.89, Seq_ID=n_identical/n_aligned= 0.220
TM-score= 0.36850 (if normalized by length of Chain_1)
TM-score= 0.36850 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MSAKPEVSLVREASRQIV-----------------------AGGSAGLV----E-ICLMHPLDVVKTRFQIQRCATDPNSYKSLVDSFRMIFQMEGLFGFYKGILPPILAETPKRAVKFFTFEQYKKLLGYVSLSPALTFAIA---------------------GLGSGLTEAIVVNPFEVVKVGLQ-ANRNTFAEQPSTVGYARQIIKKEGWGLQ-GLNKGLT-------------------------------------------A-TLGRHGVFNMVYFGFYYNVKNMIPVNKDPILEFWRKFGIGLLSGTIASVINIPF-DVAKSRIQGPQPVPGEIKYRTCFKTMATVYQEEGILALYKGLL-PKIMRLGPGGAVMLLVYEYTYSWLQENW-------------------------
                                         ....:::.    . ...:::::..                  :  ::  :       .. .::...                                                         ....::::: ::..:...:..:. ..:::....    .:: :...   . .  ..:.:..                                           . ...::::::.::::::::::::::.. . .. ......::.::::::.:::.:: .:::::.  ::.:. :::...  ::::::.    : ::::::: :.:::::.......                                       
------------------MSAKPEVSLVREASRQIVAGGSAGLVEICLMHPLDVVKTRFQIQRCA------------------T--DP--N-------SY-KSLVDS------------------------------------FRMIFQMEGLFGFYKGILPPILAETPKRAV-KFFTFEQYKKLLGYVSLSPALTF----AIA-GLGS---G-L-TEAIVVNPFEVVKVGLQANRNTFAEQPSTVGYARQIIKKEGWGLQGLNKGLTATLGRHGVFNMVYFGFYYNVKNMIPVN-K-DP-ILEFWRKFGIGLLSGTIASVINIPFDVAKS--RIQGP-QPVPGE--IKYRTCF----K-TMATVYQEEGILALYKGLLPKI--------------MRLGPGGAVMLLVYEYTYSWLQENW

