
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/nsp10.pdb                             
Name of Chain_2: pymodel/nsp10-Q96CW1.pdb_A                        
Length of Chain_1:  139 residues
Length of Chain_2:  225 residues

Aligned length=   75, RMSD=   5.22, Seq_ID=n_identical/n_aligned= 0.253
TM-score= 0.28951 (if normalized by length of Chain_1)
TM-score= 0.20847 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
AGNAT----------------------------------------------EVPANSTVLSFCAFAVDAAKAYKDYLASGGQPITNCVKMLCTHTGTG--QAITVTPEANMDQESFGGASCCLYCRCHIDHPNPKGFCDLKGKYVQIP--TTC---ANDPVGFTLKN-TVCT---VCGMWKGYGCSCDQLREPMLQ---------------------------------------------------------------------------------------------
                                                   :::  .:  :: ::::                        ..::.    ....  :       :.:::::...:.:::::.   ..:::...:::::  :::   :..:::..:.. ..     .::::..                                                                                                           
-----AGNATEVPANSTVLSFCAFAVDAAKAYKDYLASGGQPITNCVKMLCTHT--GT--GQ-AITV------------------------TPEAN--MDQESF--G-------GASCCLYCRCHIDHPNPK---GFCDLKGKYVQIPTTCANDPVGFTLKNTVCTVCGM--WKGYGCSCDQ--------------LREPMLQNRRDDRNADARNARRNARRNAANNAADAANNNDDNARRRRNDNARNDADADDRDRDRRDRRRNARNAANARAANDARNARNDDRRR


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q96CW1.pdb                            
Name of Chain_2: pymodel/nsp10-Q96CW1.pdb_B                        
Length of Chain_1:  435 residues
Length of Chain_2:  435 residues

Aligned length=  136, RMSD=   7.36, Seq_ID=n_identical/n_aligned= 0.162
TM-score= 0.18767 (if normalized by length of Chain_1)
TM-score= 0.18767 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MIGGLFIYNHKGEVLISRVYRDDIGRNAVDAFRVNVIHARQQVRSPVTNIARTSFFHVKRSNIWLAAVTKQNVNAAMVFEFLYKMCDVMAAYFGKISEENIKNNFVLIYELLDEILDFGYPQNSETGALKTFITQQGIKSQHQTKEEQSQITSQVTGQIGWR-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------REGIKYRRNELFLDVLESVNLLMSPQGQVLSAHVSGRVVMKSYLSGMPECKFGMNDKIVIEKQGKG-TADETSKSGKQ------SIAIDDCTFHQCVRLSKFDSERSISFIPPDGEFELMR-YRTTKDIILPFRVI--PLVR-EVGRTKLEVKVVIKSNFKPSLLAQKIEVRIPTPLNTSGVQVIC-----------MKGKAKYKASENAIVWKIKRM--------------------AGMKESQISAEIELLPTNDKKKWARPPISMNFEVPFAPSGL--KVRYLKVFEPKLNYSDHDVIKWVRYIGRSGIYETRC--------------------------
                                                                                                                                                                                                                                                                                                                                                                                                       ....:::..::                              :       :::..  .::.. ...  .                ....          . :.:..:.            .  ..::::::.....   ...  ...  .:.:.:....... .::::::.:.:.::..                   ..                                       ....:::::::::::::.   .....::::....  .....  ....                    ....:..                               
------------------------------------------------------------------------------------------------------------------------------------------------------------------MIGGLFIYNHKGEVLISRVYRDDIGRNAVDAFRVNVIHARQQVRSPVTNIARTSFFHVKRSNIWLAAVTKQNVNAAMVFEFLYKMCDVMAAYFGKISEENIKNNFVLIYELLDEILDFGYPQNSETGALKTFITQQGIKSQHQTKEEQSQITSQVTGQIGWRREGIKYRRNELFLDVLESVNLLMSPQGQVLSAHVSGRVVMKSYLSGMPECKFGMNDKIVIEKQGKGTADETSKSGKQS------------------------------I-------AIDDC--TFHQC-VRL-SK----------FDSERSISFI----------P-PDGEFEL------------M-RYRTTKDIILPFRV-IPLVR-EVGR--TKLEVKVVIKSNF-KPSLLAQKIEVRIPTP--------LNTSGVQVICMKG-------------------KAKYKASENAIVWKIKRMAGMKESQISAEIELLPTNDK---KKWARPPISMNFE--VPFAPSGLKVR--------------------YLKVFEP-----KLNYSDHDVIKWVRYIGRSGIYETRC

