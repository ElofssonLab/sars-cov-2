
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/nsp10.pdb                             
Name of Chain_2: pymodel/nsp10-Q969X5.pdb_A                        
Length of Chain_1:  139 residues
Length of Chain_2:  197 residues

Aligned length=   63, RMSD=   4.50, Seq_ID=n_identical/n_aligned= 0.508
TM-score= 0.29102 (if normalized by length of Chain_1)
TM-score= 0.22209 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
AGN-----ATEVPANSTVLSFCAFAVDAAKAYKDYLASGGQPITNCVKMLCT--HTGTGQAITVTPEANMDQ----------ESFGGASCCLYCRCHIDHPNPKGFCDLKGKYVQIPTTCANDPVGFTLKNTVCTVCGMWKGYGCSCDQ--------LR-EPMLQ------------------------------------------------------------------------------------------------------------
        .:::::::: :::: ::::::::::::::::::::::::::...  .:::::..                    ..:::.                      .::.                                           .  ..                                                                                                               
---AGNATEVPANSTVL-SFCA-FAVDAAKAYKDYLASGGQPITNCVKMLCTHTGTGQAITV----------TPEANMDQESFGGASC----------------------CLYC-----------------------------------RCHIDHPNP-KGF---CDLKGKYVQIPTTCANDPVGFTLKNTVCTVCGMWKGYGCSCDQLREPMLQDRRDRDANDDDDDNNDDRDNNNARNNAANDDNANAADRNADDRANARARDRRRAADAA


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q969X5.pdb                            
Name of Chain_2: pymodel/nsp10-Q969X5.pdb_B                        
Length of Chain_1:  290 residues
Length of Chain_2:  290 residues

Aligned length=  114, RMSD=   5.41, Seq_ID=n_identical/n_aligned= 0.500
TM-score= 0.26762 (if normalized by length of Chain_1)
TM-score= 0.26762 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MPFDFRRFDIYRKVPKDLTQPT----------------------YTGAIISICCCLFILFLFLSELTG-FITTEV-------------------VNELYVDDPDKDSGGKIDVSLNI-----------SLPNL-------HCELVGLDIQDEMGR----HEVGHIDNSMKIPLNNG--AG-CRFEGQFSINKVPGNFHVS-------------------------------------THSATAQPQNPDMTHVIHKLSFGDTLQVQNIHGAFNALGGADRLTSNPLASHDYILKIVPTVYEDKSGKQRYSYQYTVANKEYVAYSHTGRIIPAIWFRYDLSPITVKYTER------------------------------------------------------------------------RQPLYRFITTICAIIGGTFTVAGILDSCIFTASEAWKKIQLGKMH
                                            .. .::::::::::..:....... ..::.:                   .....                             ...:.       ::::::...          ...::  ::::::::..  .  .                                                       ....                                                                                                                                                                                    ..:::::::::::::.::.:::::::::::::::::::::..:::
----------------------MPFDFRRFDIYRKVPKDLTQPTYT-GAIISICCCLFILFLFLSELTGFITTEVVNELYVDDPDKDSGGKIDVSLNIS------------------LPNLHCELVGLDIQDEMGRHEVGHIDNSMKIP------LNNGAGCRF--EGQFSINKVPGNF-HV------------------STHSATAQPQNPDMTHVIHKLSFGDTLQVQNIHGAFNALGG------------------------------------------------------------------------------------------------------------ADRLTSNPLASHDYILKIVPTVYEDKSGKQRYSYQYTVANKEYVAYSHTGRIIPAIWFRYDLSPITVKYTERRQPLYRFITTICAIIGGTFTVAGILDSCIFTASEAWKKIQLGKMH

