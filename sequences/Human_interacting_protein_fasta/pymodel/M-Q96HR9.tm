
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-Q96HR9.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  274 residues

Aligned length=  117, RMSD=   3.34, Seq_ID=n_identical/n_aligned= 0.658
TM-score= 0.43084 (if normalized by length of Chain_1)
TM-score= 0.35831 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYR----INWITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMWSFNP-----------------------------------------------------------------------------------------------------------------------------ETNILLNVPLHGTILTRPLLESE-------------LVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQ---------------
.:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::    :::::::::::::::::::::::::::::::...                                                                                                                                     .                                   ..:                              :    :.:..                 .                                       
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMW--------SFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQDRRRNAARAAAANA----------------------AADDDAADAARNARRR------------------------------A----DRNDR-----------------A------------------------DAAARNARRAAAADD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q96HR9.pdb                            
Name of Chain_2: pymodel/M-Q96HR9.pdb_B                            
Length of Chain_1:  211 residues
Length of Chain_2:  211 residues

Aligned length=  120, RMSD=   3.98, Seq_ID=n_identical/n_aligned= 0.617
TM-score= 0.41720 (if normalized by length of Chain_1)
TM-score= 0.41720 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MDGLRQ-------------RVEHFLEQR-NLVTEVLGALEAKTGVEKRYLAA-GAVTLL-SLYLLFGYGASLLCNLIGFVYPAYASIKAIESPSKDDDTVWLTYWVVYALFGLAEFFSDLLLSWF-PFYYVGKCAFLLFCM------APRPWNGALMLYQRVVRPLFLRHHGAVDRIMNDLSGRALDAAAGITRNVLQVLARSRAGITPVAVAGPSTPLEADLKPSQTPQPKDK--------------------------------------------------------------------
                   ....:.::. ::..::  :::      :. ..  .:...: ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: :::::::::::::::      ::....:::...                                                                                                                                               
------MDGLRQRVEHFLEQRNLVTEVLGALEAKT--GVE------KR-YL-AAGAVTLLSLYLLFGYGASLLCNLIGFVYPAYASIKAIESPSKDDDTVWLTYWVVYALFGLAEFFSDLLLSWFPFYYVGKCAFLLFCMAPRPWNGALMLYQRVVRPL---------------------------------------------------------------------------FLRHHGAVDRIMNDLSGRALDAAAGITRNVLQVLARSRAGITPVAVAGPSTPLEADLKPSQTPQPKDK

