
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/N.pdb                                 
Name of Chain_2: pymodel/N-P19784.pdb_A                            
Length of Chain_1:  419 residues
Length of Chain_2:  502 residues

Aligned length=  186, RMSD=   7.31, Seq_ID=n_identical/n_aligned= 0.167
TM-score= 0.26089 (if normalized by length of Chain_1)
TM-score= 0.22882 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
M-SDN--GPQNQRNAPRITFGGPSDSTGSNQNGERSGARSKQRRPQGLPN-----------------------NTASWF--TALTQHGKEDLK----FPR-GQGVPINTNSSPDDQIGYYRRATRRIRGG---DGKMKDLSPRWYFYYL---GTGPEAGLPYGANKDGIIWVATEGALNTPKDH---IGTRNPANNAAIVLQLPQGTTLPKGF--YAEGSRGGSQASSRSSSR-------SRNSSRNSTPGSSRGTSPARMA--------------------------------------------GNGGDAALALLL-LDRLNQLE------------------------SKMSGKGQQQQ----------GQTVTKKSAAEASKKPRQKRTATKAYNVTQAFGRRGPEQTQGNFGDQELIRQGTDYKHWP---------------------------------------QIAQFAPSASAFFGMSRIGMEVTPSGT---------------------WL--T---YTGAI--KLDDK----------------------------------------------------------------------DPNFKDQVILLNKHIDAYKTFPPTEPKKDKKKKADETQALPQRQKKQQTVTL---LPAADLDDFSKQLQQSMSS-----------------ADSTQA-----------------------------
  .    ..:..::::.            ....                                        ......  .:::........    ... :::..::. . ......::::::::::::   ::.:::::::...      .....:::.......:::..               .:..:::.:::::..             ....                     ......                                                            ....::.....  .                               .....:.::..          .:::..                                                                                             ..  ..  :  :::.. ..:.:...                       ..  .   ...:.  .                                                                          .:  .                                                  ..  .  ..                           .                                  
-MS--DNGPQNQRNAPR------------ITFG-----------------GPSDSTGSNQNGERSGARSKQRRPQGLPNNTASWFTALTQHGKEDLKFPRGQGVPINTN-S-SPDDQIGYYRRATRRIRGGDGKMKDLSPRWYFYY---LGTGPEAGLPYGANKDGIIWVAT------------EGALNTPKDHIGTRNPAN-----------NAAIVL--------------QLPQGTTLPKGFY----------------AEGSRGGSQASSRSSSRSRNSSRNSTPGSSRGTSPARMAGNGGDAALALLLLDRL-NQ-------LESKMSGKGQQQQGQTVTKKSAAEASKKPRQKRTATKAYNVTQAFGRRGPE------------------------------------------------------QTQGNFGDQELIRQGTDYKHWPQIAQFAPSASAFFGMSRIG--ME--V--TPSGT-WLTYTGAI--KLDDKDPNFKDQVILLNKHIDAYKTFPPTEPKKDKKK----KADETQALPQRQKKQQTVTLLPAADLDDFSKQLQQSMSSADSTQAAARARANRRDANDDRRANNNRRNRNDD--A-----------------------------------------------NNDDD--R--AD----------RDNDRDAANRARDDDARR-----DNDRADDNDRRNNRADDRDRAAADNAAAR


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P19784.pdb                            
Name of Chain_2: pymodel/N-P19784.pdb_B                            
Length of Chain_1:  350 residues
Length of Chain_2:  350 residues

Aligned length=  154, RMSD=   7.19, Seq_ID=n_identical/n_aligned= 0.026
TM-score= 0.24697 (if normalized by length of Chain_1)
TM-score= 0.24697 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MPGPAAGSRARVY----------------------------------------------------------------------------------AEVNSLRS--R-EYWDY--------------EAH-----VP--S----W-GNQDDYQLVRKLGRGKYSEVFEAINITNNERVVVKILKPVKKKKIKREVKILENLRGGT---NIIKLI-DTVKDPVSKTPA-------LVFEYINNTDFKQLYQILTDFDIRFYMYELLKALD-YCH--S---------KGIM-HRD-VKPHNVMIDHQQKKLRLIDWGLAEFYHPAQEYNVRVASR-------------YFKGPELLVDYQMYDYSLDMW--SLG--CMLASMIFRRE--PFF------H-GQDNYDQLVRIAKVLGTEELYGYLKKYHIDLDPHFNDILGQHSR------KRWENFIHSEN-----------------RHLV---SPE-------ALDLLDKLLRYDHQQRLTAKEAMEHPYFYPVVKEQSQPCADNAVLSSGLTAAR-
                                                                                               ....::..  . .::.:              :::     ..  .    . :.:::::.              .:::..:::.                              ..:::. .::..              ..:::..  ..  ..  :.:. ::  :. :....  ...  .         .... ... .:.::...                                            ..                     .    ..::...:...  ..       . ..:...:..::::::..::::..                           .                           ..     ..        .....::::::. ....                                     
-------------MPGPAAGSRARVYAEVNSLRSREYWDYEAHVPSWGNQDDYQLVRKLGRGKYSEVFEAINITNNERVVVKILKPVKKKKIKREVKILENLRGGTNIIKLIDTVKDPVSKTPALVFEYINNTDFKQLYQILTDFDIRFYMYE--------------LLKALDYCHS---------------------------KGIMHRDVKPHNVMI-------DHQQKKLRLIDWGL--AE--FY--HPAQ-EY--NV-RVASR-YFKGPELLVDYQMYDYSLDMWSLGCMLASMIFR-------------------------------REPFFHGQDNYDQLV-------------------RIA--KVLGTEELYGYLKKYHI-DLDPHFNDILGQHSRKRWENFIHSENRHLVS---------------------PEALDLL----------DKLLRYDHQQRLTAKEAME--HPYFY-PVVKEQSQPCADNAVLSSG-LTAA------------------------------------R

