
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/N.pdb                                 
Name of Chain_2: pymodel/N-P67870.pdb_A                            
Length of Chain_1:  419 residues
Length of Chain_2:  461 residues

Aligned length=  135, RMSD=   6.63, Seq_ID=n_identical/n_aligned= 0.289
TM-score= 0.20283 (if normalized by length of Chain_1)
TM-score= 0.18891 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
M------------------------------------------------------------------------------------------------------------------------------------SDNGPQNQR----NAPRITFGGPSDSTGSNQNGERSGARSKQRRPQGLPNNTASWFTALTQHGKEDLKFPRGQGVPINTNSSPDDQIGYYRRATRRIRGGDGKMKDLSPRWYFYYLGTGPEAGLPYGANKDGIIWVATEGALNTPKDHIGTRNPANNAAIVLQLPQGTTLPKGFYAEGSRGGSQASSRSSSRSRNSSRNSTPGSSRGTSPARMAGNGGDAALALLLLDRLNQLESKMSGKGQQQQGQTVTKKSAAEASKKPRQK----------------------------------------------------------------RTATKAYNVTQAFGRRGPEQTQGNFGDQELIRQGTD-YKHWPQIAQFAPSASAF----FG-MSRIGMEVTPSGTWLTYTG-----AIKLDDKDPNFKDQVILLN----------KHIDAYKTFPPTEPKKDKKKKADETQALPQRQKKQQTVTLLPAADLDDFSKQLQQSMSSADSTQA---------------------------------------------------------------------------------------------------------
                                                                                                                                     .::..::::    ...:::         ...          ::.   .  ::..                           .:::::..::.                            .       ::::..                                        .               .:. :. .....:::...                                                                                                                        ..:::::::..::::... :::::::::..:...:. ...:::.:...          .  ..:::.      ...         ..::..                       ....:..:....                                                                                                                                                              
-MSDNGPQNQRNAPRITFGGPSDSTGSNQNGERSGARSKQRRPQGLPNNTASWFTALTQHGKEDLKFPRGQGVPINTNSSPDDQIGYYRRATRRIRGGDGKMKDLSPRWYFYYLGTGPEAGLPYGANKDGIIWVATEGALNTPKDHIGTRNP---------ANN----------AAI---V--LQLP---------------------------QGTTLPKGFYA----------------------------E-------GSRGGS----------------------------------------Q---------------ASS-RS-SSRSRNSSRNS--------------------------------------------------------TPGSSRGTSPARMAGNGGDAALALLLLDRLNQLESKMSGKGQQQQGQTVTKKSAAEASKKPRQKRTATKAYNVTQAFGRRGP-EQTQGNFGDQELIRQGTDYKHWPQIAQFA------PSASA-FFGMSRI------GME----VTPSGTWLTYT-------------GAIKLDDKDPNFKDQVILLNKH-----------------------------------------------------IDAYKTFPPTEPKKDKKKKADETQALPQRQKKQQTVTLLPAADLDDFSKQLQQSMSSADSTQARNDDDNNRADDDDNNDAAARNRADRNDADRDARRANRAAANR


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P67870.pdb                            
Name of Chain_2: pymodel/N-P67870.pdb_B                            
Length of Chain_1:  215 residues
Length of Chain_2:  215 residues

Aligned length=   90, RMSD=   5.48, Seq_ID=n_identical/n_aligned= 0.500
TM-score= 0.25655 (if normalized by length of Chain_1)
TM-score= 0.25655 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MSSSEEVSWISWFCGLRGNEFFCEVDEDYIQDKFNLTGLNEQVPHYRQALDMILDL----------------------------------------------------------EPDEELEDNPNQSDLIEQAAEMLYGLIHARYILTNRGIAQMLEKYQQGDFGYCPRVYCENQPMLPIGL-----SD-IPGEAM-VKLYCPKCMDVYTPKSSRHHHTDGAYF---GTGFP---HMLF------MVHPEYRPKRPANQFVPRLYGFKIHPMAYQLQLQA---ASNFKSPVKTIR---------------------------------------------
                                                                                                                  ....:::..  ::::::::::::::::::::....:...::::::::::.:..       :::::...     .  ..:::: ::::::::..                    ...     ..        .:..                                  .    .:..                                                
--------------------------------------------------------MSSSEEVSWISWFCGLRGNEFFCEVDEDYIQDKFNLTGLNEQVPHYRQALDMILDLEPDEELEDNPN--QSDLIEQAAEMLYGLIHARYILTNRGIAQMLEKYQQGDFGYC-------PRVYCENQPMLPIG-LSDIPGEAMVKLYCPKCM-----------------DVYTPK--SSRHH--HTDGAYFGTG-------------------------------FPHM----LFMV---HPEYRPKRPANQFVPRLYGFKIHPMAYQLQLQAASNFKSPVKTIR

