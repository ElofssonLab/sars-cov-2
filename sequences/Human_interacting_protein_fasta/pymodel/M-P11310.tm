
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-P11310.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  330 residues

Aligned length=   94, RMSD=   4.20, Seq_ID=n_identical/n_aligned= 0.777
TM-score= 0.31370 (if normalized by length of Chain_1)
TM-score= 0.22437 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMWSF------------------------NPETNI-LLNVPLHGTILTRPLLESELVIGAVILRGHLRI---------------------------------------------------------------------------------------------------------------------------------------------AGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAY----------------------------------------------------------------------SRYRIGNYKLNTDHSSSSDNIALLVQ
:.:::::::::::::::::::::::::::::::::::::..::::::::::::::::::::::..:.....                                                                 ....:: ....       .::..:..                                                                                                                                                           :                                                                                                                  :  ::                  :  
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVY-----------------------------------------RINWITGGIAIAMACLVGLMWLSYFIASFRLFART-------RSMWSFNP--------------ETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQAARRRRRANRRAARARAADRRANNDAAANANDRA--------------------------------------------ADAADNNANARDDAANAADRNRDRDNDAAADRAAAARADAARAAAARRAADRRNAAAADANADANNRDARA--RD------------------N--


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P11310.pdb                            
Name of Chain_2: pymodel/M-P11310.pdb_B                            
Length of Chain_1:  421 residues
Length of Chain_2:  421 residues

Aligned length=  146, RMSD=   5.42, Seq_ID=n_identical/n_aligned= 0.432
TM-score= 0.25386 (if normalized by length of Chain_1)
TM-score= 0.25386 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MAAGFGRCCRVLRSISRFHWRSQHTKANRQREPGLGFSFEFTEQQKEFQATARKFAREEIIPVAAEY----------------------------------------------------------------------------------------------------------------------------------------------------------DKTGEYPVPLIRRAWELGLMNTHIPENCGGLGLGTFDACLISEELAYGCTGVQTAIEGNSLGQMPIIIAGNDQQKKKYL-------------------------------------------------------------------------------------GRMTEEPLMCAYCVTEPGAGSDVAGIKTKAEKKGDEYIINGQKMWITNGGKANWYFLLARSDPDPKAPANKAFTGFIVEADTPGIQIGRKELNMGQRCSDTRGIVFEDVKVPKENVLIGDGAGFKVAM-GAFDKTRPVVAAGAVGLAQRALDEATKYALERKTFG----KLLVEHQAISFMLAEMAMKVELARMSYQRAAWEVDSGRRNTYYASIAKAFAGDIANQLATDAVQIL-----------------GGNGFNTEYPVEKLMRDAKIYQIYEGTSQIQRLIVAREHIDKYKN--------------
                                                                                                                                                                                                                             .. ::::: .::...:...                                                                                                                                                 ...:.:......                                       . . .                                                                         ..::::::: :::::::::::::::::::::.::::    ....::: :::::::::::::::.:::::::::::::::::::::::::::::::::.........                 .........:....                                             
-------------------------------------------------------------------MAAGFGRCCRVLRSISRFHWRSQHTKANRQREPGLGFSFEFTEQQKEFQATARKFAREEIIPVAAEYDKTGEYPVPLIRRAWELGLMNTHIPENCGGLGLGTFDACLISEELAYGCTGVQTAIEGNSLGQMPIIIAGNDQQKKKYLGRMTEEPLMC-AYCVT-EPGAGSDVAG------------------------------------------------------------IKTKAEKKGDEYIINGQKMWITNGGKANWYFLLARSDPDPKAPANKAFTGFIVEADTPGIQIGRKELNMGQRCSDTRGIVFEDVKVPKENVLIGDGA---------------------------------------G-F-K------------------------------------------------------------------------VAMGAFDKTR-PVVAAGAVGLAQRALDEATKYALERKTFGKLLVEHQA-ISFMLAEMAMKVELARMSYQRAAWEVDSGRRNTYYASIAKAFAGDIANQLATDAVQILGGNGFNTEYPVEKLMRDAKIYQIYEGTSQIQ-------------------------------RLIVAREHIDKYKN

