
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/N.pdb                                 
Name of Chain_2: pymodel/N-Q8TAD8.pdb_A                            
Length of Chain_1:  419 residues
Length of Chain_2:  533 residues

Aligned length=  149, RMSD=   6.35, Seq_ID=n_identical/n_aligned= 0.242
TM-score= 0.23399 (if normalized by length of Chain_1)
TM-score= 0.19395 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MSDNGP------QNQRNAPRITFGGPSDSTGSNQNGERSGARSKQRR--------PQGLPNNTASWFTALT----------Q-HGKEDLKFPRGQGVPINTNSSPDDQIGYYRR-----------------------------------------------ATRRI----R--G------------------------GDGKMKDLSPRWYFYYLGTGPEAGLPYGANKDGI------IWVATEG--ALNTPKDHIGTRNPANNAAIVLQLPQGTTLPKGFYAEGSRGGSQASSRSSSRSRNSSRNSTPGSSRGTSPARMAGNGGDAALALLLLDRLNQLESKMS-GKGQQQQGQTVTKKSAAEASKKPRQKRTATKAYNVTQAFGRRGPEQTQGNFGDQELIRQGTDYKHWPQIAQFAPSASAFFGMSRIGMEVTPSGTWLTYTGAIKLDDKDPNFKDQVILLNKHIDAYKTFPPTEPKKDKKKKADETQALPQRQKKQQTVTLLPAADLDDFS------------------------------------------------------------------------------------------------------KQLQQSMSSADST-------------------------------------------------------------------------------------------------------------------------------------------QA--------------------------------
            ..:....:::::::..                           .....::::.:.:::.          . ...::::::.:.                                                                  ..::.    .  .                        ::.:.:::.::::.:...::::...               ...:.    ...:.::.... .::.:                      ::  :: .   ::.::.::                      ::::::::..::.....: .:::..                                                                                                                                                                                                                                                                         ....:::......                                                                                                                                           .                                 
------MSDNGPQNQRNAPRITFGGPSD-------------------STGSNQNGERSGARSKQRRPQGLPNNTASWFTALTQHGKEDLKFPRGQ-------------------GVPINTNSSPDDQIGYYRRATRRIRGGDGKMKDLSPRWYFYYLGTGPEAGLPYGANKDGIIWVATEGALNTPKDHIGTRNPANNAAIVLQLPQGTTLPKGFYAEGSRGG---------SQASSRSSSRS--RNSSRNSTPGSSR-GTSPA----------------------RM--AG-N---GGDAALAL----------------------LLLDRLNQLESKMSGKGQQQQGQTV-------------------------------------------------------------------------------------------------------------------------------------------------------------------TKKSAAEASKKPRQKRTATKAYNVTQAFGRRGPEQTQGNFGDQELIRQGTDYKHWPQIAQFAPSASAFFGMSRIGMEVTPSGTWLTYTGAIKLDDKDPNFKDQVILLNKHIDAYKTFPPTEPKKDKKKKADETQALPQRQKKQQTVTLLPAADLDDFSKQLQQSMSSADSTQAARRRRRRDDARAARRDARRNRARRNARRRRNRRDRRRDRRRRARNDRDRRRRNRRDRDNARNARRRRRNDRNNAADNRARRR-NDRARRRADDARRADRRDNNNRRDRDDRDDDD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q8TAD8.pdb                            
Name of Chain_2: pymodel/N-Q8TAD8.pdb_B                            
Length of Chain_1:  396 residues
Length of Chain_2:  396 residues

Aligned length=  136, RMSD=   6.69, Seq_ID=n_identical/n_aligned= 0.059
TM-score= 0.21755 (if normalized by length of Chain_1)
TM-score= 0.21755 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MKAVKSERERGSRRRHRDGDVVLPAGVVVKQERLSPEVAPPAHRRPDHSGGSPSPPTSEPARSGHRGNRARGVSRSPPKKKNKASGRRSKSPRSKRNRSPHHSTVKVKQEREDHPRRGREDRQHREPSEQEHR------------------------------------------RARNSDRDRHRGHSHQRRTSNERPGSGQGQGRDRDTQ------N----LQAQEEEREFYNARRREHRQRNDVGGGGSESQELVPRPGGNNKEKEVPAKE-------------------KPSFELSGALLEDTNTFRGVVI------------------------------KYSEPPEARIPKKRWRLYPFKNDEVLPVMYIH-------------------------------------------------------------------RQSAYLLG-----------RHRRIAD------------I-PI-DHP-SCSKQHAVFQYRLVEYTRADGTVGRRVK-PY--IIDLGSGNGTFLNNKRIEPQRYYELKEKDVLKFGFSSREYVLLHESSDTSEIDRKDDEDEEEE------------------------EEVSD--S-------------------------------------
                                                                                                                                                                               ....::::::: ::: :::.::::.    ..            .    ...::::::::..:..:::              . ..  .. . ..:::..                   .::..                                               ......                                                                                             .::::.             ..::...            . .. ... :::::::::::::...      ...::: ::  ::::..::..                       .::.::....                                            ...    .                                     
-------------------------------------------------------------------------------------------------------------------------------------MKAVKSERERGSRRRHRDGDVVLPAGVVVKQERLSPEVAPPAHRRPDHSGGSP-SPP-TSEPARSGH----RG------NRARGVSRSPPKKKNKASGRRSKSPRSKRN--------------R-SP--HH-S-TVKVKQEREDHPRRGREDRQHREPSEQEHRR-----------------ARNSDRDRHRGHSHQRRTSNERPGSGQGQGRDRDTQ--------------------------NLQAQEEEREFYNARRREHRQRNDVGGGGSESQELVPRPGGNNKEKEVPAKEKPSFELSGALLEDTNTFRGVV--IKYSEPPEARIPKKRWRLYPFKNDEVLPVMYIHRQSAYLLGRHRRIADIPIDHPS------CSKQHAVFQYRLVEYTRADGT-----------------------VGRRVKPYII--------------------DLGSGNGTFLNNKRIEPQRYYELKEKD--VLKFGFSSREYVLLHESSDTSEIDRKDDEDEEEEEEVSDS

