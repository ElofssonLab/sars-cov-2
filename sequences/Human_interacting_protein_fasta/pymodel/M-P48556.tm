
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-P48556.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  305 residues

Aligned length=   95, RMSD=   4.01, Seq_ID=n_identical/n_aligned= 0.747
TM-score= 0.31876 (if normalized by length of Chain_1)
TM-score= 0.24487 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVI-----------------------G-FLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLA-AVYRINWITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQ-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                               . ......::::::::.:::::::::::::::::::::::::::: ::::::::: ..:::::::::::::::::::::::::: ::         :::.                   :.:::.                                        ..                                                                                                                                                                                                                          
------------------------MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITG-GIAIAMACLVGLMWLSYFIASFRLFART-RS---------MWSF-------------------NPETNI----------------------------------------LL---------------------------------NVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQRARARRRRARRARARRRARRRRAARAAAANAAAAAAANRNRNARDARDRADANNRARADNNANADDRDAAARNDARNNADAAR


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P48556.pdb                            
Name of Chain_2: pymodel/M-P48556.pdb_B                            
Length of Chain_1:  350 residues
Length of Chain_2:  350 residues

Aligned length=  107, RMSD=   4.72, Seq_ID=n_identical/n_aligned= 0.449
TM-score= 0.22800 (if normalized by length of Chain_1)
TM-score= 0.22800 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MFIKGRAPRAPPRERRRATRGGLRQVVAPPRALGSTSRPHFRRASVCRRRCRKSGGLLAASRKMAAAAVNGAAGFSSSGPAATSGAVLQAATGMYEQLKGEWNRKSPNLSKCGEELGRLKLVLLELNFLPTTGTKLTKQQLILARDILEIGAQWSILRKDIPSFERYMAQLKCYYFDYKEQLP----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ESAYMHQLLGL-NLLFLLSQNRVAEFHTELERLPAKDIQTNVYIKHPVSLEQYLMEGSY-NKVFLAK--GNIPAESYTFFIDILLDTIRDEIAGCIEKAYEKILF-TEATRILFFNTPKKMTDYAKKRGWVLGPNNYYSFASQQQKPEDTTIPSTELAKQVIEYARQLEMIV------------------------------------
                                                                                                                                                                                                                                                                                                                                                                                                 .......::.. ......:: ::: .:                    :. .::.::::: :::.::.  :::::::::::::::::::::::::::::::::::. .:..::...:: ..::::::: ::::..:..                                                                       
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------MFIKGRAPRAPPRERRRATRGGLRQVVAPPRALGSTSRPHFRRASVCRRRCRKSGGLLAASRKMAAAAVNGAAGFSSSGPAATSGAVLQAATGMYEQLKGEWNRKSPNLSKCGEELGRLKLVLLELNFLPTTGTKLTKQQLILARDILEIGAQWSILRKDIPSFERYMAQLKCYYFDYKEQLPESAYMHQLLGLNLLFLLSQNRVAEFHTELERLPAKDIQT-NVY-IK--------------------HP-VSLEQYLMEGSYNKVFLAKGNIPAESYTFFIDILLDTIRDEIAGCIEKAYEKILFTEATRILFFNTP-KKMTDYAKK-RGWVLGPNN-----------------------------------YYSFASQQQKPEDTTIPSTELAKQVIEYARQLEMIV

