
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/E.pdb                                 
Name of Chain_2: pymodel/E-Q6UX04.pdb_A                            
Length of Chain_1:   75 residues
Length of Chain_2:  192 residues

Aligned length=   70, RMSD=   1.74, Seq_ID=n_identical/n_aligned= 0.071
TM-score= 0.82802 (if normalized by length of Chain_1)
TM-score= 0.34138 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
---M-YSFVSEETGTLIVNSVLLFLAFVVFLLVTLAILTALRLCAYCCNIVNVSLVKPSFYVYSRVKNLNSSRVPD-LLV---------------------------------------------------------------------------------------------------------------------
   : ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  ....  .                                                                                                                       
MYSFVSEETGTLIVNSVLLFLAFVVFLLVTLAILTALRLCAYCCNIVNVSLVKPSFYVYSRVKNLNSSR--VPDL-LV--NNADDAARNADNRDADRRNRRAANADNRADNNDNRDDDDRNNDDRRNANRDDDDADDDDADDDNRRADANARRARRAANAAARADAARRARRDANAANDDDDRDADDDRNNRRRRRR


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q6UX04.pdb                            
Name of Chain_2: pymodel/E-Q6UX04.pdb_B                            
Length of Chain_1:  472 residues
Length of Chain_2:  472 residues

Aligned length=  204, RMSD=   7.60, Seq_ID=n_identical/n_aligned= 0.059
TM-score= 0.25549 (if normalized by length of Chain_1)
TM-score= 0.25549 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MSNIYIQEPPTNGKVLLKTTAGDIDIELWSKEAPKACRNFIQLCLEAYYDNTIFHRVVPGFIVQGGDPTGTGSGGESIYGAPFKDEFHSRLRFNRRGLVAMANAGSHDNGSQFFFTLGRADELNNKHTIFGKVTGDTVYNMLRLSEVDIDDDERPHNPHKIKSCEVLFNPFDDIIPREIKRLKKEKPEEEVK--------KL----------------K-PKG--TKNFSLLSFGEEAEEEEEEVNRVSQSMKGKSKSS------------------------HDLLKDDPHLSSVPVVESEKGDAPDLV---DDGEDESAEHDEYIDGDEKNLMRER-I-AKKLKKDTSA-NVKSAGEGEVEKKSVSRSEELRKEARQLKRELLAAKQKKVENAAKQAEKRSEEEEAPPDGAVAEYRREKQKY------------------------EALRKQQSKKG-------T--SRED--QTLALLNQFKSKLTQAIAETPENDI--------------PETEVEDDEGWMSHVLQFEDKSRKVKD--------------------------------------ASMQDSDTF-EI-------------------------------------------------------------Y----------------DPRNPVN-KRRREESKKLMREKKERR---------------------------------------------
                                                                                                                                                                                        ..:::...        :.                . ...  ....::...    :::  :::  .   .....:.                        ...... .   .::::.     ....    ....:::..........::. ...  . ....:....  .......::::....:..  .. ... .:.:::::::..:.........                                               ....::::...       .  ..    ...:::::: :: ::....                    ......:::...                                                     ....::... ..                                                             .                ..::..: ::::::....                                                     
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------MSNIYIQEPPTNGKVLLKTTAGDIDIELWSKEAPKACRNFIQLCLEAYYD----NTI--FHR--V---VPGFIVQGGDPTGTGSGGESIYGAPFKDEFHSRLRFN-R---RGLVAM-----ANAG-SHDNGSQFFFTLGRADELNNKHT-IFG-KVTGDTVYNMLR-LSEVDIDDDERPHNPHKIK--SC-EVL-FNPFDDIIPREIKRLKKEKPEE-----------------------EVKKLKPKGTKNFSLLSFGEEAEEEEEEVNRVSQSMKGKSKSSHDLL--KDDPHLSSVPV-VE-SEKGDA------PDLVDDGEDESAEHDEYIDGDEKNLM---------------RERIAKKLKKDTSANVKSAGEGEVEKKSVSRSEELRKEARQLKRELLAAKQKKVENAAKQAEKRSEEEEAPPDGAVAEYRREKQKYEALRKQQSKKGTSREDQTLALLNQFKSKLTQAIAETPENDIPETEVEDDEGWMSHVLQFE--------DKSRKVKDASMQDSDTFEIYDPRNPVNKRRREESKKLMREKKERR

