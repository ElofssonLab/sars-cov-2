
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-P13804.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  305 residues

Aligned length=  107, RMSD=   4.42, Seq_ID=n_identical/n_aligned= 0.804
TM-score= 0.34922 (if normalized by length of Chain_1)
TM-score= 0.26815 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITG---G--IAIAMACLVGLMWLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRP-------------------------------------------------------------------------------------------------------------------------------------------------------------------LLESE-----LVIGAVILRGHLRIAGHH-----------------LGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQ--------
:.::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.  :. ......     .  ..:.::::::::::::..::  .: :...                                                                                                                                                                                           ..:.      .                                  ..                                                                         
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACF--VL-AAVYRI--NWITGGIAIAMACLVGLMWLSYFIAS--FR-LFAR------------------------TRSMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQRAAARRAARAANDANAARADADAAADANAAANRAAADADRANADRDAAAA-ADDRAR-----------------NDADAAARAADANDAAAAD-----------------------------------------------------------------ANDAADAD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P13804.pdb                            
Name of Chain_2: pymodel/M-P13804.pdb_B                            
Length of Chain_1:  333 residues
Length of Chain_2:  333 residues

Aligned length=  167, RMSD=   4.36, Seq_ID=n_identical/n_aligned= 0.749
TM-score= 0.39596 (if normalized by length of Chain_1)
TM-score= 0.39596 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MFRAAAPGQLRRAASL----------------LRFQSTLVIAEHANDSLAPITLNTITAATRLGGEVSCLVAGTKCDKVAQDLCKVAGIAKVLVAQHDVYKGLLPEELTPLILATQKQFNYTHICAGASAFGKNLLPRVAA-KLEVAPI-SDIIA-IKSPDTFVRTIYA-GN--A--LCTVKCDEKVKVFSVRGTSFDAA--A--------T--S----GG-SA---S------------SEKASSTSPVEISEWLDQKLTKSDRPELTGAKVVVSGGRGLKSGENFKLLYDLADQLHAAVGASRAAVDAGFVP----------------------------------------NDMQVGQTGKIVAPELYIAVGISGAIQHLAGMKDSKTIVAINKDPEAPIFQVADYGIVADLFKVVPEMTEILKKK----------------------------------------------------------------------
                                ..::.::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::.:::::.::::::::::::::::::::::::::::::::::::: ::::... ....  .       .:::. :.  .  .::...  ...:::::: :..:.  :        .  :    :: ::   .            .::..                                                                                                             .::..                                                                                                                                            
----------------MFRAAAPGQLRRAASLLRFQSTLVIAEHANDSLAPITLNTITAATRLGGEVSCLVAGTKCDKVAQDLCKVAGIAKVLVAQHDVYKGLLPEELTPLILATQKQFNYTHICAGASAFGKNLLPRVAAKLEVAPISDIIAI-KS-------PDTFVRTIYAGNALCTVKC--DEKVKVFSV-RGTSFDAAATSGGSASSEKASSTSPVEISEWLDQKLTKSDRPELTGAKVV---------------------------------------------------------------------VSGGRGLKSGENFKLLYDLADQLHAAVGASRAAVDAGFVPNDMQV----------------------------------------------------------------------GQTGKIVAPELYIAVGISGAIQHLAGMKDSKTIVAINKDPEAPIFQVADYGIVADLFKVVPEMTEILKKK

