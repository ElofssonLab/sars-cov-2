
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-Q9Y6E2.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  312 residues

Aligned length=   86, RMSD=   5.49, Seq_ID=n_identical/n_aligned= 0.628
TM-score= 0.25183 (if normalized by length of Chain_1)
TM-score= 0.19195 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYAN-RNRFLYIIKLIFLWLLW--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------PVTLACFVLAAVYRINWITGGIAIAMA--------------CLVGLMWLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILT---RPLLESELVIGAVILRGH----LRIAG---HHLGRCDIKDLPKEITVAT-----SRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQ--------------
.::.:::::::::::::::::::::..::::::::::...  ..::::::....                                                                                                                                                                                           .: :. .               ...                .:.   .                                         : ...:                .::::   .. ...   : :            ...                       ..                                    
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYA-NRNRFLYIIKLIF-----LWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQNRRRDRDNADDAADR-DR-R---------------ADD--AARDDDNANDRNANRRA---D--------------------------------------AANA-DAAA------------AADANRANDRNRDA-DAD---R-R-------RRNDANAN-----------------------AA----------------------AADNAADAAADNAN


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q9Y6E2.pdb                            
Name of Chain_2: pymodel/M-Q9Y6E2.pdb_B                            
Length of Chain_1:  419 residues
Length of Chain_2:  419 residues

Aligned length=  178, RMSD=   5.33, Seq_ID=n_identical/n_aligned= 0.270
TM-score= 0.31266 (if normalized by length of Chain_1)
TM-score= 0.31266 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MNKHQKPVLTGQRFKTRKRDEKEKFEPTVFRDTLVQGLNEAGDDLEAVAKFL--------------------------------------------------------DSTGSRLDYRRYADTLFDILVAGSMLAPGGT-RIDDGDKT-----KMT-NHCVFSANEDHETIRNYAQVFNKLIRRYKYLEKAFEDEMKKLLLFLKAFSETEQTKLAMLSGILLGNGTL-PATILTSLFTDSLVKEGIAASFAVKLFKAWMAEKDANSVTSSLRKANLDKRLLELFPVNRQSVDHFAKYFTD-----AG-LKELSDFLRVQQSLGTRKELQ-KELQERLSQECPIKEVVLYVKEEMKRNDLPETAVIGLLWTCIMNAVEWNKKEELVAEQALKHLKQYAPLLAVFSSQGQSELILLQKVQEYCYDNIHFMKAFQKIVVLFYKADVLSEEAILKWYKEAHVAKGKSVFLDQMKKFVEWLQNAEEESESEGEEN--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                                                                                            .. ...:::::::::::::::::::.....  .            ..  .::::.. ::::::::::::::::::::::::::::::::::::::: :   ::::::::::::::..:. ..:...:..:. .:.:.::.:::::::: :.    .:::::..:. .... ..      ..:: ::::::..     :: :::::::::..:..:.....  ..                                                                                                                                                                                                                                                                                                                                        
----------------------------------------------------MNKHQKPVLTGQRFKTRKRDEKEKFEPTVFRDTLVQGLNEAGDDLEAVAKFLDSTGSR-LDYRRYADTLFDILVAGSMLAPGGTRI-DD-------GDKTKMT-NHCVFSAN-EDHETIRNYAQVFNKLIRRYKYLEKAFEDEMKKLLLFLK-A---FSETEQTKLAMLSGILLGNGTLPATILTSL-FTDSLVKEGIAASFAV-KL----FKAWMAEKDA-NSVT-SS------LRKA-NLDKRLLELFPVNRQSVDHFAKYFTDAGLKELSDFL-RVQ--------------------------------------------------------------------------------------------------------------------------------------------------------------QSLGTRKELQKELQERLSQECPIKEVVLYVKEEMKRNDLPETAVIGLLWTCIMNAVEWNKKEELVAEQALKHLKQYAPLLAVFSSQGQSELILLQKVQEYCYDNIHFMKAFQKIVVLFYKADVLSEEAILKWYKEAHVAKGKSVFLDQMKKFVEWLQNAEEESESEGEEN

