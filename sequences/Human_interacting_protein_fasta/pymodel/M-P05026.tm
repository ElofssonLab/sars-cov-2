
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-P05026.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  274 residues

Aligned length=  107, RMSD=   3.36, Seq_ID=n_identical/n_aligned= 0.710
TM-score= 0.37876 (if normalized by length of Chain_1)
TM-score= 0.31703 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAV---YRINW-ITGGIAIAMACLVGLMWLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVAT-------------------------------------------------------------------------------------------------------------------------------------------------------------SRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQ------
::::..:::::::::::::::::::::::::::::::::::::::::..:::::::::::::::::::::   :::.: ..:::::::::::::::::::::::..: :.                                                                                                                                                                                                                               ..                                                      
MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLMWLSYFIASFRLFARTR-SM------------------------------------------------------------------WSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQARANRADRARNDANRDARDDDDRDNRRRNNDNRNNNRDDDNNANDR------------------------------------------------ANDRRD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P05026.pdb                            
Name of Chain_2: pymodel/M-P05026.pdb_B                            
Length of Chain_1:  303 residues
Length of Chain_2:  303 residues

Aligned length=  136, RMSD=   6.30, Seq_ID=n_identical/n_aligned= 0.169
TM-score= 0.26663 (if normalized by length of Chain_1)
TM-score= 0.26663 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MARGKAKEEGSWKKFIWNSEKKEFLGRTGGSWFKILLFYVIFYGCLAGIFIGTIQVMLLTISEFKPTYQDRVAPPGLTQIPQ----------------------------------------------------------------------------------------IQKTEISFRPNDPKSYEAYVLNIVRFLEKY--KD--------SAQRDDMIFEDCGDVPSEPKERGDF----------------NHERGE---RKVCRFKLEWLGNCSGLN-DETYGYKEGKPCIIIKLNRV-LGFKP-KPPKNESLETYPVMKYNPNVLPVQCTGKRDEDK--D-KVGNVEYFGLGNSPGFPLQYYPYYGKLLQPKYLQPLLAVQFTNLTMDTEIR---------IECKAYGENIGYSEKDRFQGRF----------DVKIEVKS-------------------------
                                                                                                                                                                          ..:::..          ......:::::::  ..        :..::::.                                 .        ...:::...:::...::. ::.....   .::::::::: ..... .............      .......:::::::  : :::....         ..:...:.           .::.::::.                .:...                           ....::::                         
----------------------------------------------------------------------------------MARGKAKEEGSWKKFIWNSEKKEFLGRTGGSWFKILLFYVIFYGCLAGIFIGTIQVMLLTISEFKPTYQDRVAPPGLTQIPQIQKTEISFRPNDP----------KSYEAYVLNIVRFLEKYKDSAQRDDMIFEDCGD-----------------VPSEPKERGDFNHERGE-----RKVCRFKLEWLGNCSGLNDETYGYKEGKP---CIIIKLNRVLGFKPKPPKNESLETYPVMKY------NPNVLPVQCTGKRDEDKDKVGNVEY---------FGLGNSPG-----------FPLQYYPYY-------GKLLQPKYLQPLLA-----------------VQFTNLTMDTEIRIECKAYGENIGYSEKDRFQGRFDVKIEVKS

