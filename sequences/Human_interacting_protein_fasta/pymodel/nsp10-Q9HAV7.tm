
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/nsp10.pdb                             
Name of Chain_2: pymodel/nsp10-Q9HAV7.pdb_A                        
Length of Chain_1:  139 residues
Length of Chain_2:  182 residues

Aligned length=   91, RMSD=   4.92, Seq_ID=n_identical/n_aligned= 0.099
TM-score= 0.37340 (if normalized by length of Chain_1)
TM-score= 0.31009 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
AG-NATEVPANSTVLSFCAFAVD--A--AKA--YKDYLASGGQ--PITNCVKMLCTHTG---------TGQAIT-VTPE--ANM-DQESFGGASCCLYCRCHIDHPNPKG---FCDLKGKYVQIPT------------TCANDPVGFTLKNTVCTVCGMWKGYGCSCDQLREPMLQ------------------------------------------------------
   ....:::::::::: .:::.  .  .    ..          ..:::::.:.             ..:::: ::::  ::. .:::: : ::...:.:::...       .:::. ::::.::            .:.:::. ::::::.                                                                             
--AGNATEVPANSTVLS-FCAFAVDAAKA--YKDY--------LASGGQPITNCV----KMLCTHTGTGQAITVTPEANMDQESFGGASC-C-LYCRCHIDHPNPK----GFCDLKGK-YVQIPTTCANDPVGFTLKNTVCTVCG-MWKGYGC-----------------------SCDQLREPMLQAARARRAARRANNDADARAADNRRAADADADDNNNADAARRAA


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/Q9HAV7.pdb                            
Name of Chain_2: pymodel/nsp10-Q9HAV7.pdb_B                        
Length of Chain_1:  217 residues
Length of Chain_2:  217 residues

Aligned length=  119, RMSD=   4.38, Seq_ID=n_identical/n_aligned= 0.739
TM-score= 0.40270 (if normalized by length of Chain_1)
TM-score= 0.40270 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MAAQCVRLARRSLPALALSLRPSPRLLCTATKQKNSGQNL---------------------------------------------EEDMGQSEQK-ADPPATEKTLLEEKVKLEEQLKETVEKYKRALADTENLRQRSQKLVEEAKLYGIQAFCKDLLEVADVLEKATQCVPKEE--IKDDNPHL------KNL--YEG-LVM-TEVQIQKVFTKHGLLKLNPVGAKFDPYEHEALFHTPVEGKEPGTVALVSKVGYKLHGRTLR--PALVGVVKEA--------------------------------------
                                                                                     .  ..   .  ..:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: :...:.....    ..            .    ..  ... .:::::::::::.::::::::  ::                          : :   ::::  ..                                              
----------------------------------------MAAQCVRLARRSLPALALSLRPSPRLLCTATKQKNSGQNLEEDMGQ--SE---Q-KADPPATEKTLLEEKVKLEEQLKETVEKYKRALADTENLRQRSQKLVEEAKLYGIQAFCKDLLEVAD-VLEKATQCVP--KEEI------KDDNPHL--KNLY-EGLVMTEVQIQKVFTKHGLLKLNPVG--AK--------------------------F-D---PYEHEALF--------HTPVEGKEPGTVALVSKVGYKLHGRTLRPALVGVVKEA

