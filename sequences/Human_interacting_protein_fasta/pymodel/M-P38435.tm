
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/M.pdb                                 
Name of Chain_2: pymodel/M-P38435.pdb_A                            
Length of Chain_1:  222 residues
Length of Chain_2:  365 residues

Aligned length=   84, RMSD=   5.32, Seq_ID=n_identical/n_aligned= 0.071
TM-score= 0.23884 (if normalized by length of Chain_1)
TM-score= 0.16250 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MADSNGTITVEE--------LKKLLEQWNLVIGFLFLTWICLLQFAYAN-------RNRFLYIIKLIFLWLLWPVTLACFVLAAVYRINWITGGIAIAMACLVGLMWLSYFIASF---------------------------------------------------------------------------------------------------------------------------------------------------------RLFARTRSMWSF------------NP--ETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVA------------TSRTLSYYKLGASQRVAGDSGFAAYSR-----------------------------YRIGNYKLNTDHSSSSDNIALLVQ----------------------------------------------------------
                    .....:::::.:::::::::::::::::.       ........:::::::::::..::::::    :: :: :: ::::::::. ..                                                                                                                                                                .                       .   ...                                                                  .::                                                     ...                                                                               
------------MADSNGTITVEELKKLLEQWNLVIGFLFLTWICLLQFAYANRNRFLYIIKLIFLWLLWPVTLACFVLAAVY----RI-NW-IT-GGIAIAMAC-LV-------GLMWLSYFIASFRLFARTRSMWSFNPETNILLNVPLHGTILTRPLLESELVIGAVILRGHLRIAGHHLGRCDIKDLPKEITVATSRTLSYYKLGASQRVAGDSGFAAYSRYRIGNYKLNTDHSSSSDNIALLVQAAARDDARDRDRRNRDAARD-----------RDRDDRDARDARD-NNADA------------------------------------------------------NDNARRNANARADAD------------------------RDADRNARRRAARRRAANNNDRRDRNRRDADA---------------------RNDNDRRDRDAARDRADNADNDNANRANADANRRRRRRNRRRRRRNRAANRANNDNND


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P38435.pdb                            
Name of Chain_2: pymodel/M-P38435.pdb_B                            
Length of Chain_1:  758 residues
Length of Chain_2:  758 residues

Aligned length=  196, RMSD=   9.42, Seq_ID=n_identical/n_aligned= 0.122
TM-score= 0.14580 (if normalized by length of Chain_1)
TM-score= 0.14580 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MAVSAGSARTSPSSDKVQKDKAELISGPRQDSRIGKLLGFEWTDL-------------------------------------------------------------SSWRRLVTLLNRPTDPASLAVFRFLFGFLMVLDIPQERGLSSLDRKYLD-----------------GLDVCRFPLL---------DA-LRPLPLDW-----------------------------MYL--VY-TI-MFLGALGMMLGLCYRISCVLFLLPYWYVFLLDKTSWNNHSYLYGLLAFQLTFMDANHYWSVDGLLNAHRRNAHVPLWNYAVLRGQIFIVYFIAGVKKLDADWVEGYSMEYLSRHWLFSPFKLLLSEELTSLLVVHWGGLLLDLSAGFLLFFDVSRSIGLFFVSYFHCMNSQLFSIGMFSYVMLASSPLF------------CSPEWPRKLVSYCPRRLQQLLPL-K--A-A--P---QP--SVSCVYKRSRGKSGQKPG------LRH-QLG-AA--------FTLLYLLEQLFLPYSHFLTQGYNNWTNGLYGYSWDMMVHSRSH-QHVKITYR---------------------------------------------------------------------------DGRTGELGYLNPGVFTQ-SRRW-KDH-ADML--KQYA------TCL-------------------------------------------------------------------SRL--LPKYN----VTEPQIYFDIWVSINDRFQQRIFDP----------------------------------------------------------------------------------------------------------------------------------------------------------------RVDIVQAAWSPF----------------------QRTSWVQPLLMD-LSPWRAKLQEIKSSLDNHTEVVFIADFPGLHLENFVSEDLGNTSIQLLQGEVTVELVAEQKNQTLREGEKMQLPAGEYHKVYTTSPSPSCYMYVYV-------------------NTTELALEQDLAYLQEL---------------KEKV--ENG-SETGPLPPELQPLLEGEVKGGPEPTPLVQTF-------LRRQQRLQEIERRRNTPFHERFFRFLLRKLYVFRRSFLMTCISLRNLILGRPSLEQLAQEVTYANLRPFEAVGELNPSNTDSSHSNPPESNPDPVHSEF---------------
                                                                                                          ...                         .. ::::::..                           ...::...           .  ...                                  .    .. .  .........                                                                                                                                                                                                ....:.:..............:. :  . .  .   .   .......    .            ..  ..  ..        .....::..:::::...:.....:..      .  .: ....  .                                                                                  .......       ..  ...  ..  ..    .         .                                                                     ..   ....:    ..::....                                                                                                                                                                                 ...                               ..:::::..... ..                                                                                                                 ..:...:.....                    ..    ..  ..          .:.::.......              ..                                                                                                                
---------------------------------------------MAVSAGSARTSPSSDKVQKDKAELISGPRQDSRIGKLLGFEWTDLSSWRRLVTLLNRPTDPASL-------------------------AV-FRFLFGFL----------MVLDIPQERGLSSLDRKYLDGLDVC--RFPLLDALRP-LPLD-----WMYLVYTIMFLGALGMMLGLCYRISCVLFL--LPYWYV-FLLDKTSWNN------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------HSYLYGLLAFQLTFMDANHYWSVDGLLNAHRRNAHVPLWNYAVLRGQIF-IVYFIAGVK----K------LDADWVEG-YSM-EYLSRHWLFSPFKLLLSEELTSLLVVHWGGLLLDLSA------G--FL-LFFD-VS-------RSIGLFFVSYFHCMNSQLFSIGMFSYVMLASSPLFCSPEWPRKLVSYCPRRLQQLLPLKAAPQPSVSCVYKRSRGKSGQKPG-------LR-HQLG-AAF-TLL--YLL---EQLFLPY--SHFLTQGYNNWTNGLYGYSWDMMVHSRSHQHVKITYRDGRTGELGYLNPGVFTQSRRWKDHADMLKQYA-TCLSRLLPKYNVTEPQIYF-----------------DIWVSINDRFQQRIFDPRVDIVQAAWSPFQRTSWVQPLLMDLSPWRAKLQEIKSSLDNHTEVVFIADFPGLHLENFVSEDLGNTSIQLLQGEVTVELVAEQKNQTLREGEKMQLPAGEYHKVYTTSPSPSCYMYVYVNTTELALEQDLAYLQELKEKVENGSE---------TGPLPPELQPLLEGEVKGGPEPTPLVQTFLRRQQRLQ----------------------------------------------------------------------------------------------EIERRRNTPFHERFFRFLLRKLYVFRRSFLM-----TCISLRNLILGRPSLEQ--LAQE-VTY----------ANLRPFEAVGEL-------NPSNTDSSH-------------------------------------------------------------------------------------------------SNPPESNPDPVHSEF

