
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/nsp10.pdb                             
Name of Chain_2: pymodel/nsp10-P55789.pdb_A                        
Length of Chain_1:  139 residues
Length of Chain_2:  208 residues

Aligned length=   71, RMSD=   5.01, Seq_ID=n_identical/n_aligned= 0.042
TM-score= 0.26898 (if normalized by length of Chain_1)
TM-score= 0.20738 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
AGNATEVPANSTVLSFCAFAVDAAKAYKDYLASGGQPITNCVKMLCTHTGTGQAITVTPEANM------------------D-QE-SFG----------GASCCLYCRCHIDHPNPKGFCDLKGKYVQ-IPTTCANDPVGFTLK---------------------------NTVCTVCGMWKGY----GCS--CDQLREPM--LQ-----------------------------------------------------------------------
    :...::.::. .:::                                 :..                          . :: :::          :::. . .::::::    ...::: :.:. ..                                        .:::: : ..:..    ..:  .::::.::  ::                                                                       
----AGNATEVPAN-STVL---------------------------------SFC--------AFAVDAAKAYKDYLASGGQPITNCVKMLCTHTGTGQAITV-T-PEANMDQ----ESFGGA-SCCLYCR-------------CHIDHPNPKGFCDLKGKYVQIPTTCANDPVGF-T-LKNTVCTVCGMWKGYGCSCDQLREPMLQAARRNARDDADARRARRDAAAAAADADARRRRADRRDRDDRRAAADDAADRRRNDRRANNRDDDRRDDD


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/P55789.pdb                            
Name of Chain_2: pymodel/nsp10-P55789.pdb_B                        
Length of Chain_1:  205 residues
Length of Chain_2:  205 residues

Aligned length=  184, RMSD=   3.43, Seq_ID=n_identical/n_aligned= 0.739
TM-score= 0.68833 (if normalized by length of Chain_1)
TM-score= 0.68833 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
MAAPGERGR------FHGGNLF-FL-------PGGARSEMMDDLATDARGRGAGRRDAAAS-ASTP-AQAPTSDSPVAEDASRRRPC-RACVDFKTWMRTQQKRDTKFREDCPPDREELGRHSWAVLHTLAAYYPDLPTPEQQQDMAQFIHLFSKFYPCEECAEDLRKRLCRNHPDTRTRACFTQWLCHLHNEVNRKLGKPDFD---CS-KVDERWRDGWKDGSCD
               .:....  .:       ::::::::::.:: :.    ...:::::. :::. .:.:: .::::: ::..::: ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::   :: :::::::::..   : 
---------MAAPGERGRFHG-GNLFFLPGGARSEMMDDLATDAR-GR----GAGRRDAAASASTPAQAPTS-DSPVAE-DASRRRPCRACVDFKTWMRTQQKRDTKFREDCPPDREELGRHSWAVLHTLAAYYPDLPTPEQQQDMAQFIHLFSKFYPCEECAEDLRKRLCRNHPDTRTRACFTQWLCHLHNEVNRKLGKPDFDCSKVDERWRDGWKDGSC---D-

