
 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/nsp11.pdb                             
Name of Chain_2: pymodel/nsp11-O75347.pdb_A                        
Length of Chain_1:   13 residues
Length of Chain_2:   40 residues

Aligned length=   11, RMSD=   0.28, Seq_ID=n_identical/n_aligned= 0.000
TM-score= 0.69428 (if normalized by length of Chain_1)
TM-score= 0.26880 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
SA-DAQSFLNGFAV----------------------------
   :::::::::::                            
--SADAQSFLNGFAVADRRRARADNDARDRRAADRNDAARDA


 **************************************************************************
 *                        TM-align (Version 20190822)                     *
 * An algorithm for protein structure alignment and comparison            *
 * Based on statistics:                                                   *
 *       0.0 < TM-score < 0.30, random structural similarity              *
 *       0.5 < TM-score < 1.00, in about the same fold                    *
 * Reference: Y Zhang and J Skolnick, Nucl Acids Res 33, 2302-9 (2005)    *
 * Please email your comments and suggestions to: zhng@umich.edu          *
 **************************************************************************

Name of Chain_1: singlemodel/O75347.pdb                            
Name of Chain_2: pymodel/nsp11-O75347.pdb_B                        
Length of Chain_1:  108 residues
Length of Chain_2:  108 residues

Aligned length=   59, RMSD=   3.19, Seq_ID=n_identical/n_aligned= 0.068
TM-score= 0.42145 (if normalized by length of Chain_1)
TM-score= 0.42145 (if normalized by length of Chain_2)
(You should use TM-score normalized by length of the reference protein)

(":" denotes aligned residue pairs of d < 5.0 A, "." denotes other aligned residues)
-------------------------------------------M-ADPRVRQIKIKTGVVKRLVKEKVMYEKEAKQQEEKIEKMRAEDGENYDIKKQAEILQ-ESR-MMIPDCQRRLEAAYLDLQRILENEKDLEEAEEYKEARLVLDSVKLEA---
                                           . .:::::::::.::::::::::::::::::..:::::::::::: ::  ::::::::: :.  ..                                                
MADPRVRQIKIKTGVVKRLVKEKVMYEKEAKQQEEKIEKMRAEDGENYDIKKQAEILQESRMMIPDCQRRLEAAYLDLQRILENEKDL-EE--AEEYKEARLVLD-SVK---------------------------------------------LEA

