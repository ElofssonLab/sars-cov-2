# what I have done:

   *Put the 41 uniprot sequences in sequences/uniprt/
   *Got all models from the CASP targets in sequences/CASP


# What remains to be done? (if you have done any of this please let me know and put the data in the repository)

    *Run jackhmmer/hhblits against uniprot for all sequences (casp and uniprot).
      *run TrRosetta on all virus proteins - compare with CASP/PDB
    *Get the GSAID data for ncov sequences (as fasta protein).
      *run gDCA for all pairs of proteins. (probably will not do anything)
    * Get the sequences and structures of the "Krogan" dataset for protein interactions.
      *  Examine what is possible to model by homology
      *  And what can be modelled by trRosetta.
    * Get a database of the sequence of all Corona viruses and their hosts (Agustin)
      *  Align all proteins
      *  Identify all pairs of virus/hosts proteins from the Krogan set.
    * Docking of the Krogan dataset (Gabriele)
      *  Put the results from the tmDOCK in the repostory
      *  Score by standard scoring schemes,
      *  Score them by DCA (above)
    * Other interactions
      *  String viruses: http://viruses.string-db.org/cgi/network.pl
      *  Virus interaction database (Julie)

* casp
  * Casp structures and sequences
* Genbank
  * virus sequences from Krogan
* Human_interacting_protein_fasta
  * Models of the 
* uniprot
  * Databases fron uniprot (and ncbi) of all corona virus sequences
* alignments
  * Results from alignments
Short description of the files:
## viruses 
### proteins_ncbi.tar.xz 
	   56,137 SARS-CoV-2 entries from ncbi-virus.
